<?php
use ItemMaker as Items;

//轉到預設語系
Route::get('/', function () {
    return redirect(Items::url('/' . App::getLocale()) . '/');
});

//轉到預設語系
Route::get('/backen-admin', function () {
    return redirect(Items::url('/' . App::getLocale() . '/backen-admin'));
});

//轉到預設語系
Route::get('/home/{a?}/{b?}/{c?}/{d?}/{e?}', function () {
    return redirect(Items::url('/' . App::getLocale()) . '/');
});

Route::get('/events/sendmail', "EventsController@switchSendMail");

/*===後台===*/

Route::group(['prefix' => '/{locale?}'], function () {
    Route::group(['prefix' => '/backen-admin', 'middleware' => 'auth'], function () {

        //投資專區
        Route::group(['prefix' => 'corporate-situation'], function () {
            Route::get('/', 'CorporateSituationController@index');
            Route::put('/', 'CorporateSituationController@updateCorporateSituation')->name('corporate-situation.store');
        });
        Route::group(['prefix' => 'Financial-Information'], function () {
            Route::controller('Monthly-Revenue', 'Investor\Financial\FinancialMonthController');
            Route::controller('Financial-Report', 'Investor\Financial\FinancialReportController');
        });
        Route::group(['prefix' => 'Shareholder-Information'], function () {
            Route::controller('Procedure-Manual', 'Investor\Shareholder\ShareholderProcedureController');
            Route::controller('Notice-of-meeting', 'Investor\Shareholder\ShareholderNoticeController');
            Route::controller('Annual-Report', 'Investor\Shareholder\ShareholderAnnualController');
            Route::controller('Stock', 'Investor\Shareholder\ShareholderStockController');
        });
        Route::group(['prefix' => 'Stakeholder'], function () {
            Route::controller('Stakeholder-Engagement', 'Investor\Stakeholder\StakeholderController');
        });
        Route::group(['prefix' => 'Governance'], function () {
            Route::controller('Organization-Team', 'Investor\Governance\OrganizationController');
            Route::controller('Board-Director', 'Investor\Governance\BoardDirectorController');
            Route::controller('Company-rule', 'Investor\Governance\CompanyRuleController');
            Route::controller('Internal-audit', 'Investor\Governance\InternalAuditController');
            Route::controller('Main-Shareholder', 'Investor\Governance\MainShareholderController');
        });
        Route::group(['prefix' => 'Responsibility'], function () {
            Route::controller('Social-Respons', 'Investor\Responsibility\ResponsibilityController');
            Route::controller('Respon-Effects', 'Investor\Responsibility\EffectsController');
            Route::controller('Personal-Safety', 'Investor\Responsibility\PersonalSafetyController');
        });

        Route::group(['prefix' => 'Product-Profile-Table'], function () {
            Route::controller('Product', 'ProductDetail\ProductController');
            Route::controller('Profile', 'ProductDetail\ProfileController');
            Route::controller('Table', 'ProductDetail\TableController');
        });

        Route::group(['prefix' => 'application'], function () {
            Route::controller('sub_application', 'Application\SubApplicationController');
            Route::controller('/', 'Application\ApplicationController');
        });

        Route::group(['prefix' => '設計工具'], function () {
            Route::controller('模擬資料', 'Design\SimulateController');
            // Route::controller('Life-Time', 'Design\LifeTimeController');
        });


        //首頁
        Route::get('/', 'Backend\BackController@index');
        Route::controller('關於我們', 'AboutController');
        Route::controller('歷史沿革', 'HistoryController');

        Route::group(['prefix' => '產品'], function () {

            Route::controller('主類別', 'Product\ThemeController');
            Route::controller('次類別', 'Product\CategoryController');
            Route::controller('Items', 'Product\ItemController');

        });

        Route::group(['prefix' => '最新消息'], function () {
            Route::controller('消息分類', 'News\CategoryController');
            Route::controller('消息列表', 'News\ItemController');
        });

        Route::group(['prefix' => '相簿管理'], function () {
            Route::controller('相簿類別管理', 'Album\CategoryController');
            Route::controller('相簿管理', 'Album\ItemController');
        });

        Route::group(['prefix' => '檔案下載'], function () {
            Route::controller('檔案類別', 'Download\CategoryController');
            Route::controller('檔案列表', 'Download\ItemController');
        });

        Route::controller('與我們聯絡', 'ContactUsController');

        Route::controller('證書管理', 'Certificate\CertificateController');

        Route::controller('服務據點', 'DistributorController');

        Route::group(['prefix' => '廣告管理'], function () {
            Route::controller('首頁廣告', 'BannerController');
            //Route::controller('內頁廣告', 'PageBannerController');
        });

        Route::group(['prefix' => '帳號管理'], function () {
            Route::controller('帳號列表', 'Backend\AccountController');
            Route::controller('權限', 'Backend\RoleController');
        });

        Route::controller('網站基本設定', 'SettingController');
        // Route::controller('行事曆管理', 'CalendarController');
        Route::controller('googleanalytics', 'AnalyticsController');
    });

    /*===後台===*/

    /*===前台==*/

    Route::group(['prefix' => '/'], function () {
        Route::get('/', 'HomeController@index');

        Route::get('/error404', 'HomeController@error404');
        Route::get('/policy', 'HomeController@policy');
        // Route::controller('import-manager','ImportController');
        Route::get('/mailtest', 'HomeController@testPost');
        //聯絡我們
        Route::group(['prefix' => 'contact'], function () {
            Route::get('/', "ContactUsController@index");
            Route::get('/distributors', "ContactUsController@distributors");
            Route::post('/save', "ContactUsController@postStore");
        });

        //關於我們
        Route::group(['prefix' => 'about'], function () {
            Route::get('{title?}', 'AboutController@index');
//             Route::get('/', 'AboutController@index');
            // Route::get('/development-and-core', 'AboutController@dac');
            // Route::get('/certification', 'AboutController@certification');
        });
        Route::group(['prefix' => 'quality'], function () {
            Route::get('{title?}', 'QualityController@index');
        });

        //最新消息
        Route::get('news/get-category', "NewsCategoryController@get_category");
        Route::post('news/load-more', 'NewsController@loadMore');
        Route::group(['prefix' => "news"], function () {
            Route::get('/{category?}', "News\ItemController@index");

            Route::get('/{category}/{title}', "News\ItemController@detail");
        });

        Route::group(['prefix' => "download"], function () {
            Route::get('/', "Download\ItemController@index");
            Route::get('/{title?}', "Download\ItemController@download");
        });

        Route::group(['prefix' => "investor"], function () {
            Route::get('/', "HomeController@investor");
            Route::get('/Monthly-Revenue', "Investor\Financial\FinancialMonthController@index");
            Route::get('/Financial-Report', "Investor\Financial\FinancialReportController@index");
            Route::get('/Financial-Report/{id}/{field}', "Investor\Financial\FinancialReportController@Download");

            Route::get('/Procedure-Manual', "Investor\Shareholder\ShareholderProcedureController@index");
            Route::get('/Procedure-Manual/{id}/{field}', "Investor\Shareholder\ShareholderProcedureController@Download");

            Route::get('/Notice-of-meeting', "Investor\Shareholder\ShareholderNoticeController@index");
            Route::get('/Notice-of-meeting/{id}/{field}', "Investor\Shareholder\ShareholderNoticeController@Download");

            Route::get('/Annual-Report', "Investor\Shareholder\ShareholderAnnualController@index");
            Route::get('/Annual-Report/{id}/{field}', "Investor\Shareholder\ShareholderAnnualController@Download");

            Route::get('/Stock', "Investor\Shareholder\ShareholderStockController@index");
            Route::get('/Stock/{id}/{field}', "Investor\Shareholder\ShareholderStockController@Download");

            Route::get('/Stakeholder-Engagement', "Investor\Stakeholder\StakeholderController@index");

            Route::get('/Organization-Team', "Investor\Governance\OrganizationController@index");

            Route::get('/Board-Director', "Investor\Governance\BoardDirectorController@index");

            Route::get('/Company-rule', "Investor\Governance\CompanyRuleController@index");
            Route::get('/Company-rule/{id}/{field}', "Investor\Governance\CompanyRuleController@Download");

            Route::get('/Internal-audit', "Investor\Governance\InternalAuditController@index");
            Route::get('/Internal-audit/{id}/{field}', "Investor\Governance\InternalAuditController@Download");

            Route::get('/Main-Shareholder', "Investor\Governance\MainShareholderController@index");

            Route::get('/Social-Respons', "Investor\Responsibility\ResponsibilityController@index");
            Route::get('/Social-Respons/{id}/{field}', "Investor\Responsibility\ResponsibilityController@Download");

            Route::get('/Respon-Effects', "Investor\Responsibility\EffectsController@index");

            Route::get('/Personal-Safety', "Investor\Responsibility\PersonalSafetyController@index");
            Route::get('/Personal-Safety/{id}/{field}', "Investor\Responsibility\PersonalSafetyController@Download");
            Route::get('/Corporate-Governance-Implementation-Status', "CorporateSituationController@show");
        });

        Route::get('/video', "Video\ItemController@index");
        
        Route::get('/distributor', "DistributorController@index");
        
        Route::get('/certificate', "Certificate\CertificateController@index");
        
        //設計工具
        Route::group(['prefix' => 'design'], function () {
            Route::get('/simulate', 'Design\SimulateController@index');
            Route::get('/simulate/{id}', 'Design\SimulateController@download');
            Route::post('/simulate/search', 'Design\SimulateController@search');
            Route::get('/life-time-1', 'Design\LifeTimeController@index');
            Route::post('/life-time-2/search', 'Design\LifeTimeController@search');
        });
        
        //相簿
        Route::group(['prefix' => 'album'], function () {
            Route::get('/{category?}', 'Album\ItemController@index');
            Route::get('/detail/{id}', 'Album\ItemController@detail');
        });
        //產品
        Route::group(['prefix' => "product"], function () {
            Route::get('/', "Product\ItemController@index"); //產品列表
            Route::get('/product-list', "Product\ItemController@list2"); //
            
            Route::get('/product', "ProductDetail\ProductController@index");
            Route::get('/profile', "ProductDetail\ProfileController@index");
            Route::get('/table', "ProductDetail\TableController@index");
            
            Route::get('/size-code', "Product\ItemController@sizeCode"); //
            Route::get('/download/{id}', "Product\ItemController@download");
            Route::get('/download/{id}/{type}', "Product\ItemController@download2");
            Route::get('/{themeUrl}/{categoryUrl}/{itemUrl}', "Product\ItemController@detail"); //詳細頁
            Route::get('/{itemUrl}', "Product\ItemController@detail2"); //詳細頁
        });
        
        Route::get('search', "Product\ItemController@search");
        Route::post('search', "Product\ItemController@search");
        Route::get('search-filter', "ProductFilterController@index");
        Route::get('application', "Application\SubApplicationController@index");
        Route::get('/inquiry', 'Product\InquiryController@index');
        Route::post('/inquiry', 'Product\InquiryController@InquirySave');

        //產品搜尋
        Route::get('product-search/{keyword?}', "Product\ItemController@getProductSearch");

        //ajax load
        Route::controller('ajax-load', "AjaxLoadController");
        //抓產品列表
        Route::get('/getMenuList', "Product\ThemeController@getMenuList");
        //抓產品列表
        Route::get('/getSearch', "Product\ItemController@getSearch");
    });

    /*===前台==*/

    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);

});
