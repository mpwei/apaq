<?php
namespace App\Http\Controllers;

/**原生函式**/
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\HistoryController;

/**相關Controller**/

use App\Http\Models\AboutUs;
use App\Http\Models\Banner;

/**Models**/
use App\Http\Models\News\News;
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Theme;
use Mail;
use View;

class HomeController extends BackendController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($locale)
    {
        $product = [];
        $historyClass = new HistoryController;
        $history = $historyClass->findData();

        $top_Banner = Banner::where('is_visible', '1')->where('block', 1)->select('image', 'title')->orderBy('rank', 'asc')->get();

        $sec_Banner_l = Banner::where('is_visible', 1)->where("block", 2)->select('image', 'link', 'title')->orderBy('rank', 'asc')->take(1)->get();

        $news = News::where('is_visible', '1')->select('image', 'title', 'list_slogan', 'category_id', 'date')->with("Category")->orderBy('date', 'desc')->take(3)->get();

        $theme = Theme::where('is_visible', '1')->select('id', 'title', 'image')->orderBy('rank', 'asc')->get();

        $about = AboutUs::where('is_visible', 1)->select('title')->orderBy('rank', 'asc')->get()->toArray();

        foreach ($news as $key => $row) {
            $news[$key]->web_link = MakeItemV2::url('news/' . parent::processTitleToUrl($row->Category->title) . '/' . parent::processTitleToUrl($row->title));
        }
        foreach ($theme as $key => $t) {
            $p = Item::where('is_visible', '1')->where('theme_id', $t->id)->select('title', 'theme_id', 'category_id', 'image')->with("category")->orderBy('rank', 'asc')->first();
            if ($p) {
                $p->web_link = MakeItemV2::url(
                    "product/" . parent::processTitleToUrl($t->title)
                    . "/" . parent::processTitleToUrl($p->category->title)
                    . "/" . parent::processTitleToUrl($p->title)
                );
            }

            $theme[$key]->Item = $p;

        }

        return view($locale . '.home.index', [
            "about" => isset($about[0]['title']) ? $about[0]['title'] : '',
            "top_Banner" => $top_Banner,
            "sec_Banner_l" => $sec_Banner_l,
            "news" => $news,
            "product" => $theme,
            "history" => $history,
        ]);
    }

    public function indexj($locale)
    {

        $top_Banner = Banner::where('is_visible', '1')->where('block', 1)->select('image', 'title')->orderBy('rank', 'asc')->get();

        $sec_Banner_l = Banner::where('is_visible', 1)->where("block", 2)->select('image', 'link', 'title')->orderBy('rank', 'asc')->take(1)->get();

        $news = News::where('is_visible', '1')->where('is_home', 1)->select('image', 'title', 'list_slogan')->orderBy('rank', 'asc')->get();

        $product = Item::where('is_visible', 1)->where('is_home', 1)->select('title', 'category_id', 'image', 'list_price', 'price')->with('theme')->with('category')->orderBy('rank', 'asc')->take(8)->get();

        return view($locale . '.home.indexj', [
            "top_Banner" => $top_Banner,
            "sec_Banner_l" => $sec_Banner_l,
            "news" => $news,
            "product" => $product,
        ]);
    }

    public function indexl($locale)
    {

        $top_Banner = Banner::where('is_visible', '1')->where('block', 1)->select('image', 'title')->orderBy('rank', 'asc')->get();

        $sec_Banner_l = Banner::where('is_visible', 1)->where("block", 2)->select('image', 'link', 'title')->orderBy('rank', 'asc')->take(1)->get();

        $news = News::where('is_visible', '1')->where('is_home', 1)->select('image', 'title', 'list_slogan')->orderBy('rank', 'asc')->get();

        $product = Item::where('is_visible', 1)->where('is_home', 1)->select('title', 'category_id', 'image', 'list_price', 'price')->with('theme')->with('category')->orderBy('rank', 'asc')->take(8)->get();

        return view($locale . '.home.indexl', [
            "top_Banner" => $top_Banner,
            "sec_Banner_l" => $sec_Banner_l,
            "news" => $news,
            "product" => $product,
        ]);
    }

    public function indexm($locale)
    {

        $top_Banner = Banner::where('is_visible', '1')->where('block', 1)->select('image', 'title')->orderBy('rank', 'asc')->get();

        $sec_Banner_l = Banner::where('is_visible', 1)->where("block", 2)->select('image', 'link', 'title')->orderBy('rank', 'asc')->take(1)->get();

        $news = News::where('is_visible', '1')->where('is_home', 1)->select('image', 'title', 'list_slogan')->orderBy('rank', 'asc')->get();

        $product = Item::where('is_visible', 1)->where('is_home', 1)->select('title', 'category_id', 'image', 'list_price', 'price')->with('theme')->with('category')->orderBy('rank', 'asc')->take(8)->get();

        return view($locale . '.home.indexm', [
            "top_Banner" => $top_Banner,
            "sec_Banner_l" => $sec_Banner_l,
            "news" => $news,
            "product" => $product,
        ]);
    }

    public function policy($locale)
    {

        return view($locale . '.home.policy');
    }
    public function error404($locale)
    {

        return view($locale . '.error404.index');
    }

    public function legal($locale)
    {

        return view($locale . '.home.legal');
    }
    public function investor($locale)
    {
        return view($locale . '.investor.index');
    }
}
