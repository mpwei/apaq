<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use Mail;
use Config;
use Cache;
use App\Http\Controllers\Backend\MakeItemV2;

/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;



/***相關Model*****/
use App\Http\Models\Setting;

class SettingController extends BackendController
{

    public function __construct()
    {
        parent::__construct();

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        //檢查
        //LangController::checkLang($lang);

        $setting = Setting::find(1);
        //print_r(json_decode($setting->content,true));
        return view('backend.Setting.index', [
            "data" => json_decode($setting->content, true),
            "mails" => ['mails' => $setting->mails, "phone"=> $setting->phone]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request)
    {
        $data = $request->input('Setting');
        $Mails = $request->input('Mail');
        $find = Setting::find(1);

        $find->content = json_encode($data);
        $find->mails = $Mails['mails'];

        if ($find->save()) {
            //更新cache
            $data['mails'] = $Mails['mails'];
            $locale = ( !empty( parent::getRouter()->current()->parameters()['locale'] ) )? parent::getRouter()->current()->parameters()['locale'] : '';

            Cache::forever( $locale."_".'Global_Set', $data);

            return redirect( MakeItemV2::url('backen-admin/網站基本設定') )->with('Message','儲存成功')->with('Message_type','success');

        }
        else{
            return redirect( MakeItemV2::url('backen-admin/網站基本設定') )->with('Message','儲存失敗')->with('Message_type','error');

        }
    }
}
