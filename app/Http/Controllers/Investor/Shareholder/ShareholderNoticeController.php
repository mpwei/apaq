<?php

namespace App\Http\Controllers\Investor\Shareholder;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Shareholder;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class ShareholderNoticeController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Shareholder-Information/Notice-of-meeting/ajax-list/';
    public $routePreFix = 'Shareholder-Information/Notice-of-meeting';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content', 'q1'];
    public $whereRaw = "sub_category ='Notice-of-meeting'";
    protected $modelName = "Shareholder";
    protected $viewPreFix = 'Investor.Notice-of-meeting';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
       $data = [];
        $getPage = $_GET['page'];
        $currentYear = date("Y");

        $shareholder = Shareholder::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content', 'q1')
            ->where('is_visible', 1)->where('sub_category', 'Notice-of-meeting')->orderBy('rank','asc')->orderBy('year','desc')->get();

        if(!empty($shareholder)) {
            foreach ($shareholder->toArray() as $key => $value) {
                $data[$value['id']]['id'] = $value['id'];
                $data[$value['id']]['year'] = $value['year'];
                $data[$value['id']]['q1'] = $value['q1'];
            }
        }
        $page = ceil(sizeof($data)/10);

        $data = array_slice($data,(($getPage-1)*10),$getPage*10,true);

        return view($locale . '.investor.shareholder.Notice-of-meeting', [
            "url" => 'investor/Notice-of-meeting',
            "page" => $page,
            "data" => $data,
            "currentYear" => $currentYear,
        ]);
    }

    public function Download($locale,$id,$field)
    {
        $find = Shareholder::where('id', $id)->first();

        if (!empty($find) and !empty($find[$field])) {
            $file = public_path() . $find[$field];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }
}