<?php

namespace App\Http\Controllers\Investor\Financial;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\FinancialInfo;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class FinancialMonthController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Financial-Information/Monthly-Revenue/ajax-list/';
    public $routePreFix = 'Financial-Information/Monthly-Revenue';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content'];
    public $whereRaw = "sub_category = 'Monthly-Revenue'";
    protected $modelName = "FinancialInfo";
    protected $viewPreFix = 'Investor.Monthly-Revenue';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
       $data = [];
        $currentYear = date("Y");

        $financialInfo = FinancialInfo::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content')
            ->where('is_visible', 1)->where('sub_category', 'Monthly-Revenue')->orderBy('rank','asc')->orderBy('year','desc')->get()->toArray();

        foreach ($financialInfo as $key => $value) {
            $data[$value['year']]['content'] = $value['content'];
        }

        return view($locale . '.investor.financial.Monthly-Revenue', [
            "data" => $data,
            "currentYear" => $currentYear,
        ]);
    }
}