<?php

namespace App\Http\Controllers\Investor\Responsibility;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Responsibility;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class EffectsController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Responsibility/Respon-Effects/ajax-list/';
    public $routePreFix = 'Responsibility/Respon-Effects';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'title', 'content', 'file'];
    public $whereRaw = "sub_category = 'Respon-Effects'";
    protected $modelName = "Responsibility";
    protected $viewPreFix = 'Investor.Respon-Effects';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $responsibility = Responsibility::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'content')
            ->where('is_visible', 1)->where('sub_category', 'Respon-Effects')->first();

        return view($locale . '.investor.responsibility.Respon-Effects', [
            "data" => $responsibility,
        ]);
    }
}