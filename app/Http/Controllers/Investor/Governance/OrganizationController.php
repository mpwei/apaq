<?php

namespace App\Http\Controllers\Investor\Governance;

/**原生函式**/

use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Investor\Governance;
use Illuminate\Http\Request;

/**相關Controller**/

use View;

/**Models**/
class OrganizationController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Governance/Organization-Team/ajax-list/';
    public $routePreFix = 'Governance/Organization-Team';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'year', 'content', 'file', 'photo'];
    public $whereRaw = "sub_category ='Organization-Team'";
    protected $modelName = "Governance";
    protected $viewPreFix = 'Investor.Organization-Team';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas,
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink,
        ]);
    }

    public function Index($locale)
    {
        if ($locale != 'zh-tw' && $locale!='zh-cn') {
            return redirect(url('/' . $locale));
        }

        $governance = Governance::select('id', 'rank', 'is_visible', 'content', 'photo')
            ->where('is_visible', 1)->where('sub_category', 'Organization-Team')->first()->toArray();

        return view($locale . '.investor.governance.Organization-Team', [
            "data" => $governance,
        ]);
    }
}
