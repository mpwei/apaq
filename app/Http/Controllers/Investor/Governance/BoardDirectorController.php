<?php

namespace App\Http\Controllers\Investor\Governance;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Governance;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class BoardDirectorController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Governance/Board-Director/ajax-list/';
    public $routePreFix = 'Governance/Board-Director';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'year', 'content', 'file', 'photo'];
    public $whereRaw = "sub_category ='Board-Director'";
    protected $modelName = "Governance";
    protected $viewPreFix = 'Investor.Board-Director';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $governance = Governance::select('id', 'rank', 'is_visible', 'content', 'photo')
            ->where('is_visible', 1)->where('sub_category', 'Board-Director')->first()->toArray();

        return view($locale . '.investor.governance.Board-Director', [
            "data" => $governance,
        ]);
    }
}