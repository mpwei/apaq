<?php

namespace App\Http\Controllers\Investor\Governance;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Governance;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class InternalAuditController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Governance/Internal-audit/ajax-list/';
    public $routePreFix = 'Governance/Internal-audit';
    public $selectField = ['id', 'rank', 'is_visible', 'title', 'date', 'category', 'sub_category', 'year', 'content', 'file', 'photo'];
    public $whereRaw = "sub_category ='Internal-audit'";
    protected $modelName = "Governance";
    protected $viewPreFix = 'Investor.Internal-audit';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $governance = Governance::select('id', 'rank', 'is_visible','file','title','date' ,'content', 'photo')
            ->where('is_visible', 1)->where('sub_category', 'Internal-audit')->orderBy('rank', 'asc')->orderBy('date', 'desc')->get();

        if(!empty($governance)) {
            foreach ($governance->toArray() as $key => $value) {
                if ($value['title'] == 'Content-Editor') {
                    $content = $value['content'];
                } else {
                    $data[$value['id']]['content'] = $value['content'];
                    $data[$value['id']]['title'] = $value['title'];
                    $data[$value['id']]['date'] = $value['date'];
                    $data[$value['id']]['id'] = $value['id'];
                }
            }
        }

        return view($locale . '.investor.governance.Internal-audit', [
            "content" => $content,
            "data" => !empty($data) ? $data : '',
        ]);
    }

    public function Download($locale,$id,$field)
    {
        $find = Governance::where('id', $id)->first();

        if (!empty($find) and !empty($find[$field])) {
            $file = public_path() . $find[$field];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }
}