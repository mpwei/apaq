<?php 
namespace App\Http\Controllers\Download;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Config;


/**相關Controller**/
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\PublicController as PublicClass;

use App\Http\Models\Download\Category;

class CategoryController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/檔案下載/檔案類別/ajax-list/';
    public $routePreFix = '檔案下載/檔案類別';
    public $selectField = ['id', 'rank', 'is_visible', 'title'];
    protected $modelName = "DownloadCategory";
    protected $viewPreFix = 'Download/Category';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }
}