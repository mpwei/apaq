<?php 
namespace App\Http\Controllers\Download;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Config;


/**相關Controller**/
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\PublicController as PublicClass;

use App\Http\Models\Download\Item as Download;
use App\Http\Models\Download\Category;

class ItemController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/檔案下載/檔案列表/ajax-list/';
    public $routePreFix = '檔案下載/檔案列表';
    public $selectField = ['id','id_category', 'rank', 'is_visible', 'title', 'year', 'file'];
    protected $modelName = "Download";
    protected $viewPreFix = 'Download';

    public $modelBelongs = [
        "DownloadCategory" => [
            "parent" => "id_category",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $downloadCate = Category::select('id', 'rank', 'is_visible', 'title')->where('is_visible', 1)->orderBy('rank', 'asc')->get()->toArray();

        $download = Download::select('id','id_category', 'rank', 'is_visible', 'title', 'year', 'file')->where('is_visible', 1)->orderBy('rank', 'asc')->get()->toArray();

        return view($locale . '.download.Download', [
            "data" => $download,
            "downloadCate" => $downloadCate
        ]);
    }

    public function download($locale, $id)
    {
        $thisModel = new $this->modelClass;
        $find = $thisModel->where('id', $id)->first();

        if( !empty( $find ) AND !empty( $find->file ) ){
            $file = public_path().$find->file;
            if( file_exists( $file ) ){
                return response()->download($file);
            }else{
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        }else{
                echo "檔案不存在<br>";
            }
    }
}