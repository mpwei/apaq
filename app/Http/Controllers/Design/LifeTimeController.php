<?php

namespace App\Http\Controllers\Design;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Design\Simulate;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class LifeTimeController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/設計工具/模擬資料/ajax-list/';
    public $routePreFix = '設計工具/模擬資料';
    public $selectField = ['id', 'rank', 'is_visible', 'series', 'part_no','wv', 'cap', 'diameter','body_height_max', 'esr', 'rate_ripple_current' ,'file'];
    protected $modelName = "Simulate";
    protected $viewPreFix = 'Design.Simulate';

    protected $options = [
        'solid' => [
            'AREA'=>[
                'temperature' => 105,
                'hours'=> 2000,
            ],
            'AREC'=>[
                'temperature' => 105,
                'hours'=> 2000,
            ],
            'AREP'=>[
                'temperature' => 105,
                'hours'=> 3000,
            ],
            'AR5K'=>[
                'temperature' => 105,
                'hours'=> 5000,
            ],
            'ARHA'=>[
                'temperature' => 105,
                'hours'=> 5000,
            ],
            'ARHT'=>[
                'temperature' => 125,
                'hours'=> 2000,
            ],
            // 'AR5P'=>[
            //     'temperature' => 105,
            //     'hours'=> 5000,
            // ],
            // 'ARHE'=>[
            //     'temperature' => 125,
            //     'hours'=> 1000,
            // ],
            // 'ARCP'=>[
            //     'temperature' => 105,
            //     'hours'=> 2000,
            // ],
            'AVEA'=>[
                'temperature' => 105,
                'hours'=> 2000,
            ],
            'AV5K'=>[
                'temperature' => 105,
                'hours'=> 5000,
            ],
            'AVHA'=>[
                'temperature' => 105,
                'hours'=> 5000,
            ],
            // 'AVHE'=>[
            //     'temperature' => 125,
            //     'hours'=> 1000,
            // ],
            ],
        'electrolytic' => [
            'ACAS'=>[
                'temperature' => 105,
                'hours'=> 2000,
            ],
            'ACAH'=>[
                'temperature' => 105,
                'hours'=> 2000,
            ],
            'ACTH'=>[
                'temperature' => 125,
                'hours'=> 1000,
            ],
            ],
        'hybrid' => [
            // 'AVMA'=>[
            //     'temperature' => 105,
            //     'hours'=> 10000,
            // ],
            // 'AVMC'=>[
            //     'temperature' => 125,
            //     'hours'=> 4000,
            // ],
            'ARMA'=>[
                'temperature' => 105,
                'hours'=> 10000,
            ],
            'ARMC'=>[
                'temperature' => 125,
                'hours'=> 4000,
            ],
            ],
    ];
    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
    //    $simulate = Simulate::select('id', 'rank', 'is_visible', 'series', 'part_no','wv', 'cap', 'diameter','body_height_max', 'esr', 'rate_ripple_current' ,'file')
    //        ->where('is_visible', 1)->get()->toArray();

    //    foreach ($simulate as $key => $value) {
    //        $data[$value['rank']]['id'] = $value['id'];
    //        $data[$value['rank']]['file'] = $value['file'];
    //    }
    //    dd($this->options);
    //    if (!empty($data)) {
    //        ksort($data);
    //    }

        return view($locale . '.design.life-time-1', [
           "options" => $this->options,
        //    "currentYear" => $currentYear,
        ]);
    }

    public function search(Request $request, $locale)
    {
        return view($locale . '.design.life-time-2', [
           "data" => $this->options[$request->type][$request->series],
           "series_list" => $this->options[$request->type],
           "type" => $request->type,
           "series" => $request->series,
        ]);
    }
}