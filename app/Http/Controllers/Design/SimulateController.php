<?php

namespace App\Http\Controllers\Design;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Design\Simulate;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class SimulateController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/設計工具/模擬資料/ajax-list/';
    public $routePreFix = '設計工具/模擬資料';
    public $selectField = ['id', 'rank', 'is_visible', 'series', 'part_no','wv', 'cap', 'diameter','body_height_max', 'esr', 'rate_ripple_current' ,'file'];
    protected $modelName = "Simulate";
    protected $viewPreFix = 'Design.Simulate';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $simulate = Simulate::select('id', 'rank', 'is_visible', 'series', 'part_no','wv', 'cap', 'diameter','body_height_max', 'esr', 'rate_ripple_current','rate_ripple_current2' ,'file')
            ->where('is_visible', 1)->orderBy('rank','asc')->get()->toArray();

        $seriesDropDown = Simulate::select('series')->where('is_visible', 1)->distinct()->get()->toArray();
        $wvDropDown = Simulate::select('wv')->where('is_visible', 1)->distinct()->get()->toArray();
        $capDropDown = Simulate::select('cap')->where('is_visible', 1)->distinct()->get()->toArray();

        return view($locale . '.design.simulate', [
            "data" => $simulate,
            "series" => '',
            "wv" => '',
            "cap" => '',
            "seriesDropDown" => $seriesDropDown,
            "wvDropDown" => $wvDropDown,
            "capDropDown" => $capDropDown,
        ]);
    }

    public function search($locale)
    {
        $series = $_POST['series'];
        $wv = $_POST['wv'];
        $cap = $_POST['cap'];

        $simulate = Simulate::select('id', 'rank', 'is_visible', 'series', 'part_no','wv', 'cap', 'diameter','body_height_max', 'esr', 'rate_ripple_current','rate_ripple_current2' ,'file')
            ->where('is_visible', 1);
        if(!empty($_POST['series'])) {
            $simulate = $simulate->where('series',$series);
        }
        if(!empty($_POST['wv'])) {
            $simulate = $simulate->where('wv',$wv);
        }
        if(!empty($_POST['cap'])) {
            $simulate = $simulate->where('cap',$cap);
        }

        $simulate = $simulate->get()->toArray();

        $seriesDropDown = Simulate::select('series')->where('is_visible', 1)->distinct()->get()->toArray();
        $wvDropDown = Simulate::select('wv')->where('is_visible', 1)->distinct()->get()->toArray();
        $capDropDown = Simulate::select('cap')->where('is_visible', 1)->distinct()->get()->toArray();

        return view($locale . '.design.simulate', [
            "data" => $simulate,
            "series" => $series,
            "wv" => $wv,
            "cap" => $cap,
            "seriesDropDown" => $seriesDropDown,
            "wvDropDown" => $wvDropDown,
            "capDropDown" => $capDropDown,
        ]);
    }

    public function Download($locale,$id)
    {
        $find = Simulate::where('id', $id)->first();

        if (!empty($find) and !empty($find->file)) {
            $file = public_path() . $find->file;
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }
}