<?php
namespace App\Http\Controllers\Album;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;


/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Models\Album\Album as Album;

class CategoryController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/相簿管理/相簿類別管理/ajax-list/';

    protected $modelName = "AlbumCategory";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '相簿管理/相簿類別管理';

    public $viewPreFix = 'Album.Category';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title'],
        "order" => "rank",
        "sort" => ''
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    public function index($locale, $title='')
    {
        $title = Backend::revertUrlToTitle( $title );
        
        $thisModel = new $this->modelClass;
        // $album = $thisModel->where('is_visible',1)
        //             ->select('id','title')
        //             ->orderBy('rank','asc')
        //             ->get();

        // $Album = Album::where('is_visible',1) 
        //                 ->->where('category_id',1) 

        $albumCategory = $thisModel->where('is_visible',1)->get();

        $thisAlbum = $thisModel->where('title', $title)
                                ->where('is_visible',1)
                                ->first();
        $thisAlbum = ( !empty($thisAlbum) )? $thisAlbum : $thisModel->first();

        $Album = $thisModel->where('is_visible',1)
                    ->where('id',$thisAlbum->id)
                    ->with('Album')
                    ->orderBy('rank','desc')
                    ->get();
                    
        return View::make($locale.'.album.index',[
            "albumCategory" => $albumCategory,
            "album" => $Album,
            "thisAlbum" => $thisAlbum
        ]);
    }
}
