<?php 
namespace App\Http\Controllers\Album;

use Illuminate\Http\Request;
use View;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Album\Album;
use App\Http\Models\Album\AlbumCategory as Category;
use App\Http\Models\Album\Photo;

class ItemController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/相簿管理/相簿管理/ajax-list/';

    protected $modelName = "Album";

    public $index_select_field = ['id','rank','is_visible','is_ontop','list_slogan','title','category_id'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '相簿管理/相簿管理';

    public $limit_page = 12;

    public $viewPreFix = 'Album';

    public $modelBelongs = [
        "AlbumCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> false
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            ),
            "是否置頂" => Array(
                "field" => "is_ontop",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'T'
            )
        ),
    );
    public $photoTable = [
        "排序"=>'rank',
        "標題"=>'title',
        "圖片"=>'image',
        "放大圖"=> 'big_image',
        "是否顯示"=>'is_visible',
    ];
    protected $modelHas = [
        "AlbumPhoto" => [
            "parent" => "album_id",
            "filed" => 'id',
            "select" => ['title','image','big_image','id','is_visible','rank','album_id']
        ],
    ];

    protected $saveSubData = [
        [
            "modelName" => "AlbumPhoto",
            "to_parent" => "album_id"
        ],
    ];
    public function index(Request $request,$locale, $category='')
    {
        //最新消息分類
        if ($category) {
            $title = $category;
        } else {
            $category = Category::select('id','title','image')->where('is_visible', 1)->orderBy('rank', 'asc')->get();
        }

        return View::make($locale.'.album.index',[
            "category" => !empty($category) ? $category : '',
        ]);
    }

    public function detail(Request $request,$locale,$id='')
    {
        $category = Category::select('id','title','image')->where('id',$id)->where('is_visible', 1)->orderBy('rank', 'asc')->first();

        $data = Album::where('category_id',$id)
                    ->select('id','title','list_slogan','image','content')
                    ->orderBy('rank', 'asc')
                    ->get();


        return View::make($locale.'.album.detail',[
            "title" => !empty($category -> title) ? $category -> title : '',
            "data" => $data,
            "pageData" => $this->generatePage_detail($request->input('page'), $id)
        ]);

    }
    public function generatePage($page = 1,$category="") {
        if($page < 1 || $page == null) $page = 1;
        $pageData = array();
        $pageData['range'] = $this -> limit_page;
        $pageData['num_pages'] = $page;


        if($category){
                $cno = Category:: where('title',$category)->first();

                $cno = $cno -> id;
                $pageData['total_pages'] = (int)ceil( Album::where('is_visible',1)->where('category_id',$cno)
                    ->orderBy('rank', 'asc')
                    ->count()  / $this -> limit_page);
            }else{
                $pageData['total_pages'] = (int)ceil( Album::where('is_visible',1)
                    ->orderBy('rank', 'asc')
                    ->count()  / $this -> limit_page);
            }

        
        $url = $category ? 'album/'.$category : 'album/';
        $pageData['url'] = MakeItemV2::url($url);
        
            $data = Album::with('category')->
            where('is_visible',1);

            if($category){
                
                $data = $data->where('category_id',$cno)->orderBy('rank','asc')->
            skip(($page - 1) * $this -> limit_page )->
            take($this -> limit_page)->get();
            }else{
                $data = $data->orderBy('rank','asc')->
            skip(($page - 1) * $this -> limit_page )->
            take($this -> limit_page)->get();
            }
        

        $pageData['data'] = $data;
        return $pageData;
    }
    public function generatePage_detail($page = 1,$id="") {
        if($page < 1 || $page == null) $page = 1;
        $pageData = array();
        $pageData['range'] = $this -> limit_page;
        $pageData['num_pages'] = $page;


        $pageData['total_pages'] = (int)ceil( Photo::where('is_visible',1)->where('album_id',$id)
            ->orderBy('rank', 'asc')
            ->count()  / $this -> limit_page);

        
        $url = 'album_detail/'.$id;
        $pageData['url'] = MakeItemV2::url($url);
        
            $data = Photo::where('is_visible',1)->where('album_id',$id);

            $data = $data->orderBy('rank','asc')->
            skip(($page - 1) * $this -> limit_page )->
            take($this -> limit_page)->get();
        

        $pageData['data'] = $data;
        return $pageData;
    }
}