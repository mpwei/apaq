<?php

namespace App\Http\Controllers\Backend;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Config;


/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;

class PublicController extends BackendController
{
    public function getThisCategory($modelName)
    {
        //先抓暫存
        $categoryName = App::getLocale()."_".$modelName."Category";
        $category = Cache::get($categoryName);

        if( empty( $category ) ){
            $modelName = $modelName == "ProductItem" ? "Product" : $modelName;
            $thisModel = new parent::$ModelsArray[ $modelName."Category" ];
            $category = $thisModel->where('is_visible',1)
                            ->select('id','title')
                            ->orderBy('rank','asc')
                            ->get();
        }
        
        return $category;

    }
    //var $modelName, $with = [], $select = []
    //return model data
    public function getThisModelDate($set = [])
    {
        $model = App::getLocale()."_".$set['modelName'];
        $data = Cache::get($model);

        if( empty( $data ) ){

            $thisModel = new parent::$ModelsArray[ $set['modelName'] ];
            $data = $thisModel->where('is_visible',1);
                if( !empty( $set['with'] ) ){
                    foreach ($set['with'] as $row) {
                        $data = $data->with( $row );
                    }
                }
                if( !empty( $set['select'] ) ){
                    $data = $data->select( $set['select'] );
                }

            $data = $data->orderBy( $set['sortField'], $set['sort'] )->get();
                            // ->select('id','title')
                            // ->orderBy('rank','asc')
                            // ->get();
        }
        
        return $data;
    }
    public function setWebUrl($preFix, $title){
        return MakeItemV2::url(  $preFix.parent::processTitleToUrl( $title ) );
    }
}
