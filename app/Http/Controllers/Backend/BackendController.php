<?php

namespace App\Http\Controllers\Backend;

use App;
use App\Http\Controllers\Backend\PermissionController as Permission;
use App\Http\Models\ContactUs;
use App\Http\Models\News\NewsCategory;
use App\Http\Models\Product\InquiryRecord;
use App\Http\Models\Setting;
use Cache;
use Config;
use Debugbar;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

/**Models**/

use Image;
use ItemMaker;
use Mail;

abstract class BackendController extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

    //public static $ProjectName = "翔順";

    protected static $StatusOption = ["否", "是"];

    protected static $order_status_option = [
        "en" => ["The order was not yet paid", "The order was paid and not yet shipped", "The order was shipped", "The order was refunded  ", "The Order was cancelled"],
        "zh-tw" => ["未付款", "已付款未出货", "已出货", "退款", "取消订单"],
    ];

    protected static $pay_type_option = [3 => "信用卡付款", 4 => "貨到付款", 5 => "到店取貨"];

    protected static $adminer_mail = "mis@oceanad.com.tw";
    protected static $adminer_name = "鈺邦科技";

    protected static $front_show_list_data = ['AboutUs']; //前台選單控制要ＳＨＯＷ資料項目

    protected static $langSetup = [
        "en" => [
            "title" => "English",
        ],
        "zh-tw" => [
            "title" => "繁體中文",
        ],
        "zh-cn" => [
            "title" => "簡體中文",
        ],
    ];

    public static $MenuList =
        [
        "關於我們" => [
            "slogan" => "關於我們簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "關於我們",
            "en_title" => "About Us",
            "front_link" => "about-us",
            "cachePrefix" => "AboutUs",
            "link" => [
                '關於我們' => 'backen-admin/關於我們',
                '證書管理' => 'backen-admin/證書管理',
            ],
        ],
        "歷史沿革" => [
            "slogan" => "歷史沿革簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "歷史沿革",
            "en_title" => "History",
            "front_link" => "history",
            "cachePrefix" => "AboutUsHistory",
            "link" => "backen-admin/歷史沿革",
        ],
        "產品" => [
            "slogan" => "產品簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "產品",
            "en_title" => "Product",
            "front_link" => "Product",
            "cachePrefix" => "Product",
            "link" => [
                '主類別' => 'backen-admin/產品/主類別',
                '次類別' => 'backen-admin/產品/次類別',
                'Items' => 'backen-admin/產品/Items',
            ],
        ],

        "產品右側菜單" => [
            "slogan" => "Product-Profile-Table",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Product-Profile-Table",
            "en_title" => "Product-Profile-Table",
            "front_link" => "Product-Profile-Table",
            "cachePrefix" => "Product-Profile-Table",
            "link" => [
                'Product-list' => 'backen-admin/Product-Profile-Table/Product',
                'Profile-profile' => 'backen-admin/Product-Profile-Table/Profile',
                'Size-code' => 'backen-admin/Product-Profile-Table/Table',
            ],
        ],

//        "投資人專區" => [
        //            "slogan" => "投資人專區",
        //            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //            "title" => "投資人專區",
        //            "en_title" => "Investor",
        //            "front_link" => "Investor",
        //            "cachePrefix" => "Investor",
        //            "link" => [
        //                '財務資訊' => 'backen-admin/投資人專區/財務資訊',
        //                '股東資訊' => 'backen-admin/投資人專區/股東資訊',
        //                '投資人資訊' => 'backen-admin/投資人專區/投資人資訊',
        //                '公司治理' => 'backen-admin/投資人專區/公司治理',
        //                '企業社會責任' => 'backen-admin/投資人專區/企業社會責任',
        //            ]
        //        ],

        "最新消息" => [
            "slogan" => "最新消息簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "最新消息",
            "en_title" => "News",
            "front_link" => "news",
            "cachePrefix" => "News",
            "link" => [
                "消息分類" => "backen-admin/最新消息/消息分類",
                "消息列表" => "backen-admin/最新消息/消息列表",
            ],
        ],
        "應用領域" => [
            "slogan" => "應用領域",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "常見問題",
            "en_title" => "Application",
            "front_link" => "Application",
            "cachePrefix" => "Application",
            "link" => [
                '應用領域管理' => 'backen-admin/application',
                '子應用領域管理' => 'backen-admin/application/sub_application',
            ],
        ],
        "相簿管理" => [
            "slogan" => "相簿管理簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "常見問題",
            "en_title" => "Album",
            "front_link" => "fAlbumq",
            "cachePrefix" => "Album",
            "link" => [
                '相簿類別管理' => 'backen-admin/相簿管理/相簿類別管理', //
                '相簿管理' => 'backen-admin/相簿管理/相簿管理',
            ],
        ],
        "檔案下載" => [
            "slogan" => "檔案下載",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "檔案下載",
            "en_title" => "Download",
            "front_link" => "Download",
            "cachePrefix" => "Download",
            "link" => [
                '檔案類別' => 'backen-admin/檔案下載/檔案類別', //
                '檔案列表' => 'backen-admin/檔案下載/檔案列表',
            ],
        ],

        "服務據點" => [
            "slogan" => "服務據點",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "服務據點",
            "en_title" => "Distributor",
            "front_link" => "distributor",
            "cachePrefix" => "Distributor",
            "link" => "backen-admin/服務據點",
        ],
        // "詢問車管理" => [
        //     "slogan" => "訂單管理簡述",
        //     "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //     "link" => 'backen-admin/詢問車管理',
        // ],

        "設計工具" => [
            "slogan" => "設計工具",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "link" => [
                '模擬資料' => 'backen-admin/設計工具/模擬資料',
                'Life-Time' => 'backen-admin/設計工具/Life-Time',
            ],
        ],

        "與我們聯絡" => [
            "slogan" => "與我們聯絡簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "link" => "backen-admin/與我們聯絡",
        ],
//            "廣告管理" => [
        //                "slogan" => "廣告管理簡述",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "link" => [
        //                    '首頁廣告' => 'backen-admin/廣告管理/首頁廣告',
        //                    // '內頁廣告' => 'backen-admin/廣告管理/內頁廣告',
        //                ]
        //            ],
        "帳號管理" => [
            "slogan" => "帳號管理簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "link" => [
                '帳號列表' => 'backen-admin/帳號管理/帳號列表',
                '權限' => 'backen-admin/帳號管理/權限',

            ],
        ],
        "財務資訊" => [
            "slogan" => "Financial-Information",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Financial-Information",
            "en_title" => "Investor",
            "front_link" => "Investor",
            "cachePrefix" => "Investor",
            "link" => [
                '每月營收' => 'backen-admin/Financial-Information/Monthly-Revenue',
                '合併財務報表' => 'backen-admin/Financial-Information/Financial-Report',
            ],
        ],
        "股東會資訊" => [
            "slogan" => "Shareholder-Information",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Shareholders-Information",
            "en_title" => "Investor",
            "front_link" => "Investor",
            "cachePrefix" => "Investor",
            "link" => [
                '議事手冊' => 'backen-admin/Shareholder-Information/Procedure-Manual',
                '開會通知' => 'backen-admin/Shareholder-Information/Notice-of-meeting',
                '年報' => 'backen-admin/Shareholder-Information/Annual-Report',
                '股務資訊' => 'backen-admin/Shareholder-Information/Stock',
            ],
        ],
        "利害關係人" => [
            "slogan" => "Stakeholder",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Stakeholder",
            "en_title" => "Investor",
            "front_link" => "Investor",
            "cachePrefix" => "Investor",
            "link" => [
                '利害關係人' => 'backen-admin/Stakeholder/Stakeholder-Engagement',
            ],
        ],
        "公司治理" => [
            "slogan" => "Governance",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Governance",
            "en_title" => "Investor",
            "front_link" => "Investor",
            "cachePrefix" => "Investor",
            "link" => [
                '組織架構及經營團隊' => 'backen-admin/Governance/Organization-Team',
                '董事會及功能委員會' => 'backen-admin/Governance/Board-Director',
                '公司規章' => 'backen-admin/Governance/Company-rule',
                '內部稽核' => 'backen-admin/Governance/Internal-audit',
                '主要股東名單' => 'backen-admin/Governance/Main-Shareholder',
                '公司治理運作情形' => 'backen-admin/corporate-situation',
            ],
        ],
        "企業社會責任" => [
            "slogan" => "Responsibility",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "title" => "Responsibility",
            "en_title" => "Investor",
            "front_link" => "Investor",
            "cachePrefix" => "Investor",
            "link" => [
                '企業社會責任政策' => 'backen-admin/Responsibility/Social-Respons',
                '企業社會責任政策成效' => 'backen-admin/Responsibility/Respon-Effects',
                '員工福利、工作環境與人身安全保護' => 'backen-admin/Responsibility/Personal-Safety',
            ],
        ],
//            "Financial-info" => [
        //                "slogan" => "Financial-Information",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "title" => "Financial-Information",
        //                "en_title" => "Investor",
        //                "front_link" => "Investor",
        //                "cachePrefix" => "Investor",
        //                "link" => [
        //                    'Monthly-Revenue' => 'backen-admin/Financial-Information/Monthly-Revenue',
        //                    'Financial-Report' => 'backen-admin/Financial-Information/Financial-Report',
        //                ]
        //            ],
        //            "Shareholder-Info" => [
        //                "slogan" => "Shareholder-Information",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "title" => "Shareholders-Information",
        //                "en_title" => "Investor",
        //                "front_link" => "Investor",
        //                "cachePrefix" => "Investor",
        //                "link" => [
        //                    'Procedure-Manual' => 'backen-admin/Shareholder-Information/Procedure-Manual',
        //                    'Notice-of-meeting' => 'backen-admin/Shareholder-Information/Notice-of-meeting',
        //                    'Annual-Report' => 'backen-admin/Shareholder-Information/Annual-Report',
        //                    'Stock' => 'backen-admin/Shareholder-Information/Stock',
        //                ]
        //            ],
        //            "Stakeholder" => [
        //                "slogan" => "Stakeholder",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "title" => "Stakeholder",
        //                "en_title" => "Investor",
        //                "front_link" => "Investor",
        //                "cachePrefix" => "Investor",
        //                "link" => [
        //                    'Stakeholder-Engagement' => 'backen-admin/Stakeholder/Stakeholder-Engagement',
        //                ]
        //            ],
        //            "Governance" => [
        //                "slogan" => "Governance",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "title" => "Governance",
        //                "en_title" => "Investor",
        //                "front_link" => "Investor",
        //                "cachePrefix" => "Investor",
        //                "link" => [
        //                    'Organization-Team' => 'backen-admin/Governance/Organization-Team',
        //                    'Board-Director' => 'backen-admin/Governance/Board-Director',
        //                    'Company-rule' => 'backen-admin/Governance/Company-rule',
        //                    'Internal-audit' => 'backen-admin/Governance/Internal-audit',
        //                    'Main-Shareholder' => 'backen-admin/Governance/Main-Shareholder',
        //                ]
        //            ],
        //            "Responsibility" => [
        //                "slogan" => "Responsibility",
        //                "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
        //                "title" => "Responsibility",
        //                "en_title" => "Investor",
        //                "front_link" => "Investor",
        //                "cachePrefix" => "Investor",
        //                "link" => [
        //                    'Social-Respons' => 'backen-admin/Responsibility/Social-Respons',
        //                    'Respon-Effects' => 'backen-admin/Responsibility/Respon-Effects',
        //                    'Personal-Safety' => 'backen-admin/Responsibility/Personal-Safety',
        //                ]
        //            ],
        "Google Analytics" => [
            "slogan" => "Google Analytics",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "link" => "backen-admin/googleanalytics",

        ],
        "網站基本設定" => [
            "slogan" => "網站基本設定簡述",
            "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
            "link" => 'backen-admin/網站基本設定',
        ],
    ];

    protected static $ModelsArray = [
        //投資人專區
        "FinancialInfo" => App\Http\Models\Investor\FinancialInfo::class,
        "Shareholder" => App\Http\Models\Investor\Shareholder::class,
        "Governance" => App\Http\Models\Investor\Governance::class,
        "Responsibility" => App\Http\Models\Investor\Responsibility::class,

        //設計工具
        "Simulate" => App\Http\Models\Design\Simulate::class,
        //據點
        "Distributor" => App\Http\Models\Distributor::class,
        //證書
        "Certificate" => App\Http\Models\Certificate::class,

        "ReportUpload" => App\Http\Models\Investor\ReportUpload::class,
        "ReportInfo" => App\Http\Models\Investor\ReportInfo::class,
        "ReportEdit" => App\Http\Models\Investor\ReportEdit::class,
        "ReportQaRecord" => App\Http\Models\Investor\ReportQaRecord::class,
        "ReportMonth" => App\Http\Models\Investor\ReportMonth::class,
        //關於我們
        "AboutUs" => App\Http\Models\AboutUs::class,
        "AboutUsHistory" => App\Http\Models\AboutUsHistory::class,
        //關於我們
        //====================================================
        //產品
        "ProductTheme" => App\Http\Models\Product\Theme::class,
        "ProductCategory" => App\Http\Models\Product\Category::class,
        "ProductItem" => App\Http\Models\Product\Item::class,
        //====================================================
        //檔案下載
        "DownloadCategory" => App\Http\Models\Download\Category::class,
        "Download" => App\Http\Models\Download\Item::class,
        //====================================================
        //====================================================
        //相簿
        "Album" => App\Http\Models\Album\Album::class,
        "AlbumPhoto" => App\Http\Models\Album\Photo::class,
        "AlbumCategory" => App\Http\Models\Album\AlbumCategory::class,
        //====================================================
        //最新消息
        "NewsCategory" => App\Http\Models\News\NewsCategory::class,
        "News" => App\Http\Models\News\News::class,
        // "NewsPhoto" =>  App\Http\Models\News\NewsPhoto::class,
        //====================================================
        //聯絡我們
        "ContactUs" => App\Http\Models\ContactUs::class,
        //據點
        "Store" => App\Http\Models\Store::class,

        //帳號權限
        "Account" => App\Http\Models\User::class,
        "Role" => App\Http\Models\Role::class,
        "Banner" => App\Http\Models\Banner::class,

        "Application" => App\Http\Models\Application::class,
        "SubApplication" => App\Http\Models\SubApplication::class,
    ];

    public function __construct()
    {

        self::checkRouteLang();
        $parameters = (!empty(parent::getRouter()->current()->parameters())) ? parent::getRouter()->current()->parameters() : '';
        View::share('locale', $parameters['locale']);
        //Config::get('app.WEB_NAME');
        //進後台才處理
        if (!empty($_SERVER['REQUEST_URI'])) {
            if (preg_match("/backen-admin/", urldecode($_SERVER['REQUEST_URI']))) {
                $per_path = [];

                foreach (self::$MenuList as $key => $value) {
                    $per_path[$key] = $value['link'];
                }

                $newInquiry = InquiryRecord::where('is_answer', 0)->take(10)->orderBy('id', 'desc')->get();

                View::share('NewInquiry', $newInquiry);

                //後台選單設定
                View::share('SileMenu', Permission::MenuList($per_path));

                //後台首頁
                View::share('MenuList', self::$MenuList);

                //檢查是否有權限到該路徑
                Permission::PathPermission();

                //後台狀態選項
                View::share("StatusOption", self::$StatusOption);

                //未處理的聯絡我們
                View::share("Contact_count",
                    ContactUs::where('is_answer', '0')
                        ->select('id', 'name', 'created_at')
                        ->get()
                        ->toArray()
                );
            }
        }

        $setting = Setting::find(1);

        //語系
        View::share("locateLang", App::getLocale());
        //語系文字

        View::share("locateLangArray", self::$langSetup);
        //客戶名稱設定
        View::share('ProjectName', Config::get('app.WEB_NAME'));
        View::share('ProjectShareName', Config::get('app.WEB_NAME') . env('WEB_SEO'));

        View::share('googleanalytics', env('GoogleAnalytics'));

        View::share('Setting', json_decode($setting->content, true));
        //訂單狀態
        if (!empty(self::$order_status_option[$parameters['locale']])) {
            View::share('OrderStatus', self::$order_status_option[$parameters['locale']]);
        }

        //付款方式
        if (!empty(self::$pay_type_option)) {
            View::share('PayTypeOption', self::$pay_type_option);
        }

        //網站相關設定
        $globalsSet = Cache::get($parameters['locale'] . '_Global_Set');
        View::share('globalsSet', $globalsSet);
    }

    public static function checkPage()
    {
        $uri = $_SERVER["REQUEST_URI"];
        if (strpos(urldecode($uri), "about-us")) {
            return 1;
        }

        if (strpos(urldecode($uri), "news")) {
            return 2;
        }

        if (strpos(urldecode($uri), "food")) {
            return 3;
        }

        if (strpos(urldecode($uri), "Q&A")) {
            return 4;
        }

        if (strpos(urldecode($uri), "Q&A")) {
            return 4;
        }

        return 1;
    }

    public static function checkRouteLang()
    {
        $parameters = (!empty(parent::getRouter()->current()->parameters())) ? parent::getRouter()->current()->parameters() : '';

        if (isset($parameters['locale']) and !empty($parameters['locale'])) {
            App::setLocale($parameters['locale']);
            //資料庫前綴字
            switch ($parameters['locale']) {
                case 'zh-tw':
                    Config::set('app.dataBasePrefix', 'tw_');
                    break;
                case 'zh-cn':
                    Config::set('app.dataBasePrefix', 'cn_');
                    break;
                default:
                    Config::set('app.dataBasePrefix', '' . $parameters['locale'] . '_');
                    break;
            }

        }

    }
    //取得資料與關聯的資料，並分頁數
    /*
    Exp:
    $Datas = parent::findDataAndAssociate([
    "modelName" => 'News',
    "select" => ['id','rank','category_id','is_visible','is_show_home','title'],
    "belong" => [
    "NewsCategory" => [
    "parent" => "category_id",
    "filed" => 'id',
    "select" => ['title','id']
    ]
    ],
    "has" => [
    "Series" => [
    "filed" => 'category_id',
    "select" => ['title','id']
    ],
    "Photo" => [
    "filed" => 'series_id',
    "select" => ['title','image']
    ]
    ]
    ])
     */
    public function findDataAndAssociate($set = [])
    {
        $modelName = (!empty($set['modelName'])) ? $set['modelName'] : '';
        $pageShow = (isset($_GET['show_per_page']) and !empty($_GET['show_per_page'])) ? $_GET['show_per_page'] : '50'; //每頁顯示數量
        //$toParent = ( !empty( $set['parent'] ) )? $set['parent'] : '';

        $Datas = [];
        $rank = (!empty($set['rank'])) ? $set['rank'] : 'rank';
        $rank_sort = (!empty($set['rank_sort'])) ? $set['rank_sort'] : 'asc';

        if (!empty($modelName)) {
            $model = self::$ModelsArray[$modelName];

            if (isset($_GET['search']) and !empty($_GET['search'])) //如果沒有擷取到search的資料
            {
                $count = 0;
                $findWhere = '';
                foreach ($_GET['search'] as $key => $value) {
                    if ($count == 0) {
                        if (preg_match("/_id/", $key) or preg_match("/is_/", $key)) {
                            $findWhere .= " `" . $key . "` = '" . $value . "' ";
                        } else {
                            $findWhere .= " `" . $key . "` LIKE '%" . $value . "%' ";
                        }

                    } else {
                        if (preg_match("/_id/", $key) or preg_match("/is_/", $key)) {

                            $findWhere .= " OR `" . $key . "` = '" . $value . "' ";
                        } else {
                            $findWhere .= " OR `" . $key . "` LIKE '%" . $value . "%' ";
                        }

                    }
                    $count++;
                }
                $Datas = $model::select($set['select'])->whereRaw($findWhere)->orderBy($rank, $rank_sort)->paginate($pageShow)->toArray();
            } else {
                if (isset($set['where'])) {
                    $Datas = $model::select($set['select'])->whereRaw($set['where'])->orderBy($rank, $rank_sort)->paginate($pageShow)->toArray();
                } else {
                    $Datas = $model::select($set['select'])->orderBy($rank, $rank_sort)->paginate($pageShow)->toArray();
                }
            }

            if (!empty($set['belong'])) //抓父關連資料
            {
                if (is_array($set['belong']) and count($set['belong']) > 0) //關聯限定為陣列
                {

                    foreach ($set['belong'] as $name => $content) {
                        $associate = self::$ModelsArray[$name];

                        $find = $associate::select($content['select'])->get()->toArray(); // 抓全部
                        $Datas[$name] = $find;
                        foreach ($Datas['data'] as $key => $data) {

                            /*
                            $find = $associate::where($content['filed'],  $value[ $toParent ])
                            ->select( $content['select'] )
                            ->get()
                            ->first()
                            ->toArray();

                            $Datas['data'][ $key ][ strtolower( $name ) ] = $find;
                             */

                            foreach ($find as $key2 => $value2) {

                                if ($value2[$content['filed']] == $data[$content['parent']]) {
                                    $Datas['data'][$key][strtolower($name)] = $find[$key2];
                                }
                            }

                        }
                    }

                }
            }

            if (!empty($set['has'])) //抓子關連資料
            {
                if (is_array($set['has']) and count($set['has']) > 0) //關聯限定為陣列
                {

                    foreach ($set['has'] as $name => $content) {
                        $associate = self::$ModelsArray[$name];

                        foreach ($Datas['data'] as $key => $value) {
                            $find = $associate::where($content['filed'], $value['id'])
                                ->select($content['select'])
                                ->get()
                                ->toArray();

                            $Datas['data'][$key][strtolower($name)] = $find;
                        }
                    }

                }
            }
            //將已經搜尋的條件帶到下一頁
            if (!empty($_GET)) {
                if (!empty($Datas['next_page_url'])) {
                    $org_page_link = '';
                    foreach ($_GET as $key => $value) {
                        if ($key != 'page' and $key != 'search') {
                            $Datas['next_page_url'] .= '&' . $key . '=' . $value;

                        } elseif ($key == 'search') {
                            foreach ($value as $key2 => $search) {

                                $Datas['next_page_url'] .= '&search[' . $key2 . ']=' . $search;
                            }
                        }
                    }
                }

                if (!empty($Datas['prev_page_url'])) {
                    $org_page_link = '';
                    foreach ($_GET as $key => $value) {
                        if ($key != 'page' and $key != 'search') {
                            $Datas['prev_page_url'] .= '&' . $key . '=' . $value;
                        } elseif ($key == 'search') {
                            foreach ($value as $key2 => $search) {

                                $Datas['prev_page_url'] .= '&search[' . $key2 . ']=' . $search;
                            }
                        }
                    }
                }

            }
            //Debugbar::info($Datas);

            return $Datas;
        } else {
            Debugbar::error('set model is empty.');
        }

    }

    public function sendMail($set = [])
    {
        $content = self::$ModelsArray[$set['Model']]; //抓資料的model
        $type = (!empty($set['type'])) ? $set['type'] : '';

        $data = $content::findOrFail($set['id']);
        $data->contact_type = (!empty($type)) ? $type : '';

        $html = view($set['view'], ['data' => $data]);
        
        // error_log('process start');

        $postData = [
            'Projects' => ['APAQ'],
            'SenderName' => 'APAQ',
            'SenderEmail' => 'notify@notify.quickshop.tw',
            'Recipient' => $set['to'],
            'PlainText' => '此信件由系統自動發送，請勿直接回覆此信件',
            'Title' => $set['subject'],
            'Content' => $html->render()
        ];

        // error_log(print_r($postData, true));

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => env('MAILING_API'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLINFO_HEADER_OUT => true
        ));
            
        $res = curl_exec($ch);
        error_log(print_r($res, true));
        curl_close($ch);
        // error_log('process over');
        // Mail::send($set['view'], ['data' => $data], function ($m) use ($set) {
        //     $m->to($set['to'], $set['to_name'])->subject($set['subject']);
        // });
    }

    protected function addNew($set = [])
    {
        //$Datas , $modelName, $routeFix
        $new = new self::$ModelsArray[$set['modelName']];

        foreach ($set['Datas'] as $key => $value) {
            if ($key != 'Photo' and $key != 'App') {
                $new->$key = (!is_array($value)) ? $value : json_encode($value);
            }

        }

        if ($new->save()) {

            return '{ "redirect" : "' . ItemMaker::url('backen-admin/' . $set['routeFix'] . '/edit/' . $new->id) . '", "parent_id" : "' . $new->id . '" }';
        } else {
            return '{ "redirect" : "" }';
        }
    }

    protected function updateOne($Datas, $modelName, $type = '')
    {
        $model = self::$ModelsArray[$modelName];

        if (!empty($type) and $type == 'ajaxEdit') {

            foreach ($Datas as $row) {
                $work = $model::find($row['id']);

                foreach ($row as $key => $value) {
                    if ($key !== 'id') {
                        //$work->$key = ( !is_array( $value ) )? $value : json_encode($value);
                        if (!is_array($value)) {
                            $work->$key = (!empty($value)) ? $value : '';
                        } else {
                            $work->$key = (!empty($value)) ? json_encode($value) : '';
                        }
                    }
                }
                //update
                if (!$work->save()) {
                    echo "<script>alert('work id " . $row['id'] . " update fail.');</script>";
                } else {
                    //echo "".$row['id']." update success.";
                }
            }
            echo "<script>alert('儲存成功');</script>";
            //echo "<script>toastrAlert('success', '儲存成功');</script>";
        } else {
            $id = $Datas['id'];

            $find = $model::find($id);

            foreach ($Datas as $key => $value) {
                if ($key != 'id') {
                    //var_dump( $key );
                    $find->$key = (!is_array($value)) ? $value : json_encode($value);
                }

            }

            if ($find->del_photo == '1') {
                unset($find->del_photo);
                $find->image = "";
            }

            if ($find->save()) {
                //echo "<script>alert('儲存成功');</script>";
                //echo "<script>toastrAlert('success', '儲存成功');</script>";
                return true;
            }

        }
    }

    protected function updateMulti($Datas, $modelName, $parent_field, $parent_id)
    {
        if (count($Datas) > 0) {
            $message = true;
            $model = self::$ModelsArray[$modelName];
            //var_dump($Datas);
            // die;
            //儲存Introduction
            foreach ($Datas as $row) {

                if (!empty($row['id'])) //沒有id代表是新的一筆
                {
                    foreach ($row as $key => $value) {
                        $newInt = $model::where($parent_field, '=', $parent_id)->where('id', '=', $row['id'])->get();

                        if ($key != 'id') {
                            $newInt[0]->$key = $value;
                        }

                        if (!$newInt[0]->save()) {
                            //return '{ "message" : false }';
                            $message = false;
                        }

                    }
                } else if ($row['id'] == '') {

                    $newInt = new $model;

                    $newInt->$parent_field = $parent_id;

                    foreach ($row as $key => $value) {
                        if ($key != 'id') {
                            $newInt->$key = $value;
                        }

                    }
                    if (!$newInt->save()) {
                        $message = false;
                        //return '{ "message" : false }';

                    }
                }

            }

            return '{ "message" : ' . $message . ' }';

        }
    }

    protected function updataOneColumns($set = [])
    {
        $model = self::$ModelsArray[$set['modelName']];

        $find = $model::find($set['id']);
        $columms = $set['columns'];

        $find->$columms = $set['value'];

        if ($find->save()) {
            return '{ "message" : "true" }';
        } else {
            return '{ "message" : false }';
        }

    }

    protected function deleteOne($modelName, $id)
    {
        $model = self::$ModelsArray[$modelName];

        $del = $model::find($id);

        if ($del->delete()) {
            return '{ "message" : true }';
        } else {
            return '{ "message" : false }';
        }
    }

    protected function processTitleToUrl($title)
    {
        $replace1 = str_replace(' ', '+', $title);
        $replace2 = str_replace('/', '^', $replace1);
        $replace3 = str_replace('.', '`', $replace2);

        return $replace3;
    }

    protected function revertUrlToTitle($url)
    {
        $replace1 = str_replace('+', ' ', $url);
        $replace2 = str_replace('^', '/', $replace1);
        $replace3 = str_replace('`', '.', $replace2);

        return $replace3;
    }

    protected function minImage($set = [])
    {
        $resizeFolder = (!empty($set['resizeFolder'])) ? $set['resizeFolder'] : ''; //resize後的資料夾
        $image = (!empty($set['image'])) ? $set['image'] : ''; //原圖
        $width = (!empty($set['width'])) ? $set['width'] : null; //原圖
        $height = (!empty($set['height'])) ? $set['height'] : null; //原圖
        $small_image = '';
        if (!empty($image)) {
            $img = Image::make(public_path() . '/' . $image);

            //先檢查有沒有相同檔名的縮圖存在
            $small = explode('.', $img->basename);
            $resize = 'resize_image/' . $resizeFolder . '/' . $small[0] . '_rs.' . $small[1];
            //$checkRSImg = $resize;

            if (!file_exists(public_path() . '/' . $resize)) {
                $img->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });

                if ($small[1] == 'png') {
                    $img->save(public_path() . '/' . $resize, 100);
                } else {
                    $img->save(public_path() . '/' . $resize);
                }
                $small_image = $resize;
            } else {
                $small_image = $resize;
            }

            return $small_image;
        }

    }

    protected function set_optgroup($set = [])
    {
        if (!empty($set['Datas'])) {
            $Datas = [];
            $count = 0;
            foreach ($set['Datas'] as $row) {
                //產生optgroup label
                $label = '';
                if (!empty($set['parent_level'])) {
                    for ($i = 1; $i <= $set['parent_level']; $i++) {
                        $label .= ($i == 1) ? $row[$set['parent' . $i . '_key']]['title'] : " / " . $row[$set['parent' . $i . '_key']]['title'];
                    }

                }

                $Datas[$label][$count]['id'] = $row['id'];
                $Datas[$label][$count]['title'] = $row['title'];
                $count++;
            }
            return $Datas;
        } else {
            echo "Datas is empty.";
        }

    }

}
