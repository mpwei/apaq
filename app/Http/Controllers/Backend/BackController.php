<?php 
namespace App\Http\Controllers\Backend;

/***Models****/
use App\Http\Models\Calendar;

class BackController extends BackendController {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $Block = array("","red","green");
		// $calendar = '';
		// $data = Calendar::where('sub_title','!=','')->orderBy('id','asc')->get();
		// foreach($data as $row){
		// 	$calendar .= " {
        //               title: '".$row->sub_title."',
        //               start: '".$row->title."',
        //               backgroundColor: App_calendar.getLayoutColorCode('".$Block[$row->block?$row->block:1]."'),
        //               url: '/zh-tw/backen-admin/行事曆管理/edit/".$row->id."'
        //           },";
		// }
		// return view('backend.index',
        //     [
        //         'calendar_data'=>$calendar
        //     ]
        // );
		return view('backend/index');
	}

}
