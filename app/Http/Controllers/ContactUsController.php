<?php

namespace App\Http\Controllers;

/**相關Controller**/
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Setting;
use Illuminate\Http\Request;
use Validator;

class ContactUsController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/與我們聯絡/ajax-list/';

    protected $modelName = "ContactUs";

    public $index_select_field = ['id', 'is_answer', 'company', 'country', 'name', 'inquiry_type', 'category', 'email', 'created_at'];

    public $routePreFix = '與我們聯絡';

    public $viewPreFix = 'Contact';

    public $ajaxEditList = array(
        /******
        設定規則
        "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
        static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
         *****/
        "排序" => array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit" => true,
        ),
        "標題" => array(
            "field" => "name",
            "inputType" => "text",
            "is_edit" => false,
        ),
        "顯示狀態" => array(
            "是否顯示" => array(
                "field" => "is_answer",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S',
            ),
        ),
    );
    public $otherSelectOptions = [
        "type" => [
            "zh-cn" => [
                [
                    "id" => 1,
                    "title" => '業務洽詢',
                ],
                [
                    "id" => 2,
                    "title" => '產品服務洽詢',
                ],
                [
                    "id" => 3,
                    "title" => '意見與回饋',
                ],
                [
                    "id" => 4,
                    "title" => '其他',
                ],
            ],
            "zh-tw" => [
                [
                    "id" => 1,
                    "title" => '業務洽詢',
                ],
                [
                    "id" => 2,
                    "title" => '產品服務洽詢',
                ],
                [
                    "id" => 3,
                    "title" => '意見與回饋',
                ],
                [
                    "id" => 4,
                    "title" => '其他',
                ],
            ],
            "en" => [
                [
                    "id" => 1,
                    "title" => 'Business Inquiry',
                ],
                [
                    "id" => 2,
                    "title" => 'Professional Consultant For Products',
                ],
                [
                    "id" => 3,
                    "title" => 'Comments and feeback',
                ],
                [
                    "id" => 4,
                    "title" => 'Other',
                ],
            ],

        ],
        "category" => [
            "zh-cn" => [
                [
                    "id" => 1,
                    "title" => '汽車相關製造',
                ],
                [
                    "id" => 2,
                    "title" => '經銷商 / 代理商',
                ],
                [
                    "id" => 3,
                    "title" => '通訊網路',
                ],
                [
                    "id" => 4,
                    "title" => '電腦與週邊類',
                ],
                [
                    "id" => 5,
                    "title" => '設備製造',
                ],
                [
                    "id" => 6,
                    "title" => '電源相關產品',
                ],
                [
                    "id" => 7,
                    "title" => '家電相關',
                ],
                [
                    "id" => 8,
                    "title" => '其他',
                ],
            ],
            "zh-tw" => [
                [
                    "id" => 1,
                    "title" => '汽車相關製造',
                ],
                [
                    "id" => 2,
                    "title" => '經銷商 / 代理商',
                ],
                [
                    "id" => 3,
                    "title" => '通訊網路',
                ],
                [
                    "id" => 4,
                    "title" => '電腦與週邊類',
                ],
                [
                    "id" => 5,
                    "title" => '設備製造',
                ],
                [
                    "id" => 6,
                    "title" => '電源相關產品',
                ],
                [
                    "id" => 7,
                    "title" => '家電相關',
                ],
                [
                    "id" => 8,
                    "title" => '其他',
                ],
            ],
            "en" => [
                [
                    "id" => 1,
                    "title" => 'Automobile-related Manufactroy',
                ],
                [
                    "id" => 2,
                    "title" => 'Distributors / Agent Other',
                ],
                [
                    "id" => 3,
                    "title" => 'Communication and Network Industry',
                ],
                [
                    "id" => 4,
                    "title" => 'Computers and Peripheral Equipment Industry',
                ],
                [
                    "id" => 5,
                    "title" => 'Equipment Manufactory',
                ],
                [
                    "id" => 6,
                    "title" => 'Power supply related',
                ],
                [
                    "id" => 7,
                    "title" => 'Household electric appliance-related',
                ],
                [
                    "id" => 8,
                    "title" => 'Other',
                ],
            ],

        ],
    ];

    public function postStore(Request $request)
    {
        // /var_dump( $request->input('captcha') );
        $rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make($request->all(), $rules);

        //驗證碼檢查
        if ($validator->fails()) {
            // error_log('validate fail');

            return redirect(MakeItemV2::url('contact'))->with('Message', '請輸入正確驗證碼！');
            //return json_encode(['an'=>false,"message"=>"請輸入正確驗證碼！"]);
        } else {
            // error_log('validate pass');

            $Datas = $request->input('Contact');
            $Datas = $this->checkFormSendData($Datas); //檢查表單輸入的資料

            $Datas['client_ip'] = $request->ip();

            $resoult = json_decode(parent::addNew([
                "Datas" => $Datas,
                "modelName" => 'ContactUs',
                "routeFix" => 'ContactUs',
            ]), true);

            if (!empty($resoult['redirect'])) {
                $setting = Setting::find(1)->toArray();
                $emails = explode(',', $setting['mails']);

                foreach ($emails as $email) {
                    parent::sendMail(
                        [
                            'subject' => '聯絡我們',
                            'Model' => 'ContactUs',
                            'view' => 'emails.contactus',
                            'id' => $resoult['parent_id'],
                            'to' => $email,
                            'to_name' => parent::$adminer_name,
                            'type' => 'new',
                        ]
                    );
                }

//                    $mail_data = "<table>";
                //                    $mail_data .= "<tr><th colspan='2' style=text-align:center>基本資料</td></tr>";
                //                        foreach($Datas as $key => $value){
                //                            if($key == 'name' || $key == 'email' || $key == 'phone' || $key == 'company' || $key == 'message' || $key == 'subject'){
                //                                switch($key){
                //                                    case "name":
                //                                    $key = "姓名";
                //                                    break;
                //                                    case "email":
                //                                    $key = "信箱";
                //                                    break;
                //                                    case "company":
                //                                    $key = "公司";
                //                                    break;
                //                                    case "phone":
                //                                    $key = "聯絡電話";
                //                                    break;
                //                                    case "subject":
                //                                    $key = "主旨";
                //                                    break;
                //                                    case "message":
                //                                    $key = "聯絡內容";
                //                                    break;
                //                                }
                //                                $mail_data .= "<tr><td>" . $key . "</td><td>" . $value . "</td></tr>";
                //                            }
                //                        }
                //
                //
                //                    // $mail_data .= "<tr><td>姓名</td><td>" . $request->input('name') . "</td></tr>";
                //                    // $mail_data .= "<tr><td>信箱</td><td>" . $request->input('email') . "</td></tr>";
                //                    // $mail_data .= "<tr><td>聯絡電話</td><td>" . $request->input('tel') . "</td></tr>";
                //                    // $mail_data .= "<tr><td>主旨</td><td>" . $request->input('subject') . "</td></tr>";
                //                    // $mail_data .= "<tr><td>聯絡內容</td><td>" . $request->input('memo') . "</td></tr>";
                //                    $mail_data .= "</table>";
                //                    $mail_data .= "<br><br>";
                //                    $mail_data .= "<a style=text-decoration:none; href=".env('WEB_URL').">".env('WEB_NAME')."</a><br>";
                //
                //
                //
                //                $data = array();
                //                $data["data"] = $mail_data ;
                //
                //
                //                $setting = Setting::find(1);
                //                $servermail = explode(",",$setting->mails);
                //
                //                $array[0] = $Datas['email'] ;
                //                $array[1] = $servermail ;
                //
                //
                //
                //                $queue_data = [
                //                    'mail_tamplate' => 'emails.'. $request->locale .'.contactus',
                //                    'email' => $array[0],
                //                    'name'  => $setting->mails,
                //                    'subject'  => '【'.env('WEB_NAME').'】聯絡我們',
                //                    'servermail' => $setting->mails,
                //                    'maildata' => $data
                //                ];
                //
                //                // 建立隊列
                //                $queue_id = \Queue::push('App\Commands\SendEmail@fire', $queue_data);
                //
                //                pclose(popen('/usr/local/bin/php -q '.env('WEB_LINK').'cmd.php &', 'r'));
                //
                //
                //                return json_encode(['an'=>true,"message"=>"表單傳送成功，我們將盡快回覆您！"]);
                return redirect(MakeItemV2::url('contact'))->with('Message', "表單傳送成功，我們將盡快回覆您！");
            }
        }
    }

    /*============== 前台 ====================*/
    public static function index($locale)
    {
        return view($locale . '.contact.index');
    } //distributors
    public static function distributors($locale)
    {
        return view($locale . '.contact.distributors');
    }
}
