<?php
namespace App\Http\Controllers\Application;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;


/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\BackendController as Backend;

class ApplicationController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/application/ajax-list/';

    protected $modelName = "Application";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'application';

    public $viewPreFix = 'Application';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title'],
        "order" => "rank",
        "sort" => ''
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );
}
