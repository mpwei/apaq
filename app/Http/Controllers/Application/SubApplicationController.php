<?php 
namespace App\Http\Controllers\Application;

use Illuminate\Http\Request;
use View;
use Session;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Product\Item;
use App\Http\Models\Application;
use App\Http\Models\SubApplication;

class SubApplicationController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/application/sub_application/ajax-list/';

    protected $modelName = "SubApplication";

    public $index_select_field = ['id','rank','is_visible','title', 'application_id'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'application/sub_application';

    public $viewPreFix = 'Application.SubApplication';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title'],
        "order" => "rank",
        "sort" => ''
    ];

    protected $modelBelongs = [
        "Application" => [
            "parent" => "application_id",
            "filed" => 'id',
            "select" => ['title', 'id']
        ],
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    public function __construct()
    {
        parent::__construct();

        $Applications = Application::select('id', 'title')->get()->toArray();
        $Items = Item::select('id', 'title')->get()->toArray();

		view::share('Items',$Items);
		view::share('Applications',$Applications);
        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }

    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $Data = SubApplication::where('id',$id)->get()->first()->toArray();
        $Data['products'] = json_decode($Data['products'],true);
        //Debugbar::info($Data);
        return view('backend.Application.SubApplication.edit',[
                'data' => $Data,
                'actionUrl' => MakeItemV2::url('backen-admin/application/sub_application/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            //echo "批次修改<br>";
            parent::updateOne( $request->input('SubApplication'), 'SubApplication', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('SubApplication');
            
            $Datas['products'] = array_key_exists('products', $Datas)? json_encode($Datas['products']):[];
            //Debugbar::info($Datas['character']);
            //Debugbar::info(json_decode($Datas['character'], true));

            if( parent::updateOne( $Datas, 'SubApplication', '') )
            {
                return redirect( MakeItemV2::url('backen-admin/application/sub_application/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    public function index(Request $request,$locale, $category='')
    {
        $applications = Application::where('is_visible', 1)->with('sub_applications')->orderBy('rank', 'asc')->get();

        if(!empty($applications)){
            foreach($applications as $application){
                if(!empty($application->sub_applications)){
                    foreach($application->sub_applications as $sub_application){
                        $sub_application->products = Item::select('list_spec', 'image', 'title', 'category_id', 'theme_id')->with('Category')->with('Theme')->whereIn('id', json_decode($sub_application->products))->where('is_visible', 1)->orderBy('rank', 'asc')->get();
                        foreach ($sub_application->products as $item) {
                            $item->web_link = MakeItemV2::url(
                                "product/" . parent::processTitleToUrl($item->Theme->title)
                                . "/" . parent::processTitleToUrl($item->Category->title)
                                . "/" . parent::processTitleToUrl($item->title)
                            );
                        }
                    }
                }
            }
        }
        return View::make($locale.'.application.index',[
            "applications" => !empty($applications) ? $applications : '',
        ]);
    }
}