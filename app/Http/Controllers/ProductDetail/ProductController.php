<?php

namespace App\Http\Controllers\ProductDetail;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\FinancialInfo;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class ProductController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Product-Profile-Table/Product/ajax-list/';
    public $routePreFix = 'Product-Profile-Table/Product';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content'];
    public $whereRaw = "sub_category = 'Product'";
    protected $modelName = "FinancialInfo";
    protected $viewPreFix = 'ProductDetail.Product';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $financialInfo = FinancialInfo::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content')
            ->where('is_visible', 1)->where('sub_category', 'Product')->first()->toArray();

        return view($locale . '.product-detail.Product', [
            "data" => $financialInfo,
        ]);
    }
}