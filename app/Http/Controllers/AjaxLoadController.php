<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Cache;
use Mail;
use App\Http\Controllers\Backend\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\Pay\PayPalController;
use App\Http\Controllers\Product\StockController;
use App\Http\Controllers\MessageController;

use App\Http\Models\Member\Contact as MemberContact;
use App\Http\Models\Member\Account as Member;
use App\Http\Models\Member\OrderRecord;
use App\Http\Models\Shipping;
use App\Http\Models\User;

class AjaxLoadController extends BackendController
{
	public function getTestPay($locale)
	{
        // $find = User::where('email', "b3031717@gmail.com")->first();
        // $find->password = bcrypt('12341234');
        // $find->save();
		// $Pay = new App\Http\Controllers\Pay\iPayLinkController;
		// $Pay->payByiPayLink( 169 );
        // $find = Member::where('email', "bryan@amosdesign.tw")->first();
        // $find->Order = OrderRecord::with("RecordList")->where('member_id',$find->id)->first();
        // return View::make("emails.order.order_info",[
        //     "member" => $find
        // ]);

	}
    //
    public function getShipping($locale)
    {
        return View::make($locale.".load.shipping");
    }
    //多筆聯絡資訊
    public function getAddress($locale, $id='')
    {
    	$data = [];

    	if( !empty(Session::get('Member')) AND !empty( $id ) ){

    		$data = Session::get('Member')->Contact->find($id);

    	}

        return View::make($locale.".load.address_add",[
        	"data"=>$data
        ]);
    }
    public function getAddressList($locale)
    {
        $data = [];

        if( !empty(Session::get('Member')) ){

            $data = Session::get('Member')->Contact;

        }

        $shipping = [];
        if( !empty( Cache::get($locale.'_Shipping') ) ){
            $shipping = Cache::get($locale.'_Shipping');
        }else{
            $shipping = Shipping::where('is_visible','1')
                            ->select('id','rank','is_first','title','shipping_cost','exception_item')
                            ->get();

        }

        return View::make($locale.".load.address_change",[
            "data"=>$data,
            "shipping" => $shipping
        ]);
    }
    public function getAddressDetail(Request $request)
    {

        if( !empty( Session::get('Member')->Contact ) AND count( Session::get('Member')->Contact->toArray() ) >0 ){
            $find = Session::get('Member')->Contact->find( $request->input('id') );

            if( ( !empty( $find ) ) ){

                //放到session 給產生訂單時用
                Session::put("select_contact", $find);

                $find = ( !empty( $find ) )? $find->toArray() : [];
                //回傳
                return json_encode([
                    "an"=>true,
                    "data"=> $find
                ]);
            }else{
                return json_encode([
                    "an"=>false,
                    "message" =>"未找到此聯絡資訊"
                ]);
            }

        }else{
            return json_encode([
                "an"=>false,
                "message" =>"請新增其他聯絡資訊"
            ]);
        }
    }
    ////多筆聯絡資訊 新增  修改  刪除
    public function postAddress(Request $request)
    {
    	$data = $request->all();

    	if ( !empty( $data['type'] ) AND !empty( Session::get('Member') ) ) {

    		$data['Contact']['member_id'] = Session::get('Member')->id;
    		$answer = '';
    		switch ( $data['type'] ) {
    			case 'ADD':
			        $resoult = json_decode(parent::addNew([
			            "Datas" => $data['Contact'] ,
			            "modelName" => "MemberContact",
			            "routeFix" => 'isSave'
			            ]), true );

			        $answer = ( !empty( $resoult['parent_id'] ) )? true : false;
    				break;
    			case 'EDIT':
    				$answer = ( parent::updateOne( $data['Contact'], "MemberContact", '') )? true : false;
    				break;
    			case 'Delete':
                    $answer = ( parent::deleteOne( "MemberContact" , $data['Contact']['id'] ) )? true : false;
    				break;

    		}

    		if( $answer ){
    			$memberClass = new MemberController;
    			$newMemberData = $memberClass->findMemberSelf( Session::get('Member')->id );

    			Session::put("Member", $newMemberData);

    			return json_encode([
    				"an" => true,
    				"thisData"=> $data['Contact'],
    				"message" => "新增修改成功"
    			]);
    		}else{
    			return json_encode([
    				"an" => false,
    				"message" => "新增 或 修改失敗"
    			]);
    		}

    	}
    }
    //付款結果
    public function getPayment($locale)
    {
        return View::make($locale.".load.payment");
    }
    //會員卡驗證
    public function getVerify($locale)
    {
        return View::make($locale.".load.verify");
    }
    //變更密碼
    public function getPassword($locale)
    {
        return View::make($locale.".load.password");
    }
    //隱私權聲明
    public function getLegal($locale)
    {
        return View::make($locale.".load.legal");
    }
    //全站搜尋
    public function getSearch($locale)
    {
        return View::make($locale.".load.search");
    }
    //圖片放大
    public function getZoom($locale)
    {
        return View::make($locale.".load.zoom");
    }
    //取消訂單
    public function getCancel($locale)
    {
        return View::make($locale.".load.cancel");
    }
    //變更帳號 或手機
    public function getChangeAccount($locale)
    {
        return View::make($locale.".load.change_account");
    }
    //變更帳號 或手機 驗證
    public function getVerifyAccount($locale)
    {
        return View::make($locale.".load.verify_account");
    }
    public function getLangPage($locale)
    {
        return View::make($locale.".load.lang");
    }
    public function getOrderRepay($locale)
    {
        return View::make($locale.".load.repay");
    }
}
