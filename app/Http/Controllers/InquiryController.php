<?php

namespace App\Http\Controllers;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\Product\StockController;

/**Models**/

use App\Http\Models\Member\Account as Member;

class InquiryController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/詢問車管理/ajax-list/';

    protected $modelName = "MemberInquiryRecord";

    public $index_select_field = ['id','rank','member_id','shipping_store_id','is_answer','is_payed','pay_type','order_status','order_no','name','email'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '詢問車管理';

    public $viewPreFix = 'Member.Inquiry';

    protected $modelBelongs = [
        "Member" => [
            "parent" => "member_id",
            "filed" => 'id',
            "select" => ['first_name','last_name','id','email']
        ],
    ];

    protected $modelHas = [
        "MemberInquiryRecordList" => [
            "parent" => "order_id",
            "filed" => 'id',
            "select" => ['id','rank','order_id','is_resend','title','product_no','style_title','skin','size','qty','single_price','total_price']
        ],

    ];
    public $photoTable = [
        "排序"=> "rank",
        "產品名稱"=> "title",
        // "單價" => "single_price",
        // "總和" => "total_price",
        //"是否退貨"=>"is_resend",

    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "order_no",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_payed",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );
    public function getOrderList($locale, $id)
    {
    	$thisModel = new $this->modelClass;
    	$order = $thisModel->select('name')->with('Member')
    				->with('RecordList')
    				->where('id',$id)
    				->first();

    	return View::make("Backend.Member.Inquiry.list",[
    		"data" => $order
    	]);
    }

    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $request->input($this->modelName), $this->modelName, 'ajaxEdit');
            //將資料做暫存
            $this->setCacheData();
        }
        else
        {
            $Datas = ( !empty( $request->input($this->modelName) ) )?$request->input($this->modelName) : $request->input('Order_front');

            //var_dump( $Datas );
            //
            if( parent::updateOne( $Datas, $this->modelName, '') )
            {
                if( !empty( $request->input($this->modelName) ) ){
                    return redirect( MakeItemV2::url('backen-admin/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
                }elseif ( !empty( $request->input('Order_front') ) ) {

                    $thisMoel = new $this->modelClass;

                    $message = new MessageController;
                    $message->sendStateMail('order', $Datas['id']);

                    return[
                        "an" => true,
                        "data" => $thisMoel->find( $Datas['id'] )
                    ];

                }else{
                    return[
                        "an" => false,
                        "message" => "儲存失敗"
                    ];
                }

            }

        }
    }
    public function postSendMail(Request $request){
        //先找這一筆訂單
        $id = $request->input('id');

        $thisModel = new $this->modelClass;
        $find_order = $thisModel->with('RecordList')->where('id', $id)->first();
        if( !empty( $find_order ) ){
            if( $find_order->order_status != 4 ){
                $findMember = Member::where('id', $find_order->member_id)->first();
                $findMember->Order = $find_order;

                $message = new MessageController;
                $thisMessage = $message->getMessageContent(6);
                $send = $message->doSendStatusMail( $thisMessage , $findMember, "emails.".App::getLocale().".order.order_info");
                if( $send ){
                    return[
                        "an" => true,
                        "message" => "已寄出".$thisMessage->content_title
                    ];
                }else{
                    return[
                        "an" => false,
                        "message" => "寄送失敗"
                    ];
                }
            }else{
                return[
                    "an" => false,
                    "message" => "已取消的訂單無法寄送此信件"
                ];
            }
        }else{
            return[
                "an" => false,
                "message" => "找不到相關訂單紀錄"
            ];
        }


    }

    public function postOrderRepay(Request $request)
    {
    	$data = $request->all();
    	if( !empty( $data['id'] ) AND !empty( $data['type'] ) ){
    		$thisModel = new $this->modelClass;

    		$order = $thisModel->with("RecordList")->where("id", $data['id'])->first();
    		$order->pay_type = $data['type'];
    		$order->save();

    		$stock = new StockController;
    		$res = $stock->checkOrderStock($order);
    		//重新付款前確認庫存
    		if( !$res['an'] ){
                $order->order_status = 4;
                $order->save();

    			return redirect( MakeItemV2::url('member/inquiry') )->with('Message', $res['message'] );
    		}

            //銀聯再線
            if( $data['type'] == 3 ){

                $Pay = new App\Http\Controllers\Pay\UnionController;
                $Pay->payByUnionPay($data['id']);

            //PayPal 支付寶
            }elseif ( $data['type'] == 1 ) {
				$Pay = new App\Http\Controllers\Pay\PayPalController;
				$Pay->sendPayPalInfomation($data['id']);
            }
            elseif( $data['type'] == 4 ){//貨到付款
                //寄送通知信
                $message = new MessageController;
                $message->sendStateMail('order', $data['id']);

                return redirect( MakeItemV2::url('member/inquiry') );
            }

    	}else{
    		return redirect( MakeItemV2::url('member/inquiry/') )->with('Message',"資料錯誤");
    	}
    }
    /*===============後台結束=======================*/



}
