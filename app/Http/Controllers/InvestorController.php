<?php 
namespace App\Http\Controllers;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\Backend\MakeItemV2;

/**Models**/
use App\Http\Models\Banner;
use App\Http\Models\BannerPhoto;
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Theme;
use App\Http\Models\News\News;

class InvestorController extends BackendController {

	public function __construct()
	{
		parent::__construct();
	}

	public function monthlyRevenue($locale)
	{
		return view($locale.'.investor.monthly');
	}
	public function financialReport($locale)
	{
		return view($locale.'.investor.financial');
	}
	public function stock($locale)
	{
		return view($locale.'.investor.stock');
	}

	public function stakeholderEngagement($locale)
	{
		return view($locale.'.investor.stake');
	}

	public function organizationalStructure($locale)
	{
		return view($locale.'.investor.organizational');
	}

	public function internalAudit($locale)
	{
		return view($locale.'.investor.internal');
	}

    /*============== 前台 ====================*/
    public static function index($locale)
    {
        return view($locale.'.investor.index');
    }//distributors
    public static function distributors($locale)
    {
        return view($locale.'.contact.distributors');
    }
}