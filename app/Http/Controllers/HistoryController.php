<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Config;


/**相關Controller**/
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;


class HistoryController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/歷史沿革/ajax-list/';

    protected $modelName = "AboutUsHistory";

    public $index_select_field = ['id','is_visible','title'];

    //public $ajaxEditField = ['id', 'id','title', 'is_visible'];

    public $routePreFix = '歷史沿革';

    public $viewPreFix = 'AboutUs.History';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title','content','date'],
        // "order" => "id",
        // "sort" => 'id'
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        // "排序" => Array(
        //     "field" => "id",
        //     "inputType" => "text",
        //     "is_edit"=> true
        // ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );
    public function getIndex()
    {
        $Datas = [];

        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->index_select_field,
            "rank"      => 'date',
            "rank_sort" => !empty($this->rank_sort) ? $this-> rank_sort : 'asc',
        ]);
        
        return  View::make('backend.'.$this->viewPreFix.'.index',[
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }
    public function findData(){
        $model = new $this->modelClass;
        return $model->select('title','date','content')->where('is_visible',1)->orderBy('date','asc')->get();
    }
}
