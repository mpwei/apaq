<?php namespace App\Http\Controllers;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\Backend\MakeItemV2;

/**Models**/
use App\Http\Models\Banner;
use App\Http\Models\Setting;
use App\Http\Models\AboutUs;
use App\Http\Models\News\NewsCategory;
use App\Http\Models\Product\Theme;
use App\Http\Models\Product\Category;

class FrontEndController
{

	public function getSubtitle()
	{
		$subTitle = [];
		$subTitle['about'] = AboutUs::where('is_visible',1)
					->select('title')
					->orderBy('rank', 'asc')
					->get()
					->toArray();

		$subTitle['news'] = NewsCategory::where('is_visible',1)
					->select('title')
					->orderBy('rank', 'asc')
					->get()
					->toArray();

		$subTitle['product'] = Theme::where('is_visible',1)
					->select('title','id')
					->orderBy('rank', 'asc')
					->get()
					->toArray();

		foreach($subTitle['product'] as $key => $row){
			$subTitle['product'][$key]['category'] = Category::where('theme_id',$row['id'])
					->select('title')
					->orderBy('rank', 'asc')
					->get()
					->toArray();
		}

		return $subTitle;
	}
	public function getProduct()
	{
		$subTitle = Theme::where('is_visible',1)
					->select('title','id','image','icon')
					->orderBy('rank', 'asc')
					->get()
					->toArray();

		foreach($subTitle as $key => $row){
			$subTitle[$key]['category'] = Category::where('theme_id',$row['id'])->where('is_visible',1)
					->select('title')
					->orderBy('rank', 'asc')
					->get()
					->toArray();
		}

		return $subTitle;
	}

	public function getBanner($id = null)
	{
		$uri = $_SERVER["REQUEST_URI"];


		$banner = Banner::select('image','sm_image','link','is_href','title')
					->where('is_visible',1)
					->orderBy('rank', 'asc')
					->get();
					
		return $banner;
	}

	function checkPage()
	{
		$uri = $_SERVER["REQUEST_URI"];
		if(strpos(urldecode($uri),"about-us")) return 2;
		if(strpos(urldecode($uri),"news")) return 3;
		if(strpos(urldecode($uri),"food")) return 4;
		if(strpos(urldecode($uri),"Q&A")) return 5;

		return 1;
	}

	public function getHeadClass()
	{
		$uri = $_SERVER["REQUEST_URI"];
		$liClassActive = array();

		$liClassActive[1] = self::generateHeadClass($uri,array('about-us'));
		$liClassActive[2] = self::generateHeadClass($uri,array('news'));
		$liClassActive[3] = self::generateHeadClass($uri,array('food'));

		return $liClassActive;
	}

	function generateHeadClass($uri,$row)
	{
		foreach ($row as $value) {
			if(strpos(urldecode($uri),$value)) return 'active';
		}
		return '';
	}

	public function getPageHtml($pageData)
	{
		if ($pageData['total_pages'] <= 1) return ''; 
		$range = $pageData['range'];
		$num_pages = $pageData['num_pages'];
		$total_pages = $pageData['total_pages'];
		$url = $pageData['url'];


		if($num_pages > $total_pages) $num_pages = $total_pages;

		$html = '';
		// 若果正在顯示第一頁，無需顯示「前一頁」連結
		if ($num_pages > 1) {
			// 使用 << 連結回到第一頁
			// $html += " <a href={$url}?page=1><<</a> ";
			// 前一頁的頁數
			$prevpage = $num_pages - 1;
			// 使用 < 連結回到前一頁
			if (false === ($rst = strpos($url, '?')))
				$html .= ' <li class="page-item">
			                    <a class="page-link" href="'.$url.'?'.'page=' .$prevpage.'" aria-label="Previous">
			                      <i aria-hidden="true" class="fa fa-angle-left"></i>
			                      <span class="sr-only">Previous</span>
			                    </a>
			                  </li> ';
			else
				$html .= ' <li class="page-item">
			                    <a class="page-link" href="'.$url.'&'.'page='.$prevpage.'" aria-label="Previous">
			                      <i aria-hidden="true" class="fa fa-angle-left"></i>
			                      <span class="sr-only">Previous</span>
			                    </a>
			                  </li> ';
		}

		// 顯示當前分頁鄰近的分頁頁數
		for ($x = (($num_pages - $range) - 1); $x < (($num_pages + $range) + 1); $x++) {
			// 如果這是一個正確的頁數...
			if (($x > 0) && ($x <= $total_pages)) {
				// 如果這一頁等於當前頁數...
				if ($x == $num_pages) {
					// 不使用連結, 但用高亮度顯示
					$html .= "  <li class='page-item active'><a class='page-link'>".$x."</a></li> ";
					//$html .= " <a href=$url?page=".$x.">".$x."</a> ";
					// 如果這一頁不是當前頁數...
				} else {
					// 顯示連結
			if (false === ($rst = strpos($url, '?')))
					$html .= " <li class='page-item'><a class='page-link' href='$url?page=".$x."' >".$x."</a></li> ";
			else
					$html .= " <li class='page-item'><a class='page-link' href='$url&page=".$x."' >".$x."</a></li> ";
				}
			}
		}                   
              
                  
		// 如果不是最後一頁, 顯示跳往下一頁及最後一頁的連結
		 if ($num_pages != $total_pages) {
		 	// 下一頁的頁數
		 	$nextpage = $num_pages + 1;
		 	// 顯示跳往下一頁的連結
		 	if (false === ($rst = strpos($url, '?')))
				$html .= ' <li class="page-item">
			                    <a class="page-link" href="'.$url.'?'.'page='.$nextpage.'" aria-label="Previous">
			                      <i aria-hidden="true" class="fa fa-angle-right"></i>
			                      <span class="sr-only">Previous</span>
			                    </a>
			                  </li> ';
		 	else 		
				$html .= ' <li class="page-item">
			                    <a class="page-link" href="'.$url.'&'.'page='.$nextpage.'" aria-label="Previous">
			                      <i aria-hidden="true" class="fa fa-angle-right"></i>
			                      <span class="sr-only">Previous</span>
			                    </a>
			                  </li> ';
				
		// 	// 顯示跳往最後一頁的連結
		// 	//echo " <a href={$url}?page=".$total_pages.">>></a> ";
		 }
		return $html;
	}

	function getsetting()
	{
		$setting = Setting::find(1);
		$setting = json_decode($setting->content, true);

		
		return $setting;
	}
	function getnowpage($link)
	{
    	$web_link = $_SERVER["REQUEST_URI"];
		return (false !== ($rst = strpos($web_link, $link))) ? ' active':'';
	}
	function getlang($localhost)
	{
		switch($localhost){
			case 'zh-tw':
     		
			$words  = array(
				"WEB_Name"					=> Config::get('app.WEB_NAME'),		//商家名稱
				"WEB_Address"				=> Config::get('app.WEB_NAME'),		//商家地址
				"WEB_Tel"					=> Config::get('app.WEB_NAME'),		//商家電話
				"lang_tw"					=> "繁中",
				"lang_en"					=> "ENG",
				"lang_cn"					=> "簡中",
				//大標題
				"home_title"				=> "首頁",
				"quality_title"				=> "品質管理",
				"about_title"				=> "關於我們",
				"news_title"				=> "最新消息",
				"link_title"				=> "友站連結",
				"faq_title"					=> "常見問題",
				"album_title"				=> "相簿管理",
				"pic_title"					=> "相簿2管理",
				"product_title"				=> "產品介紹",
				"productinquiry_title"		=> "詢問車",
				"download_title"			=> "檔案下載",
				"contact_title"				=> "聯絡我們",
				"privacy_title"				=> "隱私權政策",

				//會員標題
				"regist_title"				=> "會員註冊",
				"member_title"				=> "會員中心",
				"video_title"				=> "影音",
				"member_data_title"			=> "會員資料",
				"change_pwd_title"			=> "修改密碼",
				"forget_pwd_title"			=> "忘記密碼",
				"order_title"				=> "訂單列表",
				"order_complete_title"		=> "訂單完成",
				"member_order_title"		=> "訂單管理",
				"member_orderdetail"		=> "訂單明細",
				"member_forgetpassword"		=> "忘記密碼?",

				//麵包屑

				"home_bread"				=> "首頁",
				"about_bread"				=> "關於我們",
				"news_bread" 				=> "最新消息",
				"faq_bread"					=> "常見問題",
				"quality_bread"				=> "品質管理",
				"product_bread"				=> "產品介紹",
				"product_search_bread"		=> "產品搜尋",
				"productcategory_bread"		=> "產品類別",
				"productinquiry_bread"		=> "INQUIRY",
				"search_bread"				=> "產品搜尋",
				"productshop_bread"			=> "產品購物車",
				"download_bread"			=> "檔案下載",
				"contact_bread"				=> "聯絡我們",
				"member_bread"				=> "會員中心",
				"member_login"				=> "會員登入",
				"join_member"				=> "加入會員",
				"join_members"				=> "已加入會員",
				"login"						=> "登入",
				"album_bread"				=> "相簿",
				"video_bread"				=> "影音",
				"member_account"			=> "帳戶資訊",
				//會員麵包屑
				"regist_bread"				=> "會員註冊",
				"member_bread"				=> "會員中心",
				"member_data_bread"			=> "會員中心",
				"change_pwd_bread"			=> "修改密碼",
				"forget_pwd_bread"			=> "忘記密碼",
				"order_bread"				=> "訂單列表",
				"order_complete_bread"		=> "訂單完成",
				"product_search_bread"		=> "產品搜尋 -> ",

				//其他
				"home"						=> "首頁",
				"faq_subtitle"				=> "常 見 問 題",
				"back"						=> "回上一頁",
				"relate_product"			=> "相關產品",
				"go_list"					=> "回列表",
				"readmore"					=> "詳細說明",
				"detail"					=> "詳細內容",
				"search"					=> "產品搜尋...",
				"inquiry_btn"				=> "我要詢價",
				"product_desc"				=> "描述Description",
				"product_spec"				=> "規格Specifications",
				"product_spec1"				=> "敘述一",
				"product_spec2"				=> "敘述二",
				"search_0_product"			=>	"查無此產品請重新搜尋。",
				"product_category"			=> "產品分類",
				"go_shopping_car"			=> "查看購物車",
				"in_car"					=> "加入購物車",
				"in_inquirycar"				=> "Inquiry",
				"go_billing"				=> "前往詢問",
				"link"						=> "Link",

				//會員註冊用
				"member_name"				=> "姓名",
				"member_tel"				=> "聯絡電話",
				"member_address"			=> "地址",
				"password"					=> "密碼",
				"check_password"			=> "再次輸入密碼",
				"member_register"			=> "立即註冊",
				"member_cancel"				=> "取消更新",
				"member_save"				=> "儲存資料",



				//聯絡我們用
				"contact_info"				=> "聯絡資訊",
				"contact_name"				=> "姓名",
				"contact_tel"	 			=> "電話",
				"contact_mail"				=> "EMAIL",
				"contact_address"			=> "地址",
				"contact_message" 			=> "訊息",
				"contact_company"			=> "公司",
				"contact_captcha" 			=> "驗證碼",
				"contact_captcha_tw"		=> "驗證碼",
				"contact_send" 				=> "送出訊息",
				"contact_subtitle"			=> "如有任何問題，歡迎填寫以下聯絡表單",
				"contact_messagesend" 		=> "已收到您的訊息，我們會盡快聯絡您！",
				"contact_messagenosend"		=> "糟了! 有東西出錯，請驗證'我不是機器人'或是重新整理此頁再試一次",
				"contact_showmap"			=> "顯示地圖",

                    


	              

				//會員中心用
				"member_account"			=> "信箱",
				"member_logintitle"			=> "我要登入",
				"member_fblogin"			=> "使用Facebook帳戶註冊",
				"member_regist"				=> "會員註冊",
				"member_ismember"			=> "已經是會員了 ?",
				"member_member_yet"			=> "尚未有會員帳號?",
				"member_clicklogin"			=> "點此登入",
				"member_name"				=> "姓名",
				"member_enname"				=> "英文姓名",
				"member_password"			=> "密碼",
				"member_checkpassword"		=> "密碼確認",
				"member_name"				=> "姓名",
				"member_sex"				=> "性別",
				"member_mail"				=> "電子信箱",
				"member_address"			=> "地址",
				"member_company"			=> "公司",
				"member_tel"	 			=> "聯絡電話",
				"member_message" 			=> "內容",
				"member_send" 				=> "送出",
				"member_logout" 			=> "登出",

				//購物車&詢問車
				"car_oid"					=> "編號",
				"car_name"					=> "訂購者姓名",
				"car_mail"					=> "信箱",
				"car_address"				=> "收件地址",
				"car_tel"					=> "聯絡電話",
				"car_payment"				=> "付款方式",
				"car_memo"					=> "備註",
				"car_data"					=> "基本資料",
				"car_product_title"			=> "產品明細",
				"car_product_name"			=> "產品名稱",
				"car_product_spec"			=> "規格",
				"car_product_price"			=> "價格",
				"car_shipfee"				=> "運費",
				"car_total"					=> "總計",
				"car_amount"				=> "小計",
				"car_pay_finished"			=> "詢問",

				"car_send_btn"				=> "送出詢價",
				"car_item"					=> "項目",
				"car_item_title"			=> "產品",
				"car_price"					=> "單價",
				"car_photo"					=> "圖片",
				"car_quantity"				=> "數量",
				"car_detail"				=> "詳細",
				"car_delete"				=> "刪除",
				"car_update"				=> "更新",
				"car_update_car"			=> "更新詢問車",







				"car_pay_type1"					=> "支付寶",
				"car_pay_type2"					=> "iPay付款",
				"car_pay_type3"					=> "線上刷卡",
				"car_pay_type4"					=> "貨到付款",
				"car_pay_type5"					=> "銀行轉帳"
				);
			break;
			case 'zh-cn':
				$words  = array(
					"WEB_Name"                  => Config::get('app.WEB_NAME'), //商家名称
					"WEB_Address"               => Config::get('app.WEB_NAME'), //商家地址
					"WEB_Tel"                   => Config::get('app.WEB_NAME'), //商家电话
					"lang_tw"                   => "繁中",
					"lang_en"                   => "ENG",
					"lang_cn"                   => "简中",
					//大标题
					"home_title"                => "首页",
					"quality_title"             => "品质管理",
					"about_title"               => "关于我们",
					"news_title"                => "最新消息",
					"link_title"                => "友站连结",
					"faq_title"                 => "常见问题",
					"album_title"               => "相册管理",
					"pic_title"                 => "相册2管理",
					"product_title"             => "产品介绍",
					"productinquiry_title"      => "询问车",
					"download_title"            => "档案下载",
					"contact_title"             => "联络我们",
					"privacy_title"             => "隐私权政策",
	
					//會員標題
					"regist_title" 				=> "会员注册",
					"member_title" 				=> "会员中心",
					"video_title"				=> "影音",
					"member_data_title" 		=> "会员资料",
					"change_pwd_title" 			=> "修改密码",
					"forget_pwd_title" 			=> "忘记密码",
					"order_title" 				=> "订单列表",
					"order_complete_title" 		=> "订单完成",
					"member_order_title" 		=> "订单管理",
					"member_orderdetail" 		=> "订单明细",
					"member_forgetpassword" 	=> "忘记密码?",
	
					
					//面包屑

					"home_bread" 				=> "首页",
					"about_bread" 				=> "关于我们",
					"news_bread" 				=> "最新消息",
					"faq_bread" 				=> "常见问题",
					"quality_bread" 			=> "品质管理",
					"product_bread" 			=> "产品介绍",
					"product_search_bread" 		=> "产品搜寻",
					"productcategory_bread" 	=> "产品类别",
					"productinquiry_bread" 		=> "INQUIRY",
					"search_bread" 				=> "产品搜寻",
					"productshop_bread" 		=> "产品购物车",
					"download_bread" 			=> "档案下载",
					"contact_bread" 			=> "联络我们",
					"member_bread" 				=> "会员中心",
					"member_login" 				=> "会员登入",
					"join_member" 				=> "加入会员",
					"join_members" 				=> "已加入会员",
					"login" 					=> "登入",
					"album_bread" 				=> "相簿",
					"video_bread" 				=> "影音",
					"member_account" 			=> "帐户资讯",
					//会员面包屑
					"regist_bread" 				=> "会员注册",
					"member_bread" 				=> "会员中心",
					"member_data_bread" 		=> "会员中心",
					"change_pwd_bread" 			=> "修改密码",
					"forget_pwd_bread" 			=> "忘记密码",
					"order_bread" 				=> "订单列表",
					"order_complete_bread" 		=> "订单完成",
					"product_search_bread" 		=> "产品搜寻 -> ",
	
										
					//其他
					"home" 						=> "首页",
					"faq_subtitle" 				=> "常 见 问 题",
					"back" 						=> "回上一页",
					"relate_product" 			=> "相关产品",
					"go_list" 					=> "回列表",
					"readmore" 					=> "详细说明",
					"detail" 					=> "详细内容",
					"search" 					=> "产品搜寻...",
					"inquiry_btn" 				=> "我要询价",
					"product_desc" 				=> "描述Description",
					"product_spec" 				=> "规格Specifications",
					"product_spec1" 			=> "叙述一",
					"product_spec2" 			=> "叙述二",
					"search_0_product" 			=> "查无此产品请重新搜寻。",
					"product_category" 			=> "产品分类",
					"go_shopping_car" 			=> "查看购物车",
					"in_car" 					=> "加入购物车",
					"in_inquirycar" 			=> "Inquiry",
					"go_billing" 				=> "前往询问",
					"link" 						=> "Link",

					//会员注册用
					"member_name" 				=> "姓名",
					"member_tel" 				=> "联络电话",
					"member_address" 			=> "地址",
					"password" 					=> "密码",
					"check_password" 			=> "再次输入密码",
					"member_register" 			=> "立即注册",
					"member_cancel" 			=> "取消更新",
					"member_save" 				=> "储存资料",

	
	
	
					//联络我们用
					"contact_info" 				=> "联络资讯",
					"contact_name" 				=> "姓名",
					"contact_tel" 				=> "电话",
					"contact_mail" 				=> "EMAIL",
					"contact_address" 			=> "地址",
					"contact_message" 			=> "讯息",
					"contact_company" 			=> "公司",
					"contact_captcha" 			=> "验证码",
					"contact_captcha_tw"		=> "验证码",
					"contact_send" 				=> "送出讯息",
					"contact_subtitle" 			=> "如有任何问题，欢迎填写以下联络表单",
					"contact_messagesend" 		=> "已收到您的讯息，我们会尽快联络您！",
					"contact_messagenosend" 	=> "糟了! 有东西出错，请验证'我不是机器人'或是重新整理此页再试一次",
					"contact_showmap" 			=> "显示地图",
					
					//会员中心用
					"member_account" 			=> "信箱",
					"member_logintitle" 		=> "我要登入",
					"member_fblogin" 			=> "使用Facebook帐户注册",
					"member_regist" 			=> "会员注册",
					"member_ismember" 			=> "已经是会员了 ?",
					"member_member_yet"			=> "尚未有会员帐号?",
					"member_clicklogin" 		=> "点此登入",
					"member_name" 				=> "姓名",
					"member_enname" 			=> "英文姓名",
					"member_password" 			=> "密码",
					"member_checkpassword" 		=> "密码确认",
					"member_name" 				=> "姓名",
					"member_sex" 				=> "性别",
					"member_mail" 				=> "电子信箱",
					"member_address" 			=> "地址",
					"member_company" 			=> "公司",
					"member_tel" 				=> "联络电话",
					"member_message" 			=> "内容",
					"member_send" 				=> "送出",
					"member_logout" 			=> "登出",
	
					//购物车&询问车
					"car_oid" 					=> "编号",
					"car_name" 					=> "订购者姓名",
					"car_mail" 					=> "信箱",
					"car_address" 				=> "收件地址",
					"car_tel" 					=> "联络电话",
					"car_payment" 				=> "付款方式",
					"car_memo" 					=> "备注",
					"car_data" 					=> "基本资料",
					"car_product_title" 		=> "产品明细",
					"car_product_name" 			=> "产品名称",
					"car_product_spec" 			=> "规格",
					"car_product_price"			=> "价格",
					"car_shipfee"				=> "运费",
					"car_total" 				=> "总计",
					"car_amount" 				=> "小计",
					"car_pay_finished" 			=> "询问",

					"car_send_btn" 				=> "送出询价",
					"car_item" 					=> "项目",
					"car_item_title" 			=> "产品",
					"car_price" 				=> "单价",
					"car_photo" 				=> "图片",
					"car_quantity" 				=> "数量",
					"car_detail" 				=> "详细",
					"car_delete" 				=> "删除",
					"car_update" 				=> "更新",
					"car_update_car" 			=> "更新询问车",
					"car_pay_type1" 			=> "支付宝",
					"car_pay_type2" 			=> "iPay付款",
					"car_pay_type3" 			=> "线上刷卡",
					"car_pay_type4" 			=> "货到付款",
					"car_pay_type5" 			=> "银行转帐"
					);
			break;
			case 'en':


			$words  = array(
				"WEB_Name"					=> Config::get('app.WEB_NAME'),		//商家名稱
				"WEB_Address"				=> Config::get('app.WEB_NAME'),		//商家地址
				"WEB_Tel"					=> Config::get('app.WEB_NAME'),		//商家電話
				"lang_tw"					=> "繁中",
				"lang_en"					=> "ENG",
				"lang_cn"					=> "簡中",
				//大標題
				"home_title"				=> "Home",
				"quality_title"				=> "品質管理",
				"about_title"				=> "關於吉勝",
				"news_title"				=> "News",
				"faq_title"					=> "常見問題",
				"product_title"				=> "Product",
				"productinquiry_title"		=> "INQUIRY",
				"download_title"			=> "檔案下載",
				"contact_title"				=> "聯絡我們",

				//會員標題
				"regist_title"				=> "會員註冊",
				"member_title"				=> "會員中心",
				"album_title"				=> "相簿",
				"video_title"				=> "影音",
				"member_data_title"			=> "會員中心",
				"change_pwd_title"			=> "修改密碼",
				"forget_pwd_title"			=> "忘記密碼",
				"order_title"				=> "訂單列表",
				"order_complete_title"		=> "訂單完成",

				//麵包屑
				"home_bread"				=> "Home",
				"about_bread"				=> "About",
				"news_bread" 				=> "News",
				"faq_bread"					=> "Faq",
				"quality_bread"				=> "Quality",
				"product_bread"				=> "Product",
                "product_search_bread"		=> "Product Search",
				"productinquiry_bread"		=> "INQUIRY",
				"productshop_bread"			=> "Product Shopping Car",
				"download_bread"			=> "Download",
				"contact_bread"				=> "Contact",
				"member_bread"				=> "Member",
				"album_bread"				=> "Album",
				"video_bread"				=> "Video",

				//會員麵包屑
				"regist_bread"				=> "Member Register",
				"member_bread"				=> "Member",
				"member_data_bread"			=> "Member Data",
				"change_pwd_bread"			=> "Update Password",
				"forget_pwd_bread"			=> "Forget Password",
				"order_bread"				=> "Order List",
				"order_complete_bread"		=> "Order Complete",

				//其他
                "detail"                    => "detail",
				"home"						=> "home",
				"back"						=> "back",
				"readmore"					=> "more",
				"faq_subtitle"				=> "Frequently Asked Question",


				//聯絡我們用
				"contact_info"				=> "Information",
				"contact_name"				=> "Name",
				"contact_tel"	 			=> "Tel",
				"contact_mail"				=> "EMAIL",
				"contact_address"			=> "Address",
				"contact_message" 			=> "Message",
				"contact_company"			=> "Company",
				"contact_captcha" 			=> "CAPTCHA",
				"contact_send" 				=> "Send",
				"contact_subtitle"			=> "It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!",
				"contact_messagesend" 		=> "We have received your message, we will contact you very soon.",
				"contact_messagenosend"		=> "Oops! Something went wrong, please verify that you are not a robot or refresh the page and try again.",
				"contact_showmap"			=> "Show Map",
	              
	                
                    
	                
				//會員中心用
				"member_logintitle"			=> "Login",
				"member_fblogin"			=> "Facebook Login",
				"member_regist"				=> "Regist",
				"member_ismember"			=> "Is Member ?",
				"member_clicklogin"			=> "Login",
				"member_name"				=> "Name",
				"member_enname"				=> "Name",
				"member_password"			=> "Password",
				"member_checkpassword"		=> "Check Password",
				"member_sex"				=> "Sex",
				"member_mail"				=> "Email",
				"member_address"			=> "Address",
				"member_company"			=> "Company",
				"member_tel"	 			=> "Tel",
				"member_message" 			=> "Message",
				"member_send" 				=> "Send",

				//購物車&詢問車
				"car_oid"					=> "No.",
				"car_name"					=> "Name",
				"car_mail"					=> "Email",
				"car_address"				=> "Address",
				"car_tel"					=> "Tel",
				"car_payment"				=> "Payment",
				"car_memo"					=> "Memo",
				"car_data"					=> "Data",
				"car_product_title"			=> "Product Detail",
				"car_product_name"			=> "Product Name",
				"car_product_spec"			=> "Spec",
				"car_product_price"			=> "Price",
				"car_shipfee"				=> "Ship fee",
				"car_total"					=> "Amount",
				"car_amount"				=> "Total",

				"car_pay_type1"					=> "Alipay",
				"car_pay_type2"					=> "iPay",
				"car_pay_type3"					=> "Credit",
				"car_pay_type4"					=> "Cash on delivery",
				"car_pay_type5"					=> "Atm"
				);
				$WEB_Name				= Config::get('app.WEB_NAME');		//商家名稱
				$WEB_Address			= Config::get('app.WEB_NAME');		//商家地址
				$WEB_Tel				= Config::get('app.WEB_NAME');		//商家電話

				//大標題
				$about_title			= "About";
				$news_title				= "News";
				$faq_title				= "Faq";
				$product_title			= "Product";
				$productinquiry_title	= "Inquiry";
				$download_title			= "Download";
				$contact_title			= "Contact";
				$member_title			= "Member";
				$album_title			= "Album";
				$video_title			= "Video";

				//會員標題
				$regist_title			= "Regist";
				$member_title			= "Member";
				$member_data_title		= "Member";
				$change_pwd_title		= "Change Password";
				$forget_pwd_title		= "Forget Password";
				$order_title			= "Order List";
				$order_complete_title	= "Order Complete";

				//麵包屑
				$about_bread			= "about";
				$news_bread 			= "news";
				$faq_bread				= "faq";
				$product_bread			= "product";
				$productinquiry_bread	= "inquiry";
				$download_bread			= "download";
				$contact_bread			= "contact";
				$member_bread			= "member";
				$album_bread			= "album";
				$video_bread			= "video";

				//會員麵包屑
				$regist_bread			= "Regist";
				$member_bread			= "Member";
				$member_data_bread		= "Member";
				$change_pwd_bread		= "Change Password";
				$forget_pwd_bread		= "Forget Password";
				$order_bread			= "Order List";
				$order_complete_bread	= "Order Complete";


			break;
		}
			return $words;
	}
}
