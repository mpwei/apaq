<?php

namespace App\Http\Controllers\Certificate;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Certificate;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class CertificateController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/證書管理/ajax-list/';
    public $routePreFix = '證書管理';
    public $selectField = ['id', 'rank', 'is_visible', 'title', 'image'];
    protected $modelName = "Certificate";
    protected $viewPreFix = 'Certificate';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $certificate = Certificate::select('id', 'rank', 'is_visible', 'title', 'image')->where('is_visible', 1)->orderBy('rank', 'asc')->get()->toArray();

        return view($locale . '.certificate.certificate', [
            "data" => $certificate,
        ]);
    }
}