<?php

namespace App\Http\Controllers;

/**原生函式**/

use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Product\Item;
/**相關Controller**/

use View;

/**Models**/
class ProductFilterController extends CRUDBaseController
{
    protected $modelName = "ProductItem";

    public function Index($locale)
    {
        $items = Item::select('desc2')->where('title', 'LIKE', '%篩選用%')->orderBy('rank', 'asc')->get();

        return view($locale . '.product.filter', [
            "data" => json_encode($items),
        ]);
    }
}
