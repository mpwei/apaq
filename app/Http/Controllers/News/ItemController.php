<?php

namespace App\Http\Controllers\News;

/**原生函式**/
use App;
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\PublicController as PublicClass;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\News\News;

/**相關Controller**/
use App\Http\Models\News\NewsCategory as Category;
use Illuminate\Http\Request;
/**Models**/

use View;

class ItemController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/最新消息/消息列表/ajax-list/';

    protected $modelName = "News";

    public $index_select_field = ['id', 'category_id', 'is_visible', 'is_home', 'title', 'date', 'rank'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '最新消息/消息列表';

    public $limit_page = 10;

    public $viewPreFix = 'News';

    public $ajaxEditList = array(
        /******
        設定規則
        "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
        static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
         *****/
        "排序" => array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit" => true,
        ),
        "標題" => array(
            "field" => "title",
            "inputType" => "text",
            "is_edit" => true,
        ),
        "顯示狀態" => array(
            "是否顯示" => array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S',
            ), /*
        "是否置頂" => Array(
        "field" => "is_ontop",
        "inputType" => "radio",
        "showColor" => 'label-danger',
        "showText" => 'T'
        ),
        "是否顯示於首頁" => Array(
        "field" => "is_show_home",
        "inputType" => "radio",
        "showColor" => 'label-danger',
        "showText" => 'H'
        ),*/
        ),
    );

    protected $modelBelongs = [
        "NewsCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title', 'id'],
        ],
    ];
    /*===============後台結束=======================*/
    public function index(Request $request, $locale, $category = '')
    {
        $category = Backend::revertUrlToTitle($category);
        //最新消息分類
        // $title = $category ? $category : Category::select('title')->where('is_visible', 1)->orderBy('rank','asc')->first()->title;
        // var_dump($title);
        $Data = Category::where('is_visible', 1)->orderBy('rank', 'asc')->get();
        foreach ($Data as $key => $row) {
            $Data[$key]->web_link = MakeItemV2::url('news/' . Backend::processTitleToUrl($row->title));
        }
        return View::make($locale . '.news.index', [
            "title" => $category,
            "pageData" => $this->generatePage($request->input('page'), $category),
            "Category" => $Data,
        ]);
    }
    public function detail($locale, $category = '', $title = '')
    {
        $public = new PublicClass;
        $newsCategory = $public->getThisCategory($this->modelName);
        $thisCategory = $newsCategory->where('title', $category)->first();

        $title = Backend::revertUrlToTitle($title);
        $thisModel = new $this->modelClass;
        $news = $thisModel->where('title', $title)->where("category_id", $thisCategory->id)->with('Category')->first();
        $link = MakeItemV2::url('news/' . Backend::processTitleToUrl($news->Category->title));
        return View::make($locale . '.news.detail', [
            "News" => $news,
            "title" => $news->title,
            "Link" => $link,
        ]);
    }

    public function setDetailLink($data, $type = '')
    {
        $category = (!empty($type) and $type == 'detail') ? Backend::processTitleToUrl($data->Category->title) : Backend::processTitleToUrl($data->title);

        $news = (!empty($type) and $type == 'detail') ? Backend::processTitleToUrl($data->title) : '';

        return MakeItemV2::url((!empty($type) and $type == 'detail') ? 'news/' . $category . '/' . $news : 'news/' . $category);
    }

    public function generatePage($page = 1, $category = "")
    {
        if ($page < 1 || $page == null) {
            $page = 1;
        }

        $pageData = array();
        $pageData['range'] = $this->limit_page;
        $pageData['num_pages'] = $page;

        if ($category) {
            $cno = Category::where('title', $category)->first();

            $cno = $cno->id;
            $pageData['total_pages'] = (int) ceil(News::where('is_visible', 1)->where('category_id', $cno)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        } else {
            $pageData['total_pages'] = (int) ceil(News::where('is_visible', 1)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        }

        $url = $category ? 'news/' . $category : 'news/';
        $pageData['url'] = MakeItemV2::url($url);

        $data = News::with('Category')->
            where('is_visible', 1);

        if ($category) {

            $data = $data->where('category_id', $cno)->orderBy('date', 'desc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->get();
        } else {
            $data = $data->orderBy('date', 'desc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->get();
        }
        foreach ($data as $key => $row) {
            $data[$key]->web_link = $this->setDetailLink($row, 'detail');
        }
        $data = $data->toArray();

        $pageData['data'] = $data;
        return $pageData;
    }
}
