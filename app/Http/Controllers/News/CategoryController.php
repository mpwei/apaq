<?php

namespace App\Http\Controllers\News;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;


/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;



class CategoryController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/最新消息/消息分類/ajax-list/';

    protected $modelName = "NewsCategory";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '最新消息/消息分類';

    public $viewPreFix = 'News.Category';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title'],
        "order" => "rank",
        "sort" => ''
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

}
