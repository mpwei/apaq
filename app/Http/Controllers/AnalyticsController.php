<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\CRUDBaseController;

class AnalyticsController extends CRUDBaseController {

    protected $modelName = "News";


    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];


    public function getIndex()
    {
       header('Location: https://analytics.google.com/analytics/web/?hl=zh-TW');
    }
}