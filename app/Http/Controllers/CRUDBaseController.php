<?php
namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Cache;
use App\Http\Controllers\Backend\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;

class CRUDBaseController extends BackendController
{
    //modal class
    protected $modelClass;

    //設定在bacend中的 Modal array key
    protected $modelName;

    //設定後台畫面路徑
    protected $viewPreFix;

    protected $index_select_field = [];

    protected $modelBelongs = [];

    protected $modelHas = [];

    protected $saveSubData = [];

    protected $routePreFix;

    protected $ajaxEditLink ;
    public $ajaxEditList = [];
    public $ajaxEditField = [];


    protected $cacheData = [];
    protected $mailSeting = [];

    public static $show_type = [
        [
            "id" => 1,
            "title" => "圖片"
        ],
        [
            "id" => 2,
            "title" => "Video"
        ],
        [
            "id" => 3,
            "title" => "Youtube"
        ],
        [
            "id" => 4,
            "title" => "Vimeo"
        ],
    ];
    public $photoTable = [];


    public $otherSelectOptions = [];

    public function __construct()
    {
        parent::__construct();

        $this->modelClass = parent::$ModelsArray[ $this->modelName ];

        //有設定多圖表單時
        if(isset( $this->photoTable )){

            View::share('bulidTable', $this->photoTable);
        }

        //路徑前贅字
        if(isset( $this->routePreFix )){
            View::share('routePreFix', $this->routePreFix);
        }

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }

        //其他選項
        if(isset( $this->otherSelectOptions )){
            View::share('otherSelectOptions', $this->otherSelectOptions);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $Datas = [];

        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->index_select_field,
            "belong" => $this->modelBelongs,
            "rank"      => !empty($this->rank) ? $this-> rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this-> rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.'.$this->viewPreFix.'.index',[
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $parent = $this->FindBelongAndHas();

        return view('backend.'.$this->viewPreFix.'.edit',
            [
                'data'=>[],
                "parent" => $parent,
                'actionUrl' => MakeItemV2::url('backen-admin/'.$this->routePreFix.'/store')
            ]
        );
    }
    public function getCopy($locale, $id)
    {
        $parent = $this->FindBelongAndHas($id);

        /*過濾掉子資料的ID*/
        if( !empty($parent['has']) )
        {
            foreach( $parent['has'] as $chlidName => $childArray )
            {
                foreach( $childArray as $key => $value )
                {
                    $parent['has'][$chlidName][$key]['id'] = '';
                }
            }
        }

        $thisModel = $this->modelClass;
        $data = $thisModel::find($id);
        $data->id = '';

        return view('backend.'.$this->viewPreFix.'.edit',[
                // 'data' => $this->modelClass::find($id),
                'data' => $data,
                "parent" => $parent,
                'actionUrl' => MakeItemV2::url('backen-admin/'.$this->routePreFix.'/store'),
            ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $resoult = json_decode(parent::addNew([
            "Datas" => $request->input( $this->modelName ) ,
            "modelName" => $this->modelName,
            "routeFix" => ''.$this->routePreFix.''
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {
            $this->checkSavedSubData($request->all(), $resoult['parent_id'] );

            //通知信
            $this->sendNoticeMail($resoult['parent_id'], 'new');

            //將資料做暫存
            $this->setCacheData();
            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $parent = $this->FindBelongAndHas($id);
        $thisModel = $this->modelClass;
        return view('backend.'.$this->viewPreFix.'.edit',[
                'data' => $thisModel::find($id),
                "parent" => $parent,
                'actionUrl' => MakeItemV2::url('backen-admin/'.$this->routePreFix.'/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
            $Datas = $request->input($this->modelName);
            if(!empty($Datas['related_id']))
            if(count( $Datas['related_id'] ) > 0 ) 
                {   
                    $Datas['related_id'] = implode($Datas['related_id'], ',');
                    unset($Datas['related_id']);
                }
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $Datas, $this->modelName, 'ajaxEdit');
            //將資料做暫存
            $this->setCacheData();
        }
        else
        {
            $Datas = $request->input($this->modelName);

            //var_dump( $Datas );
            if( parent::updateOne( $Datas, $this->modelName, '') )
            {

                $this->checkSavedSubData($request->all(), $Datas['id']);

                //通知信
                $this->sendNoticeMail($Datas['id'], 'reply');

                //將資料做暫存
                $this->setCacheData();

                return redirect( MakeItemV2::url('backen-admin/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';
        //$method =  $request->input('method');

        if( empty( $method ) )
        {
            //先刪除圖片
            $subData = $this->deleteSubData($id);

            if( $subData ){
                parent::deleteOne( $this->modelName, $id );

                //將資料做暫存
                $this->setCacheData();
            }

        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                //$method = ( $method === 'Photo' )? 'NewsPhoto': $method;

                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }

    }
    protected function deleteSubData ( $parent_id )
    {
        $res = true;

        if( !empty($this->saveSubData ) ){

            foreach ($this->saveSubData as $n) {

                $this_model = parent::$ModelsArray[ $n['modelName'] ];

                foreach ($this_model::where($n['to_parent'], $parent_id )->get() as $row) {
                    if ( !$row->delete() ) {
                        $res = false;
                    }
                }
            }
        }

        return $res;
    }
    protected function checkSavedSubData($requestDatas,  $parent_id){
        //如果這資料有子訊息時

        if( isset( $this->saveSubData ) AND !empty( $this->saveSubData ) ){
            foreach ($this->saveSubData as $row) {

                if( !empty( $requestDatas[ $row['modelName'] ] ) ){

                    $data_find = $requestDatas[ $row['modelName'] ];

                    if( !empty( $data_find ) AND count( $data_find ) > 0 ){
                        $this->updateMulti($data_find, $row['modelName'], $row['to_parent'], $parent_id);
                    }
                }

            }
        }
    }
    //檢查是否需要抓取關聯的資料
    protected function FindBelongAndHas($id='')
    {
        //抓父關聯資料
        $data = [];

        if ( isset( $this->modelBelongs ) AND !empty( $this->modelBelongs ) ) {


            foreach ($this->modelBelongs as $key => $value) {

                $this_parent = parent::$ModelsArray[ $key ];

                if( isset($value['withParentModel']) AND $value['withParentModel'] == true )
                {
                    $this_parent = $this_parent::select( $value['select'] )
                                                            ->with($value['parentModel'])
                                                            ->get()
                                                            ->toArray();
                    /*
                    原本名稱： 某某分類
                    改為： 某某故事(爺爺) - 某某分類
                     */
                    foreach( $this_parent as $key2 => $option )
                    {
                        $this_parent[$key2]['title'] = $option[ $value['relateName'] ]['title'].' - '.$option['title'];
                    }
                }
                else
                {
                    $this_parent = $this_parent::select( $value['select'] );

                    //如果有設定orderBy
                    if ( !empty( $value['orderBy'] ) ) {
                        $orderBy = explode(',', $value['orderBy']);
                        $this_parent = $this_parent->orderBy( $orderBy[0], $orderBy[1] );
                    }


                    $this_parent = $this_parent->get()->toArray();
                }


                $data['belong'][ $key ] = $this_parent;
            }

        }
        //抓子關連資料
        if ( isset( $this->modelHas ) AND !empty( $this->modelHas ) ) {

            foreach ($this->modelHas as $key => $value) {

                $this_parent = parent::$ModelsArray[ $key ];
                $this_parent = $this_parent::select($value['select'])->where( $value['parent'], $id  );

                //如果有設定orderBy
                if ( !empty( $value['orderBy'] ) ) {
                    $orderBy = explode(',', $value['orderBy']);
                    $this_parent = $this_parent->orderBy( $orderBy[0], $orderBy[1] );
                }else{
                    $this_parent = $this_parent->orderBy('rank', 'asc');
                }

                $data['has'][ $key ] = $this_parent->get()->toArray();
            }

        }
        return $data;

    }
    protected function sendNoticeMail($id, $type)
    {
        if( !empty( $this->mailSeting ) AND $this->mailSeting['active'] ){

            parent::sendMail(
                [
                    'subject' => $this->mailSeting['subject'],
                    'Model' => $this->modelName,
                    'view' => $this->mailSeting['view'],
                    'id' =>  $id,
                    'to' => parent::$adminer_mail,
                    'to_name' => parent::$adminer_name,
                    'type' => $type
                ]
            );

        }
    }

    protected function checkFormSendData($Datas)
    {
        foreach ($Datas as $key => $value) {
            $Datas[ $key ] = strip_tags($value);
        }

        return $Datas;
    }

    protected function setCacheData(){

        if( !empty( $this->cacheData ) AND $this->cacheData['active'] == true ){
            $thisModel = new $this->modelClass;
            if( !empty($this->cacheData['has']) ){
                foreach ($this->cacheData['has'] as $has) {
                    $thisModel = $thisModel->with( $has );
                }
            }
            $find = $thisModel->where("is_visible", 1 );

            //沒有select就抓所有欄位
            if( !empty( $this->cacheData['select'] ) ){
                $find = $find->select( $this->cacheData['select'] );
            }
            if( !empty( $this->cacheData['order'] ) ){
                $sort = ( !empty( $this->cacheData['sort'] ) )? $this->cacheData['sort'] : '';

                $find = $find->orderBy($this->cacheData['order'], $sort);
            }

            $locale = ( !empty( parent::getRouter()->current()->parameters()['locale'] ) )? parent::getRouter()->current()->parameters()['locale'] : '';

            $find = $find->get();
            if( !empty( $find ) AND count( $find->toArray() ) > 0 ){
                Cache::forever( $locale."_".$this->modelName, $find );
            }


        }
    }
    public function getThisModelCacheData( $cacheName ='' )
    {
        //App::getLocale().
        $cacheName = ( empty($cacheName) )? $this->modelName : $cacheName;
        $cacheData = ( !empty( Cache::get( $cacheName ) ) )? Cache::get( $cacheName ) : [];
        return $cacheData;
    }

    /*==============jQuery Ajax ====================*/
    //批次修改
    public function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");

        $select_feild = ( !empty( $this->ajaxEditField ) AND isset( $this->ajaxEditField ) )? $this->ajaxEditField : $this->index_select_field;

        $works = Array();
        $thisModel = $this->modelClass;
            foreach ($ids as $row) {

                $works[] = $thisModel::where('id','=',$row)
                            ->select( $select_feild )
                            ->get();
            }
        return view('backend.Ajax.list')
                    ->with('ajaxEditList',$this->ajaxEditList)
                    ->with('modal', $this->modelName)
                    ->with('update_link', MakeItemV2::url('backen-admin/'.$this->routePreFix.'/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();

        $res = parent::updataOneColumns([
                'modelName' =>  $this->modelName,
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        //將資料做暫存
        $this->setCacheData();

        return $res;

    }

    /*===============後台結束=======================*/

}
