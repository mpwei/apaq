<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Config;


/**相關Controller**/
use App\Http\Controllers\Backend\BackendController as Backend;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\HistoryController;

class AboutController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/關於我們/ajax-list/';

    protected $modelName = "AboutUs";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '關於我們';

    public $viewPreFix = 'AboutUs';

    public $cacheData =[
        'active' => true,
        'select' => ['id','title','content'],
        "order" => "rank",
        "sort" => 'rank'
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    public function index($locale, $title='')
    {
        $title = Backend::revertUrlToTitle( $title );
        $history = [];
        $thisModel = new $this->modelClass;

        $thisAbout = $thisModel->select('id','title',"content")->where('title', $title)->first();
        $thisAbout = ( !empty($thisAbout) )? $thisAbout : $thisAbout->first();

        //只在特定一筆資料顯示歷史沿革
        if( $thisAbout->id ==1 or $thisAbout->title == 'About Us' or $thisAbout->title == '關於我們' ){
            $historyClass = new HistoryController;
            $history = $historyClass->findData();
        }

        return View::make($locale.'.about.index',[
            "thisAbout" => $thisAbout,
            "history" => $history,
        ]);

    }
    public function dac($locale){
        return View::make($locale.'.about.dac',[]);

    }
    public function certification($locale){
        return View::make($locale.'.about.certification',[]);
    }
    // <li class="menu-item">
    //     <a class="active" href="about.html">About us</a>
    // </li>
    public function setContentListMenu($datas, $thisData='')
    {
        if( !empty( $datas ) ){
        
        $html = '<div class="SubMenu text-center">
			<ul class="isotop-classes-tab myisotop2">';
            foreach ($datas as $row) {
                $onThis = ( !empty( $thisData ) AND $thisData->title == $row->title )? "current" :'';

                $html .='<li>';
                    $html .='<a class="'.$onThis.'" href="'.MakeItemV2::url(  "about-us/".Backend::processTitleToUrl( $row->title ) ).'">'.$row->title.'</a>';
                $html .='</li>';
            }
        $html .='</ul></div>';
        }
        return $html;
    }
}
