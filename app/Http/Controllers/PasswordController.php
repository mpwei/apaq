<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Cache;
use Mail;
use App\Http\Controllers\Backend\MakeItemV2;
/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\MemberController;
use App\Http\Models\Member\Account as Member;
use App\Http\Models\Setting;

class PasswordController extends BackendController
{
    public function getForget($locale)
    {
        return View::make($locale.'.load.forget');
    }
    protected function postForget(Request $request)
    {
        $Data = $request->input('Forget');
        $rules = [
            'captcha' => 'required|captcha',
        ];
        $validator = Validator::make($request->all(), $rules);
        if( $validator->fails() ){
            return json_encode([
                "an"=> false,
                "message" => "驗證碼錯誤"
            ]);
        }else{
            $find = Member::where('email', $Data['email'])
                        ->where('phone', $Data['phone'])
                        ->first();
            if( empty( $find ) ){
                return json_encode([
                    "an"=> false,
                    "message" => "查無此帳號，請確認電話與信箱是否正確"
                ]);
            }else{
                $newPassword = $this->getRandPassword(8);
                $find->password = bcrypt( $newPassword );
                if( $find->save() ){

                    $find->new_password = $newPassword;



                $data = array();
                $data["first_name"] = $find -> first_name;
                $data["password"] = $newPassword ;
                
                $data["web"] = env('WEB_URL') ;
                $data["webmail"] = env('WEB_MAIL') ;


                $setting = Setting::find(1);
                $servermail = explode(",",$setting->mails);

                $array[0] = $Data['email'] ;
                $array[1] = $servermail ;


                $queue_data = [
                    'mail_tamplate' => 'emails.'. $request->locale .'.member.password_reset',
                    'email' => $array[0],
                    'name'  => $setting->mails,
                    'subject'  => '【'.env('WEB_NAME').'】密碼重置',
                    'servermail' => $setting->mails,
                    'maildata' => $data
                ];

                // 建立隊列
                $queue_id = \Queue::push('App\Commands\SendEmail@fire', $queue_data);


                pclose(popen('/usr/local/bin/php -q '.env('WEB_LINK').'cmd.php &', 'r'));

                return json_encode(['an'=>true,"message"=>"已寄送新密碼到連絡信箱，請查收！"]);

                }
            }
        }


    }
    private function getRandPassword($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
}
