<?php

namespace App\Http\Controllers;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Distributor;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class DistributorController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/服務據點/ajax-list/';
    public $routePreFix = '服務據點';
    public $selectField = ['id', 'rank', 'is_visible', 'title', 'address', 'email', 'phone', 'image', 'fax', 'link'];
    protected $modelName = "Distributor";
    protected $viewPreFix = 'Distributor';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $distributor = Distributor::select('id', 'rank', 'is_visible', 'title', 'address', 'email', 'phone', 'image', 'fax', 'link')
            ->where('is_visible', 1)->get()->toArray();

        foreach ($distributor as $key => $value)
        {
            $firstAlphabet = ord(substr($value['title'],0,1));
            if($firstAlphabet < 73 || ($firstAlphabet >= 97 AND $firstAlphabet<105) )
            {
               $data1[$value['title']][$key]['title'] = $value['title'];
               $data1[$value['title']][$key]['address'] = $value['address'];
               $data1[$value['title']][$key]['phone'] = $value['phone'];
               $data1[$value['title']][$key]['link'] = $value['link'];
               $data1[$value['title']][$key]['fax'] = $value['fax'];
               $data1[$value['title']][$key]['image'] = $value['image'];
               $data1[$value['title']][$key]['email'] = $value['email'];
            }

            if( ($firstAlphabet >= 73 and $firstAlphabet < 82 ) || ($firstAlphabet >= 105 AND $firstAlphabet < 114) )
            {
                $data2[$value['title']][$key]['title'] = $value['title'];
                $data2[$value['title']][$key]['address'] = $value['address'];
                $data2[$value['title']][$key]['phone'] = $value['phone'];
                $data2[$value['title']][$key]['link'] = $value['link'];
                $data2[$value['title']][$key]['fax'] = $value['fax'];
                $data2[$value['title']][$key]['image'] = $value['image'];
                $data2[$value['title']][$key]['email'] = $value['email'];
            }

            if( ($firstAlphabet >= 82  and $firstAlphabet < 91) || ($firstAlphabet >= 114 AND $firstAlphabet < 123) )
            {
                $data3[$value['title']][$key]['title'] = $value['title'];
                $data3[$value['title']][$key]['address'] = $value['address'];
                $data3[$value['title']][$key]['phone'] = $value['phone'];
                $data3[$value['title']][$key]['link'] = $value['link'];
                $data3[$value['title']][$key]['fax'] = $value['fax'];
                $data3[$value['title']][$key]['image'] = $value['image'];
                $data3[$value['title']][$key]['email'] = $value['email'];
            }
        }


        return view($locale . '.distributor.distributors', [
            "data1" => isset($data1) ? $data1 : '',
            "data2" => isset($data2) ? $data2 : '',
            "data3" => isset($data3) ? $data3 : '',
        ]);
    }
}