<?php
namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;
use Validator;
use DateTime;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
/**Models**/

use App\Http\Models\Product\Theme;
use App\Http\Models\Product\Category;
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Spec;
use App\Http\Models\Product\Photo;
use App\Http\Models\Setting;

// use App\Http\Models\Member\OrderRecord;
// use App\Http\Models\Member\OrderRecordList;
use App\Http\Models\Member\InquiryRecord;
use App\Http\Models\Member\InquiryRecordList;

class InquiryController extends CRUDBaseController
{

	public $ajaxEditLink = 'backen-admin/產品/主類別/ajax-list/';

	protected $modelName = "ProductTheme";

	public $index_select_field = ['id','rank','is_visible','is_show_home','title',"en_title"];

	//public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

	public $routePreFix = '產品/主類別';

	public $viewPreFix = 'Product.Theme';

	public $ajaxEditList = Array(
		/******
			設定規則
			"資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
			static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
		*****/
		"排序" => Array(
			"field" => "rank",
			"inputType" => "text",
			"is_edit"=> true
		),
		"標題" => Array(
			"field" => "title",
			"inputType" => "text",
			"is_edit"=> true
		),
		"顯示狀態" => Array(
			"是否顯示" => Array(
				"field" => "is_visible",
				"inputType" => "radio",
				"showColor" => 'label-danger',
				"showText" => 'S'
			)
		),
	);

	protected $cacheData = [
		"active"=> true,//是否開啟功能
		"select" =>['id','title'], //如果是空的就預設抓所有欄位
		"order" => "rank",
		"sort" => 'asc',
		'has'=>['Category','Item']
	]; // 設定黨

	/*===============後台結束=======================*/
	public function index($locale, Request $request)
	{
		$item = $request->input('item');
		$type = $request->input('type');

		switch ($type) {
			case 'add':
				$aData = self::addInquiry($request->input('item'));
				break;
			case 'del':
				$aData = self::delInquiry($request->input('item'));
				break;
		}
	$data = Session::get('Inquiry');
		// print_r($data);
		// die;
		return View::make($locale.".product.inquiry",[
			'product_Inquiry' => $data,
			]);
	}

	public function delInquiry($itemId = null)
	{
		// 判斷有沒有資料
		if( empty($itemId) ) {

		} else {
			
			// 判斷 Session 有無舊資料
			if( !empty(Session::get('Inquiry')) AND isset(Session::get('Inquiry')['list']) ) {
				$data = Session::get('Inquiry');
				
				unset($data['list'][ $itemId ]);

			} else {
				$data = [
					'list' => [
						
					]
				];
				// echo '<br> 無舊資料 <br>';
			}
		}
		Session::put( 'Inquiry' , $data );
		return $data;
	}
	public function addInquiry($itemId = null)
	{
		// $itemId = $request->input('item');
		$item = Item::select('id','title','image')
					->where('id', $itemId)
					->first();

		// 判斷有沒有資料
		if( empty($itemId) ) {

		} else {
			
			// 判斷 Session 有無舊資料
			if( !empty(Session::get('Inquiry')) AND isset(Session::get('Inquiry')['list']) ) {
				$data = Session::get('Inquiry');
				
				// DB 有資料才記錄
				if ($item ) {
					$item = $item->toArray();
					$data['list'][ $itemId ] = $item;
					$data['list'][ $itemId ]['quantity'] = 1;
				}

			} else {
				// DB 有資料才記錄
				if ($item ) {
					$item = $item->toArray();
					$data = [
						'list' => [
							$itemId => $item,
						]
					];
				} else {
					$data = [
						'list' => [
							
						]
					];
				}
			}
		}
		Session::put( 'Inquiry' , $data );
		return $data;
	}

	public function InquirySave(Request $request)
	{
        $rules = [
            'captcha' => 'required|captcha'
        ];

        // Email 檢查
        $bMail = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL);
        if(!$bMail) {
        	return json_encode(['an'=>false, 'message'=>"Email 格式錯誤"]);
        }
		/*
        $validator = Validator::make($request->all(), $rules);
        // 驗證碼檢查
        if( $validator->fails() ){
            return json_encode(['an'=>false, 'message'=>"驗證碼錯誤"]);
        }
        */

		if( !empty( Session::get('Spec') ) AND !empty( Session::get('Spec')['list'] ) ){
			$contactName = $request->input('name');  
            $contactMemo = $request->input('memo'); 
            $contactEmail = $request->input('email');  
            $contactTel = $request->input('tel');  
            $contactAddress = $request->input('address');  

            $order = new InquiryRecord;
            $order->name = $contactName;
            $order->memo = $contactMemo;
            if(!empty($contactEmail))
            $order->email = $contactEmail;
            if(!empty($contactTel))
            $order->contact_tel = $contactTel;
            if(!empty($contactAddress))
            $order->address = $contactAddress;

            if( $order->save() ){
            	$order->order_no = (new DateTime())->format('Ymd').$order->id;
                $order->save();
                if( !empty( Session::get('Spec')['list'] ) ){

                    foreach (Session::get('Spec')['list'] as $key => $row) {
                        $specId = $row['product_no'];
                    	$List = new InquiryRecordList;
                        $List->spec_id = $specId;
                        $List->order_id = $order->id;
                        $List->item_id = $specId;
                        $List->title = $row['title'];
                        $List->qty = $row['quantity'];
                        $List->product_no = $row['product_no'];
                        $List->image = $row['image'];
                        $List->save();
                    }
                }


		            $mail_data = "<table>";
		            $mail_data .= "<tr><th colspan='2' style=text-align:center>基本資料</td></tr>"; 
		            if(!empty($request->input('name')))
		            $mail_data .= "<tr><td>姓名</td><td>" . $request->input('name') . "</td></tr>"; 
		        	if(!empty($request->input('email')))
		            $mail_data .= "<tr><td>信箱</td><td>" . $request->input('email') . "</td></tr>"; 
		        	if(!empty($request->input('tel')))
		            $mail_data .= "<tr><td>聯絡電話</td><td>" . $request->input('tel') . "</td></tr>"; 
		        	if(!empty($request->input('address')))
		            $mail_data .= "<tr><td>地址</td><td>" . $request->input('address') . "</td></tr>"; 
		        	if(!empty($request->input('memo')))
		            $mail_data .= "<tr><td>聯絡內容</td><td>" . $request->input('memo') . "</td></tr>"; 

		            $mail_data .= "<tr><th colspan='3' style=text-align:center>詢問產品</td></tr>"; 

		            $mail_data .= "</table>";
		            $mail_data .= "<table>";
		            $mail_data .= "<tr><th>圖片</th><th>產品名稱</th><th>產品數量</th></tr>"; 
                    
                if( !empty( Session::get('Spec')['list'] ) ){

                    foreach (Session::get('Spec')['list'] as $key => $row) {
		            $mail_data .= "<tr><td><img style=max-width:100px;max-height:100px src=".env('WEB_URL').'/'.$row['image']."></td><td>" . $row['title'] ."</td><td>" . $row['quantity'] ."</td></tr>"; 
                    }
                }


		            $mail_data .= "</table>";

		            $mail_data .= "<br><br>";
		            $mail_data .= "<a style=text-decoration:none; href=".env('WEB_URL').">".env('WEB_NAME')."</a><br>";

				$data = array();
				$data["data"] = $mail_data ;


                $setting = Setting::find(1);
                $servermail = explode(",",$setting->mails);

                $array[0] = $request->input('email') ;
                $array[1] = $servermail ;


                $queue_data = [
                    'mail_tamplate' => 'emails.'. $request->locale .'.inquiry',
                    'email' => $array[0],
                    'name'  => $setting->mails,
                    'subject'  => '【'.env('WEB_NAME').'】詢問車',
                    'servermail' => $setting->mails,
                    'maildata' => $data
                ];

                // 建立隊列
                $queue_id = \Queue::push('App\Commands\SendEmail@fire', $queue_data);


                pclose(popen('/usr/local/bin/php -q '.env('WEB_LINK').'cmd.php &', 'r'));

                Session::forget('Spec');

                return json_encode(['an'=>true, 'message'=>"詢問表單已送出"]);
            }

		} else {
			return json_encode(['an'=>false, 'message'=>"請選擇詢問產品"]);
		}
	}

}
