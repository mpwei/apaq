<?php

namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use DateTime;
/**Models**/

use App\Http\Models\Product\Theme as Theme;

class CategoryController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/產品/次類別/ajax-list/';

    protected $modelName = "ProductCategory";

    public $index_select_field = ['id','rank','is_visible','title','theme_id'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '產品/次類別';

    public $viewPreFix = 'Product.Category';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    protected $modelBelongs = [
        "ProductTheme" => [
            "parent" => "theme_id",
            "filed" => 'id',
            "select" => ['title','is_visible','id','rank']
        ],
    ];

    protected $cacheData = [
        "active"=> true,//是否開啟功能
        "select" =>[], //如果是空的就預設抓所有欄位
        "order" => "rank",
        "sort" => 'asc',
        'has'=>['Category','Item']
    ]; // 設定黨
    /*===============後台結束=======================*/
    protected function setCacheData(){

        if( !empty( $this->cacheData ) AND $this->cacheData['active'] == true ){
            $thisModel = new parent::$ModelsArray['ProductTheme'];
            if( !empty($this->cacheData['has']) ){
                foreach ($this->cacheData['has'] as $has) {
                    $thisModel = $thisModel->with( $has );
                }
            }
            $find = $thisModel->where("is_visible", 1 );

            //沒有select就抓所有欄位
            if( !empty( $this->cacheData['select'] ) ){
                $find = $find->select( $this->cacheData['select'] );
            }
            if( !empty( $this->cacheData['order'] ) ){
                $sort = ( !empty( $this->cacheData['sort'] ) )? $this->cacheData['sort'] : '';

                $find = $find->orderBy($this->cacheData['order'], $sort);
            }

            $locale = ( !empty( parent::getRouter()->current()->parameters()['locale'] ) )? parent::getRouter()->current()->parameters()['locale'] : '';

            $find = $find->get();
            if( !empty( $find ) AND count( $find->toArray() ) > 0 ){
                Cache::forever( $locale."_ProductTheme", $find );
            }


        }
    }
    public function getThisData(){

        // $data = Cache::get(App::getLocale()."_".$this->modelName);

        // if( empty( $data ) ){

        // }
        $thisModel = new $this->modelClass;
        $data = $thisModel->with('Theme')
                    ->with("Item")
                    ->where('is_visible',1)
                    ->select('id','title','theme_id')
                    ->get();

        return $data;
    }
}
