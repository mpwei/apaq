<?php

namespace App\Http\Controllers\Product;

/**原生函式**/
use App;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\Backend\PublicController as PublicClass;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Models\Product\Category;

/**相關Controller**/
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Theme;
use Cache;
// use App\Http\Controllers\Product\ItemController;

/**Models**/
/*
use App\Http\Models\Product\Theme;
use App\Http\Models\Product\Category;
 */

use Illuminate\Http\Request;
use View;

// use App\Http\Models\Product\Spec;

class ItemController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/產品/Items/ajax-list/';

    protected $modelName = "ProductItem";

    public $index_select_field = ['id', 'rank', 'is_visible', 'is_new', 'title', 'theme_id', 'category_id'];

    public $limit_page = 9999;

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '產品/Items';

    public $viewPreFix = 'Product.Item';

    public $ajaxEditList = array(
        /******
        設定規則
        "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
        static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
         *****/
        "排序" => array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit" => true,
        ),
        "標題" => array(
            "field" => "title",
            "inputType" => "text",
            "is_edit" => true,
        ),
        "顯示狀態" => array(
            "是否顯示" => array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S',
            ),
        ),
    );

    protected $modelBelongs = [
        "ProductTheme" => [
            "parent" => "theme_id",
            "filed" => 'id',
            "select" => ['title', 'is_visible', 'id', 'rank'],
        ],
        "ProductCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title', 'is_visible', 'id', 'rank', 'theme_id'],
        ],
    ];
    /*
    當需要抓取子關聯資料時，
     */
    protected $modelHas = [
        // "ProductSpec" =>[
        //     "parent" => "item_id",
        //     "filed" => 'id',
        //     "select" => ['title','item_id','is_visible','id','rank']
        // ],
        // "ProductPhoto" =>[
        //     "parent" => "item_id",
        //     "filed" => 'id',
        //     "select" => ['title','image','big_image','id','is_visible','rank','item_id']
        // ]
    ];
    // protected $modelHas2 = [
    //     "ProductPhotos" =>[
    //         "parent" => "item_id",
    //         "filed" => 'id',
    //         "select" => ['title','image','big_image','id','is_visible','rank','item_id']
    //     ]
    // ];
    protected $saveSubData = [
        // [
        //     "modelName" => "ProductSpec",
        //     "to_parent" => "item_id"
        // ],
        // [
        //     "modelName" => "ProductPhoto",
        //     "to_parent" => "item_id"
        // ],
    ];

    // protected $saveSubData2 = [
    //     [
    //         "modelName" => "ProductPhoto",
    //         "to_parent" => "item_id"
    //     ],
    // ];

    public $photoTable = [
        "spec" => [
            "排序" => 'rank',
            "規格標題" => 'title',
            "是否顯示" => 'is_visible',
        ],
        "photo" => [
            "排序" => 'rank',
            "標題" => 'title',
            "圖片" => 'image',
            "放大圖" => 'big_image',
            "是否顯示" => 'is_visible',
        ],
    ];

    // public $photoTable2 = [

    //     "排序"=>'rank',
    //     "標題"=>'title',
    //     "圖片"=>'image',
    //     "放大圖"=> 'big_image',
    //     "是否顯示"=>'is_visible',
    // ];

    protected $cacheData = [
        "active" => true, //是否開啟功能
        "select" => [], //如果是空的就預設抓所有欄位
        "order" => "rank",
        "sort" => 'asc',
        'has' => [/*'Category',*/'Item'],
    ]; // 設定黨

    protected function setCacheData()
    {

        if (!empty($this->cacheData) and $this->cacheData['active'] == true) {
            $thisModel = new parent::$ModelsArray['ProductTheme'];
            if (!empty($this->cacheData['has'])) {
                foreach ($this->cacheData['has'] as $has) {
                    $thisModel = $thisModel->with($has);
                }
            }
            $find = $thisModel->where("is_visible", 1);

            //沒有select就抓所有欄位
            if (!empty($this->cacheData['select'])) {
                $find = $find->select($this->cacheData['select']);
            }
            if (!empty($this->cacheData['order'])) {
                $sort = (!empty($this->cacheData['sort'])) ? $this->cacheData['sort'] : '';

                $find = $find->orderBy($this->cacheData['order'], $sort);
            }

            $locale = (!empty(parent::getRouter()->current()->parameters()['locale'])) ? parent::getRouter()->current()->parameters()['locale'] : '';

            $find = $find->get();
            if (!empty($find) and count($find->toArray()) > 0) {
                Cache::forever($locale . "_ProductTheme", $find);
            }

        }
    }
    protected function setCategoryLevel()
    {
        $Data = [];
        $category = new CategoryController;
        $find = $category->getThisData();
        if (!empty($find)) {
            foreach ($find as $row) {
                $Data[] = [
                    "id" => $row->id,
                    "title" => $row->Theme->title . " - " . $row->title,
                ];
            }
        }
        return $Data;
    }
    public function getCreate()
    {

        $parent = $this->FindBelongAndHas();
        //var_dump( $this->photoTable );die;
        $related = Item::where('is_visible', 1)->orderBy('rank', 'asc')->get()->toArray();

        return view('backend.' . $this->viewPreFix . '.edit',
            [
                'data' => [],
                "parent" => $parent,
                "Tree" => $this->setCategoryLevel(),
                "bulidTable" => $this->photoTable,
                "related" => $related,
                "related_id_array" => '',

                'actionUrl' => MakeItemV2::url('backen-admin/' . $this->routePreFix . '/store'),
            ]
        );
    }
    public function getCopy($locale, $id)
    {
        $parent = $this->FindBelongAndHas($id);

        /*過濾掉子資料的ID*/
        if (!empty($parent['has'])) {
            foreach ($parent['has'] as $chlidName => $childArray) {
                foreach ($childArray as $key => $value) {
                    $parent['has'][$chlidName][$key]['id'] = '';
                }
            }
        }

        $thisModel = $this->modelClass;
        $data = $thisModel::find($id);
        $data->id = '';

        $related = Item::where('is_visible', 1)->orderBy('rank', 'asc')->get()->toArray();

        $related_id_array = explode(',', $data['related_id']);

        return view('backend.' . $this->viewPreFix . '.edit', [
            // 'data' => $this->modelClass::find($id),
            'data' => $data,
            "related" => $related,
            "related_id_array" => $related_id_array,
            "parent" => $parent,
            "Tree" => $this->setCategoryLevel(),
            'actionUrl' => MakeItemV2::url('backen-admin/' . $this->routePreFix . '/store'),
        ]);
    }
    public function getEdit($locale, $id)
    {
        $parent = $this->FindBelongAndHas($id);
        $thisModel = $this->modelClass;
        //var_dump( $id );die;
        $data = $thisModel::find($id);
        $related = Item::where('is_visible', 1)->where('id', '<>', $id)->orderBy('rank', 'asc')->get()->toArray();
        $related_id_array = explode(',', $data['related_id']);
        return view('backend.' . $this->viewPreFix . '.edit', [
            'data' => $thisModel::find($id),
            "parent" => $parent,
            "related" => $related,
            "related_id_array" => $related_id_array,
            "Tree" => $this->setCategoryLevel(),
            "bulidTable" => $this->photoTable,
            'actionUrl' => MakeItemV2::url('backen-admin/' . $this->routePreFix . '/update'),
        ]);
    }

    public function postUpdate(Request $request)
    {

        $Datas = $request->input($this->modelName);

        //var_dump( $Datas );

        if (!empty($request->input('method')) and $request->input('method') == 'ajaxEdit') {
            parent::updateOne($Datas, $this->modelName, 'ajaxEdit');
            //將資料做暫存
            $this->setCacheData();
        } else {
            if (parent::updateOne($Datas, $this->modelName, '')) {

                $this->checkSavedSubData($request->all(), $Datas['id']);

                //通知信
                //$this->sendNoticeMail($Datas['id'], 'reply');

                //將資料做暫存
                $this->setCacheData();

                return redirect(MakeItemV2::url('backen-admin/' . $this->routePreFix . '/edit/' . $Datas['id']))->with('Message', '修改成功');
            }
        }

    }
    public function postStore(Request $request)
    {
        $Datas = $request->input($this->modelName);

        $resoult = json_decode(parent::addNew([
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "routeFix" => '產品/Items',
        ]), true);

        if (!empty($resoult['redirect'])) {
            return redirect($resoult['redirect'])->with('Message', '新增成功, 轉到編輯頁面');

        }
    }
    /*===============後台結束=======================*/

    public function index(Request $request, $locale, $theme = "", $category = "")
    {
        $theme = Theme::with('Category')->where("is_visible", 1)
            ->select('id', 'title', 'image')
            ->orderBy('rank', 'asc')
            ->get()
            ->toArray();
        //產品連結
        foreach ($theme as $key => $row) {
            foreach ($row['category'] as $key2 => $category) {
                foreach ($category['item'] as $key3 => $item) {
                    $theme[$key]['category'][$key2]['item'][$key3]['web_link'] = MakeItemV2::url(
                        "product/" . parent::processTitleToUrl($row['title'])
                        . "/" . parent::processTitleToUrl($category['title'])
                        . "/" . parent::processTitleToUrl($item['title'])
                    );
                }
            }
        }
        return View::make($locale . ".product.index", [
            "Theme" => $theme,
        ]);
    }
    public function list2($locale)
    {
        return View::make($locale . ".aside_doc.01");
    }
    public function ppt($locale)
    {

        return View::make($locale . ".aside_doc.02");
    }
    public function sizeCode($locale)
    {
        return View::make($locale . ".aside_doc.03");
    }
    public function detail($locale, $theme, $category, $item)
    {
        $thisLink = MakeItemV2::url('product/' . $theme . '/' . $category . '/' . $item);
        $themeTitle = parent::revertUrlToTitle($theme);
        $categoryTitle = parent::revertUrlToTitle($category);
        $itemTitle = parent::revertUrlToTitle($item);

        $theme = Theme::where("title", $themeTitle)->select('id', 'title')->first();
        $category = Category::where("theme_id", $theme->id)
            ->with('Item')
            ->with('Theme')
            ->where('title', $categoryTitle)
            ->select('id', 'theme_id', 'title')
            ->first();
        $item = Item::where("theme_id", $theme->id)
            ->where('category_id', $category->id)
            ->where('title', $itemTitle)
            ->first();
        foreach ($category->Item as $key => $row) {
            $category->Item[$key]->web_link = MakeItemV2::url(
                "product/" . parent::processTitleToUrl($category->Theme->title)
                . "/" . parent::processTitleToUrl($category->title)
                . "/" . parent::processTitleToUrl($row->title)
            );
        }
        return View::make($locale . ".product.detail", [
            "Theme" => $theme,
            "Category" => $category,
            "Item" => $item,
            "ThisLink" => $thisLink,
        ]);
    }
    
    public function detail2($locale, $item)
    {
        $itemTitle = parent::revertUrlToTitle($item);
        $item= Item::where('title', $itemTitle)->with('Category')->with('Theme')->with('Category.Item')->with('Category.Item.Theme')->with('Category.Item.Category')
        ->first();
        
        foreach ($item->Category->Item as $key => $row) {
            $item->Category->Item[$key]->web_link = MakeItemV2::url(
                "product/" . parent::processTitleToUrl($item->Category->Item[$key]->Category->Theme->title)
                . "/" . parent::processTitleToUrl($item->Category->Item[$key]->Category->title)
                . "/" . parent::processTitleToUrl($row->title)
            );
        }
        $thisLink = MakeItemV2::url('product/' . parent::processTitleToUrl($item->Theme->title) . '/' . parent::processTitleToUrl($item->Category->title) . '/' . $item);
        return View::make($locale . ".product.detail", [
            "Theme" => $item->Theme,
            "Category" => $item->Category,
            "Item" => $item,
            "ThisLink" => $thisLink,
        ]);
    }

    public function download($locale, $id)
    {
        $find = Item::where('id', $id)->first();

        if (!empty($find) and !empty($find->file)) {
            $file = public_path() . $find->file;
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        }
    }

    public function download2($locale, $id, $type)
    {
        $find = Item::select('file', 'packing')->where('id', $id)->first()->toArray();

        if (!empty($find) and !empty($find[$type])) {
            $file = public_path() . $find[$type];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        }
    }

    public function search(Request $request, $locale, $key = "")
    {
        $modeName = "ProductIndex";

        $keywords = $request->input('key');

        $data = Item::with('Theme')->
            where('is_visible', 1)->where('title', 'like', '%' . $keywords . '%')->orderBy('rank', 'asc')->
            get()->
            toArray();

        $menu = Theme::with('Category')->
            where('is_visible', 1)->orderBy('rank', 'asc')->
            get()->
            toArray();

        return View::make($locale . ".product.search", [
            "data" => $data,
            "menu" => $menu,
            "key" => $keywords,
            "pageData" => $this->generatePage_search($request->input('page'), $keywords),
        ]);
    }

    public function category(Request $request, $locale, $theme = "")
    {
        //$theme = str_replace("-","/",$theme);
        $category_setting = env('WEB_ProductCategory');
        $theme = parent::revertUrlToTitle($theme);
        $theme = str_replace("___", "/", $theme);
        $menu = Theme::with('Category')->
            where('is_visible', 1)->orderBy('rank', 'asc')->
            get()->
            toArray();
        if (empty($theme)) {
            $theme = Theme::where('is_visible', 1)->orderBy('rank', 'asc')->first()->title;
        }

        $sub_title = Theme::where('is_visible', 1)->orderBy('rank', 'asc')->where('title', $theme)->first();

        return View::make($locale . ".product.index", [
            "theme" => $theme,
            "menu" => $menu,
            "sub_title" => $sub_title->sub_title,
            "title" => $sub_title->title,
            "pageData" => $category_setting == 2 ? $this->generatePage_theme($request->input('page'), $theme, "") : $this->generatePage($request->input('page'), $theme, ''),
        ]);
    }

    public function generatePage_theme($page = 1, $theme, $category)
    {
        if ($page < 1 || $page == null) {
            $page = 1;
        }

        $pageData = array();
        $pageData['range'] = $this->limit_page;
        $pageData['num_pages'] = $page;

        $public = new PublicClass;
        $newsCategory = $public->getThisCategory($this->modelName);

        if ($category != '') {
            $theme_id = Category::where('is_visible', 1)->where('title', $category)->first()->id;
            $pageData['total_pages'] = (int) ceil(Item::where('is_visible', 1)->where('category_id', $category_id)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        } elseif ($theme != '') {
            $theme_id = Theme::where('is_visible', 1)->where('title', $theme)->first()->id;
            $pageData['total_pages'] = (int) ceil(Category::where('is_visible', 1)->where('theme_id', $theme_id)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        } else {
            $pageData['total_pages'] = (int) ceil(Theme::where('is_visible', 1)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        }

        $modeName = "ProductCategory";
        if ($theme) {
            $theme_id = Theme::select('id', 'title', 'image')
                ->where('title', $theme)
                ->first();
            $data = Category::with('Theme')->
                where('is_visible', 1)->orderBy('rank', 'asc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->
                get();
            foreach ($data as $key => $value) {
                $data[$key]['web_link'] = MakeItemV2::url("product/" . parent::processTitleToUrl($data[$key]["theme"]['title']) . "/" . parent::processTitleToUrl($value['title']));
            }
        } else {
            $data = Category::select('id', 'theme_id', 'title', 'image')->with('Theme')->
                where('is_visible', 1)->orderBy('rank', 'asc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->
                get();
            foreach ($data as $key => $value) {
                $data[$key]['web_link'] = MakeItemV2::url("product/" . parent::processTitleToUrl($value['title']));
            }
        }

        $url = $category ? 'product/' . $theme . '/' . $category : 'product/' . $theme;
        $pageData['url'] = MakeItemV2::url($url);

        $pageData['data'] = $data;

        return $pageData;
    }

    public function generatePage($page = 1, $theme, $category)
    {
        if ($page < 1 || $page == null) {
            $page = 1;
        }

        $pageData = array();
        $pageData['range'] = $this->limit_page;
        $pageData['num_pages'] = $page;

        $public = new PublicClass;
        $newsCategory = $public->getThisCategory($this->modelName);

        if ($category != '') {
            $pageData['total_pages'] = (int) ceil($newsCategory->where('title', $category)
                    ->first()
                    ->hasMany('App\Http\Models\Product\Item', 'category_id', 'id')
                    ->where('is_visible', 1)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        } else {
            $pageData['total_pages'] = (int) ceil(Item::where('is_visible', 1)
                    ->orderBy('rank', 'asc')
                    ->count() / $this->limit_page);
        }
        $url = $category ? 'product/' . $theme . '/' . $category : 'product/' . $theme;
        $pageData['url'] = MakeItemV2::url($url);

        $theme = parent::revertUrlToTitle($theme);
        $modeName = "ProductCategory";

        if ($category) {
            $category_id = Category::select('id')
                ->where('title', $category)
                ->first();

            $data = Item::with('Category')->
                where('is_visible', 1)->orderBy('rank', 'asc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->
                get()->
                toArray();
        } else {
            $data = Item::with('Theme')->
                where('is_visible', 1)->orderBy('rank', 'asc')->
                skip(($page - 1) * $this->limit_page)->
                take($this->limit_page)->
                get()->
                toArray();
        }
        foreach ($data as $key => $value) {
            $data[$key]['web_link'] = MakeItemV2::url("productdetail/" . parent::processTitleToUrl($value['title']));
        }

        $pageData['data'] = $data;
        return $pageData;
    }

    public function generatePage_search($page = 1, $keywords)
    {
        if ($page < 1 || $page == null) {
            $page = 1;
        }

        $pageData = array();
        $pageData['range'] = $this->limit_page;
        $pageData['num_pages'] = $page;

        $public = new PublicClass;
        $newsCategory = $public->getThisCategory($this->modelName);

        $pageData['total_pages'] = (int) ceil(Item::where('is_visible', 1)->where('title', 'like', '%' . $keywords . '%')
                ->orderBy('rank', 'asc')
                ->count() / $this->limit_page);

        $url = 'search?key=' . $keywords;
        $pageData['url'] = MakeItemV2::url($url);

        $modeName = "ProductCategory";

        $data = Item::with('theme')->with('category')->
            where('is_visible', 1)
            ->where(function ($data) use ($keywords) {
                $data->orWhere('title', 'like', '%' . $keywords . '%')
                    ->orWhere('content', 'like', '%' . $keywords . '%')
                    ->orWhere('desc', 'like', '%' . $keywords . '%')
                    ->orWhere('desc2', 'like', '%' . $keywords . '%')
                    ->orWhere('detail_sub_title', 'like', '%' . $keywords . '%');
            })
            ->orderBy('rank', 'asc')->
            skip(($page - 1) * $this->limit_page)->
            take($this->limit_page)->
            get()->
            toArray();

        foreach ($data as $key => $value) {
            $data[$key]['web_link'] = MakeItemV2::url(
                "product/" . parent::processTitleToUrl($value['theme']['title']) .
                '/' . parent::processTitleToUrl($value['category']['title']) .
                '/' . parent::processTitleToUrl($value['title'])
            );
        }

        $pageData['data'] = $data;
        return $pageData;
    }
}
