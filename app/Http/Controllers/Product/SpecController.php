<?php

namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\Product\StockController;
/**Models**/
use App\Http\Models\Product\Theme;
use App\Http\Models\Product\Type;
use App\Http\Models\Product\Category;
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Spec;
use App\Http\Models\Member\OrderRecord;
use App\Http\Models\Setting;

class SpecController extends CRUDBaseController
{

	public $ajaxEditLink = 'backen-admin/產品/風格/ajax-list/';

	protected $modelName = "ProductSpec";

	public $index_select_field = ['id','rank','is_visible','title','item_id','show_size','show_color'];

	//public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

	public $routePreFix = '產品/產品規格';

	public $viewPreFix = 'Product.Spec';

	public $ajaxEditList = Array(
		/******
			設定規則
			"資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
			static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
		*****/
		"排序" => Array(
			"field" => "rank",
			"inputType" => "text",
			"is_edit"=> true
		),
		"標題" => Array(
			"field" => "title",
			"inputType" => "text",
			"is_edit"=> true
		),
		"顯示狀態" => Array(
			"是否顯示" => Array(
				"field" => "is_visible",
				"inputType" => "radio",
				"showColor" => 'label-danger',
				"showText" => 'S'
			)
		),
	 );

	// public $photoTable = [
	// 	"photo"=>[
	// 		"排序"=>'rank',
	// 		"標題"=>'title',
	// 		"圖片(寬980px)"=>'image',
	// 		"放大圖(1920 x 2400)"=> 'big_image',
	// 		"是否顯示"=>'is_visible',
	// 	],
	// 	"format"=>[
	// 		"排序"=>'rank',
	// 		"是否顯示"=>'is_visible',
	// 		"標題"=>'title',
	// 		"標題-英文版"=>'en_title',
			
	// 	],
	// 	"size"=>[
	// 		"排序"=>'rank',
	// 		"是否顯示"=>'is_visible',
	// 		"Model No."=>'model_no',
	// 		"尺寸"=>'size_type',
	// 		"庫存"=>'stock',
			
	// 	]
	// ];
	protected $modelBelongs = [
		"ProductItem" => [
			"parent" => "item_id",
			"filed" => 'id',
			"select" => ['title','id']
		],
	];
	// protected $modelHas = [
	// 	"ProductSpecPhoto" => [
	// 		"parent" => "spec_id",
	// 		"filed" => 'id',
	// 		"select" => ['title','image','big_image','id','is_visible','rank','spec_id']
	// 	],
	// 	"ProductFormat" => [
	// 		"parent" => "spec_id",
	// 		"filed" => 'id',
	// 		"select" => ['title','id','is_visible','rank','spec_id','en_title']
	// 	],
	// 	"ProductSpecSize" => [
	// 		"parent" => "spec_id",
	// 		"filed" => 'id',
	// 		"select" => ['model_no','id','is_visible','rank','spec_id','size_type','stock']
	// 	],
	// ];

	// protected $saveSubData = [
	// 	[
	// 		"modelName" => "ProductSpecPhoto",
	// 		"to_parent" => "spec_id"
	// 	],
	// 	[
	// 		"modelName" => "ProductFormat",
	// 		"to_parent" => "spec_id"
	// 	],
	// 	[
	// 		"modelName" => "ProductSpecSize",
	// 		"to_parent" => "spec_id"
	// 	],
	// ];
	public function postUpdate(Request $request)
	{
		if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
		{
			parent::updateOne( $request->input($this->modelName), $this->modelName, 'ajaxEdit');
			//將資料做暫存
			$this->setCacheData();
		}
		else
		{
			$Datas = $request->input($this->modelName);
			//$Datas['related_product'] = ( !empty( $Datas['related_product'] ) )? $Datas['related_product'] : '';
			///var_dump( $Datas );die;

			if( parent::updateOne( $Datas, $this->modelName, '') )
			{

				$this->checkSavedSubData($request->all(), $Datas['id']);

				//通知信
				$this->sendNoticeMail($Datas['id'], 'reply');

				//將資料做暫存
				$this->setCacheData();

				return redirect( MakeItemV2::url('backen-admin/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
			}

		}
	}
	/*===============後台結束=======================*/



	/*===============前台開始=======================*/
	/*產品分層

		主題Theme
			類別Category
				類型Type
					項目Item
						規格Spec v
	*/

	/*在[規格]，001包的A規格, B規格, C規格 那一頁*/
	public function index($locale, $themeUrl, $categoryUrl, $itemUrl, $specUrl='')
	{
		// Session::forget('Spec');
		// var_dump(Session::get('Spec')); die;
		/*萬一有例外狀況，就導回類型列表的頁面*/
		$goBackToTypeList = MakeItemV2::url('product/'.$themeUrl.'/'.$categoryUrl);
		//var_dump( Session::get("Reservation") );

		/*麵包屑需要*/
		$guide = [
			'themeTitle' => parent::revertUrlToTitle($themeUrl),
			'categoryTitle' => parent::revertUrlToTitle($categoryUrl),
			'themeLink' => MakeItemV2::url('product/'.$themeUrl),
			'categoryLink' => MakeItemV2::url('product/'.$themeUrl.'/'.$categoryUrl),
			//'typeTitle' => parent::revertUrlToTitle($typeUrl),
			//'typeLink' => MakeItemV2::url('product/'.$themeUrl.'/'.$categoryUrl.'/'.$typeUrl),
			'itemTitle' => parent::revertUrlToTitle($itemUrl),
			'itemLink' => MakeItemV2::url('product/'.$themeUrl.'/'.$categoryUrl.'/'.$itemUrl),
			'specTitle' => parent::revertUrlToTitle($specUrl),
			'specLink' => MakeItemV2::url('product/'.$themeUrl.'/'.$categoryUrl.'/'.$itemUrl.'/'.$specUrl)
		];

		$themeFind = ( $locale == "en" )? "en_title" : 'title';
		$thisTheme = Theme::where($themeFind, $guide['themeTitle'])->select('id','title','en_title')->first();

		//檢查Session中是否有存這一個主類別的次分類，順便比對是不是同一個
		$checkSessionCategory = ( !empty( Session::get('AllCategory') ) )? Session::get('AllCategory')->first() : [];
		$AllCategory = [];
		// if( !empty( $checkSessionCategory ) AND ( $checkSessionCategory->title == $guide['categoryTitle'] AND $checkSessionCategory->Theme->title == $guide['themeTitle'] ) ){

		// }else{
		$AllCategory = Category::with('Theme')
						->where('theme_id', $thisTheme->id)
						->select('id','rank','theme_id','is_visible','title',"en_title",'size_role')
						->orderBy('rank','asc')
						->get();
		Session::put('AllCategory', $AllCategory);
		foreach ($AllCategory as $key => $row) {

			$thisCategoryLink = 'product/'.$themeUrl.'/'.parent::processTitleToUrl( $row->$themeFind );
			$AllCategory[$key]->link = MakeItemV2::url($thisCategoryLink);
			
			if( $row->title == $guide['categoryTitle'] ){
				Session::put("CategoryID", $row->id );
			}
		}
		//}


		/*先抓出這個項目(parent)的ID*/
		$thisItem = Item::select('id','rank','is_visible','title',"en_title")
						->where($themeFind, $guide['itemTitle'])
						->orderBy('rank','asc')
						->first();



		/*稍微檢查一下，網址填入的這個[項目itemUrl]連結，是否真的存在*/
		if( $thisItem === null )
		{
			return redirect( $goBackToTypeList );
		}
		else
		{
			$itemId = $thisItem->id;
		}

		/*===========================================  抓規格資料  =============================================*/
		$Spec = $this->modelClass;


		/*網址如果是 shiatzychen.com/zh-tw/product/主題/類別/類型/項目/規格 (完整的話) */
		if( !empty($specUrl) )
		{

			/*========================== 這一項規格的資料 =================================================*/
			$spec = $Spec::select('id','rank','is_visible','item_id','color_id','skin_id','format_id','product_no','title',"en_title",'show_color','skin_image','price','dis_price','content','en_content','related_product')
								->where('is_visible',1)
								->where('item_id', $itemId)
								->where($themeFind, $guide['specTitle'])
								->with('Photo')
								->with('Size')
								->with('Format')
								->with('Item')
								->orderBy('rank','asc')
								->first();



			$spec = !empty($spec) ? $spec->toArray() : [] ;


			$thisSpecTitle = ( $locale == "en" AND !empty( $spec['en_title'] )  )? $spec['en_title'] : $spec['title'];
			$spec['link'] = MakeItemV2::url( 'product/'.$themeUrl.'/'.$categoryUrl."/".$itemUrl.'/'.parent::processTitleToUrl( $thisSpecTitle ) );



			/*========================== 這一項規格的相關產品[項目] =================================================*/
			$relatedIds = json_decode($spec['related_product']);
			$relatedItems = Item::select('id','rank','is_visible','title',"en_title",'price','image','image2','category_id')
								->with('Spec')
								->where('is_visible',1)
								->whereIn('id', $relatedIds)
								->orderBy('rank','asc')
								->get();

			$relatedItems = !empty($relatedItems) ? $relatedItems->toArray() : [] ;

			foreach ($relatedItems as $key => $row) {
				$re_cate = json_decode($row['category_id']);

				if( !empty( $re_cate ) ){
					$re_cate_title = Category::where('is_visible',1)->where('id',$re_cate[0])->select("en_title",'title','theme_id')->first();
					$thisModel = new $this->modelClass;

					$re_spec = $thisModel->where('item_id', $row['id'])->orderBy('rank','asc')->first();
					 //var_dump( $re_spec );
					if( !empty( $re_cate_title ) AND !empty( $re_spec ) ){
						$themeTitle = ( $locale == "en" AND !empty( $thisTheme->en_title )  )?$thisTheme->en_title : $thisTheme->title;
						$cateTitle = ( $locale == "en" AND !empty( $re_cate_title->en_title )  )?$re_cate_title->en_title : $re_cate_title->title;
						$itemTitle = ( $locale == "en" AND !empty( $row['en_title'] )  )?$row['en_title'] : $row['title'];
						$specTitle = ( $locale == "en" AND !empty( $re_spec->en_title )  )?$re_spec->en_title : $re_spec->title;


						$thisCategoryLink = 'product/'.parent::processTitleToUrl( $themeTitle ).'/'.parent::processTitleToUrl( $cateTitle )."/".parent::processTitleToUrl( $itemTitle )."/".parent::processTitleToUrl( $specTitle );

						$relatedItems[$key]['link'] = MakeItemV2::url($thisCategoryLink);
					}
				}

				// $thisCategoryLink = 'product/'.$themeUrl.'/'.parent::processTitleToUrl( $row->title );
				// $relatedItems[$key]['link'] = MakeItemV2::url($thisCategoryLink);
				
			}
			if( preg_match("/test-product/", $_SERVER['REQUEST_URI']) ){
				//var_dump( $relatedItems);die;
			}
			// var_dump( $relatedItems);
			// die;

			/*========================== 處理小地方 =================================================*/
					/*稍微檢查一下，網址填入的這個[規格specUrl]連結，是否真的存在*/
					if( empty($spec) ) { return redirect( $goBackToTypeList ); }


					/*規格選單列表*/
					$specs = $Spec::select('id','rank','is_visible','title',"en_title",'item_id','skin_image','show_color')
									->where('item_id', $itemId)
									->where('is_visible',1)
									->orderBy('rank','asc')
									->get()
									->toArray();
					/*將標題存成網址*/
					foreach( $specs as $key => $value )
					{
						$thisSpecTitle = ( $locale == "en" AND !empty( $value['en_title'] )  )?$value['en_title'] : $value['title'];
						$specUrl = 'product/'.$themeUrl.'/'.$categoryUrl.'/'.$itemUrl.'/'.parent::processTitleToUrl( $thisSpecTitle );
						$specs[$key]['link'] = MakeItemV2::url($specUrl);
					}


					$specsCount = count( $specs );


					/*為了配合html，稍微整理一下規格的format陣列（小地方），5個一組*/
					$orginFormats = $spec['format'];
					$formats = [];

					if( !empty($orginFormats) )
					{
						$i = 0;
						foreach( $orginFormats as $key => $value )
						{
							if( isset($formats[$i]) )
							{
								if( count($formats[$i]) === 5 )
								{
									$i+=1;
								}
							}
							$formats[$i][$key] = $value;
							$key++;
						}
					}
			//瀏覽紀錄
			$lookSession = ( !empty( Session::get("View_Product") ) )? Session::get("View_Product") : [];

			$lookSession[ $spec['item']['id']."@".$spec['id'] ] = $spec;

			Session::put('View_Product' , $lookSession);
			// var_dump( $spec );die;

			return View::make( $locale.'.product.spec',
						[
							'spec' => $spec,
							'guide' => $guide,
							'specs' => $specs,
							'specsCount' => $specsCount,
							'formats' => $formats,
							'relatedItems' => $relatedItems,
							"this_theme"=> $thisTheme,
							"AllCategory" => $AllCategory
						]);
		}




		/*網址如果是 shiatzychen.com/zh-tw/product/主題/類別/類型/項目 (不完整的話) */
		else
		{
			/*先看此項目底下是否有規格資料。 有->導進入內頁(第一筆規格) ; 沒有->導回類型列表頁 */

			$specCount = $Spec::where('is_visible',1)
								->where('item_id', $itemId)
								->count();

			if( $specCount === 0 )
			{
				return redirect( $goBackToTypeList );
			}
			else
			{
				$firstSpecTitle = $Spec::select('id','rank','is_visible','title',"en_title")
									->where('is_visible',1)
									->where('item_id', $itemId)
									->orderBy('rank','asc')
									->first()
									->title;

				return redirect( MakeItemV2::url( 'product/'.$themeUrl.'/'.$categoryUrl.'/'.$itemUrl.'/'.parent::processTitleToUrl($firstSpecTitle) ) );
			}
		}


	}


	public function addToShoppingBag ($locale, Request $request) {

		$specno = 0;

		if($specno){
			$spec = Spec::where('item_id', $request->input('item_id'))
						->where('show_size', $request->input('show_size'))
						->where('show_color', $request->input('show_color'))
						->first();
			$specId = $spec -> id;
		}else{
			$specId = $request->input('item_id');

		}
		

		$qty = !empty($request->input('quantity')) && $request->input('quantity') > 0 ? $request->input('quantity') : 1;
		

		$Spec = $this->modelClass;
		

		if($specno){
			$spec = $Spec::with('Item')
					->select('id','title','item_id','show_size','show_color','show_price')
					->where('id', $specId)
					->first();
		}else{
			$spec = Item::select('id','category_id','title','image','price')
					->where('id', $specId)
					->first();
		}
		// print("<pre>");
		// print_r($request -> all());
		// print("</pre>");
		// echo $specId;die;
		$setting = Setting::find(1);

		$setting = json_decode($setting->content, true);

		/*Session::put( 'shipping_cost' , $setting['shipping_cost'] );
		Session::put( 'shipping_cost_free' , $setting['shipping_cost_free'] );
		*/
		/*避免有人從前台故意把資料拿掉，造成錯誤*/
		if( empty($specId) )
		{
			$data = [
				'addToList' => false,
				'message' => ( $request->locale == 'en' )? "Spec empty.":"請選擇規格"
				];
			return $data;
		}
		/*資料都沒問題的話，才繼續做下面的*/
		else
		{
		if($specno){
			$spec->Category = Category::with("Theme")->where('id', json_decode($spec->item->category_id, true)[0])->first();
		}else{
			$spec->Category = Category::with("Theme")->where('id', json_decode($spec->category_id, true)[0])->first();
		}

			$find_this_size = 9999;

			//檢查是否還有庫存
			if( $find_this_size > 0 ){

				$spec->item = (array)$spec->item;
				$spec = $spec->toArray();
				

				$spec['item_link'] = "product";
				$spec['item_link'] .= "/".parent::processTitleToUrl( $spec['Category']['theme']['title'] );
				$spec['item_link'] .= ( !empty( $spec['Category'] ) )? "/".parent::processTitleToUrl( $spec['Category']['title'] ) : '';

				if($specno){
					$spec['item_link'] .= ( !empty( $spec['item']['title'] ) )? "/".parent::processTitleToUrl( $spec['item']['title'] ) : '';
				}else{
					$spec['item_link'] .= ( !empty( $spec['title'] ) )? "/".parent::processTitleToUrl( $spec['title'] ) : '';
				}
				if( !empty(Session::get('Spec')) AND isset(Session::get('Spec')['list']) )
				{
					$data = Session::get('Spec');

					/*用sizeModelNo判斷是不是已經加過第一次了，如果是的話，數量要遞加上去*/
					$added = false;
					$this_pick_key = $specId;
					$this_pick_qty = ( !empty($data['list'][$this_pick_key]) )? $data['list'][$this_pick_key]['quantity'] : 0;

					if( !empty( $data['list'][ $this_pick_key ] ) ){
						if( $this_pick_qty <  $find_this_size  ){
							$data['list'][$this_pick_key]['quantity'] += $qty;
							$added = true;
						}else{
							return [
								'addToList' => false,
								'message' => ( $request->locale == 'en' )? "Can not add more than inventory.":"無法加入超過庫存的數量"
							];
							
						}

					}

					/*如果還沒加入購物清單過，就放到清單新項目裡面*/
					if( $added === false )
					{
						$data['list'][$spec['id']] = [
							'ItemTitle' => $specno ? $spec['item']['title'] : $spec['title'],
							'title' => $specno ? $spec['item']['title'] : $spec['title'],
							'image' => $specno ? $spec['item']['image'] : $spec['image'],
							'price' => $specno ? $spec['item']['price'] : $spec['price'],
							'product_no' => $specno ? $spec['item']['id'] : $spec['id'],
							'specTitle' => $specno ? $spec['title'] : '',
							'spec' => $spec,
							'item_link' => MakeItemV2::url($spec['item_link']),
							'quantity' => $qty
						];
						$data['addToList'] =  true;
					}

				}
				else
				{
					$data = [
						'list' => [
							$spec['id'] => [
								'ItemTitle' => $specno ? $spec['item']['title'] : $spec['title'],
								'title' => $specno ? $spec['item']['title'] : $spec['title'],
								'image' => $specno ? $spec['item']['image'] : $spec['image'],
								'price' => $specno ? $spec['item']['price'] : $spec['price'],
								'product_no' => $specno ? $spec['item']['id'] : $spec['id'],
								'specTitle' => $specno ? $spec['title'] : '',
								'spec' => $spec,
								'item_link' => MakeItemV2::url($spec['item_link']),
								'quantity' => $qty
							]
						],
						'addToList' => true
					];
				}

				/*計算總額*/
				$totalSum = 0;
				foreach( $data['list'] as $row )
				{
					if( !empty( $row['dis_price'] ) ){
						$totalSum+= (int)$row['price'] * $row['quantity'] ;
					}else{
						$totalSum+= (int)$row['price'] * $row['quantity'] ;
					}
					
				}
				$data['totalSum'] = $totalSum;
				$data['amountSum'] = $totalSum;

				// 運費
				/*
				$data['shipping_cost'] = Session::get('shipping_cost');
				$data['shipping_cost_free'] = Session::get('shipping_cost_free');

				if($totalSum < $data['shipping_cost_free'])
					$data['totalSum'] += $data['shipping_cost'];
				else
					$data['shipping_cost'] = 0;
					*/
				Session::put( 'Spec' , $data );

				$total_no = count($data['list']);
				$data['an'] = true;
				$data['total_inquiry'] = $total_no;
				$data['carHtml'] = self::generateCarHtml();
				$data['message'] = '已加入詢問車';
				return $data; 
				
			}else{
				$data = [
					'addToList' => false,
					'message' => ( $request->locale == 'en' )? "This size is no longer in stock":"此尺寸已經沒有庫存"
				];
				return $data;
			}

		}


	}

	public function generateCarHtml() 
	{
		$data = [];
		$html = array();
		$totalSum = 0;
		$qty = 0;
		//$html[] = '<div class="cart-area">';

		if(!empty( Session::get('Spec')['list'] )) {
			foreach( Session::get('Spec')['list'] as $row ) {
				$img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/500?text=noPic";

				$html[] = '<tr>';
                $html[] = '<td class="quantity">'. $row['quantity'] .' x</td>';
                $html[] = '<td class="product"><a href="'.MakeItemV2::url('productdetail/'.$row['title']) .'">'.$row['title'] .'</a></td>';
                $html[] = '</tr>';	
								
				$totalSum += (int)$row['price'] * $row['quantity'] ;
				$qty += $row['quantity'];
			}
		}
					
				/*$data['shipping_cost'] = Session::get('shipping_cost');
				$data['shipping_cost_free'] = Session::get('shipping_cost_free');

				if($totalSum >= $data['shipping_cost_free'])
					$data['shipping_cost'] = '免運費';			
 				else
                     $data['shipping_cost'] = 'NTD ' . $data['shipping_cost'];
		        */
                                  
                                    
                                  
                $html[] = '<tr>';
                $html[] = '<td class="total-quantity" colspan="2">Total '. $qty .' 件</td>';
                $html[] = '</tr>';


		return implode("",$html);
	}
	/*
								<div class="cart-area">
									<a href="{{ ItemMaker::url('shipping') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>{{ count(Session::get('Spec')['list']) }}</span></a>
									<ul>



										@if (!empty( Session::get('Spec')['list'] ))
											 @foreach( Session::get('Spec')['list'] as $row )
												<?php
													$img = ( !empty($row ->image) AND file_exists(public_path()."/". $row ->image))?$row ->image : "http://placehold.it/500?text=noPic";
												?>
												<li>
													<div class="cart-single-product">
														<div class="media">
															<div class="pull-left cart-product-img">
																<a href="#">
																	<img class="img-responsive" alt="product" src="{{ $img }}">
																</a>
															</div>
															<div class="media-body cart-content">
																<ul>
																	<li>
																		<h1><a href="#">{{ $row['title'] }}</a></h1>
																		<h2><span>規格:</span> {{ $row['specTitle'] }}</h2>
																	</li>
																	<li>
																		<p>X {{ $row['quantity'] }}</p>
																	</li>
																	<li>
																		<p>${{ $row['price'] }}</p>
																	</li>
																	<li>
																		<a class="trash" href="#"><i class="fa fa-trash-o"></i></a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</li>
											@endforeach
										@endif

										<li>
											<span><span>合計</span></span><span>${{ Session::get('Spec')['totalSum'] }}</span>
										</li>
										<li>
											<ul class="checkout">
												<li><a href="{{ ItemMaker::url('shipping') }}" class="btn-checkout"><i class="fa fa-shopping-cart" aria-hidden="true"></i>View Cart</a></li>
												<li><a href="{{ ItemMaker::url('billing') }}" class="btn-checkout"><i class="fa fa-share" aria-hidden="true"></i>Checkout</a></li>
											</ul>
										</li>
									</ul>
								</div>
	*/

	public function removeFromShoppingBag($locale, Request $request) {

		$removeKey = $request->input('key');

		/*先稍微擋一下沒有key資料的狀況，*/
		if( empty($removeKey) )
		{
			$newData['removeFromList'] = false;
		}
		else
		{
			/*移除key，再把新資料存回去Session*/
			$originData = Session::get('Spec');

			unset( $originData['list'][$removeKey] );

			/*重新計算總金額*/
			$totalSum = 0;
			foreach( $originData['list'] as $row )
			{
				$totalSum += (int)$row['price'] * $row['quantity'] ;
			}
			$originData['amountSum'] = $totalSum;
			$originData['totalSum'] = $totalSum;

			/*
			$data['shipping_cost'] = Session::get('shipping_cost');
			$data['shipping_cost_free'] = Session::get('shipping_cost_free');

	
			if($totalSum >= $data['shipping_cost_free'])
				$data['shipping_cost'] = '免運費';	
				else		
			$data['shipping_cost'] = 'NTD ' . $data['shipping_cost'] ;	
			
			$originData['shipping_cost'] = $data['shipping_cost'];
			*/
			/*存成新資料*/
			$newData = $originData;
			$newData['removeFromList'] = true;
			$newData['noItemInList'] = count($newData['list']) == 0 ? true : false;
			$newData['countItems'] = count($newData['list']);


			Session::put( 'Spec' , $newData );
			$newData['carHtml'] = self::generateCarHtml();
		}
		return $newData;

	}

	public function updFromShoppingBag($locale, Request $request) {


		$setting = Setting::find(1);

		$setting = json_decode($setting->content, true);
		/*
		Session::put( 'shipping_cost' , $setting['shipping_cost'] );
		Session::put( 'shipping_cost_free' , $setting['shipping_cost_free'] );
		*/
		$removeKey = $request->input('key');

		/*先稍微擋一下沒有key資料的狀況，*/
		if( empty($removeKey) )
		{
			$newData['removeFromList'] = false;
		}
		else
		{
			/*修改key，再把新資料存回去Session*/
			$originData = Session::get('Spec');
			$update_arr = $request -> all();
			unset($update_arr['_token']);
			unset($update_arr['key']);
			foreach($update_arr as $key => $row){
				$originData['list'][$key]['quantity'] = $row >= 0 ? $row : 0;
			}
			//	單一修改 $originData['list'][$removeKey]['quantity'] = $request->input('quantity');

			/*重新計算總金額*/
			$totalSum = 0;
			foreach( $originData['list'] as $row )
			{
				$totalSum+= (int)$row['price'] * $row['quantity'] ;
			}
			/*
			$data['shipping_cost'] = Session::get('shipping_cost');
			$data['shipping_cost_free'] = Session::get('shipping_cost_free');

			if($totalSum >= $data['shipping_cost_free'])
				$data['shipping_cost'] = '0';	

			$originData['amountSum'] = $totalSum;
			$originData['totalSum'] = $totalSum + $data['shipping_cost'];
			
	
			if($totalSum >= $data['shipping_cost_free'])
				$data['shipping_cost'] = '免運費';	
				else		
			$data['shipping_cost'] = 'NTD ' . $data['shipping_cost'] ;	
			
			$originData['shipping_cost'] = $data['shipping_cost'];
			*/
			/*存成新資料*/
			$newData = $originData;
			$newData['removeFromList'] = true;
			$newData['noItemInList'] = count($newData['list']) == 0 ? true : false;
			$newData['countItems'] = count($newData['list']);
			$newData['message'] = '詢問車修改完成';
			$newData['an'] = true;

			Session::put( 'Spec' , $newData );
			$newData['carHtml'] = self::generateCarHtml();
		}

		return $newData;

	}
	
	//購物頁
	public function getShipping($locale)
	{
		if( !empty( Session::get('Spec')['list'] ) AND count( Session::get('Spec')['list'] ) >0 ){

			// Session::forget('DiscountCode');
			//先檢查購物車裡的產品還有沒有庫存
			
			//$stock = new StockController;
			//$stock->checkShoppingCarStock();
			// die;
			//取得優惠資訊
			//$discount = new DiscountController;
			//$thisDiscount = $discount->visibleDiscount();
			// var_dump( $thisDiscount );die;

			//$reData = $discount->checkItemisInException( Session::get('Spec') );
			//var_dump( $reData );
			//Session::put('Spec' ,$discount->reCountTotal( $reData ));
			//var_dump( Session::get('Spec') );
			//die;
			return View::make($locale.".product.shipping",[
				"data" => Session::get('Spec')
				//"Discount" => $thisDiscount
			]);
		}else{
			return redirect( MakeItemV2::url('product') )->with('Message',( $locale == 'en' )? "No Item in shipping car!" :'購物車中並沒有選擇任何產品！');
		}


	}


	//購物頁
	public function getBilling($locale)
	{
		if( !empty( Session::get('Spec')['list'] ) AND count( Session::get('Spec')['list'] ) >0 ){

			// Session::forget('DiscountCode');
			//先檢查購物車裡的產品還有沒有庫存
			
			//$stock = new StockController;
			//$stock->checkShoppingCarStock();
			// die;
			//取得優惠資訊
			//$discount = new DiscountController;
			//$thisDiscount = $discount->visibleDiscount();
			// var_dump( $thisDiscount );die;

			//$reData = $discount->checkItemisInException( Session::get('Spec') );
			//var_dump( $reData );
			//Session::put('Spec' ,$discount->reCountTotal( $reData ));
			//var_dump( Session::get('Spec') );
			//die;
			return View::make($locale.".product.billing",[
				"userdata" => !empty(Session::get("Member")) ? Session::get("Member") : '',
				"data" => Session::get('Spec')
				//"Discount" => $thisDiscount
			]);
		}else{
			return redirect( MakeItemV2::url('product') )->with('Message',( $locale == 'en' )? "No Item in shipping car!" :'購物車中並沒有選擇任何產品！');
		}
	}
	
	public function esafe($locale,$order_id){

		$oData = OrderRecord::find($order_id);

		$Member = App\Http\Models\Member\Account::find($oData->member_id);
		$total = $oData->pay_price + $oData->ship_cost;
		$gBuysafeWeb = "0000";
		$order_no = $oData->order_no;
		$contact_name = $oData->contact_name;
		$contact_tel = $oData->contact_tel;
		$contact_email = $Member->email;
		$gEsafePassWord = '0000';

		$gBuysafeInfo= "Pure 訂單編號：";
		$form = '<FORM NAME="esafe" ACTION="https://www.esafe.com.tw/Service/Etopm.aspx" METHOD="post" >
					<input type="hidden" name="web" value="'.$gBuysafeWeb.'" />
					<input type="hidden" name="MN" value="'.$total.'" />
					<input type="hidden" name="OrderInfo" value="'.$gBuysafeInfo.$order_no.'" />
					<input type="hidden" name="Td" value="'.$order_no.'" />
					<input type="hidden" name="sna" value="'.$contact_name.'" />
					<input type="hidden" name="sdt" value="'.$contact_tel.'" />
					<input type="hidden" name="email" value="'.$contact_email.'" />
					<input type="hidden" name="note1" value="" />
					<input type="hidden" name="note2" value="" />
					<input type="hidden" name="Card_Type" value="" />
					<input type="hidden" name="ChkValue" value="'.strtoupper(sha1($gBuysafeWeb.$gEsafePassWord.$total)).'" />
				</form>';
		$submit = '
			<script language=javascript>
			document.forms.esafe.submit();
			</script>';
		return $form.$submit;

		//return $OrderRecord->contact_name;
	
	}
	
	//扣除庫存數
	public function doReduceStock($order_id)
	{
		if( !empty( $order_id ) AND !empty(Session::get("Member")) ){
			//已結帳的訂單
			$findBuy = OrderRecord::with('RecordList')
							->where("member_id", Session::get('Member'))
							->where('id', $order_id)
							->select('id','is_payed')
							->first();
			if( !empty( $findBuy ) AND $findBuy->is_payed == 1 AND count($findBuy->RecordList) > 0 ){
				foreach ($findBuy->RecordList as $row) {

				}
			}
		}
	}
	public function postSpecColor(Request $request)
	{
		$data = $request->all();

		if( !empty( $data['item'] ) AND !empty( $data['spec'] ) ){
			$thisModel = new $this->modelClass;
			$find = $thisModel->with('Photo')
						->with('Item')
						->where('id', $data['spec'])
						->where('item_id', $data['item'])
						->first();


			if( !empty( $find ) ){
				$category = Category::where('id', (json_decode( $find->Item->category_id, true )) )->with('Theme')->where('is_visible', 1)->first();
				$Theme = Theme::where('id',$category->theme_id)->first();
				// return $Theme;
				//$find->category = $category ;
				$titlePrefix = ( App::getLocale() == 'en' )? 'en_title' : 'title';
				//$link = "product/"+ parent::processTitleToUrl($theme->$titlePrefix) + "/" + parent::processTitleToUrl($category->$titlePrefix) + "/" + parent::processTitleToUrl($find->Item->$titlePrefix) + "/" + parent::processTitleToUrl($find->$titlePrefix);
				$link = "product/";
				$link .= (string)(parent::processTitleToUrl($Theme->$titlePrefix))."/";
				$link .= (string)(parent::processTitleToUrl($category->$titlePrefix))."/";
				$link .= (string)(parent::processTitleToUrl($find->Item->$titlePrefix))."/";
				$link .= (string)(parent::processTitleToUrl($find->$titlePrefix));

				$find->web_title = MakeItemV2::url($link);

				return[
					"an" =>true,
					"data" => $find
				];
			}else{
				return[
					"an" =>false,
					"message" => ( $request->locale == 'en' )? "Wrong Color" : "找不到指定風格顏色"
				];
			}


		}else{
			return[
				"an" =>false,
				"message" => ( $request->locale == 'en' )? "Wrong Information" :"傳送資料不齊全"
			];
		}
	}

}
