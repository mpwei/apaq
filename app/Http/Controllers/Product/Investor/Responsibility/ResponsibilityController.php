<?php

namespace App\Http\Controllers\Investor\Responsibility;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Responsibility;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class ResponsibilityController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Responsibility/Social-Respons/ajax-list/';
    public $routePreFix = 'Responsibility/Social-Respons';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'title', 'content', 'file'];
    public $whereRaw = "sub_category = 'Social-Respons'";
    protected $modelName = "Responsibility";
    protected $viewPreFix = 'Investor.Social-Respons';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
        $content = '';
       $data = [];
        $responsibility = Responsibility::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'title', 'content', 'file')
            ->where('is_visible', 1)->where('sub_category', 'Social-Respons')->orderBy('rank','asc')->orderBy('created_at','desc')->get()->toArray();

        foreach ($responsibility as $key => $value) {
            if ($value['title'] == 'Content-Editor') {
                $content = $value['content'];
            } else {
                $data[$value['title']]['file'] = $value['file'];
                $data[$value['title']]['id'] = $value['id'];
            }
        }

        return view($locale . '.investor.responsibility.Social-Respons', [
            "content" => $content,
            "data" => $data,
        ]);
    }

    public function Download($locale,$id,$field)
    {
        $find = Responsibility::where('id', $id)->first();

        if (!empty($find) and !empty($find[$field])) {
            $file = public_path() . $find[$field];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }
}