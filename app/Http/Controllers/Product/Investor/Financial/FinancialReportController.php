<?php

namespace App\Http\Controllers\Investor\Financial;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\FinancialInfo;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class FinancialReportController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Financial-Information/Financial-Report/ajax-list/';
    public $routePreFix = 'Financial-Information/Financial-Report';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content', 'q1', 'q2', 'q3', 'q4'];
    public $whereRaw = "sub_category ='Financial-Report'";
    protected $modelName = "FinancialInfo";
    protected $viewPreFix = 'Investor.Financial-Report';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
       $data = [];
        $getPage = $_GET['page'];
        $currentYear = date("Y");

        $financialInfo = FinancialInfo::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'month', 'year', 'content', 'q1', 'q2', 'q3', 'q4')
            ->where('is_visible', 1)->where('sub_category', 'Financial-Report')->orderBy('rank','asc')->orderBy('year','desc')->get()->toArray();

        foreach ($financialInfo as $key => $value) {
            $data[$value['year']]['id'] = $value['id'];
            $data[$value['year']]['q1'] = $value['q1'];
            $data[$value['year']]['q2'] = $value['q2'];
            $data[$value['year']]['q3'] = $value['q3'];
            $data[$value['year']]['q4'] = $value['q4'];
        }

        $page = ceil(sizeof($data)/10);

        $data = array_slice($data,(($getPage-1)*10),$getPage*10,true);

        return view($locale . '.investor.financial.Financial-Report', [
            "url" => 'investor/Financial-Report',
            "page" => $page,
            "data" => $data,
            "currentYear" => $currentYear,
        ]);
    }

    public function Download($locale,$id,$field)
    {
        $find = FinancialInfo::where('id', $id)->first();
        
        if (!empty($find) and !empty($find[$field])) {
            $file = public_path() . $find[$field];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }

    public function pageDivide()
    {
        $pageContent = "";
    }
}