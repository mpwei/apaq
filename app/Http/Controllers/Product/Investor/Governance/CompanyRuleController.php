<?php

namespace App\Http\Controllers\Investor\Governance;
/**原生函式**/

use Illuminate\Http\Request;
use View;
use Mail;

/**相關Controller**/

use App\Http\Models\Investor\Governance;
use App\Http\Controllers\CRUDBaseController;

/**Models**/
class CompanyruleController extends CRUDBaseController
{
    public $ajaxEditLink = 'backen-admin/Governance/Company-rule/ajax-list/';
    public $routePreFix = 'Governance/Company-rule';
    public $selectField = ['id', 'rank', 'is_visible', 'category', 'sub_category', 'year', 'file'];
    public $whereRaw = "sub_category ='Company-rule'";
    protected $modelName = "Governance";
    protected $viewPreFix = 'Investor.Company-rule';

    public function getIndex()
    {
        $Datas = $this->findDataAndAssociate([
            "modelName" => $this->modelName,
            "select" => $this->selectField,
            'where' => $this->whereRaw,
            "belong" => $this->modelBelongs,
            "rank" => !empty($this->rank) ? $this->rank : 'rank',
            "rank_sort" => !empty($this->rank_sort) ? $this->rank_sort : 'asc',
            "has" => $this->modelHas
        ]);

        return view('backend.' . $this->viewPreFix . '.index', [
            "Datas" => $Datas,
            "modelName" => $this->modelName,
            "ajaxEditLink" => $this->ajaxEditLink
        ]);
    }

    public function Index($locale)
    {
       $data = [];
        $getPage = $_GET['page'];
        $currentYear = date("Y");

        $governance = Governance::select('id', 'rank', 'is_visible', 'category', 'sub_category', 'year', 'file')
            ->where('is_visible', 1)->where('sub_category', 'Company-rule')->orderBy('created_at','desc')->get()->toArray();

        foreach ($governance as $key => $value) {
            $data[$value['year']]['id'] = $value['id'];
            $data[$value['year']]['file'] = $value['file'];
        }

        $page = ceil(sizeof($data)/10);

        $data = array_slice($data,(($getPage-1)*10),$getPage*10,true);

        return view($locale . '.investor.governance.Company-rule', [
            "url" => 'investor/Company-rule',
            "page" => $page,
            "data" => $data,
            "currentYear" => $currentYear,
        ]);
    }

    public function Download($locale,$id,$field)
    {
        $find = Governance::where('id', $id)->first();

        if (!empty($find) and !empty($find[$field])) {
            $file = public_path() . $find[$field];
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo "檔案不存在<br>";
                echo '<input type="button" onClick="window.close()" value="關閉">';
            }
        } else {
            echo "檔案不存在<br>";
        }
    }
}