<?php

namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
/**Models**/

use App\Http\Models\Product\Category;
use App\Http\Models\Product\Item;

class ThemeController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/產品/主類別/ajax-list/';

    protected $modelName = "ProductTheme";

    public $index_select_field = ['id','rank','is_visible','is_show_home','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '產品/主類別';

    public $viewPreFix = 'Product.Theme';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    protected $cacheData = [
        "active"=> true,//是否開啟功能
        "select" =>['id','title'], //如果是空的就預設抓所有欄位
        "order" => "rank",
        "sort" => 'asc',
        'has'=>['Category','Item']
    ]; // 設定黨

    /*===============後台結束=======================*/
    public function index($locale, $theme="",$category='')
    {
        $theme = parent::revertUrlToTitle($theme);
        $category = parent::revertUrlToTitle($category);

        $Theme = Cache::get($locale."_".$this->modelName);
        if( empty( $Theme ) ){
            $thisModel = new $this->modelClass;
            $Theme = $thisModel->with('Category')->with("Item")->where('is_visible',1)->select('id','title','image')->get();
        }
            $thisModel = new $this->modelClass;
            $Theme = $thisModel->with('Category')->with("Item")->where('is_visible',1)->select('id','title','image')->get();
        //如果還沒有選主類別,把data給Theme
        //如果已經選某一個主類別,把data換成category  或  item
        if( !empty( $theme ) ){

            $data = $Theme->where('title',$theme)->first()->Category;
            $thisCategory = $data->where('title', $category)->first();

            if( empty( $data ) OR $data->count() ==  0 ){
                $data = $Theme->where('title',$theme)->first()->Item;

                //產品連結
                foreach ($data as $key => $value) {
                    $data[ $key ]->web_link = MakeItemV2::url("productdetail/".parent::processTitleToUrl( $theme )."/".parent::processTitleToUrl( $value->title ));
                }
            //選到某一層次分類
            }elseif ( !empty( $thisCategory ) ) {
                $data = $thisCategory->Item;
                
                //產品連結
                foreach ($data as $key => $value) {
                    $data[ $key ]->web_link = MakeItemV2::url("product/".parent::processTitleToUrl( $theme )."/".parent::processTitleToUrl( $value->title ));
                }
            }else{
            	//產品連結
                foreach ($data as $key => $value) {
                    $data[ $key ]->web_link = MakeItemV2::url("product/".parent::processTitleToUrl( $theme )."/".parent::processTitleToUrl( $value->title ));
                }
            }
        }else{
            $data = $Theme;
            foreach ($data as $key=> $row) {
                $data[ $key ]->web_link = MakeItemV2::url("product/".parent::processTitleToUrl( $row->title ));
            }
        }

        return View::make($locale.".product.index",[
            "data" => $data,
            "theme" => $theme? $theme : ''
        ]);
    }

}
