<?php
namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
/**Models**/

use App\Http\Models\Product\Theme;
use App\Http\Models\Product\Category;
use App\Http\Models\Product\Item;
use App\Http\Models\Product\Spec;
use App\Http\Models\Product\Photo;
use App\Http\Models\Setting;

class DetailController extends CRUDBaseController
{

    public $ajaxEditLink = 'backen-admin/產品/主類別/ajax-list/';

    protected $modelName = "ProductTheme";

    public $index_select_field = ['id','rank','is_visible','is_show_home','title',"en_title"];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '產品/主類別';

    public $viewPreFix = 'Product.Theme';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    protected $cacheData = [
        "active"=> true,//是否開啟功能
        "select" =>['id','title'], //如果是空的就預設抓所有欄位
        "order" => "rank",
        "sort" => 'asc',
        'has'=>['Category','Item']
    ]; // 設定黨

    /*===============後台結束=======================*/
    public function index($locale,$item='')
    {

        $item = parent::revertUrlToTitle($item);
        $item = str_replace('___','/',$item);

        $Product = Item::where('is_visible',1)
                    ->where('title',$item)
                    ->with('Theme')
                    ->with('Category')
                    ->select('*')
                    ->orderBy('rank','asc')
                    ->first();
        
        $prev = Item::where('is_visible',1)
                    ->where('rank','<',$Product -> rank)
                    ->where('category_id','=',$Product -> category_id)
                    ->where('theme_id','=',$Product -> theme_id)
                    ->select('id','title','theme_id','category_id')
                    ->first();

        $next = Item::where('is_visible',1)
                    ->where('rank','>',$Product -> rank)
                    ->where('category_id','=',$Product -> category_id)
                    ->where('theme_id','=',$Product -> theme_id)
                    ->select('id','title','theme_id','category_id')
                    ->orderBy('rank','asc')
                    ->first();
                    
        $Photo = Photo::where('is_visible',1)
                    ->where('item_id',$Product -> id)
                    ->select('*')
                    ->orderBy('rank','asc')
                    ->get();

        $Relate = Item::with('Theme')
                    ->with('Category')
                    ->where('is_visible',1)
                    ->whereIn('id',explode(",",$Product -> related_id))
                    ->select('*')
                    ->orderBy('rank','asc')
                    ->get();


        return View::make($locale.".product.detail",[
            "data" => $Product,
            "Photo" => $Photo,
            "item" => $item ? $item : '',
            "prev" => $prev ? $prev : '',
            "next" => $next ? $next : '',
            "Relate" => $Relate,
            "category" => !empty($Product -> Category -> title) ? $Product -> Category -> title : '',
            "theme" => !empty($Product -> Theme -> title) ? $Product -> Theme -> title : ''
        ]);


    }

}
