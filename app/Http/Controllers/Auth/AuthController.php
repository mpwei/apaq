<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

use Session;
use Config;
use App;
use View;
use App\Http\Controllers\Backend\BackendController as Backend;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */

	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
		
		//檢查路徑語系
		Backend::checkRouteLang();

		View::share('ProjectName', Config::get('app.WEB_NAME'));
	}

	public function getLogout(){
		$this->auth->logout();

		//登出後刪除左側選單的Session
		if (Session::get('MenuList'))
		{
			Session::forget('MenuList');
		}
		//return redirect()->to('/backen-admin');
		return redirect(App::getLocale().'/backen-admin');
		//return redirect(url('/'));
	}
}
