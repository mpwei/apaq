<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\Controller;
use App\Http\Models\CorporateSituation;
use App\Http\Models\CorporateSituationEvent;
use App\Http\Models\CorporateSituationPdf;
use Illuminate\Http\Request;

class CorporateSituationController extends BackendController
{
    protected $entity;
    protected $event_entity;
    protected $pdf_entity;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        CorporateSituation $entity,
        CorporateSituationEvent $event_entity,
        CorporateSituationPdf $pdf_entity
    ) {
        $this->entity = $entity;
        $this->event_entity = $event_entity;
        $this->pdf_entity = $pdf_entity;
        parent::__construct();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->entity->where('language', App::getLocale())->with('events')->with('pdfs')->first();
        return view('backend.CorporateSituation.index', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCorporateSituation(Request $request)
    {
        $this->entity->firstOrCreate([
            'date' => $request->holiday['date'],
            'note' => $request->holiday['note'],
        ]);
        $holidays = $this->entity->all()->toArray();
        return response()->json($holidays, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateCorporateSituation($locale, Request $request)
    {
        $data = $this->entity->where('language', App::getLocale())->first();
        $request['status'] = $request->status ? true : false;

        $data->update($request->all());

        if ($request->pdfs) {
            foreach ($request->pdfs as $pdf) {
                if (!strstr($pdf['id'], 'new')) {
                    $pdfIds[] = $pdf['id'];
                }
            }
            if (!empty($pdfIds)) {
                $deletePdfs = $this->pdf_entity->where('corporate_situation_id', $data->id)->whereNotIn('id', $pdfIds)->get();
                foreach ($deletePdfs as $deletePdf) {
                    $deletePdf->delete();
                }
            }
            foreach ($request->pdfs as $key => $pdf) {
                $destinationPath = 'upload/corporate';
                if ($pdf['file'] != '') {
                    $file = $pdf['file'];
                    $file->move($destinationPath, time() . $file->getClientOriginalName());
                    $pdf['path'] = 'upload/corporate/' . time() . $file->getClientOriginalName();
                }
                unset($pdf['file']);

                $pdf['status'] = array_key_exists('status', $pdf) ? true : false;
                if (!strstr($pdf['id'], 'new')) {
                    $this->pdf_entity->where('id', $pdf['id'])->update($pdf);
                } else {
                    $this->pdf_entity->create($pdf);
                }
            }
        } else {
            $pdfs = $this->pdf_entity->where('corporate_situation_id', $data->id)->get();
            foreach ($pdfs as $pdf) {
                $pdf->delete();
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holiday = $this->entity->find($id);
        $holiday->delete();
        $holidays = $this->entity->all()->toArray();

        return response()->json($holidays, 200);

    }

    public function show($locale)
    {
        $data = $this->entity->where('language', $locale)->with(['pdfs' => function ($query) {
            $query->orderBy('order')->where('status', 1);
        }])->first();
        return view($locale . '.investor.governance.Corporate-Situation', [
            "data" => !empty($data) ? $data : '',
        ]);
    }

}
