<?php 
namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use App\Http\Controllers\Backend\MakeItemV2;

/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;

/***Models****/
use App\Http\Models\PageBanner;

class PageBannerController extends BackendController {

    //批次修改路徑
    protected static $ajaxEditLink = 'backen-admin/廣告管理/內頁廣告/ajax-list/';

    //批次修改顯示及編輯的欄位設定
    public static $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> false
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    // public static $show_type = [
    //     [
    //         "id" => 1,
    //         "title" => "圖片"
    //     ],
    //     [
    //         "id" => 2,
    //         "title" => "Video"
    //     ],
    //     // [
    //     //     "id" => 3,
    //     //     "title" => "Youtube"
    //     // ],
    //     // [
    //     //     "id" => 4,
    //     //     "title" => "Vimeo"
    //     // ],
    // ];
    /*
        MidCenter  	上下左右置中 
        LeftBottom		左下
        LeftTop 		左上
        LeftMid		左中
        RightBottom	右下
        RightTop		右上
        RightMid		右中
    */
    public static $text_position = [
        [
            "id" => 1,
            "title" => "上下左右置中"
        ],
        [
            "id" => 2,
            "title" => "左下"
        ],
        [
            "id" => 3,
            "title" => "左上"
        ],
        [
            "id" => 4,
            "title" => "左中"
        ],
        [
            "id" => 5,
            "title" => "右下"
        ],
        [
            "id" => 6,
            "title" => "右上"
        ],
        [
            "id" => 7,
            "title" => "右中"
        ],
    ];
    public static $Block = [
        [
            "id" => 1,
            "title" => "公司簡介"
        ],
        [
            "id" => 2,
            "title" => "最新消息"
        ],
        [
            "id" => 3,
            "title" => "產品介紹"
        ],
        [
            "id" => 4,
            "title" => "常見問題"
        ],
        [
            "id" => 5,
            "title" => "聯絡我們"
        ],
        // [
        //     "id" => 3,
        //     "title" => "首頁第三區"
        // ]
    ];

    public function __construct()
    {
        parent::__construct();

        //View::share('Show_type', self::$show_type);

        View::share('Block', self::$Block);

        View::share('Text_Position', self::$text_position);

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $Datas = [];

        $Datas = parent::findDataAndAssociate([
            "modelName" => 'PageBanner',
            "select" => ['id','is_visible','title','block','rank']
        ]);

        return view('backend.PageBanner.index',[
            "Datas" => $Datas,
            "ajaxEditLink" => self::$ajaxEditLink
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {

        return view('backend.PageBanner.edit',
            [
                'data'=>[],
                'actionUrl' => MakeItemV2::url('backen-admin/廣告管理/內頁廣告/store')
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $resoult = json_decode(parent::addNew([
            "Datas" => $request->input('PageBanner') ,
            "modelName" => 'PageBanner',
            "routeFix" => '廣告管理/內頁廣告'
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {

            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $data = PageBanner::where('id',$id)->get()->first()->toArray();

        return view('backend.PageBanner.edit',[
                'data' => $data,
                'actionUrl' => MakeItemV2::url('backen-admin/廣告管理/內頁廣告/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $request->input('PageBanner'), 'PageBanner', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('PageBanner');

            //var_dump( $Datas );

            if( parent::updateOne( $Datas, 'PageBanner', '') )
            {
                return redirect( MakeItemV2::url('backen-admin/廣告管理/內頁廣告/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';
        //$method =  $request->input('method');

        if( empty( $method ) )
        {
            parent::deleteOne( 'PageBanner', $id );
            
        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                $method = ( $method === 'Photo' )? 'PageBannerPhoto': $method;

                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }

    }
    /*==============jQuery Ajax ====================*/
    //批次修改
    public static function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");
        //$works = Work::where('id','in',$ids)->get();
        $works = Array();
            foreach ($ids as $row) {
                $works[] = PageBanner::where('id','=',$row)
                            ->select('id', 'rank','title', 'is_visible')
                            ->get();
            }
        return view('backend.Ajax.list')
                    ->with('ajaxEditList',self::$ajaxEditList)
                    ->with('modal','PageBanner')
                    ->with('update_link', MakeItemV2::url('backen-admin/廣告管理/內頁廣告/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();

        $res = parent::updataOneColumns([
                'modelName' => 'PageBanner',
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        return $res;

    }

}
