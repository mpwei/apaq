<?php 
namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use App\Http\Controllers\Backend\MakeItemV2;

/**相關Controller**/
use App\Http\Controllers\Backend\BackendController;

/***Models****/
use App\Http\Models\Banner;

class BannerController extends BackendController {

    //批次修改路徑
    protected static $ajaxEditLink = 'backen-admin/廣告管理/首頁廣告/ajax-list/';

    //批次修改顯示及編輯的欄位設定
    public static $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> false
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    public static $text_position = [
        [
            "id" => 1,
            "title" => "上下左右置中"
        ],
        [
            "id" => 2,
            "title" => "左下"
        ],
        [
            "id" => 3,
            "title" => "左上"
        ],
        [
            "id" => 4,
            "title" => "左中"
        ],
        [
            "id" => 5,
            "title" => "右下"
        ],
        [
            "id" => 6,
            "title" => "右上"
        ],
        [
            "id" => 7,
            "title" => "右中"
        ],
    ];
    public static $Block = [
        [
            "id" => 1,
            "title" => "首頁大圖"
        ],
    ];

    public function __construct()
    {
        parent::__construct();

        View::share('Block', self::$Block);

        View::share('Text_Position', self::$text_position);

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $Datas = [];

        $Datas = parent::findDataAndAssociate([
            "modelName" => 'Banner',
            "select" => ['id','is_visible','title','block','rank']
        ]);

        return view('backend.Banner.index',[
            "Datas" => $Datas,
            "ajaxEditLink" => self::$ajaxEditLink
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {

        return view('backend.Banner.edit',
            [
                'data'=>[],
                'actionUrl' => MakeItemV2::url('backen-admin/廣告管理/首頁廣告/store')
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $resoult = json_decode(parent::addNew([
            "Datas" => $request->input('Banner') ,
            "modelName" => 'Banner',
            "routeFix" => '廣告管理/首頁廣告'
            ]), true );

        if( !empty( $resoult['redirect'] ) )
        {

            return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($locale, $id)
    {
        $data = Banner::where('id',$id)->get()->first()->toArray();

        return view('backend.Banner.edit',[
                'data' => $data,
                'actionUrl' => MakeItemV2::url('backen-admin/廣告管理/首頁廣告/update'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request)
    {
        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $request->input('Banner'), 'Banner', 'ajaxEdit');
        }
        else
        {
            $Datas = $request->input('Banner');

            if( parent::updateOne( $Datas, 'Banner', '') )
            {
                return redirect( MakeItemV2::url('backen-admin/廣告管理/首頁廣告/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDestroy(Request $request)
    {
        $id = $request->input('id');

        $method = ( !empty( $request->input('method') ) )? $request->input('method') : '';

        if( empty( $method ) )
        {
            parent::deleteOne( 'Banner', $id );
            
        }
        else
        {
            if( !empty( $id ) AND count( $id ) > 0 )
            {
                $method = ( $method === 'Photo' )? 'BannerPhoto': $method;

                foreach ($id as $row) {
                    parent::deleteOne($method, $row );
                }
            }
        }

    }
    /*==============jQuery Ajax ====================*/
    public static function postAjaxList(Request $request)
    {
        $ids = $request->input("ids");

        $works = Array();
            foreach ($ids as $row) {
                $works[] = Banner::where('id','=',$row)
                            ->select('id', 'rank','title', 'is_visible')
                            ->get();
            }
        return view('backend.Ajax.list')
                    ->with('ajaxEditList',self::$ajaxEditList)
                    ->with('modal','Banner')
                    ->with('update_link', MakeItemV2::url('backen-admin/廣告管理/首頁廣告/update'))
                    ->with('datas',$works);
    }

    public function postChangeStatic(Request $request)
    {
        $datas = $request->all();

        $res = parent::updataOneColumns([
                'modelName' => 'Banner',
                'id' => $datas['id'],
                'columns' => $datas['columns'],
                'value' => $datas['value']
            ]);

        return $res;

    }

}
