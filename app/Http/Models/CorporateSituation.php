<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CorporateSituation extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'language', 'title', 'content', 'pdf_title', 'pdf_file', 'status',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function events()
    {
        return $this->hasMany('App\Http\Models\CorporateSituationEvent', 'corporate_situation_id', 'id');
    }

    public function pdfs()
    {
        return $this->hasMany('App\Http\Models\CorporateSituationPdf', 'corporate_situation_id', 'id');
    }
}
