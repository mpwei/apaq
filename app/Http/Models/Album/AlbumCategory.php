<?php

namespace App\Http\Models\Album;

use Illuminate\Database\Eloquent\Model;
use Config;

class AlbumCategory extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."album_categories");
		}else{
			$this->setTable("album_categories");
		}
	}

	public function Album(){
		return $this->hasMany('App\Http\Models\Album\Album','category_id','id')->select('id','category_id','title','list_slogan');
	}

}
