<?php

namespace App\Http\Models\Album;

use Illuminate\Database\Eloquent\Model;
use Config;

class Photo extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."albums_photoes");
		}else{
			$this->setTable("albums_photoes");
		}
	}

	public function Album(){
		return $this->belongsTo('App\Http\Models\Album\Album','album_id')->select('id','title');
	}

}
