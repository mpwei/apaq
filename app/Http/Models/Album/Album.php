<?php

namespace App\Http\Models\Album;

use Illuminate\Database\Eloquent\Model;
use Config;

class Album extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."albums");
		}else{
			$this->setTable("albums");
		}
	}

	public function Category(){
		return $this->belongsTo('App\Http\Models\Album\AlbumCategory','category_id')->select('id','title');
	}
}
