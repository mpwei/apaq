<?php

namespace App\Http\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
	//
	public function __construct()
	{
		
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."certificate");
		}else{
			$this->setTable("certificate");
		}
	}
}
