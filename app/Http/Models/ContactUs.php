<?php

namespace App\Http\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
	//
	public function __construct()
	{
		
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."contactus");
		}else{
			$this->setTable("contactus");
		}
	}
}
