<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;


/*產品分層

主題Theme
	類別Category
		類型Type
			項目Item
				規格Spec
 */



class Item extends Model //項目
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_items");
		}else{
			$this->setTable("products_items");
		}
	}
	public function Theme(){
		return $this->belongsTo('App\Http\Models\Product\Theme','theme_id')->select("id",'title','image');
	}
	public function Category(){
		return $this->belongsTo('App\Http\Models\Product\Category','category_id')->select('id','title', 'theme_id');
	}
    public function Item()
    {
     return $this->hasMany('App\Http\Models\Product\Item','category_id','id')
                    ->select('id','category_id','rank','is_visible','title','image')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }


	//歸屬於[類型]
	// public function Type(){
	// 	return $this->belongsTo('App\Http\Models\Product\Type','type_id');
	// }


	//擁有[規格]
	public function Spec()
    {
    	return $this->hasMany('App\Http\Models\Product\Spec','item_id','id')
                    ->select('id','item_id','show_price','show_color','show_size','stock','rank','is_visible','title')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }
	public function Photo()
    {
    	return $this->hasMany('App\Http\Models\Product\Photo','item_id','id')
                    ->select('title','image','big_image','id','is_visible','rank','item_id')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }

}
