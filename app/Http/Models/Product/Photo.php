<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

class Photo extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_photoes");
		}else{
			$this->setTable("products_photoes");
		}
	}

	public function Item(){
		return $this->belongsTo('App\Http\Models\Product\Item','item_id')->select('id','title');
	}

}
