<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

/*產品分層

主題Theme
	類別Category
		類型Type
			項目Item
				規格Spec
 */


class Category extends Model //類別
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_categories");
		}else{
			$this->setTable("products_categories");
		}
	}

	//歸屬於[主題]
	public function Theme(){
		return $this->belongsTo('App\Http\Models\Product\Theme','theme_id')->select('id','title');
	}
    public function Item()
    {
     return $this->hasMany('App\Http\Models\Product\Item','category_id','id')
                    ->select('id','category_id','rank','is_visible','title','image','list_spec')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }

	//擁有[類型]
    // public function Type()
    // {
    // 	return $this->hasMany('App\Http\Models\Product\Type','category_id','id')
    //                 ->select('id','category_id','rank','is_visible','title','sub_title','image','sm_image','size_role')
    //                 ->where('is_visible',1)
    //                 ->orderBy('rank','asc');
    // }

}
