<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

class SpecSize extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_specs_sizes");
		}else{
			$this->setTable("products_specs_sizes");
		}
	}

	public function Spec(){
		return $this->belongsTo('App\Http\Models\Product\Spec','spec_id')->select('id','title',"en_title");
	}
}
