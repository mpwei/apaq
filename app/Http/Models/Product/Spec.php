<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

/*產品分層

主題Theme
	類別Category
		類型Type
			項目Item
				規格Spec
 */


class Spec extends Model //規格
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_specs");
		}else{
			$this->setTable("products_specs");
		}
	}


	//歸屬於[項目]
	public function Item(){
		return $this->belongsTo('App\Http\Models\Product\Item','item_id')->select('id','category_id','title','image','price');
	}


	//擁有[規格的多圖片]
	public function Photo(){
		return $this->hasMany('App\Http\Models\Product\SpecPhoto','spec_id','id')
					->select('id','is_visible','spec_id','rank','title','image','big_image')
					->where('is_visible',1)
					->orderBy('rank','asc');
	}


/*
	//擁有[規格的尺寸]
	public function Size(){
		return $this->hasMany('App\Http\Models\Product\SpecSize','spec_id','id')
					->select('id','is_visible','spec_id','rank','model_no','size_type','stock')
					->where('is_visible',1)
					->orderBy('rank','asc');
	}


	//擁有[規格的Format]
	public function Format(){
		return $this->hasMany('App\Http\Models\Product\Format','spec_id','id')
					->select('id','is_visible','spec_id','rank','title',"en_title")
					->where('is_visible',1)
					->orderBy('rank','asc');
	}
*/



}
