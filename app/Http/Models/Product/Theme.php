<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

/*產品分層

主題Theme
	類別Category
		類型Type
			項目Item
				規格Spec
 */


class Theme extends Model //主題
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_themes");
		}else{
			$this->setTable("products_themes");
		}
	}


	//擁有[類別]
    public function Category()
    {
    	return $this->hasMany('App\Http\Models\Product\Category','theme_id','id')
                    ->select('id','theme_id','rank','is_visible','title','image')
                    ->with('Item')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }
    public function Item()
    {
        return $this->hasMany('App\Http\Models\Product\Item','theme_id','id')
                    ->select('id','theme_id','rank','is_visible','title','image')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }


}
