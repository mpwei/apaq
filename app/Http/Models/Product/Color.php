<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

class Color extends Model
{
	public function __construct()
	{
		
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_colors");
		}else{
			$this->setTable("products_colors");
		}
	}

	// public function Category(){
	// 	return $this->belongsTo('App\Http\Models\Product\Category','categroy_id')->select('id','title');
	// }

	// public function Item(){
	// 	return $this->hasMany('App\Http\Models\Product\Item','type_id','id');
	// }
}
