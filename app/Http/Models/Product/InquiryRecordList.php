<?php

namespace App\Http\Models\Member;

use Illuminate\Database\Eloquent\Model;
use Config;

class InquiryRecordList extends Model
{
	public function __construct()
	{
		if(!empty(Config::get('app.dataBasePrefix')) AND 0 )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."inquiry_lists");
		}else{
			$this->setTable("inquiry_lists");
		}
	}

	public function InquiryRecord(){
		return $this->hasMany('App\Http\Models\Product\Type','member_id','id');
	}
	
}
