<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

class InquiryRecord extends Model
{
	public function __construct()
	{
		//2016.9.12 會員相關資料表共用
		if(!empty(Config::get('app.dataBasePrefix')) AND 0 )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."inquiry_records");
		}else{
			$this->setTable("inquiry_records");
		}
	}


	public function RecordList(){
		return $this->hasMany('App\Http\Models\Member\InquiryRecordList','order_id','id')->select('id','order_id','spec_id','title','style_title','product_no','image','color','skin','size','qty','single_price','total_price');
	}


}
