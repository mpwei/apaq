<?php

namespace App\Http\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Config;

/*產品分層

主題Theme
	類別Category
		類型Type
			項目Item
				規格Spec
 */



class Type extends Model //類型
{
	public function __construct()
	{
		
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."products_types");
		}else{
			$this->setTable("products_types");
		}
	}

	//歸屬於[類別]
	public function Category(){
		return $this->belongsTo('App\Http\Models\Product\Category','categroy_id')->select('id','title');
	}


	//擁有[項目]
    public function Item()
    {
    	return $this->hasMany('App\Http\Models\Product\Item','type_id','id')
                    ->select('id','type_id','rank','is_visible','title','image','size','color_search','skin_search')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }


    //擁有[材質屬性]
    // public function Size()
    // {
    // 	return $this->hasMany('App\Http\Models\Product\Item','type_id','id')
    //                 ->select('id','type_id','rank','is_visible','title','image','size','color_search','skin_search')
    //                 ->where('is_visible',1)
    //                 ->orderBy('rank','asc');
    // }

}
