<?php

namespace App\Http\Models\Design;

use Illuminate\Database\Eloquent\Model;
use Config;

class Simulate extends Model
{
	public function __construct()
	{
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."design_tool_simulate");
		}else{
			$this->setTable("design_tool_simulate");
		}
	}
}
