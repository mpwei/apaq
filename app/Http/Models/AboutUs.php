<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class AboutUs extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."aboutus");
		}else{
			$this->setTable("aboutus");
		}
	}


}
