<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Setting extends Model
{
    protected $guarded = ['id'];
	public function __construct()
	{
		
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."settings");
		}else{
			$this->setTable("settings");
		}
	}
}
