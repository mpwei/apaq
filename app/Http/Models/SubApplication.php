<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class SubApplication extends Model
{
	public function __construct()
	{
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."sub_applications");
		}else{
			$this->setTable("sub_applications");
		}
	}

	public function application(){
		return $this->belongsTo('App\Http\Models\Application', 'application_id');
	}
}
