<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CorporateSituationEvent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'corporate_situation_id', 'date', 'order', 'topic', 'result',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}
