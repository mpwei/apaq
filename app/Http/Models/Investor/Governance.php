<?php

namespace App\Http\Models\Investor;

use Illuminate\Database\Eloquent\Model;
use Config;

class Governance extends Model
{
	public function __construct()
	{
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."investor_governance");
		}else{
			$this->setTable("investor_governance");
		}
	}
}
