<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Application extends Model
{
	public function __construct()
	{
		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."applications");
		}else{
			$this->setTable("applications");
		}
	}

	public function sub_applications(){
		return $this->hasMany('App\Http\Models\SubApplication')->where('is_visible', 1)->orderBy('rank', 'asc');
	}
}
