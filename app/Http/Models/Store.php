<?php

namespace App\Http\Models\Store;

use Illuminate\Database\Eloquent\Model;
use Config;

class Store extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."stores");
		}else{
			$this->setTable("stores");
		}
	}



	public function Country(){
		return $this->belongsTo('App\Http\Models\Store\StoreCountry','country_id')->select('id','title',"en_title");
	}

	public function City(){
		return $this->belongsTo('App\Http\Models\Store\StoreCity','city_id')->select('id','title',"en_title");
	}

	// public function Time(){
	// 	return $this->hasMany('App\Http\Models\Store\StoreTime','store_id','id')->select('day','store_id','open_time','close_time');
	// }
}
