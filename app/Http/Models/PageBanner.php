<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class PageBanner extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."pagebanners");
		}else{
			$this->setTable("pagebanners");
		}
	}
}
