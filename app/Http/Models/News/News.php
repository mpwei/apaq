<?php

namespace App\Http\Models\News;

use Illuminate\Database\Eloquent\Model;
use Config;

class News extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."news");
		}else{
			$this->setTable("news");
		}
	}

	public function Category(){
		return $this->belongsTo('App\Http\Models\News\NewsCategory','category_id')->select('id','title');
	}

	public function Photo(){
		return $this->hasMany('App\Http\Models\News\NewsPhoto','news_id','id')->select('id','rank','is_visible','image','news_id','video_link','is_link_out');
	}
}
