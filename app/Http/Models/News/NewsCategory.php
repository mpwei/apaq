<?php

namespace App\Http\Models\News;

use Illuminate\Database\Eloquent\Model;
use Config;

class NewsCategory extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."news_categories");
		}else{
			$this->setTable("news_categories");
		}
	}

	public function News(){
		return $this->hasMany('App\Http\Models\News\News','category_id','id')->select('id','category_id','title');
	}

}
