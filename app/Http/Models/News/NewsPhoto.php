<?php

namespace App\Http\Models\News;

use Illuminate\Database\Eloquent\Model;
use Config;

class NewsPhoto extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."news_photoes");
		}else{
			$this->setTable("news_photoes");
		}
	}

	public function News()
	{
		return $this->belongsTo('App\Http\Models\News\News','news_id')->select('id','title');
	}

}
