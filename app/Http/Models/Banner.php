<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Banner extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."banners");
		}else{
			$this->setTable("banners");
		}
	}
}
