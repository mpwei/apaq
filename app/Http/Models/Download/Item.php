<?php

namespace App\Http\Models\Download;

use Illuminate\Database\Eloquent\Model;
use Config;

class Item extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."downloads");
		}else{
			$this->setTable("downloads");
		}
	}

	public function Category(){
		return $this->belongsTo('App\Http\Models\Download\Category','category_id')->select('id','title');
	}

}
