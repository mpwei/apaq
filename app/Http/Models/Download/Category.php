<?php

namespace App\Http\Models\Download;

use Illuminate\Database\Eloquent\Model;
use Config;

class Category extends Model
{
	public function __construct()
	{

		if(!empty(Config::get('app.dataBasePrefix')) )
		{
			$this->setTable(Config::get('app.dataBasePrefix')."downloads_categories");
		}else{
			$this->setTable("downloads_categories");
		}
	}

	public function Download(){
		return $this->hasMany('App\Http\Models\Download\Item','category_id','id')->select('id','category_id','title');
	}

}
