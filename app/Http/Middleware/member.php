<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Controllers\Backend\MakeItemV2;
use App;

class member
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( empty( Session::get( 'Member' ) ) )
        {
            Session::put('backUrl', $request->url());
            return redirect()->guest( MakeItemV2::url( $request->locale.'/sign-in') );
        }elseif ( !empty( empty( Session::get( 'Member' ) ) ) AND !empty( Session::get('backUrl') ) ) {
            Session::forget('backUrl');

        }

        return $next($request);
    }
}
