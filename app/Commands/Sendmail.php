<?php namespace App\Commands;

class SendEmail {

    /**
     * 執行隊列
     *
     * @return void
     */
    public function fire($job, $data)
    {
        // 寄送 Email
        \Mail::send('emails.welcome', [], function($message) use ($data)
        {
            $message->to($data['email'], $data['name'])->subject('歡迎使用 Laravel 5 資料庫隊列寄送 Email!!!');
        });

        // 執行成功，刪除隊列
        $job->delete();
    }
}