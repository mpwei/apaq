<?php 
namespace App\Presenters;
use Config;

class HtmlItemPresenter
{
	/*
	|--------------------------------------------------------------------------
	| 語系路徑
	|--------------------------------------------------------------------------
	*/
	public static function url($path)
	{
		$path = (!empty(Config::get('app.dataBasePrefix')) )? App::getLocale().'/'.$path : $path;
		return url( $path );
	}

	/*
	|--------------------------------------------------------------------------
	| 頁碼
	|--------------------------------------------------------------------------
	*/
	public static function pages()
	{
		$path = (!empty(Config::get('app.dataBasePrefix')) )? App::getLocale().'/'.$path : $path;
		return url( $path );
	}

	

}