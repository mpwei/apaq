
var checkSign = function()
{
	//console.log('check sing');

	$('a[id="submit"], div[id="go"]').on('click', function(event) {
		event.preventDefault();

		//console.log('submit click');

		var thisFix = $(this).data('model');

		var content = checkContent(thisFix);
		console.log("content : "+content);
		if (content)
		{
			//console.log("form : "+$('form#contact_form').prop('action'));
			$('form[id="'+ $(this).data('form') +'"]').submit();
		}else
		{
			//console.log('content is empty.');
		}
		//

	});

	var checkContent = function(thisFix)
	{
		var result = false;

		//檢查e-mail格式
		validEmail = function(v) {
			var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
			return (v.match(r) == null) ? false : true;
		}


		$('input').each(function(index, el) {
			if ($(this).data('type') == 'required')
			{
				//console.log("this input : "+$(this).prop('name'));
				var name = $(this).prop('name'),
					baseName = '';

				baseName = name.replace(thisFix,'');//移除name前贅字
				baseName = baseName.replace('[','');
				baseName = baseName.replace(']','');

				if ($(this).val() == '')
				{
					//$(this).parent('li').css('border' ,'1px solid #c0392b');
					$(this).addClass('wrongForm');


					//console.log("remove Fix Model input name : "+baseName);

					switch (baseName)
					{
						case 'first_name':
								$('p#messages').append('請填入名<br>');
								break;
						case 'last_name':
								$('p#messages').append('請填入姓<br>');
								break;
						case 'email':

								$('p#messages').append('請填入E-mail<br>');
								break;
						case 'title':

								$('p#messages').append('請填入主旨<br>');
								break;
						case 'company_name':

								$('p#messages').append('請填入公司名稱<br>');
								break;
						case 'address':

								$('p#messages').append('請填入地址<br>');
								break;
						case 'theme':

								$('p#messages').append('請填入詢問主題<br>');
								break;
						case 'tel':
								$('p#messages').append('請填入電話<br>');
								break;
						case 'mobile':
								$('p#messages').append('請填入手機號碼<br>');
								break;
						case 'message':
								$('p#messages').append('請填入詢問事項<br>');
								break;
						case 'captcha':
								$('p#messages').append('請填入驗證碼<br>');
								break;
					}

					result = false;
				}else
				{
					//$(this).parent('li').css('border' ,'1px solid black');
					$(this).parent('li').addClass('wrong');
					if(baseName == 'email')
					{

						if( !validEmail( $(this).val() ) )
						{
							//$(this).parent('li').css('border' ,'1px solid red');
							$(this).parent('li').addClass('wrong');
							$('p#messages').append('請填入正確mail<br>');

							result = false;
						}else
						{
							result = true;
						}
					}
					if(baseName == 'count')
					{

						if( !$.isNumeric($(this).val()) )
						{
							//$(this).parent('li').css('border' ,'1px solid red');
							$(this).parent('li').addClass('wrong');
							$('p#messages').append('預約人數請填入「數字」<br>');

							result = false;
						}else
						{
							result = true;
						}
					}
				}
			}

		});
		$('textarea').each(function(index, el) {
			if ($(this).data('type') == 'required')
			{
				//console.log("this input : "+$(this).prop('name'));
				var name = $(this).prop('name'),
					baseName = '';

				baseName = name.replace(thisFix,'');//移除name前贅字
				baseName = baseName.replace('[','');
				baseName = baseName.replace(']','');

				if ($(this).val() == '')
				{
					//$(this).parent('li').css('border' ,'1px solid #c0392b');
					$(this).parent('li').addClass('wrong');


					//console.log("remove Fix Model input name : "+baseName);

					switch (baseName)
					{
						case 'message':
								$('p#messages').append('請填入詢問事項<br>');
								break;
					}

					result = false;
				}else
				{
					result = true;
				}
			}

		});
			//console.log("final result : "+result);
		return result;

	}

	//重新取得驗證碼
	if( $('#changeCap').size() > 0 )
	{
        $('#changeCap').on('click', function(evt) {
        	//console.log('changeCap');
            var d = new Date();
            var src = $(this).children('img').prop('src').split('?')[0] + '?t=' + d.getMilliseconds();

            $(this).children('img').prop('src', src);

        });
	}

}
