var Spec = function ()
{
    var specTable = $('table[id="Spec_table"]'),//規格table
        bassLink = $('base').prop('href');

    var spec_thead = function()
    {
        var tags = specTable.data('tags').split(','),
            html = '';

        html += '<th width="20" class="sorting_disabled" rowspan="1" colspan="1" aria-label=""><input type="checkbox" class="group-checkable"></th>';

        for (var i in tags) {
            switch (tags[i]) {
                case 'rank':
                    html += '<th class="header_th_'+i+'">排序</th> ';
                    break;
                case 'file':
                    html += '<th class="header_th_'+i+'">檔案</th> ';
                    break;
                case 'is_visible':
                    html += '<th class="header_th_'+i+'">是否顯示</th> ';
                    break;
                default:
                    html += '<th class="header_th_'+i+'">'+tags[i]+'</th> ';
            }

        }
        return html;
    }

    $('select[name="Item[form_type]"]').off('change').on('change', function(event) {
        event.preventDefault();
        if( confirm( '改變密度規格表將會導致已輸入的規格遺失，確定要繼續嗎？' ) )
        {
            //清空thead 和 tbody
            specTable.find('thead').empty();
            specTable.find('tbody').empty();

            $.ajax({
                url: bassLink + '/Backend/產品/密度/form',
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: $('meta[name="csrf-token"]').prop('content'),
                    form_id : $(this).val()
                }
            })
            .done(function(data) {
                specTable.data('tags', data.tags);

                //console.log(specTable.data('tags'));
                specTable.find('thead').append( spec_thead() );

                //觸發檔案管理
				FilePicke();
				//觸發排序
				Rank();
				//觸發color picker
				colorPicker();
				//觸發表格刪除
				dataDelete();
            })
            .fail(function() {
                console.log("error");
            });


        }
    });
}
