var Rank = function()
{	
	var _token = $('meta[name="csrf-token"]').attr('content');

	//排序往上
	$("a[name='rank_up']").on('click', function(event) {

		var thisElement = $(this),
			thisModal = $(this).data('model'),
			thisID = $(this).data('id'),
			thisRankValue = $(this).data('rank'),
			thisCheck = $(this).data('checkfield');

		//console.log(thisModal+" id -> "+thisID+" rank -> "+thisRankValue+" is click.");

		$.ajax({
				url: $('base').attr('href')+'/Backend/Rank/Up',
				type: 'POST',
				dataType : 'json',
				data: {
					_token: _token,
					id : thisID,
					rank : thisRankValue,
					model : thisModal,
					check : thisCheck
				},
			})
			.done(function(data) {
				console.log("success");
				//檢查目前這個tr 在哪一個順序
				if(data.message =='ture')
				{
					var table = $('table[id="'+thisModal+'_table"]'),
						tbody = table.find('tbody[id="itemTbody"]'),
						eleIndex = tbody.children('tr').index(thisElement.parent('td').parent('tr')),
						thisTr = thisElement.parent('td').parent('tr');//目前這筆資料在表格中的順序

					//0代表已經是最上面	
					if(eleIndex != 0)
					{

						var thisSortNum = thisTr.find('td[id="sortNum"]').children('h1'),
							thisRankInput = thisTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]'),
							prevTr = thisTr.prev('tr'),
							prevSortNum = prevTr.find('td[id="sortNum"]').children('h1'),
							prevRankInput = prevTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]');

						//如果兩個值一樣，就互換輸入值
						if (parseInt(data.rank) == parseInt(prevRankInput.attr('value')) ) 
						{
							//先處理要被更換的物件

							prevSortNum.text(parseInt(prevSortNum.text())+1);
							prevRankInput.attr('value', data.prevRank);
							prevTr.find('a[name="'+thisElement.attr('name')+'"]').data('rank',data.prevRank);


							thisSortNum.text(parseInt(thisSortNum.text())-1);//更改目前顯示的順序號碼
							thisRankInput.attr('value',data.rank);//更改input中的值
							thisElement.data('rank',data.rank);//更換a標籤上的值

							thisTr.insertBefore(thisTr.prev('tr'));//更換順序

						}else{
							thisElement.data('rank',data.rank);//更換a標籤上的值
							thisRankInput.attr('value',data.rank);//更改input中的值

							alert("已更換排序值");
						}

					}

				}else
				{
					console.log('沒有更變Rank值, '+data);
				}


			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
				
	});
}