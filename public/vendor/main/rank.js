var Rank = function()
{	


	//排序往上
	$("a[name='rank_up'], a[name='rank_down']").on('click', function(event) {

		var thisElement = $(this),
			thisModal = $(this).data('prefix'),
			table = $('table[id="'+thisModal+'_table"]'),
			tbody = table.find('tbody[id="itemTbody"]'),
			checkbox = tbody.find('input[type="checkbox"]'),
			checkbox_ischeck = [];
			checkbox_count = 0;

		checkbox.each(function(index, el) {
			if( $(this).is(':checked') )
			{
				checkbox_ischeck.push( $(this).prop('name') );
			}
		});

		//console.log("is check input"+checkbox_ischeck);
		
		if( checkbox_ischeck.length > 1 )//先看是單筆還是多筆
		{
			//多筆
			console.log( 'checkbox_ischeck length is biigger than : '+checkbox_ischeck.length );

			var firstChange;

			for (var i = 0; i < checkbox_ischeck.length; i++) {
				var thisCheckInput = $('input[name="'+checkbox_ischeck[i]+'"]'),
					eleIndex = tbody.children('tr').index(thisCheckInput.parent('td').parent('tr')),//目前這筆資料在表格中的順序
					thisTr = thisCheckInput.parent('td').parent('tr'),
					nextTr = ($(this).attr('name') == 'rank_up') ? thisTr.prev('tr') : thisTr.next('tr')//下一個tr
					thisSortNum = thisTr.find('td[id="sortNum"]').children('h1'),
					thisRankInput = thisTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]'),
					prevSortNum = nextTr.find('td[id="sortNum"]').children('h1'),
					prevRankInput = nextTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]');

				console.log("多筆排序的 rank input : " + thisCheckInput.prop('name'));
				if (thisElement.prop('name') == 'rank_up') 
				{
					//console.log('is rank-up ');
					if(eleIndex != 0)
					{
						//如果兩個值一樣，就互換輸入值
						if (parseInt(thisRankInput.attr('value'))-1 == parseInt(prevRankInput.attr('value')) ) 
						{
							//先處理要被更換的物件
							prevSortNum.text(parseInt(prevSortNum.text())+1);
							prevRankInput.attr('value', thisRankInput.attr('value'));
							
							thisSortNum.text(parseInt(thisSortNum.text())-1);//更改目前顯示的順序號碼
							thisRankInput.attr('value',thisRankInput.attr('value')-1);//更改input中的值

							thisTr.insertBefore(nextTr);//更換順序

						}else{

							//如果往上沒有相同的ＲＡＮＫ值時
							thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))-1);//更改input中的值

							//alert("已更換排序值，Rank值與上一個不同所以不調換位置");
							toastrAlert('warning', '已更換排序值，Rank值與上一個不同所以不調換位置');
						}
					}else
					{
						//console.log('eleIndex is 0');
						toastrAlert('warning',"已置頂");
					}

				}else
				{
					//console.log('is rank-down ');
					if(eleIndex != parseInt(tbody.children('tr').size())-1)
					{
						//如果兩個值一樣，就互換輸入值
						if (parseInt(thisRankInput.attr('value'))+1 == parseInt(prevRankInput.attr('value')) ) 
						{
							//先處理要被更換的物件
							prevSortNum.text(parseInt(prevSortNum.text())-1);
							prevRankInput.attr('value', thisRankInput.attr('value'));
							
							thisSortNum.text(parseInt(thisSortNum.text())+1);//更改目前顯示的順序號碼
							thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

							thisTr.insertAfter(nextTr);//更換順序

						}else{

							//如果往上沒有相同的ＲＡＮＫ值時
							thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

							//alert("已更換排序值，Rank值與上一個不同所以不調換位置");
							toastrAlert('warning', '已更換排序值，Rank值與上一個不同所以不調換位置');
						}
					}else
					{
						//alert("已置底");
						toastrAlert('warning',"已置頂");
					}

				}

			}
		}
		else
		{
			//單筆
			//console.log( 'checkbox_ischeck length is not biigger than : '+ checkbox_ischeck.length);

			var thisCheckInput = $('input[name="'+checkbox_ischeck[0]+'"]'),
				eleIndex = tbody.children('tr').index(thisCheckInput.parent('td').parent('tr')),//目前這筆資料在表格中的順序
				thisTr = thisCheckInput.parent('td').parent('tr'),
				nextTr = ($(this).attr('name') == 'rank_up') ? thisTr.prev('tr') : thisTr.next('tr')//下一個tr
				thisSortNum = thisTr.find('td[id="sortNum"]').children('h1'),
				thisRankInput = thisTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]'),
				prevSortNum = nextTr.find('td[id="sortNum"]').children('h1'),
				prevRankInput = nextTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]');
			//console.log('next rank input : '+prevRankInput.prop('name'));

			if (thisElement.prop('name') == 'rank_up') 
			{
				//console.log('is rank-up ');
				if(eleIndex != 0)
				{
					//console.log('eleIndex is not 0');

					//console.log('this rank input value : '+thisRankInput.prop('value'));
					//如果兩個值一樣，就互換輸入值
					if (parseInt(thisRankInput.attr('value'))-1 == parseInt(prevRankInput.attr('value')) ) 
					{
						//先處理要被更換的物件
						prevSortNum.text(parseInt(prevSortNum.text())+1);
						prevRankInput.attr('value', thisRankInput.attr('value'));
						
						thisSortNum.text(parseInt(thisSortNum.text())-1);//更改目前顯示的順序號碼
						thisRankInput.attr('value',thisRankInput.attr('value')-1);//更改input中的值

						thisTr.insertBefore(nextTr);//更換順序

					}else{

						//如果往上沒有相同的ＲＡＮＫ值時
						thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))-1);//更改input中的值

						//alert("已更換排序值，Rank值與上一個不同所以不調換位置");
						toastrAlert('warning', '已更換排序值，Rank值與上一個不同所以不調換位置');
					}
				}else
				{
					//console.log('eleIndex is 0');
					//alert("已置頂");
					toastrAlert('warning', '已置頂!');
				}

			}else
			{
				//console.log('is rank-down ');
				if(eleIndex != parseInt(tbody.children('tr').size())-1)
				{
					//如果兩個值一樣，就互換輸入值
					if (parseInt(thisRankInput.attr('value'))+1 == parseInt(prevRankInput.attr('value')) ) 
					{
						//先處理要被更換的物件
						prevSortNum.text(parseInt(prevSortNum.text())-1);
						prevRankInput.attr('value', thisRankInput.attr('value'));
						
						thisSortNum.text(parseInt(thisSortNum.text())+1);//更改目前顯示的順序號碼
						thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

						thisTr.insertAfter(nextTr);//更換順序

					}else{

						//如果往上沒有相同的ＲＡＮＫ值時
						thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

						//alert("已更換排序值，Rank值與上一個不同所以不調換位置");
						toastrAlert('warning', '已更換排序值，Rank值與上一個不同所以不調換位置');
					}
				}else
				{
					//alert("已置底");
					toastrAlert('warning', '已置頂!');
				}

			}
		}
				
	});
}