var actionDiv = $("div[class='actions']");

var reSortRankNum = function( thisTable )
{
	//改變排序數字
	trCount = 1;
	thisTable.find('tbody[id="itemTbody"]').children('tr').each(function(index, el) {
		var tr = $(this),
			tdRank = tr.find('td[id="sortNum"]');
		tdRank.find('h1').text(trCount);
		tdRank.find('input[name*="rank"]').prop('value', trCount);

		trCount++;
	});
}

//表格刪除資料
var dataDelete = function()
{

	$('a[id="delete-multi"], a[id="dataDel"]').off('click').on('click',function(event)
	{
		var thisButton = $(this);
		var delUrl = $('base').attr('href')+ '/backen-admin/'+thisButton.data('routename')+'/destroy';
		//console.log("del url : "+delUrl);
		if (confirm('刪除資料將會遺失，確定要刪除嗎？'))
		{
			//一次刪除多筆
			if( thisButton.prop('id') == 'delete-multi' )
			{
				console.log("delete-multi");
				var $thisTable = $('table[id="'+thisButton.data('prefix')+'_table"]'),
					ids = [];

				$thisTable.children('tbody[id="itemTbody"]').find('input[type="checkbox"]').each(function(index, el) {
					if( $(this).is(':checked') )
					{
						//console.log( $(this).prop('name')+' is checked.' );
						if( !$.isEmptyObject( $(this).prop('value') ) )
						{
							ids.push( $(this).val() );
						}

						$(this).parent('span').parent('div').parent('td').parent('tr').remove();
					}
					else
					{
						console.log( $(this).prop('name')+' is not checked.' );
					}
				});
				//console.log('ids : '+ids);
				//改變Rank排序數字
				reSortRankNum( $thisTable );

				if( ids.length > 0 )
				{
					$.ajax({
						url: delUrl,
						type: 'POST',
						data:
						{
							id: ids,
							method : thisButton.data('prefix'),
							_token : $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function() {
						console.log("success");
						//改變排序數字

						reSortRankNum( $thisTable );
						toastrAlert('success', '刪除資料成功');
						//$('div[id="messager"]').show('fast').append('<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><i class="fa fa-warning fa-lg"></i> Delete Success.')
					})
					.fail(function() {
						console.log("error");
					});
				}


			}
			else if( thisButton.prop('id') == 'dataDel' )
			{
				$.ajax({
					url: delUrl,
					type: 'POST',
					data:
					{
						id: thisButton.data('id'),
						_token : $('meta[name="csrf-token"]').attr('content')
					}
				})
				.done(function() {
					//console.log("success");
					thisButton.parent('td').parent('tr').remove();
					toastrAlert('success', '刪除資料成功');
					//$('div[id="messager"]').show('fast').append('<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><i class="fa fa-warning fa-lg"></i> Delete Success.')
				})
				.fail(function() {
					console.log("error");
				});
			}

		}


	});
}

/**** Data Table ***/
var activeDataTable = function()
{

	var table = $('table#datatable_list').DataTable({
			"paging": false,
			"searching": false,

		});

	// $('input[id="formFilter"]').on('keyup',  function(event) {
	// 	console.log('input form filter. '+$(this).val());
	// 	table
	// 		.columns($(this).data('columns'))
	// 		.search(this.value)
	// 		.draw();
	// });

	// $('select[id="formFilter"]').on('change',  function(event) {
	// 	console.log('select form filter.'+$(this).data('columns'));
	// 	table
	// 		.columns($(this).data('columns'))
	// 		.search(this.value)
	// 		.draw();
	// });

	var setSearchLink = function( value , type )
	{
		var parts = window.location.search.substr(1).split("&"),
			value = ( type != 'json' )?value.split("=") : value,
			count = 0,
			string = '',
			link = {};
		if( parts.length > 0 )
		{
			for (var i = 0; i < parts.length; i++)
			{
			    var temp = parts[i].split("=");

			    if( !link.hasOwnProperty( value[0] ) && type != 'json' )//如果key不存在
			    {
			    	link[decodeURIComponent(value[0])] = decodeURIComponent(value[1]);
			    }

			    if( ( value[0] == temp[0] && value[1] != '' ) && type != 'json')//如果
			    {
			    	link[decodeURIComponent(temp[0])] = decodeURIComponent(value[1]);
			    }
			    else
			    {
			    	link[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
			    }

			}
		}

		if( type == 'json' )
		{
			$.each(value, function(i, v){
				if( !$.isEmptyObject( value[i] ) && i !='' )
				{
					link[ decodeURIComponent( i ) ] = decodeURIComponent( v );
				}
			});
		}

		for(k in link)
		{
			if( !$.isEmptyObject( link[k] ) && k !=''  )
			{
				string += ( count == 0 )? '?' + k +'=' + link[k] : '&' + k +'=' + link[k] ;
				count++;
			}


		}

		return string;

	}

	var setSearch = function (el, thisFix)
	{
		var string='',
			name = el.prop('name'),
			baseName;

		baseName = name.replace(thisFix,'');//移除name前贅字
		baseName = baseName.replace('[','');
		baseName = baseName.replace(']','');


		if( el.prop('name') == "group_static" )
		{

			baseName = el.prop('value').split(",");
			//string += '?search[' + baseName[0] + ']=' + baseName[1];

			string += setSearchLink( 'search[' + baseName[0] + ']=' + baseName[1]);

		}else
		{
			string += setSearchLink( 'search[' + baseName + ']=' + el.val());
			//string += '?search[' + baseName + ']=' + el.val();
		}

		return string;
	}
	//console.log(window.location);

	$('a[id="searchData"]').off('click').on('click', function(event){
		var thisLink = window.location.pathname,
			searchString ='',
			model = $(this).data('model'),
			baseName = '',
			search = {};


		$('thead[id="searchThead"]').find('input').each(function(index, el) {

			if( !$.isEmptyObject( $(this).prop('value') ) )
			{
				baseName = $(this).prop('name').replace(model,'');//移除name前贅字
				baseName = baseName.replace('[','');
				baseName = baseName.replace(']','');
				//searchString += setSearch($(this),  model);
				search[  'search['+baseName+']' ] = decodeURIComponent( $(this).val() );
			}

		});

		if( $('thead[id="searchThead"]').find('select').size() > 0 )
		{

			$('thead[id="searchThead"]').find('select').each(function(index, el) {
				if( !$.isEmptyObject( $(this).prop('value') ) )
				{
					if( $(this).prop('name') =='group_static' )
					{
						baseName = $(this).prop('value');//移除name前贅字
						baseName = baseName.split(',');

						search[ 'search['+baseName[0]+']' ] = decodeURIComponent( baseName[1] );
					}
					else
					{
						baseName = $(this).prop('name').replace(model,'');//移除name前贅字
						baseName = baseName.replace('[','');
						baseName = baseName.replace(']','');
						// searchString += setSearch($(this),  model);

						search[ 'search['+baseName+']' ] = decodeURIComponent( $(this).val() );
					}

				}

			});
		}
		searchString = setSearchLink( search, 'json' );
		if( searchString != '' )
		{
			window.location.href = window.location.pathname + searchString;
		}

	});

	$('input[name="to_page"]').on('keyup', function(event) {
		var searchSting ;

		if( event.keyCode == 13 )//按下enter在轉址
		{
			searchSting = ( $.isEmptyObject(window.location.search) )? '?page=' + $(this).val() : setSearchLink( 'page=' + $(this).val() );
			window.location.href = window.location.pathname + searchSting;
		}

	});

	$('select[name="per_page_count"]').on('change', function(event) {
		var searchSting ;

		searchSting = ( $.isEmptyObject(window.location.search) )? '?show_per_page=' + $(this).val() : setSearchLink( 'show_per_page=' + $(this).val() );
		window.location.href = window.location.pathname + searchSting;

	});

}

/**** Data Table ***/

//點選表格最上方checkbox 時
$("input[id='idCheckAll']").on('click', function(event) {
	if ($(this).is(':checked'))
	{
		$('input[id="ids"]').each(function(index, el) {
			if(!$(this).is(':checked')){
				el.click();
			}
		});
	}else{
		$('input[id="ids"]').each(function(index, el) {
			if($(this).is(':checked')){
				el.click();
			}
		});
	}

	actionDiv.show('fast');
});



var iframeAjax = function()
{
	$('iframe[id="fancyBox"]').load(function() {

		//get iFrame's content.`

		var iFrameContent = $(this).contents();

		$('div[id="show_ajax_edit"]').html( iFrameContent.find('div[id="ajax_list_div"]').contents() );//取得產出的批次修改畫面

		$('a[id="toColorbox"]').trigger('click');



		$('button[id="ajaxEditClose"], button[class="close"]').on('click', function(event) {
			event.preventDefault();

			window.location.href=window.location.href;
		});

		appActive();
		changeStatic();//批次修改狀態切換

	});
}

//批次修改
$("a[id='ajaxEdit']").on('click', function(event) {
	var countCheck = $("input[id='ids']").is(':checked');

	if(countCheck){
		$("form[id='dataContent']").submit();

		iframeAjax();
	}

});
