var News = function (){
	this.show_type = document.querySelector('input[name="show_type"]'),
	this.image_content = document.querySelectorAll('div[id="News[image]"]'),
	this.select = $('select[name="News[show_type]"]'),
	this.__construct();
}

News.prototype.__construct = function(){
	//顯示類類型  預設圖片

	(this.select).on('change', this.select_change_event);
	this.do_check_to_show();
};

News.prototype.select_change_event = function(){
	$('input[name="show_type"]').prop('value', $(this).val());

	var news = new News();
	news.do_check_to_show();

};
News.prototype.do_check_to_show = function(){

	if( this.show_type.value === '1' || this.show_type.value === '2'  || this.show_type.value === '' ){
		this.check_type('image');
	}else if ( this.show_type.value === '3' || this.show_type.value === '4' ) {
		this.check_type('input');
	}
};


News.prototype.check_type = function(type){
	//隱藏不顯示的部份
	[].slice.call( this.image_content ).forEach( function(el, index) {

		//選擇類型為圖片或是Video時，顯示圖片區塊
		if( type ==='image' && el.querySelector('img') !== null ){
			el.style.display = 'block';
		}else if( type === 'input' && el.querySelector('img') === null ){
			el.style.display = 'block';
		}else{
			el.style.display = 'none';
		}
		 

	});
		

};