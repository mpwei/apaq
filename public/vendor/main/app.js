/*非單一功能所使用的js code*/
var appActive = function()
{
	$(document).ready(function() {

		//Metronic.init(); // init metronic core componets
		//Layout.init(); // init layout

		if( $('.tags').size() > 0 ){
	    	$('.tags').tagsInput();
		}
		if( $(".select2").size() > 0 ){
	    	$(".select2").select2({
				allowClear: true
			});
		}
		if( $(".select2_sample2").size() > 0 )
		{
			$(".select2_sample2").select2({
				placeholder: "Type to select an option",
				allowClear: true,
				minimumInputLength: 1,
				query: function (query) {
					var data = {
						results: []
					}, i, j, s;
					for (i = 1; i < 5; i++) {
						s = "";
						for (j = 0; j < i; j++) {
							s = s + query.term;
						}
						data.results.push({
							id: query.term + i,
							text: s
						});
					}
					query.callback(data);
				}
			});
		}

	    //html 編輯器
	    if( $('.wysihtml5').size() > 0 )
		{
			$('.wysihtml5').wysihtml5();
		}

	    if( $('.date-picker').size() > 0 )
		{
			$('.date-picker').datepicker({
	                format: 'yyyy/mm/dd',
	                autoclose: true,
	        });
		}

		 if( $('.fancybox-button').size() > 0 )
		 {
		 	$('.fancybox-button').colorbox({
					height : "90%"
			});
		 }
		if( $("input.mask_currency").size() > 0 )
		{
			$("input.mask_currency").inputmask('999,999,999,999', {
	            numericInput: true
	        }); //123456  =>  € ___.__1.234,56
		}

		//檔案總管
		if( $('a[id="toFileManager"]').size() > 0 )
		{
			$('a[id="toFileManager"]').colorbox({
				iframe : true,
				width : "90%",
				height : "800",
				overlayClose : false,
			});
		}

		//checkbox switch
	/*	if( $('input[class="make-switch"]').size() > 0 )
		{
			var $ch = $('input[class="make-switch"]');

			console.log("check box : "+$ch.data('name') );

			$ch.on('click', function(event) {
				event.preventDefault();
				var el = $(this);

				console.log('is click');

				if( el.is('checked') )
				{
					console.log('is checked.');
				}
				else
				{
					console.log('this is not checked');
				}
			});

		}*/
	});
}
appActive();


var setFileValue = function (input, selectFiles, type)
{
	getReturnFile(input, selectFiles, type);

	toastrAlert('success', '選擇成功');

	$("button[id='cboxClose']").click();
}

//批次修改更改顯示狀態
var changeStatic = function()
{

	$('a[id="EditStatic"]').on('click', function(event) {
		//console.log("is changeStatic ");
		var thisA = $(this),
			nowValue = $('input[name="'+thisA.data('field')+'"]').prop('value'),//目前值
			//thisStaticTex = (thisA.data('showtext')).split(","),
			vaddClass = ( !$.isEmptyObject( thisA.data('colortype') ) )? thisA.data('colortype') : 'blue' ,
			vremoveClass = ( !$.isEmptyObject( thisA.data('colortype') ) )? 'label-default' : 'default',
			hiddenInput = $('input[name="'+thisA.data('field')+'"]');
			//console.log("vaddClass : "+vaddClass);

		//更改隱藏欄位的值
		if(nowValue == '1')
		{
			//console.log(hiddenInput.prop('name')+" value change to 0.");

			//change hidden input value
			hiddenInput.prop('value', '0');
			if( $.isEmptyObject( thisA.data('colortype') ) )
			{
				// change a text
				thisA.children('i').removeClass('fa-check');
				thisA.children('i').addClass('fa-close');
			}
			//remove and add class
			thisA.removeClass(vaddClass);
			thisA.addClass(vremoveClass);
		}else
		{
			//console.log(hiddenInput.prop('name')+" value change to 1.");

			//change hidden input value
			hiddenInput.prop('value', '1');

			// change a text
			//thisA.text(thisStaticTex[hiddenInput.val()]);
			if( $.isEmptyObject( thisA.data('colortype') ) )
			{
				thisA.children('i').removeClass('fa-close');
				thisA.children('i').addClass('fa-check');
			}


			//remove and add class
			thisA.removeClass(vremoveClass);
			thisA.addClass(vaddClass);
		}

	});
}

var changeStatic_ajax = function()
{
	if( $('a[id="list_ajax_static"]').size() > 0 )
	{
		//console.log("is changeStatic_ajax ");
		$('a[id="list_ajax_static"]').off('clcik').on('click', function(event) {
			event.preventDefault();

			var el = $(this),
				nowValue = $('input[name="'+el.data('hiddenvalue')+'"]').prop('value'),//目前值
				hiddenInput = $('input[name="'+el.data('hiddenvalue')+'"]'),
				showClass = el.data('colortype'),
				value_to_change;

			//console.log('hiddenvalue : '+nowValue);
			//更改畫面的值
			if(nowValue == '1')
			{

				//change hidden input value
				hiddenInput.prop('value', '0');
				value_to_change = 0;
				//remove and add class
			}else
			{

				//change hidden input value
				hiddenInput.prop('value', '1');
				value_to_change = 1;
				//remove and add class
			}
			//console.log('ajaxlink : '+$('base').prop('href') + '/Backend/' + el.data('model') +'/change-static');

			if( el.data('ajax') == true )
			{
				$.ajax({
					url: $('base').prop('href') + '/backen-admin/' + el.data('model') + '/change-static',
					type: 'POST',
					dataType: 'JSON',
					data: {
						_toden : $('meta[name="csrf-token"]').prop('content'),
						id: el.data('parent'),
						columns: el.data('columns'),
						value: value_to_change,

					},
				})
				.done(function(data) {
					//console.log('data : '+data.message);
					//console.log("success");

					if( data.message == 'true' )
					{
						if(value_to_change == '0')
						{
							//remove and add class
							el.removeClass(showClass);
							el.addClass('label-default');


						}else
						{
							//remove and add class
							el.removeClass('label-default');
							el.addClass(showClass);
						}
						$('div[id="'+el.data('hiddenvalue')+'"]').text(el.data('columns')+value_to_change);
						//location.reload();
						toastrAlert('success', '狀態更改成功');
					}
				})
				.fail(function() {
					console.log("error");
				});
			}



		});
	}
}

var toastrAlert = function(type, message)
{
	//toastr.info('Are you the 6 fingered man?');

	Command: toastr[ type ]( message );
	// types ['warning', 'success', 'info', 'error']
	toastr.options = {
	  "closeButton": false,
	  "debug": true,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
}
