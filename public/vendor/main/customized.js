/*jshint jquery:true */
/*global $:true */

var $ = jQuery.noConflict();

$(document).ready(function($) {

	/* ---------------------------------------------------------------------- */
	/*	
	/* ---------------------------------------------------------------------- */

	(function() {
        	$('#dropdownMenuLink .selected').html($('#languages-select .dropdown-item > .current').html());
	})();

});
