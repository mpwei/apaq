var Rank = function()
{	

	//排序往上
	$("a[name='rank_up'], a[name='rank_down']").on('click', function(event) {

		var thisElement = $(this),
			thisModal = $(this).data('model'),
			table = $('table[id="'+thisModal+'_table"]'),
			tbody = table.find('tbody[id="itemTbody"]'),
			eleIndex = tbody.children('tr').index(thisElement.parent('td').parent('tr')),//目前這筆資料在表格中的順序
			thisTr = thisElement.parent('td').parent('tr'),
			nextTr = ($(this).attr('name') == 'rank_up') ? thisTr.prev('tr') : thisTr.next('tr'),//下一個tr
			thisSortNum = thisTr.find('td[id="sortNum"]').children('h1'),
			thisRankInput = thisTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]'),
			prevSortNum = nextTr.find('td[id="sortNum"]').children('h1'),
			prevRankInput = nextTr.find('td[id="sortNum"]').children('input[name^="'+thisModal+'["]');

		if ($(this).attr('name') == 'rank_up') 
		{
			if(eleIndex != 0)
			{
				//如果兩個值一樣，就互換輸入值
				if (parseInt(thisRankInput.attr('value'))-1 == parseInt(prevRankInput.attr('value')) ) 
				{
					//先處理要被更換的物件
					prevSortNum.text(parseInt(prevSortNum.text())+1);
					prevRankInput.attr('value', thisRankInput.attr('value'));
					
					thisSortNum.text(parseInt(thisSortNum.text())-1);//更改目前顯示的順序號碼
					thisRankInput.attr('value',thisRankInput.attr('value')-1);//更改input中的值

					//更換a標籤上的值
					thisElement.data('rank',thisRankInput.attr('value'));
					nextTr.find('a[name="'+thisElement.attr('name')+'"]').data('rank',prevRankInput.attr('value'));

					thisTr.insertBefore(nextTr);//更換順序

				}else{

					//如果往上沒有相同的ＲＡＮＫ值時
					thisElement.data('rank',parseInt(thisRankInput.attr('value'))-1);//更換a標籤上的值
					thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))-1);//更改input中的值

					alert("已更換排序值，Rank值與上一個不同所以不調換位置");
				}
			}else
			{
				alert("已置頂");
			}

		}else
		{
			if(eleIndex != parseInt(tbody.children('tr').size())-1)
			{
				//如果兩個值一樣，就互換輸入值
				if (parseInt(thisRankInput.attr('value'))+1 == parseInt(prevRankInput.attr('value')) ) 
				{
					//先處理要被更換的物件
					prevSortNum.text(parseInt(prevSortNum.text())-1);
					prevRankInput.attr('value', thisRankInput.attr('value'));
					
					thisSortNum.text(parseInt(thisSortNum.text())+1);//更改目前顯示的順序號碼
					thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

					//更換a標籤上的值
					thisElement.data('rank',thisRankInput.attr('value'));
					nextTr.find('a[name="'+thisElement.attr('name')+'"]').data('rank',prevRankInput.attr('value'));

					thisTr.insertAfter(nextTr);//更換順序

				}else{

					//如果往上沒有相同的ＲＡＮＫ值時
					thisElement.data('rank',parseInt(thisRankInput.attr('value'))+1);//更換a標籤上的值
					thisRankInput.attr('value',parseInt(thisRankInput.attr('value'))+1);//更改input中的值

					alert("已更換排序值，Rank值與上一個不同所以不調換位置");
				}
			}else
			{
				alert("已置底");
			}

		}

				
	});
}