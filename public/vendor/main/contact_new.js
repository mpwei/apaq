var ContactUs = function () {
 	var submitBtn =  document.querySelector('a.aaSubmit');
	var inputs = document.querySelectorAll('input');
	var textArea = document.querySelector('textarea');
	var select = $('select[id="country"]');
	var result = false;

 	var validEmail = function(v) {
		var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
		return (v.match(r) == null) ? false : true;
	}

	var inputBlurEvent = function () {
		if ( this.getAttribute("data-required") !== null && this.getAttribute("data-required") == 'true'  ) {
			if ( this.value == '' && ( this.placeholder ).search("必填") < 0 ) {

				this.placeholder += ' [必填]';

			}else if ( this.value !='' && this.type == 'email' ) {

				if ( !validEmail( this.value ) ) {
					this.placeholder = "請輸入正確格式的E-mail"
				}

			}
		}
	}

	var submitClick = function () {
		//console.log('is click submit');

		var form = document.querySelector('form#contact_form');
		var base_link = document.querySelector('base');
		var isOk = [];

		//檢查必填欄位是否空白
		[].slice.call( inputs ).forEach( function(el, index) {
			if( el.value != '' && (el.name).startsWith('Contact') || el.name == 'captcha' ){
				if(  el.type == 'email' ){

					if ( !validEmail( el.value ) ) {
						isOk.push(false);
					}else {
						isOk.push(true);
					}	
					
				}else{
					isOk.push(true);
				}
				

			}else if( el.value == '' && ( (el.name).startsWith('Contact') || el.name == 'captcha' ) ){

				isOk.push(false);
			}
		});
		if( textArea.value != '' ){
			isOk.push(true);
		}else{
			isOk.push(false);
		}
		var check_value = isOk.filter(function(value) { return value !== true }).length;


		if( check_value == 0 && isOk.length >0 ){
			//console.log( "form is good to go" );
			var ajax = new XMLHttpRequest();
			ajax.onreadystatechange = function() {
				if (ajax.readyState == 4 && ajax.status == 200) {
					var res = JSON.parse(ajax.responseText);
					if( res.answer ){
						alert('感謝您的詢問，我們將盡快回應您。');
						form.reset();
						document.querySelector('a[id="changeCap"]').click();
					}
					
				}
			};
			ajax.open("POST", base_link.href + "/contactus/save", true);
			ajax.send(new FormData(form));
		}else{
			alert("尚有必填欄位未填寫，或是格式錯誤");
		}

	}

	var selectChangeEvent = function () {

	}

    document.querySelector('a[id="changeCap"]').addEventListener("click", function () {

        var d = new Date();
        var img = this.childNodes[0];

        img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();

    });


	//add submit btn event
	submitBtn.addEventListener("click", submitClick);

	//input textarea event 
	[].slice.call( inputs ).forEach( function(el) {
		
		if( el.name !== null && ( (el.name).startsWith('Contact') || el.name == 'captcha' ) ){
			el.addEventListener("blur", inputBlurEvent);
		}
		
	});

	textArea.addEventListener("blur", inputBlurEvent);
	//jquery object
	select.on("change", selectChangeEvent);

}