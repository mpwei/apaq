var FilePicke = function ()
{
	$('input[id="filePicker"], a[id="multi-add"] , a[id="filePicker"]' ).off('click').on('click', function(event) {

		var link =  '/vendor/elfinder-2.0/elfinder.php',
			iframe = $('iframe[id="filePick"]');
		$('a[id="toFileManager"]').attr('href', '')
		if ($(this).attr('id') == 'multi-add')
		{
			//console.log("選擇批次新增");
			var name = $(this).data('name');

			$('a[id="toFileManager"]').attr('href', link+'?name=' + name + '&type=multi');

			//console.log("送出的連結 多選 ： "+link+'?name='+name);
		}else
		{
			name = ( !$.isEmptyObject($(this).prop('name') ) )? $(this).attr('name') : $(this).data('name') ;

			$('a[id="toFileManager"]').attr('href', link+'?name=' + name + '&type=single');
			//console.log("送出的連結 單選： "+link+'?name='+ name  + '&type=single');
		}


		$('a[id="toFileManager"]').trigger('click');

	});

	$('a[id="text-add"]').off('click').on('click', function(event){
		getReturnFile($(this).data('name'), 1, 'text' );
	});
}

var getReturnFile = function(input, valueFiles, type)
{
	// valueFiles = valueFiles.substr(1,999);
	if( type =='multi' )
	{
		var files = valueFiles.split(',');
		
		//篩選掉空的
		files = files.filter(Boolean);
	}
	else
	{
		var files = valueFiles;
	}


	//2015.10.26 修改批次新增也可以選單選, 調整用類型判斷多筆還是單筆

	if( type == 'multi' || type =='text' )
	{
		//console.log("挑選一個檔案以上");
		var el = $('#'+input);
			//console.log( "el is "+el.prop('id') );
			if (el.is('table')) 
			{
				
				var tags = el.data('tags');//get table tags
				var thisTbody = el.children('tbody[id="itemTbody"]');//要加入的位子
				var trCount = thisTbody.children('tr').size();
				var nameGroup = el.attr('name');
				var FormStr = '';
				var staticArray = ['否','是'],
			    	preArray = ['25% ','50% '],
			    	fileCount = ( type =='multi' )? files.length: files;

				tags = tags.split(",");

				//console.log("files : "+files);
				//console.log("files.length : "+files.length);

				for (var i = 0; i < fileCount; i++) {
					FormStr += '<tr>';
						FormStr += '<td >';
							FormStr += '<div >';
								FormStr += '<span>';
                        			FormStr += '<input type="checkbox" name="'+nameGroup+'['+trCount+'][id]" value="">';
                        			FormStr += '<input type="hidden" name="'+nameGroup+'['+trCount+'][id]" value="" />';
                        		FormStr += '</span>';
                        	FormStr += '</div>';
                        FormStr += '</td>';
						for (var x = 0; x < tags.length; x++) {
							//圖片
							if (tags[x] == 'image' || tags[x].endsWith('_image') ) 
							{
								var file_type = files[i].split('.').pop(),
									show_img = ( file_type === 'mp4' )? '/vendor/main/file_icon/mp4.png' : files[i];

								FormStr += '<td class="change_img">';
							        FormStr += '<a href="'+show_img+'" class="fancybox-button">';
							            FormStr += '<img id="showPic"  src="'+show_img+'"  style="width : 200px; height: auto;">';
							            FormStr += '<input type="hidden" name="'+nameGroup+'['+trCount+']['+tags[x]+']" id="filePicker" value="/'+files[i]+'" >';
							        FormStr += '</a>';
							        FormStr += '<div class="images_icon">';
							            FormStr += '<a id="filePicker" data-name="'+nameGroup+'['+trCount+']['+tags[x]+']" href="javascript:;" class="label label-sm label-success tooltips" data-container="body" data-placement="top" data-original-title="編輯圖片" data-rel="fancybox-button">';
							                FormStr += '<i class="fa fa-pencil"></i> ';
							            FormStr += '</a>';
							        FormStr += '</div>';
							        FormStr += '<div class="images_icon2">';
							            FormStr += '<a id=" " href="javascript:;" class="label label-sm label-info tooltips" data-container="body" data-placement="top" data-original-title="複製路徑" data-rel="fancybox-button">';
							                FormStr += '<i class="fa fa-chain"></i> ';
							            FormStr += '</a>';
							        FormStr += '</div>';
						      	FormStr += '</td>';
								//FormStr += '<td><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;"><img src="'+files[i]+'"></div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new">Select image </span><span class="fileinput-exists">Change </span><input type="text" name="'+nameGroup+'['+trCount+']['+tags[x]+']" id="filePicker" value="'+files[i]+'"></span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a></div></div></td>';
							}else if(tags[x] == 'rank')
							{

								FormStr += '<td id="sortNum" align="center"><h1>'+(trCount+1)+'</h1><input type="hidden" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+trCount+'" /></td>';
							}
							else if( tags[x].endsWith('file')  )
							{
								FormStr += '<td><input class="form-control form-filter input-sm" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value=""> <a href="javascript:;" data-name="'+nameGroup+'['+trCount+']['+tags[x]+']" id="filePicker" class="btn default btn-file">Select File</a></td>';
							}
							else if( tags[x].endsWith('_color') )//color picker
							{
								FormStr +='<td><input class="mycolor" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="" /></td>';

							}
							else if(tags[x] == 'features' || tags[x].endsWith('_content'))
							{
								FormStr +='<td><textarea class="wysihtml5 form-control" name="'+nameGroup+'['+trCount+']['+tags[x]+']" rows="6" ></textarea></td>';
							}
							else if( tags[x].startsWith('is_') )//radio
							{

								if(tags[x] == 'is_perc')
								{
									FormStr +='<td>';
									for (var k = 0; k < preArray.length; k++) {
										if( k == 1)
										{
											FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" checked="checked" />'+preArray[k]+'</label>';
										}
										else
										{
											FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" />'+preArray[k]+'</label>';	
										}
										
									}
									FormStr +='</td>';
								}
								else
								{
									FormStr +='<td>';
									for (var k = 0; k < staticArray.length; k++) {
										if( k == 1)
										{
											FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" checked="checked" />'+staticArray[k]+'</label>';
										}
										else
										{
											FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" />'+staticArray[k]+'</label>';	
										}
										
									}
									FormStr +='</td>';
								}


							}
							else
							{
								var dsiable = ( tags[x] =='created_at' || tags[x] =='updated_at' )? 'disabled="disabled"' : '';

								//文字欄位
								FormStr +='<td><input class="form-control form-filter input-sm" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="" '+dsiable+' /></td>';
							}
						}

					FormStr += '</tr>';
					trCount++;
				}

				thisTbody.append(FormStr);
				//觸發檔案管理

				FilePicke();
				//觸發資料刪除
				//photoDelete();
				//觸發排序
				Rank();
				//觸發color picker
				colorPicker();
				//觸發表格刪除
				dataDelete();
			}else
			{
				console.log("這個元素不是Table");
			}


	}else
	{
		var thisInput = $('input[name="'+input+'"]'),
			a,
			file_type = files.split('.').pop(),
			show_img = ( file_type === 'mp4' )? 'vendor/main/file_icon/mp4.png' : files;
		if( thisInput.parent('a').size() > 0 )
		{
			var changImg = thisInput.parent('a').children('img');
			a = thisInput.parent('a');
			a.prop("href", "/"+files);
		}
		else
		{

			var changImg = thisInput.parent('span').parent('a').parent('div').parent('div').children('div[id="showPic_div"]').children('img');
		}
		//console.log("更換預覽圖 ： "+changImg.prop("src"));

		
		changImg.prop('src', "/"+show_img)
		thisInput.prop('value', "/"+files);
	}

}
var colorboxClickClose = function () {
	if( $('a[id="toFileManager"]').size() > 0 ){
		$('a[id="toFileManager"]').colorbox.close();
	}
}