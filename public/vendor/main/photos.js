var basHref = $('base').attr("href") + '/',
	_token = $('meta[name="csrf-token"]').attr('content');

var addItem = function(){
	//新增項目
	$('a[id="addItem_min"]').click(
		function()
		{
			if ($('tr[id="is_no_data"]').size() > 0) 
			{

				$('tr[id="is_no_data"]').remove();
			}

			//目前有幾筆資料
			var thisTable = $(this).parent('th').parent('tr').parent('thead').parent('table'),
				trCount = thisTable.children('tbody[id="itemTbody"]').children('tr').size(),
				nameGroup = thisTable.data('model'),
				table_tags = $(this).data('tags'), 
			    FormStr,
			    staticArray = ['否','是'],
			    preArray = ['25% ','50% '];


			table_tags = table_tags.split(",");

			FormStr += '<tr>';
				if(table_tags.length > 1 )
				{
					for (var i = 0; i < table_tags.length; i++) {
						//圖片
						if (table_tags[i] == 'image' || table_tags[i] == 'big_image' || table_tags[i] == 'image_sm') 
						{
							FormStr += '<td><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"></div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new">Select image </span><span class="fileinput-exists">Change </span><input type="text" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" id="filePicker"></span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a></div></div></td>';
						
						}else if(table_tags[i] == 'rank')
						{
							//Rank 欄位
						
							FormStr +='<td align="center" id="sortNum"><h1>'+(trCount+1)+'</h1><input type="hidden" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+(trCount+1)+'" /></td>';
						}
						else if( table_tags[i].startsWith('is_') )//radio
						{
							if(table_tags[i] == 'is_perc')
							{
								FormStr +='<td>';
								for (var k = 0; k < preArray.length; k++) {
									if( k == 1)
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" checked="checked" />'+preArray[k]+'</label>';	
									}
									else
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" />'+preArray[k]+'</label>';	
									}
									
								}
								FormStr +='</td>';
							}else
							{
								FormStr +='<td>';
								for (var k = 0; k < staticArray.length; k++) {
									if( k == 1)
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" checked="checked" />'+staticArray[k]+'</label>';	
									}
									else
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" />'+staticArray[k]+'</label>';	
									}
									
								}
								FormStr +='</td>';
							}


						}
						else if( table_tags[i].endsWith('_color') )//color picker
						{
							FormStr +='<td><input class="mycolor" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="" /></td>';

						}
						else if(table_tags[i] == 'features')
						{
							FormStr +='<td><textarea class="wysihtml5 form-control" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" rows="6" ></textarea></td>';
						}
						else if(table_tags[i] == 'time')
						{
							FormStr +='<td>';
								FormStr +='<table class="table table-bordered table-hover" data-model ="'+nameGroup+'" data-tags="time" >';
									FormStr +='<thead>';
										FormStr +='<tr>';
											FormStr +='<th>';
												FormStr +='&nbsp';
											FormStr +='</th>';
											FormStr +='<th>';
												FormStr +='<a href="javascript:;" id="addItem" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>';
											FormStr +='</th>';
										FormStr +='</tr>';
									FormStr +='</thead>;';
									FormStr +='<tbody id="itemTbody">';
									FormStr +='</tbody>';
								FormStr +='</table>';
							FormStr +='</td>';
						}
						else
						{
							//文字欄位
							FormStr +='<td><input name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="" /></td>';
						}

						
					}
					FormStr +='<td><input type="hidden" name="'+nameGroup+'['+trCount+'][id]" value="" /><a href="javascript:;" name="rank_up"  class="btn purple" data-id="" data-rank="'+(trCount+1)+'" data-model="'+nameGroup+'"  ><i class="fa fa-level-up"></i></a><a href="javascript:;" name="rank_down" class="btn purple" data-id="" data-rank="'+(trCount+1)+'" data-model="'+nameGroup+'"><i class="fa fa-level-down"></i> </a><a href="javascript:;" class="btn red" id="dataDel"  data-id="" data-model="'+nameGroup+'"><i class="fa fa-times"></i></a></td>';
				}else
				{

					var thisParent = thisTable.parent('td').parent('tr').index();

					//文字欄位
					FormStr +='<td><input name="'+nameGroup+'['+thisParent+']['+trCount+']['+table_tags+']" value="" /></td>';
					FormStr +='<td><input type="hidden" name="'+nameGroup+'['+thisParent+']['+trCount+'][id]" value="" /><a href="javascript:;" class="btn red" id="dataDel"  data-id="" data-model="'+nameGroup+'"><i class="fa fa-times"></i></a></td>';
				}
				

			FormStr += '</tr>';


			thisTable.children('tbody[id="itemTbody"]').append(FormStr);

			//觸發檔案管理
			FilePicke();
			//觸發資料刪除
			photoDelete();
			//觸發排序
			Rank();
			//觸發color picker
			colorPicker();
		}
	);
}

var photoDelete = function()
{
	//刪除
	$('a[id^="dataDel"]').click(function(event) {

		var thisA = $(this);

		if (confirm('刪除將會造成資料遺失，確定要刪除嗎？')) 
		{
			
			var dataID = thisA.data("id"),
				workID = $("input[name='id']").val(),
				modelName = thisA.data('model');

			//data-id空的是動態產生的一列資料，所以不做ajax的部分	

			if ($.isEmptyObject(dataID)) {
				thisA.parent('td').parent('tr').remove();
			}else{
				thisA.parent('td').parent('tr').remove();

				$.ajax({
					url: basHref+'Backend/'+modelName+'/Delete/',
					type: 'POST',
					data: {
						work_id : workID,
						photo_id : dataID,
						_token : _token
					},
				})
				.done(function(data) {
					console.log("data : "+data);
					console.log("Work Photo Delete success");
				})
				.fail(function(data) {
					console.log();
					console.log("Work Photo Delete error");
				});
			}


		}else
		{
			return false;
		}
	});	
}

//新增項目
$('a[id="addItem"]').click(
	function()
	{
		if ($('tr[id="is_no_data"]').size() > 0) 
		{

			$('tr[id="is_no_data"]').remove();
		}

		//目前有幾筆資料
		var thisTable = $(this).parent('th').parent('tr').parent('thead').parent('table'),
			trCount = thisTable.children('tbody[id="itemTbody"]').children('tr').size(),
			nameGroup = thisTable.data('model'),
			table_tags = $(this).data('tags'), 
		    FormStr,
		    staticArray = ['否','是'],
		    preArray = ['25% ','50% '];


		table_tags = table_tags.split(",");

		FormStr += '<tr>';
			if(table_tags.length > 1 )
			{

				for (var i = 0; i < table_tags.length; i++) {
					//圖片
					if (table_tags[i] == 'image' || table_tags[i] == 'big_image' || table_tags[i] == 'image_sm') 
					{
						FormStr += '<td><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"></div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new">Select image </span><span class="fileinput-exists">Change </span><input type="text" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" id="filePicker"></span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a></div></div></td>';
					
					}else if(table_tags[i] == 'rank')
					{
						//Rank 欄位
					
						FormStr +='<td align="center" id="sortNum"><h1>'+(trCount+1)+'</h1><input type="hidden" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+(trCount+1)+'" /></td>';
					}
					else if( table_tags[i].startsWith('is_') )//radio
					{
							if(table_tags[i] == 'is_perc')
							{
								FormStr +='<td>';
								for (var k = 0; k < preArray.length; k++) {
									if( k == 1)
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" checked="checked" />'+preArray[k]+'</label>';	
									}
									else
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" />'+preArray[k]+'</label>';	
									}
									
								}
								FormStr +='</td>';
							}else
							{
								FormStr +='<td>';
								for (var k = 0; k < staticArray.length; k++) {
									if( k == 1)
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" checked="checked" />'+staticArray[k]+'</label>';	
									}
									else
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="'+k+'" />'+staticArray[k]+'</label>';	
									}
									
								}
								FormStr +='</td>';
							}

					}
					else if( table_tags[i].endsWith('_color') )//color picker
					{
						FormStr +='<td><input class="mycolor" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="" /></td>';

					}
					else if(table_tags[i] == 'features')
					{
						FormStr +='<td><textarea class="wysihtml5 form-control" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" rows="6" ></textarea></td>';
					}
					else if(table_tags[i] == 'time')
					{
						FormStr +='<td>';
							FormStr +='<table class="table table-bordered table-hover" data-model ="'+nameGroup+'" data-tags="time" >';
								FormStr +='<thead>';
									FormStr +='<tr>';
										FormStr +='<th>';
											FormStr +='&nbsp';
										FormStr +='</th>';
										FormStr +='<th>';
											FormStr +='<a href="javascript:;" id="addItem_min" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>';
										FormStr +='</th>';
									FormStr +='</tr>';
								FormStr +='</thead>';
								FormStr +='<tbody id="itemTbody">';
								FormStr +='</tbody>';
							FormStr +='</table>';
						FormStr +='</td>';
					}
					else
					{
						//文字欄位
						FormStr +='<td><input name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="" /></td>';
					}

					
				}
				FormStr +='<td><input type="hidden" name="'+nameGroup+'['+trCount+'][id]" value="" /><a href="javascript:;" name="rank_up"  class="btn purple" data-id="" data-rank="'+(trCount+1)+'" data-model="'+nameGroup+'"  ><i class="fa fa-level-up"></i></a><a href="javascript:;" name="rank_down" class="btn purple" data-id="" data-rank="'+(trCount+1)+'" data-model="'+nameGroup+'"><i class="fa fa-level-down"></i> </a><a href="javascript:;" class="btn red" id="dataDel"  data-id="" data-model="'+nameGroup+'"><i class="fa fa-times"></i></a></td>';
			}else
			{
				var thisParent = thisTable.parent('td').parent('tr').index();

				//文字欄位
				FormStr +='<td><input name="'+nameGroup+'['+thisParent+']['+trCount+']['+table_tags+']" value="" /></td>';
				FormStr +='<td><input type="hidden" name="'+nameGroup+'['+thisParent+']['+trCount+'][id]" value="" /><a href="javascript:;" class="btn red" id="dataDel"  data-id="" data-model="'+nameGroup+'"><i class="fa fa-times"></i></a></td>';
			}
			

		FormStr += '</tr>';


		thisTable.children('tbody[id="itemTbody"]').append(FormStr);

		//觸發檔案管理
		FilePicke();
		//觸發資料刪除
		photoDelete();
		//觸發排序
		Rank();
		//觸發color picker
		colorPicker();

		addItem();//新增項目
	}
);
