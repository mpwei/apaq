<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>elFinder 2.0</title>
		
		<!-- jQuery and jQuery UI (REQUIRED) -->
	
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

		<!-- elFinder CSS (REQUIRED) -->
        <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/elfinder.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/theme.css">
		
		<style>
		.btn{
			border-width: 0px;
			padding: 7px 14px;
			font-size: 14px;
			outline: medium none !important;
			background-image: none !important;
			filter: none;
			box-shadow: none;
			text-shadow: none;
			color: #FFF;
			background-color: #26A69A;
		}
		body{ margin:0}
		
		</style>
		<!-- elFinder JS (REQUIRED) -->
		<script type="text/javascript" src="js/elfinder.min.js"></script>

		<!-- elFinder translation (OPTIONAL) -->
		<script type="text/javascript" src="js/i18n/elfinder.zh_TW.js"></script>


		<!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
			$().ready(function() {
				var elf = $('#elfinder').elfinder({
					url : 'php/connector.php',  // connector URL (REQUIRED)
					lang: 'zh_TW',             // language (OPTIONAL)
					handlers : {
						select : function(event, elfinderInstance) {
							var selected = event.data.selected,
								count = 0,
								value = [];
							if(selected.length > 0)
							{
								var file = elfinderInstance.file(selected[0]);
								var value = [];
								for (var i = 0; i < selected.length; i++) 
								{
									value[i]= elfinderInstance.path(selected[i]);

								}
								
								$('input[name="selectFiles"]').attr('value',value);
							}
							//console.log(value);
						}
					}
				}).elfinder('instance');

				$('.btn_close').on("click", function (e) {
					$('a[id="toFileManager"]').colorbox().close();
				});
			});

			function passInputName ()
			{
				var inputName = $('input[name="getSelectInput"]').val();

				return inputName;
			}

			function selectType ()
			{
				var type = $('input[name="selectType"]').val();

				return type;
			}

			function passFiles ()
			{
				var files = $('input[name="selectFiles"]').val();

				return files;
			}
/*			function thisClose()
			{

				console.log("close btn: "+$("button[id='cboxClose']").size());
			}*/
/*			$('button[name="sendFile"]').on('click', '.selector', function(event) {
				event.preventDefault();
				console.log("sendFile is click.");
			});*/
		</script>
	</head>
	<body>
		<!-- Element where elFinder will be created (REQUIRED) -->
		<input type="hidden" name="getSelectInput" value="<?php echo $_REQUEST['name'];?>">
		<input type="hidden" name="selectType" value="<?php echo $_REQUEST['type'];?>">
		<input type="hidden" name="selectFiles" value="">


        <div class="manager_title_bg">
        </div>
        <div class="manager_title">
        <div class="modal-header">
                <h4 class="modal-title">News - 批次修改</h4>
            </div>
        </div>
		<div id="elfinder">
		</div>
		<div class="manager_button_bg">
		<div class="manager_button">
			<button class="btn btn_save" name="sendFile" value="" onclick="parent.setFileValue(passInputName(),passFiles(), selectType() )">確定</button>
		</div>
		</div>
	</body>
</html>
