/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function (config) {
	config.filebrowserBrowseUrl =
		'/vendor/ckeditor/ckfinder/ckfinder.html?Type=Images';

	config.templates_files = ['/vendor/ckeditor/templates/templates.js?1'];
	config.extraPlugins = 'youtube';

	config.allowedContent = true; //不移除ＨＴＭＬ類別與ＩＤ

	// config.protectedSource.push( /<span[\s\S]*?\>/g );
	// config.protectedSource.push( /<\/span[\s\S]*?\>/g );

	config.toolbarGroups = [
		{
			name: 'document',
			groups: ['mode', 'document', 'doctools', 'Templates'],
		},
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
		{ name: 'forms', groups: ['forms'] },
		{ name: 'templates', groups: ['templates'] },
		'/',
		{ name: 'links', groups: ['links'] },
		{ name: 'insert', groups: ['insert'] },
		'/',
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'styles', groups: ['styles'] },
		{ name: 'colors', groups: ['colors'] },
		{ name: 'tools', groups: ['tools'] },
		{ name: 'paragraph', groups: ['align'] },
		{ name: 'others', groups: ['others'] },
		{ name: 'about', groups: ['about'] },
		{ name: 'removeformat', groups: ['removeformat'] },
	];

	config.extraPlugins = 'youtube';

	config.removeButtons =
		'NewPage,Preview,Print,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,HiddenField,Button,Select,Flash,PageBreak,Iframe,ShowBlocks,About';
	config.font_names =
		'新細明體;標楷體;微軟正黑體;' +
		'Arial/Arial, Helvetica, sans-serif;' +
		'Comic Sans MS/Comic Sans MS, cursive;' +
		'Courier New/Courier New, Courier, monospace;' +
		'Georgia/Georgia, serif;' +
		'Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;' +
		'Tahoma/Tahoma, Geneva, sans-serif;' +
		'Times New Roman/Times New Roman, Times, serif;' +
		'Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;' +
		'Verdana/Verdana, Geneva, sans-serif';
};
