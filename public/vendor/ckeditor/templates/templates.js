CKEDITOR.addTemplates(
'default',
{
    //圖片資料夾路徑，放在同目錄的images資料夾內
    imagesPath: ('../../../assets/images/'),
    templates: [
    {
        //標題
        title: '組織架構新增',
        image: 'logo.png',//圖片
        description: '內容可編輯', //樣板描述
        //自訂樣板內容
        html: '<div className="col-12 col-md-6">'+
              '<div className="text-shadow text-shadow-cc">' +
              '<ul>' +
              '<li className="d-flex align-items-start"><span className="invest-subtitle">・職稱：</span><span>執行長</span></li>' +
              '<li className="d-flex align-items-start"><span className="invest-subtitle">・姓名：</span><span>林清封</span></li>' +
              '<li className="d-flex align-items-start"><span className="invest-subtitle">・經歷：</span> <span>成功大學材料博士<br/>' +
              '美國愛荷華州立大學化工博士<br/>' +
              '永剛科技(股)公司執行副總經理<br/>' +
              '立敦科技(股)公司執行副總經理</span></li>' +
              '</ul>' +
              '</div>' +
              '</div>'
    },
    //第二個樣板
    {
        title: 'MyTemplate222',
        image: 'logo.png',
        description: '內容可編輯',
        html: '<div cellspacing="0" cellpadding="0" style="width:100%" border="0">'+
              '<tr><td style="width:50%"><h3>Title 1</h3></td>'+
              '<td></td><td style="width:50%"><h3>Title 2</h3></td>'+
              '</tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></div>'+
              '<p>More text goes here.</p>'
    }
    ]
});
