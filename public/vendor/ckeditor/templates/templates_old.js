CKEDITOR.addTemplates(
'default',
{
    //圖片資料夾路徑，放在同目錄的images資料夾內
    imagesPath: ('../../../assets/images/'),
    templates: [
    {
        //標題
        title: 'MyTemplate1',
        image: 'logo.png',//圖片
        description: '內容可編輯', //樣板描述
        //自訂樣板內容
        html: '<h3>我的標題</h3>'+
              '<p>Type the text here</p>' +
              '<div><img src="assets/images/logo.png" />' +
              '</div>'  
    },
    //第二個樣板
    {
        title: 'MyTemplate222',
        image: 'logo.png',
        description: '內容可編輯',
        html: '<div cellspacing="0" cellpadding="0" style="width:100%" border="0">'+
              '<tr><td style="width:50%"><h3>Title 1</h3></td>'+
              '<td></td><td style="width:50%"><h3>Title 2</h3></td>'+
              '</tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></div>'+
              '<p>More text goes here.</p>'
    }
    ]
});