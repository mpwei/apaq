var basHref = $('base').attr("href"),
	nameGroup = $('input[name="method"]').val(),
	_token = $('meta[name="csrf-token"]').attr('content');

//新增項目
$('a[id="addItem"]').click(
	function()
	{
		//目前有幾筆資料
		var trCount = $('tbody[id="itemTbody"] tr').size(),
			table_tags = $(this).data('tags'), 
		    FormStr;



		if ($('tr[id="is_no_data"]').size() > 0) 
		{

			$('tr[id="is_no_data"]').remove();
		}

		table_tags = table_tags.split(",");

		FormStr += '<tr>';
			for (var i = 0; i < table_tags.length; i++) {
				//圖片
				if (table_tags[i] == 'image' || table_tags[i] == 'big_image') 
				{
					
					FormStr += '<td><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"></div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new">Select image </span><span class="fileinput-exists">Change </span><input type="text" name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" id="filePicker"></span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a></div></div></td>';
				}else
				{
					//文字欄位
					FormStr +='<td><input name="'+nameGroup+'['+trCount+']['+table_tags[i]+']" value="" /></td>';
				}
			}

			FormStr +='<td><a href="javascript:;" class="btn red btn-sm" id="btnDelete_'+trCount+'"><i class="fa fa-times"></i></a></td>';

		FormStr += '</tr>';
		$('tbody[id="itemTbody"]').append(FormStr);

		//觸發檔案管理
		FilePicke();
	}
);

//刪除
$('a[id^="btnDelete_"]').click(function(event) {
	var thisID = $(this).attr('id'),thisNum;

		thisNum = thisID.split("_");

	if (confirm('刪除將會造成資料遺失，確定要刪除嗎？')) 
	{
		
		var dataID = $('#thisID_'+thisNum[1]).val(),
			workID = $("input[name='id']").val();

		$(this).parent('td').parent('tr').remove();

		$.ajax({
			url: basHref+'backen-admin/WorkPhoto/Delete/',
			type: 'POST',
			data: {
				work_id : workID,
				photo_id : dataID,
				_token : _token
			},
		})
		.done(function(data) {
			console.log("data : "+data);
			console.log("Work Photo Delete success");
		})
		.fail(function(data) {
			console.log();
			console.log("Work Photo Delete error");
		});

	}else
	{
		return false;
	}
});