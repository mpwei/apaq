var FilePicke = function ()
{
	$('input[id="filePicker"], a[id="multi-add"] , a[id="filePicker"]' ).on('click', function(event) {

		//console.log("file Picker : "+$(this).attr('name'));

		//var link = '/vendor/elfinder-2.0/elfinder.php';
		var link = 'http://www.xwadex.com/calitech/vendor/elfinder-2.0/elfinder.php';
		$('a[id="toFileManager"]').attr('href', '');

		if ($(this).attr('id') == 'multi-add')
		{
			//console.log("選擇批次新增");
			var name = $(this).data('name');

			$('a[id="toFileManager"]').attr('href', link+'?name=' + name + '&type=multi');

			console.log("送出的連結 多選 ： "+link+'?name='+name);
		}else
		{
			name = ( !$.isEmptyObject($(this).prop('name') ) )? $(this).attr('name') : $(this).data('name') ;
			//console.log( name);
			$('a[id="toFileManager"]').attr('href', link+'?name=' + name + '&type=single');
			console.log("送出的連結 單選： "+link+'?name='+ name  + '&type=single');
		}


		$('a[id="toFileManager"]').trigger('click');

		//不要讓預設的檔案選擇器打開
		return false;
	});
}

var getReturnFile = function(input, valueFiles, type)
{
	if ($('tr[id="is_no_data"]').size() > 0) 
	{

		$('tr[id="is_no_data"]').remove();
	}
	var files = valueFiles.split(',');
	
	//篩選掉空的
	files = files.filter(Boolean);

	//console.log("要寫入的input : "+input);
	//console.log("要寫入的file : "+files);
	//2015.10.26 修改批次新增也可以選單選, 調整用類型判斷多筆還是單筆

	if( type == 'multi' )
	{
		//console.log("挑選一個檔案以上");
		var el = $('#'+input);
			if (el.is('table')) 
			{
				
				var tags = el.data('tags');//get table tags
				var thisTbody = el.children('tbody[id="itemTbody"]');//要加入的位子
				var trCount = thisTbody.children('tr').size();
				var nameGroup = el.attr('name');
				var FormStr = '';
				var staticArray = ['否','是'];

				tags = tags.split(",");
				console.log("files : "+files);
				console.log("files.length : "+files.length);

				for (var i = 0; i < files.length; i++) {
					FormStr += '<tr>';
						for (var x = 0; x < tags.length; x++) {
							//圖片
							if (tags[x] == 'image' || tags[x] == 'big_image' ) 
							{
								
								FormStr += '<td><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;"><img src="'+files[i]+'"></div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div><div><span class="btn default btn-file"><span class="fileinput-new">Select image </span><span class="fileinput-exists">Change </span><input type="text" name="'+nameGroup+'['+trCount+']['+tags[x]+']" id="filePicker" value="'+files[i]+'"></span><a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Remove </a></div></div></td>';
							}else if(tags[x] == 'rank')
							{
								FormStr += '<td align="center"><h1>'+(trCount+1)+'</h1><input type="hidden" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+trCount+'" /></td>';
							}
							else if( tags[x].endsWith('_color') )//color picker
							{
								FormStr +='<td><input class="mycolor" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="" /></td>';

							}
							else if(tags[x] == 'features')
							{
								FormStr +='<td><textarea class="wysihtml5 form-control" name="'+nameGroup+'['+trCount+']['+tags[x]+']" rows="6" ></textarea></td>';
							}
							else if( tags[x].startsWith('is_') )//radio
							{
								FormStr +='<td>';
								for (var k = 0; k < staticArray.length; k++) {
									if( k == 1)
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" checked="checked" />'+staticArray[k]+'</label>';
									}
									else
									{
										FormStr +='<label class="radio-inline"><input type="radio" name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="'+k+'" />'+staticArray[k]+'</label>';	
									}
									
								}
								FormStr +='</td>';

							}
							else
							{
								//文字欄位
								FormStr +='<td><input name="'+nameGroup+'['+trCount+']['+tags[x]+']" value="" /></td>';
							}
						}
						FormStr +='<td><input type="hidden" name="'+nameGroup+'['+trCount+'][id]" value="" /><a href="javascript:;" name="rank_up"  class="btn purple" data-id="" data-rank="'+trCount+'" data-model="'+nameGroup+'"  ><i class="fa fa-level-up"></i></a><a href="javascript:;" name="rank_down" class="btn purple" data-id="" data-rank="'+trCount+'" data-model="'+nameGroup+'"><i class="fa fa-level-down"></i> </a><a href="javascript:;" class="btn red" id="dataDel"  data-id="" data-model="'+nameGroup+'"><i class="fa fa-times"></i></a></td>';
					FormStr += '</tr>';
					trCount++;
				}

				thisTbody.append(FormStr);
				//觸發檔案管理
				FilePicke();
				//觸發資料刪除
				photoDelete();
				//觸發排序
				Rank();
				//觸發color picker
				colorPicker();
			}else
			{
				console.log("這個元素不是Table");
			}


	}else
	{
		var thisInput = $('input[name="'+input+'"]');

		//console.log("挑選單個檔案");
		//console.log("input name "+ thisInput.prop('name'));
		//console.log("file name : "+files);
		//console.log("this input is : "+thisInput.size());

		//改變預覽圖
		var changImg = thisInput.parent('span').parent('div').parent('div').children('div[id="showPic"]').children('img');

		//console.log("更換預覽圖 ： "+changImg.attr("src"));
		changImg.prop('src',"/"+files)
		//thisInput.prop('value', "/"+files);
		thisInput.prop('value', files);
	}
	

}