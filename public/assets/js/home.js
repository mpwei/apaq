$(document).ready(function() {
    $('.owl-carousel1').owlCarousel({
        animateOut: 'fadeIn',
        items: 1,
        nav: true,
        dots: false,
        autoplay: true,
        loop: true,
        navText: ["", ""],
        navContainer: '#idxbnNav'
    });
    $('.owl-carousel2').owlCarousel({
        items: 3,
        nav: true,
        navText: ["", ""],
        loop: true,
        dots: false,
        autoplay: true,
        margin: 24,
        navContainer: '#customNav',
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            450: {
                items: 2,
                margin: 16
            },
            768: {
                items: 3
            }
        }
    });

    // init controller
    var controller = new ScrollMagic.Controller();

    // create a scene
    new ScrollMagic.Scene({
            triggerElement: ".idx-about-wrap",
            offset: 30
        })
        .setClassToggle(".idx-core-logo", "animation")
        .addTo(controller); // assign the scene to the controller
    var scene01 = new ScrollMagic.Scene({
            triggerElement: ".idx-achieve-item1",
            offset: 80
        })
        .setClassToggle(".stat-count", "timer")
        .reverse(false)
        .addTo(controller); // assign the scene to the controller

    scene01.on("enter", function(event) {
        $('.timer').countTo();
    });
    var scene02 = new ScrollMagic.Scene({
            triggerElement: ".idx-achieve-item2",
            offset: 80
        })
        .setClassToggle(".stat-count2", "timer")
        .reverse(false)
        .addTo(controller); // assign the scene to the controller

    scene02.on("enter", function(event) {
        $('.timer').countTo();
    });

    if ($('.owl-horizontal-timeline').length > 0) {
        var owl_tl = $(".owl-timeline");
        var owl_thumbs = $('.owl-thumbs');
        var thumbs_items = $('.owl-thumbs .owl-item');
        var syncedSecondary = true;
        if (owl_tl.length > 0) {
            owl_tl.owlCarousel({
                    items: 1,
                    nav: true,
                    dots: false,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    loop: true,
                    navText: ["", ""],
                    navContainer: '#owl-timeline-nav'
                })
                .on("changed.owl.carousel", syncPosition);

            owl_thumbs.on("initialized.owl.carousel", function() {
                owl_thumbs.find(".owl-item").eq(0).addClass("current").trigger('to.owl.carousel', [1, 100]);
            }).owlCarousel({
                loop: false,
                nav: false,
                dots: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 4
                    },
                    1000: {
                        items: 6
                    }
                }
            })

            function syncPosition(el) {
                //if loop is set to false, then you have to uncomment the next line
                //var current = el.item.index;
                //to disable loop, comment this block
                var count = el.item.count - 1;
                var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

                if (current < 0) {
                    current = count;
                }
                if (current > count) {
                    current = 1;
                }
                owl_thumbs
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                var onscreen = owl_thumbs.find(".owl-item.active").length;
                var start = owl_thumbs
                    .find(".owl-item.active")
                    .first()
                    .index();
                var end = owl_thumbs
                    .find(".owl-item.active")
                    .last()
                    .index();
                if (current > end) {
                    owl_thumbs.data("owl.carousel").to(current, 100, true);
                }
                if (current < start) {
                    console.log('current > start ' + current + " > " + start);
                    owl_thumbs.data("owl.carousel").to(current - onscreen, 100, true);
                }
            }

            owl_thumbs.on("click", ".owl-item", function(e) {
                e.preventDefault();
                owl_tl.trigger('to.owl.carousel', [$(this).index(), 300]);
            });

        }
    }

    $('.mobile-search-btn').on('click', function(e) {
        $('input[name="key"]').val($('#mobilesearchkey').val());
        $('#search').submit();
    });
});

window.onload = function() {
    var elem = $('.page-loader');
    if (elem.length > 0) {
        setTimeout(function() {
            $('body.unprepared').removeClass("unprepared");
            elem.addClass("fadeOutDown");
        }, 650);
    }

};