function KENContactUs() {
	/*** Constructor start ***/
	var KENContactUs = this,
		ContactForm = $('#active');

	init();

	/*** Constructor end ***/
	function init() {
		initEvent($(this));
	}

	function initEvent(el) {
		$('#sub').on('click', function(e) {
			e.preventDefault();
			if(checkData()) {
				$.ajax({
					'type' : 'POST',
					'data' : $('#active').serialize(),
					'url' : location.pathname + '/save',
					'dataType' : 'json',
					'error' : function(xhr) {
						console.log(xhr);
					},
					'success' : function(oJson) {
						if(oJson.an) {
		                    $('.successMessage').fadeIn();
							$('#changeCap img').click();
							$( "#contact-form-with-recaptcha input").each(function( index ) {
								if($( this ).attr('name') != '_token' || $( this ).attr('type') != 'submit')
									$( this ).val('');
							});
						} else {
							$('#changeCap img').click();
							alert(oJson.message);
						}

					}
				});
			}
		});

		function checkData() {
			console.log(1222);
			Formobj = ContactForm.serializeArray();
			var time = false;
			var testpro = false;
			var eat = false;
			var ok = false;
			$.each( Formobj, function( key, row ) {
				switch(row.name) {
					case 'Contact[company]':
						if(row.value == '') {
							alert("請填寫公司");
							ok = true;
							return false;
						}
						break;
					case 'Contact[name]':
						if(row.value == '') {
							alert("請填寫姓名");
							ok = true;
							return false;
						}
						break;
					case 'Contact[tel]':
						if(row.value == '') {
							alert("請填寫手機/電話");
							ok = true;
							return false;
						}
						break;

					case 'Contact[email]':
						if(row.value == '') {
							alert("請填寫電子信箱");
							ok = true;
							return false;
						}else{
							if (!IsEmail($('#email').val())) 
							{
								alert("信箱格式錯誤");
								ok = true;
								return false;
							}
						}
						break;
					case 'Contact[message]':
						if(row.value == '') {
							alert("請填寫內容");
							ok = true;
							return false;
						}
						break;
					case 'captcha':
						if(row.value == '') {
							alert("請填寫驗證碼");
							ok = true;
							return false;
						}
						break;
				       break;
				}
			});

			if(ok) {
				return false;
			}

			return true;
		}
		function IsEmail(email) {
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		}
	}

}

$(function() {
	$('.changeCap img').on('click', function(e) {
		var d = new Date();
		$(this).attr("src",$(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds() );
	});

	$('input[name="Contact[inquiry_type]"]').on('click', function(e) {
		if($('input[name="Contact[inquiry_type]"]:checked').val() == 1){
			$('.contact_product').hide();
			$('#comment').attr('placeholder','相關說明');
		}else if($('input[name="Contact[inquiry_type]"]:checked').val() == 2){
			$('.contact_product').show();
			$('#comment').attr('placeholder','相關說明');
		}else if($('input[name="Contact[inquiry_type]"]:checked').val() == 3){
			$('.contact_product').hide();
			$('#comment').attr('placeholder','請描述產品/機台狀況或其他說明');

		}
	});
	
	var KEN_ContactUs = new KENContactUs();
});