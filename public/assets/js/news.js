$(document).ready(function () {
    if($('.cc-grid').length > 0 ) {
        // init Isotope
        var $grid = $('.cc-grid').isotope({});
        // filter items on button click
        $('.filter-button-group').on( 'click', 'a', function() {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });
        });
    }
});