$( document ).ready(function() {
    new WOW().init();
    $("#btn-main-menu").click(function(){
        var e = $(this)
        if(!e.hasClass('active')) {
            $("#btn-main-menu").each(function(idx, elm){
                $(elm).removeClass('active');
            });

            e.addClass('active');
            if(!$('body').hasClass('menu-opened'))
                $('body').addClass('menu-opened');

            // $('aside-btn')
        } else {
            closeAside();
        }
    });
    $("#main-menu-close").click(function(){
        closeAside();
    });
    $("#btn-close-mainmenu").click(function(){
        closeAside();
    });
    
    function closeAside() {
        $("#btn-main-menu").each(function(idx, elm){
            $(elm).removeClass('active');
        });
        $('body').removeClass('menu-opened');
    }

    $("#btn-main-menu").on("mouseenter", function() {
        if(!$('body').hasClass("menu-opened")) {
            var tl = new TimelineLite();
            var el = $(this);
            var bars = el.find(".icon-bar");
            console.log('tst');
            bars.removeAttr("style");
            tl.add("start")
            .staggerFromTo(bars, 0.1, {
                left: "-40px",
            }, {
                left: "0",
                opacity: 1,
                ease: Power4.ease,
            }, 0.1, "+=0.2");
        }
    });
});