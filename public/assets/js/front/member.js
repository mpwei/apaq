
$('#member_btn').on('click', function(e) {
	e.preventDefault();
	var d = {};
	d['_token'] = $('#memberForm input[name="_token"]').val();
	d['first_name'] = $('#memberForm input[name="first_name"]').val();
	d['password'] = $('#memberForm input[name="password"]').val();
	d['check_password'] = $('#memberForm input[name="check_password"]').val();
	d['phone'] = $('#memberForm input[name="phone"]').val();
	d['address'] = $('#memberForm input[name="address"]').val();
	d['Member_front'] = 1;

	$.ajax({
		'type' : 'POST',
		'data' : d,
		'url' : 'zh-tw/member/update',
		'dataType' : 'json',
		'error' : function(xhr) {
			alert('Error');
		},
		'success' : function(oJson) {
			if(oJson.an){
				//$("#MessageSent").removeClass("hidden-xs-up").hide(2000);
				$("#MessageSent").show(500).delay(1000).hide(2000);
				$("#MessageNotSent").hide();
				$(".submit-button").removeClass("btn-default").addClass("btn-success").prop('value', 'Message Sent');

			}else{
				$("#MessageNotSent").show(500).delay(1000).hide(2000);
				$("#MessageSent").hide();
			}
		}
	});
});

$('#member_cancel').on('click', function(e) {
	memberForm.reset();
});