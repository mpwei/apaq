$('.addcarbtn').on('click', function(e) {
	e.preventDefault();
	var d = {};
	d['_token'] = $('input[name="_token"]').val();
	d['item_id'] = $('input[name="item_no"]').val();
	d['specId'] = $('select[name="specId"]').val();
	d['quantity'] = $('input[name="quantity"]').val();
	d['show_color'] = $('input[name="product_color"]:checked').val();
	d['show_size'] = $('input[name="product_size"]:checked').val();

	var btnType = $(this).attr("type");
	$.ajax({
		'type' : 'POST',
		'data' : d,
		'url' : 'zh-tw/AddToShoppingBag',
		'dataType' : 'json',
		'error' : function(xhr) {
			alert('Error');
		},
		'success' : function(oJson) {
			
			if(oJson.message) {
				if(btnType == 'billing') {
					window.location.href='./zh-tw/billing'; 
				} else {
					alert(oJson.message);
					$('.lcart').html(Object.keys(oJson.list).length);
					$('.cart-area').html(oJson.carHtml);
				}
			}
			else {
				if(btnType == 'billing') {
					window.location.href='./zh-tw/billing'; 
				} else {
					alert('已加入詢問車');
					$('.lcart').html(Object.keys(oJson.list).length);
					$('.cart-area').html(oJson.carHtml);
				}
				
			}
			// if(btnType == 'billing') {
			// 		window.location.href='./zh-tw/billing'; 
			// } 
		}
	});
	
});

$('#gocheckout').on('click', function(e) {
	window.location.href='./zh-tw/billing'; 
});