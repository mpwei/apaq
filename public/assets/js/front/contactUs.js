// if($("#contact-form-with-recaptcha-cc").length>0) {
//     $("#contact-form-with-recaptcha-cc").validate({
//         submitHandler: function(form) {
//             $('.submit-button').button("loading");
//             $.ajax({
//                 type: "POST",
//                 url: "php/email-sender-recaptcha.php",
//                 data: {
//                     "name": $("#contact-form-with-recaptcha #name").val(),
//                     "email": $("#contact-form-with-recaptcha #email").val(),
//                     "message": $("#contact-form-with-recaptcha #message").val(),
//                     "g-recaptcha-response": $("#g-recaptcha-response").val()
//                 },
//                 dataType: "json",
//                 success: function (data) {
//                     if (data.sent == "yes") {
//                         $("#MessageSent").removeClass("hidden-xs-up");
//                         $("#MessageNotSent").addClass("hidden-xs-up");
//                         $("#contact-form-with-recaptcha .form-control").each(function() {
//                             $(this).prop('value', '').parent().removeClass("has-success").removeClass("has-error");
//                         });
//                     } else {
//                         $("#MessageNotSent").removeClass("hidden-xs-up");
//                         $("#MessageSent").addClass("hidden-xs-up");
//                     }
//                 }
//             });
//         },
//         errorPlacement: function(error, element) {
//             error.insertBefore( element );
//         },
//         onkeyup: false,
//         onclick: false,
//         rules: {
//             name: {
//                 required: true,
//                 minlength: 2
//             },
//             email: {
//                 required: true,
//                 email: true
//             },
//             tel: {
//                 required: true
//             },
//             message: {
//                 required: true,
//                 minlength: 10
//             }
//         },
//         messages: {
//             name: {
//                 required: "Please specify your name",
//                 minlength: "Your name must be longer than 2 characters"
//             },
//             email: {
//                 required: "We need your email address to contact you",
//                 email: "Please enter a valid email address e.g. name@domain.com"
//             },
//             tel: {
//                 required: "We need your phone number to contact you"
//             },
//             message: {
//                 required: "Please enter a message",
//                 minlength: "Your message must be longer than 10 characters"
//             }
//         },
//         errorElement: "span",
//         highlight: function (element) {
//             $(element).parent().removeClass("has-success").addClass("has-error");
//             $(element).siblings("label").addClass("hide");
//         },
//         success: function (element) {
//             $(element).parent().removeClass("has-error").addClass("has-success");
//             $(element).siblings("label").removeClass("hide");
//         }
//     });
// };
//
// $(function() {
//     $('.changeCap img').on('click', function(e) {
//         var d = new Date();
//         $(this).attr("src", $(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds());
//     });
//     var KEN_ContactUs = new KENContactUs();
// });
//
// if($("#contact-form-with-recaptcha-cc").length>0) {
//     $("#contact-form-with-recaptcha-cc").validate({
//         submitHandler: function(form) {
//             $('.submit-button').button("loading");
//             $.ajax({
//                 type: "POST",
//                 url: "php/email-sender-recaptcha.php",
//                 data: {
//                     "name": $("#contact-form-with-recaptcha #name").val(),
//                     "email": $("#contact-form-with-recaptcha #email").val(),
//                     "message": $("#contact-form-with-recaptcha #message").val(),
//                     "g-recaptcha-response": $("#g-recaptcha-response").val()
//                 },
//                 dataType: "json",
//                 success: function (data) {
//                     if (data.sent == "yes") {
//                         $("#MessageSent").removeClass("hidden-xs-up");
//                         $("#MessageNotSent").addClass("hidden-xs-up");
//                         $("#contact-form-with-recaptcha .form-control").each(function() {
//                             $(this).prop('value', '').parent().removeClass("has-success").removeClass("has-error");
//                         });
//                     } else {
//                         $("#MessageNotSent").removeClass("hidden-xs-up");
//                         $("#MessageSent").addClass("hidden-xs-up");
//                     }
//                 }
//             });
//         },
//         errorPlacement: function(error, element) {
//             error.insertBefore( element );
//         },
//         onkeyup: false,
//         onclick: false,
//         rules: {
//             name: {
//                 required: true,
//                 minlength: 2
//             },
//             email: {
//                 required: true,
//                 email: true
//             },
//             tel: {
//                 required: true
//             },
//             message: {
//                 required: true,
//                 minlength: 10
//             }
//         },
//         messages: {
//             name: {
//                 required: "Please specify your name",
//                 minlength: "Your name must be longer than 2 characters"
//             },
//             email: {
//                 required: "We need your email address to contact you",
//                 email: "Please enter a valid email address e.g. name@domain.com"
//             },
//             tel: {
//                 required: "We need your phone number to contact you"
//             },
//             message: {
//                 required: "Please enter a message",
//                 minlength: "Your message must be longer than 10 characters"
//             }
//         },
//         errorElement: "span",
//         highlight: function (element) {
//             $(element).parent().removeClass("has-success").addClass("has-error");
//             $(element).siblings("label").addClass("hide");
//         },
//         success: function (element) {
//             $(element).parent().removeClass("has-error").addClass("has-success");
//             $(element).siblings("label").removeClass("hide");
//         }
//     });
// };
//
// $(function() {
//     $('.changeCap img').on('click', function(e) {
//         var d = new Date();
//         $(this).attr("src", $(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds());
//     });
//     var KEN_ContactUs = new KENContactUs();
// });

function KENContactUs() {
    /*** Constructor start ***/
    var KENContactUs = this,
        ContactForm = $('#active');
    init();
    /*** Constructor end ***/
    function init() {

        initEvent();
    }

    function initEvent() {
        $('#active input[type=submit]').on('click', function(e) {
            e.preventDefault();

            if (checkData()) {
                alert('success');
                $.ajax({
                    'type': 'POST',
                    'data': $('#active').serialize(),
                    'url': location.pathname + '/save',
                    'dataType': 'json',
                    'error': function(xhr) {
                        console.log(xhr);
                    },
                    'success': function(oJson) {
                        if (oJson.an) {
                            alert(oJson.message);
                            $('#changeCap img').click();
                            location.reload();
                        } else {
                            $('#changeCap img').click();
                            alert(oJson.message);
                        }
                    }
                });
            }
        });

        function checkData() {
            Formobj = ContactForm.serializeArray();
            var time = false;
            var testpro = false;
            var eat = false;
            var ok = false;
            console.log(time);
            $.each(Formobj, function(key, row) {
                switch (row.name) {
                    case 'Contact[name]':
                        if (row.value == '') {
                            alert("請填寫姓名");
                            ok = true;
                            return false;
                        }
                        break;
                    case 'Contact[tel]':
                        if (row.value == '') {
                            alert("請填寫電話");
                            ok = true;
                            return false;
                        }
                        break;

                    case 'Contact[country]':
                        if (row.value == '') {
                            alert("請填寫國家");
                            ok = true;
                            return false;
                        }
                        break;

                    case 'Contact[email]':
                        if (row.value == '') {
                            alert("請填寫信箱");
                            ok = true;
                            return false;
                        } else {
                            if (!IsEmail($('#email').val())) {
                                console.log($('#email').val());
                                alert("信箱格式錯誤");
                                ok = true;
                                return false;
                            }
                        }
                        break;
                    case 'Contact[address]':
                        if (row.value == '') {
                            alert("請填寫地址");
                            ok = true;
                            return false;
                        }
                        break;


                    case 'captcha':
                        if (row.value == '') {
                            alert("請填寫驗證碼");
                            ok = true;
                            return false;
                        }
                        break;
                    case 'Contact[time][]':
                        time = true;
                        break;
                    case 'Contact[testpro][]':
                        testpro = true;
                        break;
                    default:
                        break;
                }
            });

            if (ok) {
                return false;
            }

            return true;
        }

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    }

}

$(function() {
    $('.changeCap img').on('click', function(e) {
        var d = new Date();
        $(this).attr("src", $(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds());
    });
    var KEN_ContactUs = new KENContactUs();
});
