$('.remove_spec_list').on('click', function(e) {
	e.preventDefault();
	var d = {};
	d['_token'] = $('input[name="_token"]').val();
	d['key'] = $(this).attr('type');
	//console.log($(this).parent().parent('tr'));
	//$(this).parent().parent('tr').remove();
	var jRemove = $(this).closest("tr");
	$.ajax({
		'type' : 'POST',
		'data' : d,
		'url' : location.pathname.split("/")[1] + '/RemoveFromShoppingBag',
		'dataType' : 'json',
		'error' : function(xhr) {
			console.log(xhr);
		},
		'success' : function(oJson) {
			console.log(oJson);

			// 刪除清單
			jRemove.remove();

			// 設定總額
			$('.amountSum').html('NTD ' + oJson.amountSum + ' ');
			$('.totalSum').html('NTD ' + oJson.totalSum + ' ');
			$('.shipp_cost').html(oJson.shipping_cost);
            $('.cart-area').html(oJson.carHtml);
            $('.lcart').html(Object.keys(oJson.list).length);
		}
	});
});

(function($) {

 

        $('#shipping a[type=submit]').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                'type' : 'POST',
                'data' : $('#shipping').serialize(),
                'url' : location.pathname.split("/")[1] + '/updFromShoppingBag',
                'dataType' : 'json',
                'error' : function(xhr) {
                    console.log(xhr);
                },
                'success' : function(oJson) {
                    if(oJson.an) {
                        alert(oJson.message);
                        $('.cart-area').html(oJson.carHtml);
                        
                    } else {
                        $('#changeCap img').click();
                        alert(oJson.message);
                    }

                }
            });
            
        });

    function updSpecQty(key,val,itemPrice) {//Overriding to init

        var d = {};
        d['_token'] = $('input[name="_token"]').val();
        d['key'] = key;
        d['quantity'] = val;
        $.ajax({
            'type' : 'POST',
            'data' : d,
            'url' : location.pathname.split("/")[1] + '/updFromShoppingBag',
            'dataType' : 'json',
            'error' : function(xhr) {
                console.log(xhr);
            },
            'success' : function(oJson) {
                // 設定總額
                var quantity = parseInt(oJson['list'][d['key']]['quantity']);
                var price = parseInt(oJson['list'][d['key']]['price']);
                var t = quantity * price;
                console.log(oJson);
                itemPrice.html('NTD ' + t);
                $('.amountSum').html('NTD ' + oJson.amountSum);
                $('.totalSum').html('NTD ' + oJson.totalSum);
				$('.shipp_cost').html('NTD ' + oJson.shipping_cost);
            }
        });
        
    };
    
   
})(jQuery);