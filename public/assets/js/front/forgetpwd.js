function Forget() {
	/*** Constructor start ***/
	var Forget = this,
		ForgetForm = $('#active');

	init();
	/*** Constructor end ***/
	function init() {
		initEvent();
	}

	function initEvent() {
		$('#changeCap img').on('click', function(e) {
			var d = new Date();
			$(this).attr("src",$(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds() );
		});

		$('#forgetBtn').on('click', function(e) {
			e.preventDefault();
			console.log($('#active').serialize());

			if(checkData()) {
				$.ajax({
					'type' : 'POST',
					'data' : $('#active').serialize(),
					'url' : location.pathname,
					'dataType' : 'json',
					'error' : function(xhr) {
						console.log(xhr);
					},
					'success' : function(oJson) {
						if(oJson.an) {
							alert(oJson.message);
							$('#changeCap img').click();
							$( "#active input").each(function( index ) {
								if($( this ).attr('name') != '_token' && $( this ).attr('name') != 'forbtn')
									$( this ).val('');
							});
						} else {
							$('#changeCap img').click();
							alert(oJson.message);
						}

					}
				});
			}
		});

		function checkData() {
			Formobj = ForgetForm.serializeArray();
			var ok = false;
			$.each( Formobj, function( key, row ) {
				// alert( key + ": " + value );
				switch(row.name) {
					case 'Forget[email]':
						if(row.value == '') {
							alert("請填寫信箱");
							ok = true;
							return false;
						}
						break;
					case 'Forget[phone]':
						if(row.value == '') {
							alert("請填寫電話");
							ok = true;
							return false;
						}
						break;
					
					case 'captcha':
						if(row.value == '') {
							alert("請填寫驗證碼");
							ok = true;
							return false;
						}
						break;
				    default:
				       break;
				}
			});

			if(ok) {
				console.log('checkok');
				return false;
			}

			return true;
		}
	}

}

$(function() {
	var Forgetpwd = new Forget();
});