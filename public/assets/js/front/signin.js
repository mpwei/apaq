
$('#signBtn').on('click', function(e) {
	e.preventDefault();
	var d = {};
	d['_token'] = $('#FormSign input[name="_token"]').val();
	d['email'] = $('#FormSign input[name="email"]').val();
	d['password'] = $('#FormSign input[name="password"]').val();

		if( $('#FormSign input[name="email"]').val() == '') {

			alert('請輸入Email ');

			return 0;

		}

		if( $('#FormSign input[name="password"]').val() == '') {

			alert('請輸入密碼');

			return 0;

		}

	$.ajax({
		'type' : 'POST',
		'data' : d,
		'url' : 'zh-tw/sign-in',
		'dataType' : 'json',
		'error' : function(xhr) {
			alert('Error');
			console.log(xhr);
		},
		'success' : function(oJson) {
			if(oJson.an){
			alert(oJson.message);
			window.location.assign(window.location.href);
			}else{
				alert(oJson.message);
			}
		}
	});
});


$('#registrBtn').on('click', function(e) {
	e.preventDefault();
	var d = {};
	
		if( $('#FormRegist input[name="first_name"]').val() == '') {

			alert('請輸入姓名');

			return 0;

		}

		if( $('#FormRegist input[name="email"]').val() == '') {

			alert('請輸入Email ');

			return 0;

		}

		if( $('#FormRegist input[name="password"]').val() == '') {

			alert('請輸入密碼');

			return 0;

		}

		if( $('#FormRegist input[name="check_password"]').val() == '') {

			alert('請再次輸入密碼');

			return 0;

		}else{
			if($('#FormRegist input[name="check_password"]').val() != $('#FormRegist input[name="password"]').val()){
				alert('再次輸入密碼需與密碼相同');
			return 0;
			}
		}
	d['_token'] = $('#FormRegist input[name="_token"]').val();

	d['first_name'] = $('#FormRegist input[name="first_name"]').val();
	d['email'] = $('#FormRegist input[name="email"]').val();
	d['password'] = $('#FormRegist input[name="password"]').val();
	d['check_password'] = $('#FormRegist input[name="check_password"]').val();



	$.ajax({
		'type' : 'POST',
		'data' : d,
		'url' : 'zh-tw/register',
		'dataType' : 'json',
		'error' : function(xhr) {
			alert('Error');
		},
		'success' : function(oJson) {
			console.log(oJson);
			alert(oJson.message);
		}
	});
});

    $('#changeCap img').on('click', function(e) {
      var d = new Date();
      $(this).attr("src",$(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds() );
    });
