



	$('#signBtn').on('click', function(e) {

		e.preventDefault();

		var d = {};

		d['_token'] = $('#checkoutForm input[name="_token"]').val();



		d['email'] = $('#loginModal input[name="email"]').val();

		d['password'] = $('#loginModal input[name="password"]').val();

		

		$.ajax({

			'type' : 'POST',

			'data' : d,

			'url' : 'zh-tw/sign-in',

			'dataType' : 'json',

			'error' : function(xhr) {

				alert('Error');

				console.log(xhr);

			},

			'success' : function(oJson) {

				if (oJson.an){

					$('#order_btn').attr('status','login');

					$('#loginModal').modal('hide');

					$('#form-email').prop('disabled', true);

					$('#isEmail').attr('status','ok');

					$('.divPassword').hide();

					$('#loginModalbtn').hide();

					$('#form-name').val(oJson.Member.first_name);

					$('#form-phone').val(oJson.Member.phone);

					$('#form-email').val(oJson.Member.email);

					$('#form-address').val(oJson.Member.address);

				} else {

					alert('帳號密碼錯誤，請重新輸入。');

				}

			}

		});

	});



	$('#order_btn').on('click', function(e) {

		e.preventDefault();

		console.log($(this).attr('status'));

		if( $('#form-name').val() == '') {

			alert('請輸入姓名');

			return 0;

		}

		if( $('#form-email').val() == '') {

			alert('請輸入信箱');

			return 0;

		}

		if( $('#form-phone').val() == '') {

			alert('請輸入電話');

			return 0;

		}

		if( $('#form-address').val() == '') {

			alert('請輸入地址');

			return 0;

		}




		if($(this).attr('status') == 'login') {

			shipping();

		} else {

			registr();

		}

	});



		$( "#form-email" ).focusout(function() {

			if($("#form-email").val() != ''){

			var d = {};

			d._token = $('#billing-information input[name="_token"]').val();

			d.email = $( "#form-email" ).val();



			$.ajax({

				'type' : 'POST',

				'data' : d,

				'url' : location.pathname.split("/")[1] + '/checkMail',

				'dataType' : 'json',

				'error' : function(xhr) {

					alert('Error');

					console.log(xhr);

				},

				'success' : function(oJson) {

					if(oJson.count == 0) {

						$('#isEmail').show();

						$('#isEmail').html('帳號可使用');

						$('#isEmail').attr('status','ok');

						$('#isEmail').addClass('bg-success').removeClass('bg-danger');

					} else {

						$('#isEmail').show();

						$('#isEmail').html('帳號已存在');

						$('#isEmail').attr('status','error');

						$('#isEmail').addClass('bg-danger').removeClass('bg-success');

					}

				}

			});

			}

		});

	function registr() {

		if( $('#form-password').val() == '') {

			alert('請輸入密碼');

			return 0;

		}



		var d = {};

		d['_token'] = $('#billing-information input[name="_token"]').val();

		d['first_name'] = $('#form-name').val();

		d['email'] = $('#form-email').val();

		d['password'] = $('#form-password').val();

		d['check_password'] = $('#form-checkpassword').val();

		d['phone'] = $('#form-phone').val();

		d['address'] = $('#form-address').val();


		$.ajax({

			'type' : 'POST',

			'data' : d,

			'url' : 'zh-tw/register',

			'dataType' : 'json',

			'error' : function(xhr) {

				alert('Error');

			},

			'success' : function(oJson) {

				if(oJson.an == true) {

					//shipping();

					var d = {};

					d['_token'] = $('#billing-information input[name="_token"]').val();

					d['email'] = $('#form-email').val();

					d['password'] = $('#form-password').val();



					console.log(d);

					$.ajax({

						'type' : 'POST',

						'data' : d,

						'url' : 'zh-tw/sign-in',

						'dataType' : 'json',

						'error' : function(xhr) {

							alert('Error');

							console.log(xhr);

						},

						'success' : function(oJson) {

							if (oJson.an){

								$('#order_btn').attr('status','login');

								//$('#loginModal').modal('hide');

								$('#form-email').prop('disabled', true);

								//$('#isEmail').show('status','ok');

							    //$('.divPassword').hide();

								shipping();

							}

						}

					});

				} else {

					alert(oJson.message);

				}

			}

		});

	}



	function shipping() {

		var d = {};

		d._token = $('#billing-information input[name="_token"]').val();

		d.name = $('#form-name').val();

		d.address = $('#form-address').val();

		d.phone = $('#form-phone').val();

		d.email = $('#form-email').val();


		d.memo = $('#form-memo').val();

		d.type = $(' input[name="type"]:checked').val();

		

      $('html').addClass('order-active');

      $('#loading-item').addClass('active');

      console.log(d);

		$.ajax({

			'type' : 'POST',

			'data' : d,

			'url' : location.pathname.split("/")[1] + '/member/shipping',

			'dataType' : 'json',

			'error' : function(xhr) {

				alert('Error');

				console.log(xhr);
      			$('#loading-item').removeClass('active');

			},

			'success' : function(oJson) {

				console.log(oJson);
				if(oJson.an == true){
					if (d.type == 1) {

						window.location.href='./zh-tw/creditpay/' + oJson.order_id;

					} else {

						//atm

						window.location.href='./zh-tw/member/ordercomplete';

					}
				}else{
					alert(oJson.message);
      				$('#loading-item').removeClass('active');
				}

			}

		});

	}


	$(document).ready(function() {
		var payment_items = $('#payment-information .payment-item');
		$('#payment-information').on('click', 'input[type="radio"]', function() {
			var cc = $(this);
			if(cc.is(':checked')){
				payment_items.removeClass('active');
				cc.closest('.payment-item').addClass('active');
			}
		});
	});
