
$(function() {
    $( "#send-Inquiry" ).on('click', function(e) {
        e.preventDefault();
        if( $('#form-name').val() == '') {
            alert('請輸入姓名');
            return 0;
        }
        if( $('#form-email').val() == '') {
            alert('請輸入信箱');
            return 0;
        }
        if( $('#form-phone').val() == '') {
            alert('請輸入電話');
            return 0;
        }
        if( $('#form-memo').val() == '') {
            alert('請輸入詢問內容');
            return 0;
        }
        /*if( $('#captcha').val() == '') {
            alert('請輸入驗證碼');
            return 0;
        }*/

        InquiryAjax();
    })

	$('#changeCap img').on('click', function(e) {
		var d = new Date();
		$(this).attr("src",$(this).attr("src").split('?')[0] + '?t=' + d.getMilliseconds() );
	});

    function InquiryAjax() {
        var d = {};
        d['_token'] = $('input[name="_token"]').val();
        d['name'] = $('#form-name').val();
        d['email'] = $('#form-email').val();
        d['tel'] = $('#form-phone').val();
        d['memo'] = $('#form-memo').val();
        //d['captcha'] = $('#captcha').val();
        $.ajax({
            'type' : 'POST',
            'data' : d,
            'url' : location.pathname.split("/")[1] + '/inquiry',
            'dataType' : 'json',
            'error' : function(xhr) {
                alert('Error');
				$('#changeCap img').click();
            },
            'success' : function(oJson) {
                if(oJson.an) {
					$('#changeCap img').click();
                    alert(oJson.message);
                    window.location.href='./';

                } else {
					$('#changeCap img').click();
                    alert(oJson.message);
                }
            }
        });
    }
});