$(document).ready(function() {
    if ($('.prod-cata-menu-list').length > 0) {
        $('.prod-cata-menu-list a').smoothScroll({
            offset: -50
        });
    }
    // init controller
    if ($('.prod-cata-wrap').length > 0 && $('.prod-cata-pic img').length > 0) {
        var ScrollMagic_controller = new ScrollMagic.Controller();
        $('.prod-cata-pic img').each(function(idx, elem) {
            var scene02 = new ScrollMagic.Scene({
                    triggerElement: this,
                    duration: 500,
                    offset: -80
                })
                .setTween(this, 0.2, {
                    opacity: 1,
                    top: "-160px"
                })
                .addTo(ScrollMagic_controller);
        });
    }
    if ($('.page-aside').length > 0 && $('.inner-main'.length > 0)) {
        var ScrollMagic_controller = new ScrollMagic.Controller();
        var scene02 = new ScrollMagic.Scene({
                triggerElement: ".inner-main"
            })
            .offset(200)
            .setClassToggle(".page-aside", "active")
            .addTo(ScrollMagic_controller);
    }
    $('input[name="Contact[inquiry_type]"]').on('click', function(e) {
        if ($('input[name="Contact[inquiry_type]"]:checked').val() == 2) {
            $('.contact_product').show();
        }
    });

    $('.mobile-search-btn').on('click', function(e) {
        $('input[name="key"]').val($('#mobilesearchkey').val());
        $('#search').submit();
    });
});