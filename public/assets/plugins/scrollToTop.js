
jQuery(window).scroll(function (event) {

    var threshold = jQuery(document).height() - jQuery(window).height() - jQuery('footer').height() + 60;
    if (jQuery(window).scrollTop() > 200) {
        jQuery('#PageTopBtn').css({
            display: "block",
            top: "auto",
            right: "40px",
            bottom: "10px",
            position: "fixed"
        });
        jQuery('div.pagetopbtn .icon-gotop').css({
            opacity: .8
        });
        if ( jQuery(window).scrollTop() >= threshold) {
            jQuery('#PageTopBtn').css({
                top: "-60px",
                right: "40px",
                bottom: "auto",
                position: "absolute"
            });
        } else {
            jQuery('#PageTopBtn').css({
                top: "auto",
                right: "40px",
                bottom: "10px",
                position: "fixed"
            });
        }
    } else {
        jQuery('div.pagetopbtn .icon-gotop').css({
            opacity: 0
        });
        jQuery('#PageTopBtn').css({
            display: "none"
        });
    }
});

jQuery('div.pagetopbtn').click(function () {
    jQuery("html, body").animate({scrollTop:0}, 500, 'swing');
});
