jQuery(document).ready(function($) {

	$.backstretch([
      "http://www.amosdesign.tw/backend/bg1.png", 
      "http://www.amosdesign.tw/backend/bg1.png"
  	], {duration: 3000, fade: 750});
		
});


var Login=function(){var e=function(){$(".login-form").validate({errorElement:"span",errorClass:"help-block",focusInvalid:!1,rules:{username:{required:!0},password:{required:!0},remember:{required:!1}},messages:{username:{required:"Username is required."},password:{required:"Password is required."}},invalidHandler:function(e,r){$(".alert-danger",$(".login-form")).show()},highlight:function(e){$(e).closest(".form-group").addClass("has-error")},success:function(e){e.closest(".form-group").removeClass("has-error"),e.remove()},errorPlacement:function(e,r){e.insertAfter(r.closest(".input-icon"))},submitHandler:function(e){e.submit()}}),$(".login-form input").keypress(function(e){return 13==e.which?($(".login-form").validate().form()&&$(".login-form").submit(),!1):void 0})};return{init:function(){e(),$(".login-bg").backstretch(["http://www.amosdesign.tw/backend/bg1.png","http://www.amosdesign.tw/backend/bg2.png"],{fade:1e3,duration:8e3})}}}();jQuery(document).ready(function(){Login.init()});