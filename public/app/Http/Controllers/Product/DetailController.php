<?php
echo 'YY';
die;
namespace App\Http\Controllers\Product;


/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use App;
use Cache;

/**相關Controller**/
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Backend\MakeItemV2;
/**Models**/

use App\Http\Models\Product\Category;
use App\Http\Models\Product\Item;

class ThemeController extends CRUDBaseController
{

    public $ajaxEditLink = 'Backend/產品/主類別/ajax-list/';

    protected $modelName = "ProductTheme";

    public $index_select_field = ['id','rank','is_visible','is_show_home','title',"en_title"];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '產品/主類別';

    public $viewPreFix = 'Product.Theme';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-danger',
                "showText" => 'S'
            )
        ),
    );

    protected $cacheData = [
        "active"=> true,//是否開啟功能
        "select" =>['id','title'], //如果是空的就預設抓所有欄位
        "order" => "rank",
        "sort" => 'asc',
        'has'=>['Category','Item']
    ]; // 設定黨

    /*===============後台結束=======================*/
    public function index($locale, $theme="",$category='',$item='')
    {

        $d[] = $locale;
        $d[] = $theme;
        $d[] = $category;

        var_dump($d);
        


    }

}
