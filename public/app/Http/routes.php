<?php
use App\Http\Controllers\Backend\BackendController as myBackEnd;
use ItemMaker as Items;

//轉到預設語系
Route::get('/',function(){
	return redirect( Items::url('/'.App::getLocale()).'/' );
});

//轉到預設語系
Route::get('/backen-admin',function(){
	return redirect( Items::url('/'.App::getLocale().'/backen-admin') );
});

//轉到預設語系
Route::get('/home',function(){
	return redirect( Items::url('/'.App::getLocale().'/backen-admin') );
});

Route::get('/events/sendmail', "EventsController@switchSendMail");

/*===後台===*/

Route::group(['prefix'=>'/{locale?}'],function(){
	Route::group(['prefix'=>'/backen-admin','middleware'=> 'auth'],function(){
		//首頁
		Route::get('/','Backend\BackController@index');
		Route::controller('關於我們', 'AboutController');
		Route::controller('歷史沿革', 'HistoryController');

		Route::group(['prefix'=>'產品'],function(){

			Route::controller('主類別', 'Product\ThemeController');
			Route::controller('次類別', 'Product\CategoryController');
			Route::controller('Items', 'Product\ItemController');

		});

		Route::group(['prefix'=>'最新消息'],function(){
			Route::controller('消息分類', 'News\CategoryController');
			Route::controller('消息列表', 'News\ItemController');
		});

		Route::group(['prefix' => '相簿管理'], function() {
		 	Route::controller('相簿類別管理', 'Album\CategoryController');
			Route::controller('相簿管理', 'Album\ItemController');
		});

		Route::group(['prefix' => '檔案下載'], function() {
			Route::controller('檔案分類', 'Download\CategoryController');
			Route::controller('檔案列表', 'Download\ItemController');
		});
		
		Route::controller('與我們聯絡', 'ContactUsController');
		

		Route::group(['prefix'=>'廣告管理'],function(){
			Route::controller('首頁廣告', 'BannerController');
			//Route::controller('內頁廣告', 'PageBannerController');
		});

		Route::group(['prefix'=>'帳號管理'],function(){
			Route::controller('帳號列表', 'Backend\AccountController');
			Route::controller('權限', 'Backend\RoleController');
		});

		Route::controller('網站基本設定', 'SettingController');
		// Route::controller('行事曆管理', 'CalendarController');
		Route::controller('googleanalytics', 'AnalyticsController');
	});


	/*===後台===*/

	/*===前台==*/
	Route::group(['prefix'=>'/'],function(){
		Route::get('/', 'HomeController@index');
		Route::get('/error404', 'HomeController@error404');
		// Route::controller('import-manager','ImportController');
		Route::get('/mailtest', 'HomeController@testPost');
		//聯絡我們
		Route::group(['prefix'=>'contact'],function(){
			Route::get('/', "ContactUsController@index");
			Route::get('/distributors', "ContactUsController@distributors");
			Route::post('/save', "ContactUsController@postStore");
		});


		//關於我們
		Route::group(['prefix'=>'about'],function(){
			Route::get('{title?}', 'AboutController@index');
			// Route::get('/', 'AboutController@index');
			// Route::get('/development-and-core', 'AboutController@dac');
			// Route::get('/certification', 'AboutController@certification');
		});
		Route::group(['prefix'=>'quality'],function(){
			Route::get('{title?}', 'QualityController@index');
		});

		//最新消息
		Route::get('news/get-category', "NewsCategoryController@get_category");
		Route::post('news/load-more','NewsController@loadMore');
		Route::group(['prefix' => "news"], function()
		{
			Route::get('/{category?}', "News\ItemController@index");

			Route::get('/{category}/{title}', "News\ItemController@detail");
		});

		Route::group(['prefix' => "download"], function()
		{
			Route::get('/', "Download\ItemController@index");
			Route::get('/{title?}', "Download\ItemController@download");

		});

		Route::group(['prefix' => "investor"], function()
		{
			Route::get('/monthly-revenue', "InvestorController@monthlyRevenue");
			Route::get('/financial-report', "InvestorController@financialReport");
			Route::get('/stock', "InvestorController@stock");
			Route::get('/stakeholder-engagement', "InvestorController@stakeholderEngagement");
			Route::get('/organizational-structure-and-team', "InvestorController@organizationalStructure");
			Route::get('/internal-audit', "InvestorController@internalAudit");
		});

		Route::get('/video', "Video\ItemController@index");

		//相簿
		Route::group(['prefix'=>'album'],function(){
			Route::get('/{category?}', 'Album\ItemController@index');
			Route::get('/detail/{id}', 'Album\ItemController@detail');
		});
		//產品
		Route::group(['prefix' => "product"], function()
		{
			Route::get('/', "Product\ItemController@index"); //產品列表
			Route::get('/product-list', "Product\ItemController@list"); //
			Route::get('/product-profile-table', "Product\ItemController@ppt"); //
			Route::get('/size-code', "Product\ItemController@sizeCode"); //
			Route::get('/download/{id}', "Product\ItemController@download"); //
			Route::get('/{themeUrl}/{categoryUrl}/{itemUrl}', "Product\ItemController@detail"); //詳細頁
		});
		
		Route::get('search', "Product\ItemController@search");
		Route::get('/inquiry', 'Product\InquiryController@index');
		Route::post('/inquiry', 'Product\InquiryController@InquirySave');

		//產品搜尋
		Route::get('product-search/{keyword?}', "Product\ItemController@getProductSearch");

		//ajax load
		Route::controller('ajax-load', "AjaxLoadController");
		//抓產品列表
		Route::get('/getMenuList', "Product\ThemeController@getMenuList");
		//抓產品列表
		Route::get('/getSearch', "Product\ItemController@getSearch");

		
	});

	/*===前台==*/

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

});
