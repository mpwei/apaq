@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'Life Time'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Life Time</p>
            <p class="bn-description">设计工具</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">首页</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Life Time</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <!-- 20181210 -->
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-2 page-aside02">
                    <ul class="investor-list aside02-list">
                        <h4 class="aside-title">技术支持</h4>
                        <li>
                            <a href="{{ItemMaker::url('design/simulate')}}" class="d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>频谱特性</span></a>
                        </li>
                        <li class="active">
                            <a href="{{ItemMaker::url('design/life-time-1')}}" class="active d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Life Time</span></a>
                        </li>
                    </ul>
                </div>

                <div class="col-12 col-lg-10 investor-table-wrap">
                    <!-- Life Time -->
                    <h2 class="title-investor text-base">Life Time</h2>

                    <!-- Life Time page-->
                    <div class="table-responsive">
                        <table class="table table-bordered align-middle my-2">
                            <tbody>
                                <tr>
                                    <td>
                                        @if($type=="solid")
                                        <img src="assets/images/Conductive Polymer Aluminum Solid Capacitors.png" alt="" class="mt-0">
                                        @elseif($type=="electrolytic")
                                        <img src="assets/images/Conductive Polymer Aluminum Electrolytic Capacitors.png" alt="" class="mt-0">
                                        @else
                                        <img src="assets/images/Conductive Polymer Hybrid Aluminum Electrolytic Capacitors.png" alt="" class="mt-0">
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered align-middle mt-1">
                            <tbody>
                                <tr>
                                    <th width="20%">系列</th>
                                    <td class="form-inline contact-cata-wrap">
                                        <!-- <label class="my-1 mr-2">Series :</label> -->
                                        <div class="custom-control contact-form-gp pl-0 ml-0">
                                            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                                                @foreach($series_list as $key => $item)
                                                <option @if($key==$series)selected @endif value="{{$item['hours']}}-{{$item['temperature']}}">{{$key}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%">产品寿命规格值</th>
                                    <td height="85px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" readonly value="{{$data['hours']}}" class="form-control" id="hours">
                                        </div>
                                        <label class="unit my-1 mr-4">Hours</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">工作温度规格</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" readonly value="{{$data['temperature']}}" class="form-control" id="temperature">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>


                                <tr>
                                    <th width="20%">电容器表体温度</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="b" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>

                                @if($type=="solid")
                                <tr>
                                    <th width="20%">电容器环境温度</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="c" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">纹波电流实际值</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="d" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">mArms</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">额定纹波电流<br>*请参考型录上的规格值</th>
                                    <td height="230px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="e" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">mArms</label>
                                    </td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    <div class="form-inline contact-cata-wrap mt-2 justify-content-center">

                        <div class="contact-form-gp">
                            <!-- <input type="submit" value="Calculate" class="btn contactBtn mb-0"> -->
                            <a id="btn_size" class="btn btn-h-base">Calculate</a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered align-middle">
                            <tbody>
                                <tr>
                                    <th width="20%">寿命时间(hours)</th>
                                    <td height="85px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0" id="f">
                                        </div>
                                        <label class="unit my-1 mr-4">Hours</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%">寿命年限(years)</th>
                                    <td class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0" id="g">
                                        </div>
                                        <label class="unit my-1 mr-4">Years</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%">備註</th>
                                    <td class="form-inline contact-cata-wrap">
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">L<sub>x</sub>：在实际使用条件中推算的寿命（小时）</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">L<sub>0</sub>：产品可承受的寿命规格值（小时）</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">T<sub>0</sub>：产品可承受的温度规格值（℃）</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">T<sub>x</sub>：实际使用时测量的电容表面温度（℃）</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">T<sub>a</sub>：实际使用时的环境温度</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">I<sub>r</sub>：实际使用时的纹波电流（A r.m.s）</label>
                                        <label class="unit my-1 mr-4" style="justify-content: left; flex: 0 0 100%;">I<sub>0</sub>：产品可承受的最大纹波电流规格值（A r.m.s）</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-inline contact-cata-wrap mt-2 justify-content-center">

                        <div class="contact-form-gp">
                            <!-- <input type="submit" value="Back to select the series" class="btn contactBtn mb-0"> -->
                            <a class="btn btn-h-base" href="{{ItemMaker::url('design/life-time-1')}}">Back to List</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    window.onload = function() {
        const selectElement = document.querySelector('#inlineFormCustomSelectPref');
                
        selectElement.addEventListener('change', (event) => {
        console.log();
        let result = event.target.value.split('-');
        const temperature = document.querySelector('#temperature');
        const hours = document.querySelector('#hours');
        hours.value = result[0];
        temperature.value = result[1];
        });
    <?php if($type=="solid"){ ?>
        alert("请输入电容器表体温度或是输入电容器环境温度");

        document.getElementById("b").addEventListener("keyup", (event)=>{
            if(document.getElementById("b").value!=""){
                document.getElementById("c").readOnly = true;
                document.getElementById("d").readOnly = true;
                document.getElementById("e").readOnly = true;
            }else{
                document.getElementById("c").readOnly = false;
                document.getElementById("d").readOnly = false;
                document.getElementById("e").readOnly = false;
            }
        });
        document.getElementById("c").addEventListener("keyup", (event)=>{
            if(document.getElementById("c").value!=""){
                document.getElementById("b").readOnly = true;
            }else{
                document.getElementById("b").readOnly = false;
            }
        });
    <?php } else { ?>
        alert("请输入电容器表体温度");
    <?php } ?>
    document.getElementById("btn_size").addEventListener("click", (event)=>{
        <?php if($type=="solid"){ ?>
            const b = document.getElementById("b").value;
            const c = document.getElementById("c").value;
            const d = document.getElementById("d").value;
            const e = document.getElementById("e").value;
            const hours = document.getElementById("hours").value;
            const temperature = document.getElementById("temperature").value;
            if(b=="" && c==""){
                alert("请输入电容器表体温度或是输入电容器环境温度");
            }else if(b!=""){
                alert('“提醒！”\n(1). 请注意推算出来的电容器寿命结果，仅供参考。\n(2). 在对设备进行寿命设计时，请使用寿命充裕的电容器。\n推定寿命计算结果超过15年的场合，按15年为上限，如需推定寿命15年以上的产品，请与我们联系。');                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20)/24/365 * 10) /10;
            }else if(c!="" && d!="" && e!=""){
                if(parseInt(d)>parseInt(e)){
                    alert("提醒！涟波电流实际值不可超过涟波电流规格值，请重新输入数值。");
                }else{
                    alert('“提醒！”\n(1). 请注意推算出来的电容器寿命结果，仅供参考。\n(2). 在对设备进行寿命设计时，请使用寿命充裕的电容器。\n推定寿命计算结果超过15年的场合，按15年为上限，如需推定寿命15年以上的产品，请与我们联系。');
                    let c7 = 20 * Math.pow((parseInt(d)/parseInt(e)),2);
                    document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - c7 - parseInt(c))/20));
                    document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - c7 - parseInt(c))/20)/24/365 *10) /10;
                }
            }else{
                alert("Please enter the values of Actual ambient temperature of the capacitor, Applied ripple current to capacitor and Rated ripple current of capacitor.");
            }
        <?php } else { ?>
            const b = document.getElementById("b").value;
            const hours = document.getElementById("hours").value;
            const temperature = document.getElementById("temperature").value;
            if(b==""){
                alert("请输入电容器表体温度");
            }
            <?php if($type=="electrolytic"){ ?>
                alert('“提醒！”\n(1). 请注意推算出来的电容器寿命结果，仅供参考。\n(2). 在对设备进行寿命设计时，请使用寿命充裕的电容器。\n推定寿命计算结果超过15年的场合，按15年为上限，如需推定寿命15年以上的产品，请与我们联系。');
                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20)/24/365 *10) /10;
            <?php } else { ?>
                alert('“提醒！”\n(1). 请注意推算出来的电容器寿命结果，仅供参考。\n(2). 在对设备进行寿命设计时，请使用寿命充裕的电容器。\n推定寿命计算结果超过15年的场合，按15年为上限，如需推定寿命15年以上的产品，请与我们联系。');
                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(2, (temperature - parseInt(b))/10));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(2, (temperature - parseInt(b))/10)/24/365 *10) /10;
            <?php } ?>
        <?php } ?>
    });
};

</script>