@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'Life Time'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Life Time</p>
            <p class="bn-description">设计工具</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">首页</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Life Time</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>

<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-2 page-aside02">
                    <ul class="investor-list aside02-list">
                        <h4 class="aside-title">技术支持</h4>
                        <li>
                            <a href="{{ItemMaker::url('design/simulate')}}" class="d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>频谱特性</span></a>
                        </li>
                        <li class="active">
                            <a href="{{ItemMaker::url('design/life-time-1')}}" class="active d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Life Time</span></a>
                        </li>
                    </ul>
                </div>

                <div class="col-12 col-lg-10 investor-table-wrap">

                    <h2 class="title-investor text-base">Life Time</h2>
                    <ul class="list_pic">
                        <li>
                            <p class="mt-5 ">导电性高分子固态铝电解电容器(卷绕型)</p>
                        </li>
                        {!! Form::open( ["url" => ItemMaker::url('design/life-time-2/search'), 'method'=> 'POST', 'class' => 'form-inline contact-cata-wrap mt-5', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                        <div class="dropdown custom-control contact-form-gp pl-0 ml-0">
                            <input type="hidden" value="solid" name="type" />
                            <select class="dropdown_list" name="series">
                                <option>Select Series</option>
                                @foreach($options['solid'] as $key => $value)
                                <option value="{{ $key }}">{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Search" class="d-inline-block align-middle ml-2 ml-md-4 btn-control btn-circle">
                        {!! Form::close() !!}


                        <li>
                            <p class="mt-5 ">导电性高分子固态铝电解电容器(堆叠型)</p>
                        </li>
                        {!! Form::open( ["url" => ItemMaker::url('design/life-time-2/search'), 'method'=> 'POST', 'class' => 'form-inline contact-cata-wrap mt-5', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                        <div class="dropdown custom-control contact-form-gp pl-0 ml-0">
                            <input type="hidden" value="electrolytic" name="type" />
                            <select class="dropdown_list" name="series">
                                <option>Select Series</option>
                                @foreach($options['electrolytic'] as $key => $value)
                                <option value="{{ $key }}">{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Search" class="d-inline-block align-middle ml-2 ml-md-4 btn-control btn-circle">
                        {!! Form::close() !!}


                        <li>
                            <p class="mt-5">导电性高分子混合型铝电解电容器</p>
                        </li>
                        {!! Form::open( ["url" => ItemMaker::url('design/life-time-2/search'), 'method'=> 'POST', 'class' => 'form-inline contact-cata-wrap mt-5', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                        <div class="dropdown custom-control contact-form-gp pl-0 ml-0">
                            <input type="hidden" value="hybrid" name="type" />
                            <select class="dropdown_list" name="series">
                                <option>Select Series</option>
                                @foreach($options['hybrid'] as $key => $value)
                                <option value="{{ $key }}">{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Search" class="d-inline-block align-middle ml-2 ml-md-4 btn-control btn-circle">
                        {!! Form::close() !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection