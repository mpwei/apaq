@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass->getlang($locateLang);
?>
@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
<link rel="stylesheet" href="assets/plugins/nouislider/nouislider.css">
<link rel="stylesheet" href="assets/css/search.css">
<style>
  [v-cloak] {
    display: none;
  }
</style>
@endsection


@extends('page')
@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn-search.png)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
    <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
      <p class="bn-page-title">筛选搜寻页</p>
      <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首页</a></li>
          <li class="breadcrumb-item active" aria-current="page">筛选搜寻页</li>
        </ol>
      </nav>
    </div>
  </div>
  <canvas id="inbn_particlesR"></canvas>
</div>
<div class="inner-main" id="app" v-cloak>
  <div class="wrap-filter d-flex justify-content-center">
    <!-- filter start-->
    <div class="container-cc" v-if="items.length">
      <div class="row no-gutters row-cc row-filter">
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Operating temperature range(℃)</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="temperature in sort([...temperature_set])">
              <input :id="'temperature' + temperature" type="checkbox" :value="temperature" v-model="temperature_filter" @change="filter">
              <label :for="'temperature' + temperature">@{{ temperature }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.temperature_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Endurance(h)</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="endurance_slider" :data-min="Math.min(...endurance_set)" :data-max="Math.max(...endurance_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_endurance" type="text" @change="changeValue(min_endurance, max_endurance, 'endurance_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_endurance" type="text" @change="changeValue(min_endurance, max_endurance, 'endurance_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="endurance in sort([...endurance_set])" :class="{ 'option-disabled': min_endurance != Math.min(...endurance_set) || max_endurance != Math.max(...endurance_set) }">
              <input :id="'endurance' + endurance" type="checkbox" :disabled="min_endurance != Math.min(...endurance_set) || max_endurance != Math.max(...endurance_set)" :value="endurance" v-model="endurance_filter" @change="filter">
              <label :for="'endurance' + endurance">@{{ endurance }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.endurance_filter = []; this.filter(); changeValue(Math.min(...endurance_set), Math.max(...endurance_set), 'endurance_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Application</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="application in sort([...application_set])">
              <input :id="'application' + application" type="checkbox" :value="application" v-model="application_filter" @change="filter">
              <label :for="'application' + application">@{{ application }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.application_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Body shape</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="shape in sort([...shape_set])">
              <input :id="'shape' + shape" type="checkbox" :value="shape" v-model="shape_filter" @change="filter">
              <label :for="'shape' + shape">@{{ shape }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.shape_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Series</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="series in sort([...series_set])">
              <input :id="'series' + series" type="checkbox" :value="series" v-model="series_filter" @change="filter">
              <label :for="'series' + series">@{{ series }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.series_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Rated voltage(DC) (V)</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="voltage_slider" :data-min="Math.min(...voltage_set)" :data-max="Math.max(...voltage_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_voltage" type="text" @change="changeValue(min_voltage, max_voltage, 'voltage_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_voltage" type="text" @change="changeValue(min_voltage, max_voltage, 'voltage_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="voltage in sort([...voltage_set])" :class="{ 'option-disabled': min_voltage != Math.min(...voltage_set) || max_voltage != Math.max(...voltage_set) }">
              <input :id="'voltage' + voltage" type="checkbox" :disabled="min_voltage != Math.min(...voltage_set) || max_voltage != Math.max(...voltage_set)" :value="voltage" v-model="voltage_filter" @change="filter">
              <label :for="'voltage' + voltage">@{{ voltage }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.voltage_filter = []; this.filter(); changeValue(Math.min(...voltage_set), Math.max(...voltage_set), 'voltage_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Capacitance</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="capacitance_slider" :data-min="Math.min(...capacitance_set)" :data-max="Math.max(...capacitance_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_capacitance" type="text" @change="changeValue(min_capacitance, max_capacitance, 'capacitance_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_capacitance" type="text" @change="changeValue(min_capacitance, max_capacitance, 'capacitance_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="capacitance in sort([...capacitance_set])" :class="{ 'option-disabled': min_capacitance != Math.min(...capacitance_set) || max_capacitance != Math.max(...capacitance_set) }">
              <input :id="'capacitance' + capacitance" type="checkbox" :disabled="min_capacitance != Math.min(...capacitance_set) || max_capacitance != Math.max(...capacitance_set)" :value="capacitance" v-model="capacitance_filter" @change="filter">
              <label :for="'capacitance' + capacitance">@{{ capacitance }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.capacitance_filter = []; this.filter(); changeValue(Math.min(...capacitance_set), Math.max(...capacitance_set), 'capacitance_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Diameter (mm)</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="diameter_slider" :data-min="Math.min(...diameter_set)" :data-max="Math.max(...diameter_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_diameter" type="text" @change="changeValue(min_diameter, max_diameter, 'diameter_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_diameter" type="text" @change="changeValue(min_diameter, max_diameter, 'diameter_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="diameter in sort([...diameter_set])" :class="{ 'option-disabled': min_diameter != Math.min(...diameter_set) || max_diameter != Math.max(...diameter_set) }">
              <input :id="'diameter' + diameter" type="checkbox" :disabled="min_diameter != Math.min(...diameter_set) || max_diameter != Math.max(...diameter_set)" :value="diameter" v-model="diameter_filter" @change="filter">
              <label :for="'diameter' + diameter">@{{ diameter }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.diameter_filter = []; this.filter(); changeValue(Math.min(...diameter_set), Math.max(...diameter_set), 'diameter_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Height (mm)</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="slider" :data-min="Math.min(...height_set)" :data-max="Math.max(...height_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_height" type="text" @change="changeValue(min_height, max_height, 'slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_height" type="text" @change="changeValue(min_height, max_height, 'slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="height in sort([...height_set])" :class="{ 'option-disabled': min_height != Math.min(...height_set) || max_height != Math.max(...height_set) }">
              <input :id="'height' + height" type="checkbox" :disabled="min_height != Math.min(...height_set) || max_height != Math.max(...height_set)" :value="height" v-model="height_filter" @change="filter">
              <label :for="'height' + height">@{{ height }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.height_filter = []; this.filter(); changeValue(Math.min(...height_set), Math.max(...height_set), 'slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Length (mm)</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="length in sort([...length_set])">
              <input :id="'length' + length" type="checkbox" :value="length" v-model="length_filter" @change="filter">
              <label :for="'length' + length">@{{ length }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.length_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Size Code</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="size in sort([...size_set])">
              <input :id="'size' + size" type="checkbox" :value="size" v-model="size_filter" @change="filter">
              <label :for="'size' + size">@{{ size }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.size_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">ESR</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="esr_slider" :data-min="Math.min(...esr_set)" :data-max="Math.max(...esr_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_esr" type="text" @change="changeValue(min_esr, max_esr, 'esr_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_esr" type="text" @change="changeValue(min_esr, max_esr, 'esr_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="esr in sort([...esr_set])" :class="{ 'option-disabled': min_esr != Math.min(...esr_set) || max_esr != Math.max(...esr_set) }">
              <input :id="'esr' + esr" type="checkbox" :disabled="min_esr != Math.min(...esr_set) || max_esr != Math.max(...esr_set)" :value="esr" v-model="esr_filter" @change="filter">
              <label :for="'esr' + esr">@{{ esr }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.esr_filter = []; this.filter(); changeValue(Math.min(...esr_set), Math.max(...esr_set), 'esr_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Leakage Current</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="leakage_slider" :data-min="Math.min(...leakage_set)" :data-max="Math.max(...leakage_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_leakage" type="text" @change="changeValue(min_leakage, max_leakage, 'leakage_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_leakage" type="text" @change="changeValue(min_leakage, max_leakage, 'leakage_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="leakage in sort([...leakage_set])" :class="{ 'option-disabled': min_leakage != Math.min(...leakage_set) || max_leakage != Math.max(...leakage_set) }">
              <input :id="'leakage' + leakage" type="checkbox" :disabled="min_leakage != Math.min(...leakage_set) || max_leakage != Math.max(...leakage_set)" :value="leakage" v-model="leakage_filter" @change="filter">
              <label :for="'leakage' + leakage">@{{ leakage }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.leakage_filter = []; this.filter(); changeValue(Math.min(...leakage_set), Math.max(...leakage_set), 'leakage_slider');}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Rated Ripple Current(℃)</div>
          </div>
          <div class="row-options">
            <div class="option" v-for="ripple in sort([...ripple_set])">
              <input :id="'ripple' + ripple" type="checkbox" :value="ripple" v-model="ripple_filter" @change="filter">
              <label :for="'ripple' + ripple">@{{ ripple }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.ripple_filter = []; this.filter();}">重置</div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-md-4 col-sm-6">
          <div class="row-title text-center">
            <div class="filter-title">Rated Ripple Current (mA)(rms)</div>
          </div>
          <div class="row-options">
            <div class="filter">
              <div class="filter-slider-price" ref="ripple_rms_slider" :data-min="Math.min(...leakage_set)" :data-max="Math.max(...ripple_rms_set)" data-step="0.1"></div>
              <div class="row-label">
                <label class="filter-label">
                  <input class="filter-input" v-model="min_ripple_rms" type="text" @change="changeValue(min_ripple_rms, max_ripple_rms, 'ripple_rms_slider')">
                </label>
                <label class="filter-label">
                  <input class="filter-input" v-model="max_ripple_rms" type="text" @change="changeValue(min_ripple_rms, max_ripple_rms, 'ripple_rms_slider')">
                </label>
              </div>
            </div>
            <div class="option" v-for="ripple_rms in sort([...ripple_rms_set])" :class="{ 'option-disabled': min_ripple_rms != Math.min(...ripple_rms_set) || max_ripple_rms != Math.max(...ripple_rms_set) }">
              <input :id="'ripple_rms' + ripple_rms" type="checkbox" :disabled="min_ripple_rms != Math.min(...ripple_rms_set) || max_ripple_rms != Math.max(...ripple_rms_set)" :value="ripple_rms" v-model="ripple_rms_filter" @change="filter">
              <label :for="'ripple_rms' + ripple_rms">@{{ ripple_rms }}</label>
            </div>
          </div>
          <div class="row-btn d-flex justify-content-center">
            <div class="btn-reset" @click="()=> {this.ripple_rms_filter = []; this.filter(); changeValue(Math.min(...ripple_rms_set), Math.max(...ripple_rms_set), 'ripple_rms_slider');}">重置</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- result start-->
  <div class="wrap-result d-flex justify-content-center">
    <div class="container-cc">
      <div class="row no-gutters row-cc row-result">
        <div class="result">
          <h4>Found </h4>
          <div class="between-result">@{{ data.length }}</div>
          <h4>matches /</h4>
          <h4 class="content-result">@{{ items.length }}</h4>
          <h4>件中</h4>
        </div>
        <div class="btn-all-reset" @click="clear_filter">重新设定筛选条件</div>
      </div>
      <div class="row no-gutters row-cc row-data">
        <div class="prod-design-data-wrap">
          <div class="table-responsive">
            <table class="table text-center align-middle table-content-center table-investor01 table-scroller">
              <tbody>
                <tr>
                  <th>Part No.</th>
                  <th>Operating temperature range(℃)</th>
                  <th>Endurance(hours)</th>
                  <th>Application</th>
                  <th>Body shape</th>
                  <th>Series</th>
                  <th>Rated voltage(DC)(V)</th>
                  <th>Capacitance(μF)</th>
                  <th>Diameter (mm)</th>
                  <th>Height (mm)</th>
                  <th>Length (mm)</th>
                  <th>Size Code</th>
                  <th>ESR(mΩ)</th>
                  <th>Leakage Current(μA)</th>
                  <th>Rated Ripple Current(℃)</th>
                  <th>Rated Ripple Current (mA)(rms)</th>
                  <th>Contact us</th>
                </tr>
                <tr v-for="item in current_items">
                  <td><a :href="item.link" target="_blank">@{{ item['Part No.']}}</a></td>
                  <td>@{{ item['Operating temperature range(℃)']}}</td>
                  <td>@{{ item['Endurance(h)']}}</td>
                  <td>@{{ item['Application']}}</td>
                  <td>@{{ item['Body shape']}}</td>
                  <td>@{{ item['Series']}}</td>
                  <td>@{{ item['Rated voltage(DC)(V)']}}</td>
                  <td>@{{ item['Capacitance']}}</td>
                  <td>@{{ item['Diameter (mm)']}}</td>
                  <td>@{{ item['Height (mm)']}}</td>
                  <td>@{{ item['Length (mm)']}}</td>
                  <td>@{{ item['Size Code']}}</td>
                  <td>@{{ item['ESR']}}</td>
                  <td>@{{ item['Leakage Current']}}</td>
                  <td>@{{ item['Rated Ripple Current(℃)']}}</td>
                  <td>@{{ item['Rated Ripple Current (mA)(rms)']}}</td>
                  <td><a class="btn-contact-product" href="{{ItemMaker::url('contact')}}">Contact us</a></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="row-page-number">
            <ul class="pagination">
              <li v-if="current_page>11"><a class="page-link" @click="current_page -=10">
                  <<</a> </li> <li v-if="current_page>1"><a class="page-link" @click="current_page -=1">Previous</a></li>
              <li v-for="page in page_range" :class="{ active: page==current_page }"><a class="page-link" @click="current_page = page">@{{ page }}</a></li>
              <li v-if="total_page>current_page"><a class="page-link" @click="current_page +=1">Next</a></li>
              <li v-if="total_page-10>current_page"><a class="page-link" @click="current_page +=10">>></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include($locateLang.'.includes.aside')
  @stop

  @section('script')

  <script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>

  <script src="/assets/plugins/nouislider/nouislider.js"></script>
  <script src="vendor/backend/plugins/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
  <script type="text/javascript" src="vendor/backend/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

  <script type="text/javascript" src="vendor/main/datatable.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js" type="text/javascript"></script>

  <script>
    new Vue({
    el: '#app',
    data() {
        var items = {!! $data !!};
        let data = [];
        let count = 0;
        let temperature_set = new Set();
        let endurance_set = new Set();
        let application_set = new Set();
        let shape_set = new Set();
        let series_set = new Set();
        let voltage_set = new Set();
        let capacitance_set = new Set();
        let diameter_set = new Set();
        let height_set = new Set();
        let length_set = new Set();
        let size_set = new Set();
        let esr_set = new Set();
        let leakage_set = new Set();
        let ripple_set = new Set();
        let ripple_rms_set = new Set();
        items.forEach(item => {
            let xmlString = item.desc2;
            var doc = new DOMParser().parseFromString(xmlString, "text/xml");
            let trs = doc.firstChild.getElementsByTagName('tr');
            let key = [];

            for (var index = 0; index < trs.length; index++) {
                if (index == 0) {
                    let ths = trs[index].getElementsByTagName('th');
                    for (var ths_index = 0; ths_index < ths.length; ths_index++) {
                        key[ths_index] = ths[ths_index].textContent;
                    }
                } else {
                    let tds = trs[index].getElementsByTagName('td');
                    let entity = {};
                    for (var tds_index = 0; tds_index < tds.length; tds_index++) {
                        entity[key[tds_index]] = tds[tds_index].textContent;
                        if(tds[tds_index].textContent.trim()!=''){
                          switch (key[tds_index]) {
                              case 'Operating temperature range(℃)':
                                  temperature_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Endurance(h)':
                                  endurance_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Application':
                                  application_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Body shape':
                                  shape_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Series':
                                  series_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Rated voltage(DC)(V)':
                                  voltage_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Capacitance':
                                  capacitance_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Diameter (mm)':
                                  diameter_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Height (mm)':
                                  height_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Length (mm)':
                                  length_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Size Code':
                                  size_set.add(tds[tds_index].textContent);
                                  break;
                              case 'ESR':
                                  esr_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Leakage Current':
                                  leakage_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Rated Ripple Current(℃)':
                                  ripple_set.add(tds[tds_index].textContent);
                                  break;
                              case 'Rated Ripple Current (mA)(rms)':
                                  ripple_rms_set.add(tds[tds_index].textContent);
                                  break;
                          }
                        }
                    }
                    entity['link'] = '/{{ $locateLang }}/product/' + entity.Series + '+Series';
                    data.push(entity);
                }
            }
        });
        return {
            temperature_filter: [],
            endurance_filter: [],
            application_filter: [],
            shape_filter: [],
            series_filter: [],
            voltage_filter: [],
            capacitance_filter: [],
            diameter_filter: [],
            height_filter: [],
            length_filter: [],
            size_filter: [],
            esr_filter: [],
            leakage_filter: [],
            ripple_filter: [],
            ripple_rms_filter: [],

            temperature_set: temperature_set,
            endurance_set: endurance_set,
            application_set: application_set,
            shape_set: shape_set,
            series_set: series_set,
            voltage_set: voltage_set,
            capacitance_set: capacitance_set,
            diameter_set: diameter_set,
            height_set: height_set,
            length_set: length_set,
            size_set: size_set,
            esr_set: esr_set,
            leakage_set: leakage_set,
            ripple_set: ripple_set,
            ripple_rms_set: ripple_rms_set,

            min_height: Math.min(...height_set),
            max_height: Math.max(...height_set),
            min_voltage: Math.min(...voltage_set),
            max_voltage: Math.max(...voltage_set),
            min_esr: Math.min(...esr_set),
            max_esr: Math.max(...esr_set),
            min_endurance: Math.min(...endurance_set),
            max_endurance: Math.max(...endurance_set),
            min_capacitance: Math.min(...capacitance_set),
            max_capacitance: Math.max(...capacitance_set),
            min_diameter: Math.min(...diameter_set),
            max_diameter: Math.max(...diameter_set),
            min_leakage: Math.min(...leakage_set),
            max_leakage: Math.max(...leakage_set),
            min_ripple_rms: Math.min(...ripple_rms_set),
            max_ripple_rms: Math.max(...ripple_rms_set),
            
            items: data,
            data: data,
            current_page: 1
        }
    },
    computed:{
        total_page() {
            return Math.ceil(this.data.length / 10);
        },
        page_range() {
            let range =[];
            if(this.current_page<6){
              for(let index = 1; index<=this.total_page && index<10; index++){
                range.push(index);
              }
            }else{
              for(let index = this.current_page-5; index<=this.total_page && index<this.current_page+5; index++){
                range.push(index);
              }
            }
            return range;
        },
        current_items() {
          return this.data.slice((this.current_page - 1) * 10, (this.current_page * 10) - 1);
        }
    },
    watch: {
        series_filter() {
            let _this = this;
            this.filter();
        },
    },
    mounted(){
      noUiSlider.create(this.$refs.slider, {
        start: [parseFloat(this.min_height), parseFloat(this.max_height)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_height),
          max: parseFloat(this.max_height),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_height' : 'min_height'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.voltage_slider, {
        start: [parseFloat(this.min_voltage), parseFloat(this.max_voltage)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_voltage),
          max: parseFloat(this.max_voltage),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.voltage_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_voltage' : 'min_voltage'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.esr_slider, {
        start: [parseFloat(this.min_esr), parseFloat(this.max_esr)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_esr),
          max: parseFloat(this.max_esr),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.esr_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_esr' : 'min_esr'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.endurance_slider, {
        start: [parseFloat(this.min_endurance), parseFloat(this.max_endurance)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_endurance),
          max: parseFloat(this.max_endurance),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.endurance_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_endurance' : 'min_endurance'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.capacitance_slider, {
        start: [parseFloat(this.min_capacitance), parseFloat(this.max_capacitance)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_capacitance),
          max: parseFloat(this.max_capacitance),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.capacitance_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_capacitance' : 'min_capacitance'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.diameter_slider, {
        start: [parseFloat(this.min_diameter), parseFloat(this.max_diameter)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_diameter),
          max: parseFloat(this.max_diameter),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.diameter_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_diameter' : 'min_diameter'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.leakage_slider, {
        start: [parseFloat(this.min_leakage), parseFloat(this.max_leakage)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_leakage),
          max: parseFloat(this.max_leakage),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.leakage_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_leakage' : 'min_leakage'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 

      noUiSlider.create(this.$refs.ripple_rms_slider, {
        start: [parseFloat(this.min_ripple_rms), parseFloat(this.max_ripple_rms)],
        connect: true,
        step: 0.1,
        range: {
          min: parseFloat(this.min_ripple_rms),
          max: parseFloat(this.max_ripple_rms),
        },

        // make numbers whole
        format: {
          to: (value) => value,
          from: (value) => value,
        },
      });
      this.$refs.ripple_rms_slider.noUiSlider.on('update',(values, handle) => {
        this[handle ? 'max_ripple_rms' : 'min_ripple_rms'] = Math.round(parseFloat(values[handle])*10)/10;
        this.filter();
      }); 
    },
    methods: {
        filter() {
            let _this = this;
            let filterItems = this.items;
            if (this.temperature_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.temperature_filter.includes(item['Operating temperature range(℃)'])
                );
            }
            if(this.min_endurance != Math.min(...this.endurance_set) || this.max_endurance != Math.max(...this.endurance_set)){
              filterItems = filterItems.filter(item =>
              item['Endurance(h)'] >= this.min_endurance && item['Endurance(h)'] <= this.max_endurance ); } else if (this.endurance_filter.length> 0) {
                filterItems = filterItems.filter(item =>
                this.endurance_filter.includes(item['Endurance(h)'])
              );
            }
            if (this.application_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.application_filter.includes(item['Application'])
                );
            }
            if (this.shape_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.shape_filter.includes(item['Body shape'])
                );
            }
            if (this.series_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.series_filter.includes(item['Series'])
                );
            }
            if(this.min_voltage != Math.min(...this.voltage_set) || this.max_voltage != Math.max(...this.voltage_set)){
                filterItems = filterItems.filter(item =>
                    item['Rated voltage(DC)(V)'] >= this.min_voltage && item['Rated voltage(DC)(V)'] <= this.max_voltage
                );
            } else if (this.voltage_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.voltage_filter.includes(item['Rated voltage(DC)(V)'])
                );
            }
            if(this.min_capacitance != Math.min(...this.capacitance_set) || this.max_capacitance != Math.max(...this.capacitance_set)){
              filterItems = filterItems.filter(item =>
              item['Capacitance'] >= this.min_capacitance && item['Capacitance'] <= this.max_capacitance ); } else if (this.capacitance_filter.length> 0) {
              filterItems = filterItems.filter(item =>
              this.capacitance_filter.includes(item['Capacitance'])
              );
            }
            if(this.min_diameter != Math.min(...this.diameter_set) || this.max_diameter != Math.max(...this.diameter_set)){
              filterItems = filterItems.filter(item =>
              item['Diameter (mm)'] >= this.min_diameter && item['Diameter (mm)'] <= this.max_diameter ); } else if (this.diameter_filter.length> 0) {
              filterItems = filterItems.filter(item =>
              this.diameter_filter.includes(item['Diameter (mm)'])
              );
            }
            if(this.min_height != Math.min(...this.height_set) || this.max_height != Math.max(...this.height_set)){
                filterItems = filterItems.filter(item =>
                    item['Height (mm)'] >= this.min_height && item['Height (mm)'] <= this.max_height
                );
            } else if (this.height_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.height_filter.includes(item['Height (mm)'])
                );
            }
            if (this.length_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.length_filter.includes(item['Length (mm)'])
                );
            }
            if (this.size_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.size_filter.includes(item['Size Code'])
                );
            }
            if(this.min_esr != Math.min(...this.esr_set) || this.max_esr != Math.max(...this.esr_set)){
                filterItems = filterItems.filter(item =>
                    item['ESR'] >= this.min_esr && item['ESR'] <= this.max_esr
                );
            } else if (this.esr_filter.length > 0) {
                filterItems = filterItems.filter(item =>
                    this.esr_filter.includes(item['ESR'])
                );
            }
            if(this.min_leakage != Math.min(...this.leakage_set) || this.max_leakage != Math.max(...this.leakage_set)){
              filterItems = filterItems.filter(item =>
              item['Leakage Current'] >= this.min_leakage && item['Leakage Current'] <= this.max_leakage ); } else if (this.leakage_filter.length> 0) {
              filterItems = filterItems.filter(item =>
              this.leakage_filter.includes(item['Leakage Current'])
              );
              }
            if (this.ripple_filter.length > 0) {
              filterItems = filterItems.filter(item =>
              this.ripple_filter.includes(item['Rated Ripple Current(℃)'])
              );
            }
            if(this.min_ripple_rms != Math.min(...this.ripple_rms_set) || this.max_ripple_rms != Math.max(...this.ripple_rms_set)){
              filterItems = filterItems.filter(item =>
              item['Rated Ripple Current (mA)(rms)'] >= this.min_ripple_rms && item['Rated Ripple Current (mA)(rms)'] <= this.max_ripple_rms ); } else if (this.ripple_rms_filter.length> 0) {
              filterItems = filterItems.filter(item =>
              this.ripple_rms_filter.includes(item['Rated Ripple Current (mA)(rms)'])
              );
            }

            this.data = filterItems;
            this.current_page = 1;
        },
        clear_filter() {
            this.temperature_filter = [];
            this.endurance_filter = [];
            this.application_filter = [];
            this.shape_filter = [];
            this.series_filter = [];
            this.voltage_filter = [];
            this.capacitance_filter = [];
            this.diameter_filter = [];
            this.height_filter = [];
            this.length_filter = [];
            this.size_filter = [];
            this.esr_filter = [];
            this.leakage_filter = [];
            this.ripple_filter = [];
            this.ripple_rms_filter = [];
            this.min_height= Math.min(...this.height_set);
            this.max_height= Math.max(...this.height_set);
            this.changeValue(this.min_height, this.max_height, 'slider');
            this.min_voltage= Math.min(...this.voltage_set);
            this.max_voltage= Math.max(...this.voltage_set);
            this.changeValue(this.min_voltage, this.max_voltage, 'voltage_slider');
            this.min_esr= Math.min(...this.esr_set);
            this.max_esr= Math.max(...this.esr_set);
            this.changeValue(this.min_esr, this.max_esr, 'esr_slider');
            this.min_endurance= Math.min(...this.endurance_set);
            this.max_endurance= Math.max(...this.endurance_set);
            this.changeValue(this.min_endurance, this.max_endurance, 'endurance_slider');
            this.min_capacitance= Math.min(...this.capacitance_set);
            this.max_capacitance= Math.max(...this.capacitance_set);
            this.changeValue(this.min_capacitance, this.max_capacitance, 'capacitance_slider');
            this.min_diameter= Math.min(...this.diameter_set);
            this.max_diameter= Math.max(...this.diameter_set);
            this.changeValue(this.min_diameter, this.max_diameter, 'diameter_slider');
            this.min_leakage= Math.min(...this.leakage_set);
            this.max_leakage= Math.max(...this.leakage_set);
            this.changeValue(this.min_leakage, this.max_leakage, 'leakage_slider');
            this.min_ripple_rms= Math.min(...this.ripple_rms_set);
            this.max_ripple_rms= Math.max(...this.ripple_rms_set);
            this.changeValue(this.min_ripple_rms, this.max_ripple_rms, 'ripple_rms_slider');
            this.filter();
        },
        open_filter($event) {
            $event.target.classList.add("active");
        },
        close_filter($event) {
            $event.target
                .closest(".filter")
                .querySelector(".title")
                .classList.remove("active");
        },
        changeValue(minValue, maxValue, slider){
          this.$refs[slider].noUiSlider.set([minValue, maxValue]);
          this.filter();
        },
        sort(array){
          array.sort(function(a, b) {
            if(parseFloat(a)>parseFloat(b)) return 1;
            if(parseFloat(a)<parseFloat(b)) return -1;
            if (a > b) return 1;
            if (a < b) return -1;
            return 0;
          });
          return array;
        }
    }
});
    
  </script>
  @stop