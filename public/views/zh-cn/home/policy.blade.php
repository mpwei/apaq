@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
    $lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/news-bn01.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">隐私权政策</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首页</a></li>
                  <li class="breadcrumb-item active" aria-current="page">隐私权政策</li>
              </ol>
          </nav>
      </div>
  </div>

  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
  <div class="d-flex justify-content-center">
      <div class="container-cc">
          <div class="tab-content news-tab-content wow fadeInUp" data-wow-delay=".1s">
              <div id="tab01" class="tab-pane active">
                <section style="background-image: url(upload/about/about-01.png);background-repeat: no-repeat; background-position: 50% 50%">
                  <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp" id="about-01">
                  <div class="d-flex row-cc row no-gutters">
                  <div class="col-12">
                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 22.5px; line-height: normal; font-family: Arial; color: rgb(38, 38, 38);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">非常欢迎您光临钰邦科技股份有限公司</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">(</span><span class="s1" style="font-kerning: none;">以下简称本网站</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">)</span><span class="s1" style="font-kerning: none;">，为了让您能够安心的使用本网站的各项服务与资讯，特此向您说明本网站的隐私权保护政策，以保障您的权益，请您详阅下列内容：</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">一、隐私权保护政策的适用范围</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">隐私权保护政策内容，包括本网站如何处理在您使用网站服务时收集到的个人识别资料。隐私权保护政策不适用于本网站以外的相关连结网站，也不适用于非本网站所委托或参与管理的人员。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">二、个人资料的搜集、处理及利用方式</span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">当您造访本网站或使用本网站所提供之功能服务时，我们将视该服务功能性质，请您提供必要的个人资料，并在该特定目的范围内处理及利用您的个人资料；非经您书面同意，本网站不会将个人资料用于其他用途。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本网站在您使用服务信箱、问卷调查等互动性功能时，会保留您所提供的姓名、电子邮件地址、联络方式及使用时间等。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">于一般浏览时，伺服器会自行记录相关行径，包括您使用连线设备的</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">IP</span><span class="s1" style="font-kerning: none;">位址、使用时间、使用的浏览器、浏览及点选资料记录等，做为我们增进网站服务的参考依据，此记录为内部应用，决不对外公布。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">为提供精确的服务，我们会将收集的问卷调查内容进行统计与分析，分析结果之统计数据或说明文字呈现，除供内部研究外，我们会视需要公布统计数据及说明文字，但不涉及特定个人之资料。</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">三、资料之保护</span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本网站主机均设有防火墙、防毒系统等相关的各项资讯安全设备及必要的安全防护措施，加以保护网站及您的个人资料采用严格的保护措施，只由经过授权的人员才能接触您的个人资料，相关处理人员皆签有保密合约，如有违反保密义务者，将会受到相关的法律处分。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">如因业务需要有必要委托其他单位提供服务时，本网站亦会严格要求其遵守保密义务，并且采取必要检查程序以确定其将确实遵守。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本网站将利用您所提供的资料来答复您的问询、提供您所索取的本网站产品资讯及服务。您自愿提供的个人信息，表明您已同意本网站可授权第三方来收集、追踪和处理这些资料。</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;"> </span><span class="s4" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none; background-color: rgb(208, 208, 208);">(8/24</span><span class="s5" style="font-kerning: none; background-color: rgb(208, 208, 208);">新增</span><span class="s4" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none; background-color: rgb(208, 208, 208);">)</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">四、儿童与家长</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;"> </span><span class="s4" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none; background-color: rgb(208, 208, 208);">(8/24</span><span class="s5" style="font-kerning: none; background-color: rgb(208, 208, 208);">新增</span><span class="s4" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none; background-color: rgb(208, 208, 208);">)</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">未经家长或监护人许可，本网站将不会收集</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">13</span><span class="s1" style="font-kerning: none;">岁以下儿童的个人身份资料。</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">13</span><span class="s1" style="font-kerning: none;">岁以下的儿童只有在家长或监护人明确同意之下才可向本网站提供个人身份资料。</span><s</p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">五、网站对外的相关连结</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本网站的网页提供其他网站的网路连结，您也可经由本网站所提供的连结，点选进入其他网站。但该连结网站不适用本网站的隐私权保护政策，您必须参考该连结网站中的隐私权保护政策。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">六、与第三人共用个人资料之政策</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本网站绝不会提供、交换、出租或出售任何您的个人资料给其他个人、团体、私人企业或公务机关，但有法律依据或合约义务者，不在此限。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">前项但书之情形包括不限于：</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">经由您书面同意。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">法律明文规定。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">为免除您生命、身体、自由或财产上之危险。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">与公务机关或学术研究机构合作，基于公共利益为统计或学术研究而有必要，且资料经过提供者处理或搜集者依其揭露方式无从识别特定之当事人。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">当您在网站的行为，违反服务条款或可能损害或妨碍网站与其他使用者权益或导致任何人遭受损害时，经网站管理单位研析揭露您的个人资料是为了辨识、联络或采取法律行动所必要者。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">有利于您的权益。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本网站委托厂商协助搜集、处理或利用您的个人资料时，将对委外厂商或个人善尽监督管理之责。</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p3" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0);"><span class="s6" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">七、</span><span class="s1" style="font-kerning: none;">Cookie</span><span class="s6" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">之使用</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">为了提供您最佳的服务，本网站会在您的电脑中放置并取用我们的</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">，若您不愿接受</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">的写入，您可在您使用的浏览器功能项中设定隐私权等级为高，即可拒绝</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">的写入，但可能会导致网站某些功能无法正常执行。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">八、隐私权保护政策之修正</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本网站隐私权保护政策将因应需求随时进行修正，修正后的条款将刊登于网站上。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">九、有关于隐私权处理或者我们的隐私权政策声明</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;"> </span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">如果您对于我们的隐私权处理政策，网络隐私权处理声明或者您的个人信息处理有任何疑问，请聯繫我们：</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">钰邦科技股份有限公司</span></p>

                  <p class="p3" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0);"><span class="s6" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">电话：</span><span class="s1" style="font-kerning: none;">+886-37-777588</span></p>

                  <p class="p6" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>
                  </div>
                  </div>
                  </div>
                  </section>

              </div>
          </div>
      </div>
  </div>
</div>
@include($locateLang.'.includes.aside')
@stop
