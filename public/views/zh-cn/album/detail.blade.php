@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'album'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
    @include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')


@section('script')
    <script type="text/javascript" src="assets/js/subnav.js"></script>
@endsection




<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn03.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
        <!-- <p class="bn-page-title">{{$lang['about_title']}}</p> -->
            <p class="bn-page-title">相册管理</p>
            <p class="bn-description">钰邦科技股份有限公司</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">{{ $lang['home_title'] }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('album')}}">{{ $lang['album_title'] }}</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>

                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<section class="main-container">

    <div class="container">
        <div class="album_detail_title"><h3>{{$title}} </h3></div>
        <div class="row m-auto">
            @foreach($data as $item)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 listing-item shadow white-bg bordered mb-20">
                    <div class="overlay-container">
                        <div class="pic">
                            <p align="center"  style="font-weight:bold;">{{$item->title}}</p>
                            <a data-fancybox="gallery" href="{{ $item->image }}"><img src="{{ $item->image }}" alt=""></a>
                        </div>
                        <div class="detail_text">
                            @if (!empty($item->list_slogan))
                            <p>{{$item->list_slogan}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-lg-12 back_buttton">
                <a class="btn btn-h-base bnt-block" href="javascript:history.back()">返回上一頁</a>
            </div>

        </div>

        @endsection

        <script>
            if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0) || $(".owl-carousel--popup-img").length > 0) {
                console.log(1);
                $(".popup-img").magnificPopup({
                    type: "image",
                    gallery: {
                        enabled: true,
                    }
                });
                if ($(".owl-carousel--popup-img").length > 0) {
                    $(".owl-carousel").each(function () {
                        $(this).find(".owl-item:not(.cloned) .owl-carousel--popup-img").magnificPopup({
                            type: "image",
                            gallery: {
                                enabled: true,
                            }
                        });
                    });
                }
                $(".popup-img-single").magnificPopup({
                    type: "image",
                    gallery: {
                        enabled: false,
                    }
                });
                $('.popup-iframe').magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    preloader: false,
                    fixedContentPos: false
                });
            }
            ;
        </script>


        @section('script')
            <script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
            <script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
            <script src="assets/js/particlesR.js"></script>
            <script src="assets/js/template.js"></script>
            <script src="assets/js/pages.js"></script>
            <script src="assets/js/front/contactUs.js?12"></script>

@endsection