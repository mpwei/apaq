@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
@include($locateLang.'.includes.css-in')
@endsection
@section('title', "Financial Report - ".$ProjectShareName)
@section('keywords', "Financial Report ,".$ProjectShareName)
@section('description',"Financial Report ,".$ProjectShareName)


@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">档案下载</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首页</a></li>
                    <li class="breadcrumb-item active" aria-current="page">档案下载</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-10 investor-table-wrap investor-table-center">
                    @foreach($downloadCate as $key => $value)
                    <h2 class="title-investor text-base">{{$value['title']}}</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-fixed table-content-center table-investor01">
                            <thead>
                                <tr>
                                    <th style="width: 60%; font-size: 1.2rem;">下载內容標題</th>
                                    <th style="width: 40%; font-size: 1.2rem;">檔案</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key2 => $value2)
                                @if($value2['id_category'] == $value['id'])
                                <tr>
                                    <td>{{$value2['title']}}</td>
                                    <td>
                                        @if(!empty($value2['file']))
                                        <a href="{{ ItemMaker::url('download/'.$value2['id'].'?time='.time())}}"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
@include($locateLang.'.includes.aside')

@stop