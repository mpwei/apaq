@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn03.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Development and Core</p>
            <p class="bn-description">APAQ TECHNOLOGY CO., LTD.</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首页</a></li>
                    <li class="breadcrumb-item"><a href="#">About</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Development and Core</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main about-bg">
    <section class="main-about-02">
        <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 col-md-6 about-img text-center">
                    <p class="about-title-02">DEVELOPMENT</p>
                    <img class="about-img-1" src="upload/about/about-06-en.png" alt="">
                </div>
                <div class="col-12 col-md-6 about-img text-center">
                    <p class="about-title-02">CORE TECHNOLOGY</p>
                    <img class="about-img-1" src="upload/about/about-07-en.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="main-about-01">
        <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 about-img text-center">
                    <p class="about-title-02">APAQ PATENT</p>
                    <p class="about-text-02">The patent is the best arms for competitive capacity of enterprises. APAQ is leading the industry, distributed in Taiwan, China, America and Japan. Amounted 253 items. Among them, the Process item which accounts for 42% to all of the patent is the top.</p>
                    <img class="about-img-1" src="upload/about/about-08-en.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="main-about">
        <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 col-md-6 text-center">
                    <p class="about-title-02">RESEARCH AND DEVELOPMENT TEAM</p>
                    <p class="about-text-03 text-left">APAQ gathered the outstanding research and development members of Conductive Polymer Aluminum Solid Capacitors from Industrial Technology Research Institute (ITRI) and established a research and development center. We keep to develop more forward-looking technology with raw material vendor and equipment vendor, then communicate with our customers intimately about market trends. Based on those conclusion to drive superior market trends of process technology and develop of new product.</p>
                </div>
                <div class="col-12 col-md-6 text-center">
                    <p class="about-title-02">MANUFACTURE AND EQUIPMENT</p>
                    <p class="about-text-03 text-left">APAQ possesses Materials, Chemical, Mechanical and Electronic professional background of outstanding research and development members. We also self- developed high efficiency and low cost critical process and equipment technology.<br>Advantage:Flexible Equipment Dispatch / Huge Capacity of Production / Short Delivery Time / Advantage in The Cost / Excellent Quality</p>
                </div>
            </div>
        </div>
    </section>
    <section class="main-about-01">
        <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 about-img text-center">
                    <p class="about-title-02">QUALITY CONTROL</p>
                    <p class="about-text-02" style="max-width:465px">Plan:Predict and set the product target.Develop and confirm the plan.<br>Do:Do the target and plan.<br>Check:Review the plan speed. Evaluation the Result. Proposed improvement method.<br>Act/Adjust:Implement corrective measures accurately.</p>
                    <img class="about-img-1" src="upload/about/about-09-en.png" alt="">
                </div>
            </div>
        </div>
    </section>
</div>
</div>
@endsection