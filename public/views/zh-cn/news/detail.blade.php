@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
    $mon = ['','January ','February ',"March ","April ","May ","June ","July ","August ","September ","October ","November ","December "];
    $date = explode("-",$News->date);
?>

@section('title', $News->title.' - '.$ProjectShareName)
@section('keywords', $News->title.', '.$ProjectShareName)
@section('description',$News->title.', '.strip_tags($News->content).','.$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/news-bn01.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">News</p>
          <p class="bn-description">News and Exhibition</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{$Link}}">最新消息</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{$News->title}}</li>
              </ol>
          </nav>
      </div>
  </div>
  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
  <div class="d-flex justify-content-center detail-wrap">
      <div class="container-cc">
          <div class="detail-title wow fadeInUp" data-wow-delay=".1s">
              <h1 class="detail-title">{{$News->title}}</h1>
              <p class="news-date">{{$News->date}}</p>
          </div>
          <div class="detail-main py-5 wow fadeInUp" data-wow-delay=".2s">
              <!-- 編輯器內容start -->
              <div class="editor">
                {!! $News->content !!}
              </div>
              <!-- 編輯器內容end -->
              </div>

              <p class="my-5 text-center">
                  <a href="{{$Link}}" class="btn btn-h-base">回列表</a>
              </p>
          </div>
      </div>
  </div>
</div>
@include($locateLang.'.includes.aside')
@stop