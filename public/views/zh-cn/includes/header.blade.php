@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
$qty = 0;
$total = 0;
?>
<header class="main-header d-flex justify-content-center">
    <div class="container-cc d-inline-flex justify-content-between">
        <div class="d-flex align-items-center">
            <!-- <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"> -->
            <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="logo-pic"><img src="assets/images/logo/logo.png" alt="APAQ 鈺邦科技"></span></a>
            <span class="logo-word d-flex flex-column">
                <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="company_name">钰邦科技股份有限公司</span></a>
                <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="company_name_en">TECHNOLOGY CO., LTD.</span></a>
                {{-- <a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blank"><span><p class="job104">104徵才資訊</p></span></a> --}}
            </span>

        </div>
        <div class="d-flex align-items-center">
            <div class="main-menu01 d-none d-lg-none d-xl-flex">
                <ul>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <span>{{$lang['product_title']}}</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <!-- 目前所在頁面，current  -->
                        <!-- <a class="current" href="../products/product-list.php">产品介绍</a> -->
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('product')}}">固态产品</a></li>
{{--                            <li class="main__dropmenu__item"><a href="http://www.sz-gather.com/products.html" target="_blank">液态产品</a></li>--}}
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('application')}}">应用领域</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('news')}}">{{$lang['news_title']}}</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/最新消息')}}">最新消息</a></li>
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/展览资讯')}}">展览资讯</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">投资人专区</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">财务资讯</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">每月营收</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">合并财务报表</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">股东会资讯</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">议事手册</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">开会通知</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">年报</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Stock')}}">股务资讯</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <a class="dropmenu__subitem" href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">利害关系人</a>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">公司治理</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Organization-Team')}}">组织架构及经营团队</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Board-Director')}}">董事会及功能委员会</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">公司规章</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Internal-audit')}}">内部稽核</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">主要股东名单</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">公司治理运作情形</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">企业社会责任</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Social-Respons')}}">企业社会责任政策</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">企业社会责任实施成效</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">员工福利、工作环境与人身安全保护</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="main-h-link d-inline-flex align-items-center" id="btn-main-menu" href="#main-menu">
                <div class="collapse-button">
                    <span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </div>
                <div class="h-link-text">Menu</div>
            </div>
            <div class="main-h-link d-none d-lg-none d-xl-flex">
                <div class="main-menu02 d-flex align-items-center">
                    <div class="search-wrap">
                        <a href="#" id="btn-search-open"><span class="fas fa-search"></span></a>
                        {!! Form::open(['class'=>"search-input",'id'=>"search", 'name' => 'search', "url"=> ItemMaker::url('search') ]) !!}
                        <div class="search-form d-flex align-items-center">
                            <input type="text" class="form-control" placeholder="產品關鍵字..." name="key">
                            <button id="search-button" class="fas fa-search"></button>
                        </div>
                        {!! Form::close() !!}
                        <div class="search-form d-flex align-items-center">
                        </div>
                    </div>
                    <div class="lang-wrap">
                        <span class="btn-lang">
                            <span>语系</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="lang-list">
                            <li><a href="en">Eng</a></li>
                            <li><a href="zh-tw">繁中</a></li>
                            <li class="active"><a href="zh-cn">簡中</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of container-cc -->
    <div id="main-menu" class="main-menu">
        <div id="btn-close-mainmenu">
            <div class="collapse-button">
                <div id="cross">
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="h-link-text">Close</div>
        </div>

        <div class="menu-list">
            <div class="m-search-wrap d-flex d-xl-none align-items-start">
                <input type="text" class="form-control" placeholder="產品關鍵字..." id="mobilesearchkey">
                <a class="btn btn-cc btn-search mobile-search-btn"><span class="fas fa-search"></span></a>
            </div>
            <div class="m-lang-wrap lang-wrap d-flex flex-column d-xl-none">
                <span id="m-btn-lang">
                    <span>语系</span>
                    <i class="fas fa-caret-down"></i>
                </span>
                <ul class="lang-list d-inline-flex justify-content-start">
                    <li><a href="en">Eng</a></li>
                    <li><a href="zh-tw">繁中</a></li>
                    <li class="active"><a href="zh-cn">簡中</a></li>
                </ul>
            </div>
            <ul>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">{{$lang['product_title']}}<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('product')}}">固态产品</a></li>
{{--                        <li class="my-1 px-2 py-2"><a href="http://www.sz-gather.com/products.html" target="_blank">液态产品</a></li>--}}
                        <li class="my-1 px-2 py-2"><a href="{{ ItemMaker::url('application') }}">1应用领域</a></li>
                    </ul>
                </li>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">{{$lang['news_title']}}<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/最新消息')}}">最新消息</a></li>
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/展览资讯')}}">展览资讯</a></li>
                    </ul>
                </li>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">投资人专区<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">财务资讯<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">每月营收</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">合并财务报表</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">股东会资讯<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">议事手册</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">开会通知</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">年报</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stock')}}">股务资讯</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">利害关系人<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">利害关系人</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">公司治理<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Organization-Team')}}">组织架构及经营团队</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Board-Director')}}">董事会及功能委员会</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">公司规章</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Internal-audit')}}">内部稽核</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">主要股东名单</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">公司治理运作情形</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">企业社会责任<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Social-Respons')}}">企业社会责任政策</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">企业社会责任实施成效</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">员工福利、工作环境与人身安全保护</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="btn-main-submenu">关于我们<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        @if( $getTitle['about'])
                        @foreach($getTitle['about'] as $row )
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/'.$row['title'])}}">{{$row['title']}}</a></li>
                        @endforeach
                        @endif
                        {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about')}}">About Us</a>
                </li> --}}
                <!-- <li class="my-1 px-2 py-2"><a href="#">{{$lang['about_title']}}</a></li> -->
                {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/development-and-core')}}">Development and Core</a></li> --}}
                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('certificate')}}">证书</a></li>
            </ul>
            </li>
            <li>
                <a class="btn-main-submenu">设计工具<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/simulate')}}">频谱特性</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/life-time-1')}}">Life Time</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('search-filter')}}">筛选搜寻</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ItemMaker::url('contact')}}">联络我们</a>
            </li>
            @if(0)
            <li>
                <a class="btn-main-submenu">联络我们<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <!--                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">{{$lang['contact_title']}}</a></li>-->
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">联络我们</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('distributor')}}">經銷商</a></li>
                </ul>
            </li>
            @endif
            {{-- <li><a href="{{ItemMaker::url('album')}}">相册</a></li>
            <li><a href="{{ItemMaker::url('download')}}">档案下载</a></li> --}}
            <li><a href="{{ItemMaker::url('policy')}}">隐私权政策</a></li>
            {{-- <li><a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blank">徵才資訊</a></li> --}}
            </ul>
        </div>
    </div>

    
    <div id="main-menu-close"></div>
</header>
