@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();

?>

@section('title', 'Contact Us'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">經銷商</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"></a></li>
                    <li class="breadcrumb-item active" aria-current="page">經銷商</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="submenu-list-wrap d-flex flex-column justify-content-center align-items-center">
    <div class="d-flex container-cc wow fadeInUp justify-content-start" data-wow-delay=".1s">
        <a href="#" class="btn btn-base d-block d-md-none btn-submenu">類別 <span class="arrow-cc"></span></a>
    </div>
    <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
        <div class="submenu-list d-block d-md-flex flex-md-fill filter-button-group">
            <ul class="nav nav-pills d-block d-md-flex" id="news-tabs-wrap" role="tablist">
                <li class="nav-item flex-fill">
                    <a class="nav-link active" data-toggle="pill" href="#tab01">A~H</a>
                </li>
                <li class="nav-item flex-fill">
                    <a class="nav-link" data-toggle="pill" href="#tab02">I~Q</a>
                </li>
                <li class="nav-item flex-fill">
                    <a class="nav-link" data-toggle="pill" href="#tab03">R~Z</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="inner-main">
    <div class="d-flex justify-content-center">
        <div class="main-contact main-contact-cc container-cc">
            <div class="contact-cata-wrap tab-content news-tab-content wow fadeInUp" data-wow-delay=".1s">
                <div id="tab01" class="tab-pane active">
                    <div class="d-flex row-cc row no-gutters">
                        @if(!empty($data1))
                        @foreach($data1 as $key => $value)
                            @foreach($value as $key2 => $value2)
                                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                                    <div class="row row-cc border-cc">
                                        <div class="col-12 col-xl-4">
                                            <img src="{{$value2['image']}}" alt="" class="icon-flags">
                                        </div>
                                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                            <p>{{$value2['title']}}</p>
                                        </div>
                                    </div>
                                    <ul class="text-left">
                                        @if(!empty($value2['address']))
                                            <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>{{$value2['address']}}</span></li>
                                        @endif
                                        @if(!empty($value2['phone']))
                                            <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:{{$value2['phone']}}</span></li>
                                        @endif
                                        @if(!empty($value2['fax']))
                                            <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:{{$value2['fax']}}</span></li>
                                        @endif
                                        @if(!empty($value2['email']))
                                            <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: {{$value2['email']}}</span></li>
                                        @endif
                                        @if(!empty($value2['link']))
                                            <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="{{substr($value2['link'],0,4) == 'http' ? $value2['link'] : 'http://'.$value2['link']}}"  target="_blank">{{$value2['link']}}</a></span></li>
                                        @endif
                                    </ul>
                                </div>
                                @endforeach
                        @endforeach
                        @endif
                    </div>
                </div>
                <div id="tab02" class="tab-pane fade">
                    <div class="d-flex row-cc row no-gutters">
                        <div class="d-flex row-cc row no-gutters">
                            @if(!empty($data2))
                            @foreach($data2 as $key => $value)
                                    @foreach($value as $key2 => $value2)
                                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                                    <div class="row row-cc border-cc">
                                        <div class="col-12 col-xl-4">
                                            <img src="{{$value2['image']}}" alt="" class="icon-flags">
                                        </div>
                                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                            <p>{{$value2['title']}}</p>
                                        </div>
                                    </div>
                                    <ul class="text-left">
                                        @if(!empty($value2['address']))
                                            <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>{{$value2['address']}}</span></li>
                                        @endif
                                        @if(!empty($value2['phone']))
                                            <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:{{$value2['phone']}}</span></li>
                                        @endif
                                        @if(!empty($value2['fax']))
                                            <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:{{$value2['fax']}}</span></li>
                                        @endif
                                        @if(!empty($value2['email']))
                                            <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: {{$value2['email']}}</span></li>
                                        @endif
                                        @if(!empty($value2['link']))
                                            <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="{{substr($value2['link'],0,4) == 'http' ? $value2['link'] : 'http://'.$value2['link']}}"  target="_blank">{{$value2['link']}}</a></span></li>
                                        @endif
                                    </ul>
                                </div>
                                    @endforeach
                            @endforeach
                                @endif
                        </div>
                    </div>
                </div>
                <div id="tab03" class="tab-pane fade">
                    <div class="d-flex row-cc row no-gutters">
                        @if(!empty($data3))
                        @foreach($data3 as $key => $value)
                                @foreach($value as $key2 => $value2)
                            <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                                <div class="row row-cc border-cc">
                                    <div class="col-12 col-xl-4">
                                        <img src="{{$value2['image']}}" alt="" class="icon-flags">
                                    </div>
                                    <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                        <p>{{$value2['title']}}</p>
                                    </div>
                                </div>
                                <ul class="text-left">
                                    @if(!empty($value2['address']))
                                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>{{$value2['address']}}</span></li>
                                    @endif
                                    @if(!empty($value2['phone']))
                                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:{{$value2['phone']}}</span></li>
                                    @endif
                                    @if(!empty($value2['fax']))
                                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:{{$value2['fax']}}</span></li>
                                    @endif
                                    @if(!empty($value2['email']))
                                        <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: {{$value2['email']}}</span></li>
                                    @endif
                                    @if(!empty($value2['link']))
                                        <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="{{substr($value2['link'],0,4) == 'http' ? $value2['link'] : 'http://'.$value2['link']}}"  target="_blank">{{$value2['link']}}</a></span></li>
                                    @endif
                                </ul>
                            </div>
                            @endforeach
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection



@section('script')
<script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="assets/js/particlesR.js"></script>
<script src="assets/js/pages.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(24.7069993, 120.9194525), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.7069993, 120.9194525),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script> -->
<script src="assets/js/front/contactUs.js"></script>
@endsection