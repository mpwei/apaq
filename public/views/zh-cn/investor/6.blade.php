@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
        <!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">投资人专区</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">投资人专区</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">组织架构及经营团队</h2>
                            <img src="../../upload/invest/invest.jpg" alt="">
                            <div class="row investor-c-style02">
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>董事長</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>鄭敦仁</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>成功大學材料博士
                                        <br>工業技術研究院材料所正研究員
                                        <br>乾坤科技研發資深經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>執行長</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>林清封</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>成功大學材料博士
                                        <br>美國愛荷華州立大學化工博士
                                        <br>永剛科技(股)公司執行副總經理
                                        <br>立敦科技(股)公司執行副總經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>總經理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>林溪東</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>中正理工學院機械碩士
                                        <br>中山科學研究院計畫品質策進會副主委
                                        <br>佳邦科技(股)公司品保處處長
                                        <br>禾邦電子(中國)有限公司總經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>業務處副總經理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>林瀚淵</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>成功大學化學工程所碩士
                                        <br>工業技術研究院IEK產業分析師
                                        <br>工業技術研究院生醫所副研究員</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>海外業務處副總經理兼品保中心副總經理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>Liong Chee Keong</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>Certificate in Management
                                        <br>中山科學研究院計畫品質策進會副主委
                                        <br>(University Of Malaya)</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>行政處副總經理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>鄭富仁</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>中原大學企業管理系
                                        <br>鈺邦科技(股)公司總經理特助
                                        <br>佳鼎電子(股)公司奈米事業部生產處長
                                        <br>永剛科技(股)有限公司總經理特助
                                        <br>溫州泰慶皮革有限公司副廠長
                                        <br>上海威泰製革廠副總經理兼廠長</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>行銷部協理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>劉士山</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>成功大學電機研究所碩士
                                        <br>工業技術研究院材料化工研究所研究員</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>研發處處長</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>陳明宗</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>清華大學材料所碩士
                                        <br>聯華電子(股)公司高級工程師
                                        <br>工業技術研究院材料化工研究所副研究員</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>新產品處處長</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>錢明谷</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>清華大學材料科學工程學系學士、碩士
                                        <br>工業技術研究院材料化工研究所副研究員
                                        <br>統達能源(股)公司總經理室經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>財務部經理</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>李佩玲</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>文化大學會計系
                                        <br>安侯建業聯合會計師事務所審計部副理
                                        <br>禾邦電子(蘇州)有限公司管理處副理
                                        <br>佳邦科技(股)公司會計經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=" text-shadow text-shadow-cc">
                                    <ul>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・職稱：</span><span>新產品處處長</span></li>
                                        <li class="d-flex align-items-start"><span class="invest-subtitle">・姓名：</span><span>郭少隴</span></li>
                                        <li class="d-flex align-items-start">
                                            <span class="invest-subtitle">・經歷：</span>
                                            <span>逢甲大學會計系
                                        <br>新加坡優普集團優普電子(股)公司資深經理
                                        <br>鈺邦電子(無錫)有限公司財務部經理
                                        <br>鈺邦科技(股)公司總經理室專案經理</span></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->
    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>

@include($locateLang.'.includes.aside')
@stop