<?php
$route = explode('/', $_SERVER['REQUEST_URI']);
$routeGO = $route[sizeof($route) - 1];
?>

                            <p class="d-block d-lg-none">
                                <a class="btn btn-base btn-m-aside-trigger" >投資專區選單<i class="pl-2 fas fa-caret-down"></i>
                                </a>
                            </p>
                            <ul class="investor-list aside02-list">
                                <h4 class="aside-title">财务信息</h4>
                                <li @if($routeGO == 'Monthly-Revenue') class="active" @endif>
                                    <a href="{{ ItemMaker::url('investor/Monthly-Revenue') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>每月营收</span></a>
                                </li>
                                <li @if($routeGO == 'Financial-Report') class="active" @endif>
                                    <a href="{{ ItemMaker::url('investor/Financial-Report?page=1') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>合併財務报表</span></a>
                                </li>
                                <h4 class="aside-title">股东会信息</h4>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Procedure-Manual?page=1') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>议事手册</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Notice-of-meeting?page=1') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>开会通知</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Annual-Report?page=1') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>年报</span></a>
                                </li>
                                <!-- <li>
                                    <a href="investor9.php" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>議事錄</span></a>
                                </li> -->
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Stock') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>股务信息</span></a>
                                </li>
                                <h4 class="aside-title">利害关系人</h4>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Stakeholder-Engagement') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>利害关系人</span></a>
                                </li>
                                <h4 class="aside-title">公司治理</h4>
                                @if(1)
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Organization-Team') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>组织架构及经营团队</span></a>
                                </li>
                                @endif
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Board-Director') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>董事会及功能委员会</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Company-rule?page=1') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>公司规章</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Internal-audit') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>内部稽核</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Main-Shareholder') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>主要股东名单</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Corporate-Governance-Implementation-Status') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>公司治理运作情形</span></a>
                                </li>
                                <h4 class="aside-title">企业社会责任</h4>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Social-Respons') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>企业社会责任政策</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Respon-Effects') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>企业社会责任实施成效</span></a>
                                </li>
                                <li>
                                    <a href="{{ ItemMaker::url('investor/Personal-Safety') }}" class="d-flex align-items-start">
                                        <i class="fas fa-angle-right pr-2"></i><span>员工福利、工作环境与人身安全保护</span></a>
                                </li>
                            </ul>
