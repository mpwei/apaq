@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">投资人专区</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">投资人专区</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail corporate">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">{{ $data['title'] }}</h2>
                            @if($data['status']) {!! $data['content'] !!} @endif
                            @if(count($data->pdfs))
                            <div class="pb-5 table-cc">
                                <table class="table table-striped table-fixed table-content-center table-investor01">
                                    <thead>
                                        <tr>
                                            <th>{{ $data['pdf_title'] }}</th>
                                            <th>{{ $data['pdf_file'] }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($data->pdfs as $key => $pdf)
                                        <tr>
                                            <td>{{ $pdf->title }}</td>
                                            <td><a target="_blank" href="{{ $pdf->path }}"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->

    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop