@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
    @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Financial Report - ".$ProjectShareName)
@section('keywords', "Financial Report ,".$ProjectShareName)
@section('description',"Financial Report ,".$ProjectShareName)


@extends('page')
@section('content')
    <div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">投资人专区</p>
                <p class="bn-description"></p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">投资人专区</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="container-cc">
                <div class="row investor-detail">
                    <div class="col-12 col-lg-3 page-aside02">
                        @include($locateLang.'.investor.investor-aside')
                    </div>
                    <div class="col-12 col-lg-9 investor-table-wrap">
                        <h2 class="title-investor text-base">合併財務报表</h2>
                        <div class="table-responsive">
                            <table class="table table-striped table-fixed table-content-center table-investor01">
                                <thead>
                                <tr>
                                    <th>年度</th>
                                    <th>Q1</th>
                                    <th>Q2</th>
                                    <th>Q3</th>
                                    <th>Q4</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <td>{{$value['year']}}</td>
                                        @if($value['q1'])
                                            <td><a href="{{ ItemMaker::url('investor/Financial-Report/'.$value['id'].'/q1'.'?time='.time())}}"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($value['q2'])
                                            <td><a href="{{ ItemMaker::url('investor/Financial-Report/'.$value['id'].'/q2'.'?time='.time())}}"><img src="../../assets/images/icons/icon-pdf.png" alt=""
                                                                                                                                                    class="icon-pdf"></a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($value['q3'])
                                            <td><a href="{{ ItemMaker::url('investor/Financial-Report/'.$value['id'].'/q3'.'?time='.time())}}"><img src="../../assets/images/icons/icon-pdf.png" alt=""
                                                                                                                                                    class="icon-pdf"></a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($value['q4'])
                                            <td><a href="{{ ItemMaker::url('investor/Financial-Report/'.$value['id'].'/q4'.'?time='.time())}}"><img src="../../assets/images/icons/icon-pdf.png" alt=""
                                                                                                                                                    class="icon-pdf"></a></td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="pageNum text-center mt-5">
                                <ul class="pagination pagination-sm" style="justify-content: center">
                                    @if((4 < $_GET['page'] && 7 < $page))
                                        <li class="page-item">
                                            <a class="page-link" href="{{ ItemMaker::url($url.'?page=1') }}">1...</a>
                                        </li>
                                        <li class="page-item">
                                            <a class="page-link" href="{{ ItemMaker::url($url.'?page='.($_GET['page']-1)) }}" aria-label="Previous"><i aria-hidden="true " class="fa fa-angle-left "></i></a>
                                        </li>
                                    @endif
                                    @for($i=1;$i<=$page;$i++)
                                        @if(($i < 8 || $i - 4 < $_GET['page']) && ($_GET['page'] < $i + 4 || $page - 7 < $i))
                                            <li class="page-item">
                                                <a class="page-link" href="{{ ItemMaker::url($url.'?page='.$i) }}">{{$i}}</a>
                                            </li>
                                        @endif
                                    @endfor
                                    @if($_GET['page'] < $page - 3)
                                        <li class="page-item">
                                            <a class="page-link" href="{{ ItemMaker::url($url.'?page='.($_GET['page']+1)) }}" aria-label="After"><i aria-hidden="true " class="fa fa-angle-right "></i></a>
                                        </li>
                                        <li class="page-item">
                                            <a class="page-link" href="{{ ItemMaker::url($url.'?page='.$page) }}">...{{$page}}</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include($locateLang.'.includes.aside')

@stop