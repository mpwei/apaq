@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{--
<pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Invesotment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Invesotment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">企業社會責任政实施成效</h2>
                            <div class="text-shadow">
                                <p>員工福利措施：</p>
                                <ul>
                                    <li>1.公司提供之福利措施：員工除勞工保險、全民健康保險外另享有員工免費團體保險、出差旅行平安險及年終獎金等，另每年亦提供免費健康檢查。</li>
                                    <li>2.職工福利委員會：本公司已依職工福利金條例成立職工福利委員會，統籌各項職工福利，推動成立社團並予以經費補助，每年度編制預算及福利規劃，除對員工之婚、喪、病及生育予以各項補助外，尚有生日及年節禮金，並定期辦理各項旅遊活動，以調劑員工身心並加強員工間之聯誼。</li>
                                </ul>
                                <p>員工進修與訓練：</p>
                                <ul>
                                    <li>本公司有制定員工教育訓練程序，用以幫助新進員工適應工作環境、提高員工工作的技巧與能力以及配合員工職涯發展規劃相關訓練以滿足未來需求。</li>
                                </ul>
                                <p>員工退休制度與實施情形：</p>
                                <ul>
                                <li>為使本公司員工在職時能安心工作，並維護其退休後生活，員工退休係依勞工退休金條例及相關規定辦理。本公司全體員工均適用勞退新制，依個人薪資提繳百分之六存入勞保局個人退休金專戶，若勞工有自願提繳者亦將其提款金額存入相同帳號中。</li></ul>
                                <p class="pb-5"><span>為落實本公司之社會責任政策「以人權為藥物、以人性為基石、員工安全健康、保護環境生態、遵守法令法規、關懷社會大眾」，捐贈教學研究單位以提升教育環境品質；參加工研院舉辦之義賣活動，藉由愛心認購在地農產品活動幫助農民，且將義賣所得捐蹭予創世、心路等多個弱勢社福團體；捐贈民間協會，為研究及促進公司組織現代化、企業化、配合國家經濟發展盡一份心力。</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->

    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop