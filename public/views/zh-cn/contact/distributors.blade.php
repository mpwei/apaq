@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', 'Contact Us'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Distributors</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Distributors</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="submenu-list-wrap d-flex flex-column justify-content-center align-items-center">
    <div class="d-flex container-cc wow fadeInUp justify-content-start" data-wow-delay=".1s">
        <a href="#" class="btn btn-base d-block d-md-none btn-submenu">類別 <span class="arrow-cc"></span></a>
    </div>
    <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
        <div class="submenu-list d-block d-md-flex flex-md-fill filter-button-group">
            <ul class="nav nav-pills d-block d-md-flex" id="news-tabs-wrap" role="tablist">
                <li class="nav-item flex-fill">
                    <a class="nav-link active" data-toggle="pill" href="#tab01">A~H</a>
                </li>
                <li class="nav-item flex-fill">
                    <a class="nav-link" data-toggle="pill" href="#tab02">I~Q</a>
                </li>
                <li class="nav-item flex-fill">
                    <a class="nav-link" data-toggle="pill" href="#tab03">R~Z</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="inner-main">
    <div class="d-flex justify-content-center">
        <div class="main-contact main-contact-cc container-cc">
            <div class="contact-cata-wrap tab-content news-tab-content wow fadeInUp" data-wow-delay=".1s">
                <div id="tab01" class="tab-pane active">
                    <div class="d-flex row-cc row no-gutters">
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Bulgaria.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Bulgaria-Microdis Electronics EOOD</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Profesor Georgi Zlatarski street 13 1700
                                    Sofia, Bulgaria</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+359 2 4211124</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+359 2 4211123</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Bulgaria@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Croatia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Croatia-Microdis Electronics GmbH</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Rheinauer Strasse 1 68766 Hockenheim, Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+49 6205 28094-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+49 6205 28094-27</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: microdis.hr@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Czech-Republic.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Czech Republic-Microdis Electronics s.r.o</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>K Nemocnici 103 251 62 Tehovec, Czech Rep.</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+420 323 661780</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+420 323 661838</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Czech@microdis.net</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Czech-Republic.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Czech Republic-Microdis Electronics s.r.o</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Čapkova 22 678 01 Blansko, Czech Rep</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +420 516 414561</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+420 516 411138</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: microdis.hr@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Egypt.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Egypt-EGYelectronics</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>58, Section Six-Zahaa Maadi-App.33 P.OBox:
                                    114321, Cairo, Egypt</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +2(02) 29705574,+2(012) 11142487</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+2(02) 27515108</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Mobile:+2 (010) 68828820</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: ramy.raafat@egyelectronics.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="www.egyelectronics.com">www.egyelectronics.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Egypt.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Estonia-SIA "Microdis Electronics" Latvia</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Maskavas iela 322b 1063 Riga, Latvia</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +3716 7187170</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+3716 7187171</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Latvia@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Germany.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Germany-Chip 1 Exchange GmbH & Co. KG</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Martin-Behaim-Straße 10-21 63263 Neu-Isenburg
                                    Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +49-(0) 6102-8169-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +49-(0) 6102-8169-105</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: info@de.chip-1.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="www.chip-1.com">www.chip-1.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Germany.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Germany-WDI AG</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Industriestrasse 25a Industriezentrum 22880
                                    Wedel (Holstein) Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +49 4103 1800-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +49 4103 1800-200</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: fladiges@wdi.ag</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.wdi.ag" target="_blank">http://www.wdi.ag</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Hungary.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Hungary-Microdis Electronics Kft</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Bécsi u. 100. 1. Emelet 1034 Budapest, Hungary</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +36 1 236 0353</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +36 1 236 0355</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Hungary@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="tab02" class="tab-pane fade">
                    <div class="d-flex row-cc row no-gutters">
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Israel.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Israel RAM N.S Technologies Ltd.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>1 Hamasger St. Ra'anana, Israel 4365201</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+972-0-779208111</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+972-0-779208112</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: nati@ram-tech.co.il</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.ram-tech.co.il" target="_blank">http://www.ram-tech.co.il</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Israel.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Israel SEGTRO Ltd.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>6 Menachem Begin St. Petach Tikva, Israel 4973206</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+972-0-505772333 </span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: ami@segtro.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.segtro.com" target="_blank">http://www.segtro.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Italy.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Italy Elteco S.r.l.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Strada Del Cascinotto, 165, Torino (TO), 10156 Italy</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+39 011 8227093</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+39 011 8224047</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: info@eltecosrl.it</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.eltecosrl.it" target="_blank">http://www.eltecosrl.it</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Latvia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Latvia SIA "Microdis Electronics" Latvia</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Maskavas iela 322b 1063 Riga, Latvia</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+3716 7187170</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+3716 7187171</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Latvia@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Lithuania.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Lithuania Microdis Electronics</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Naugarduko 96 03202 Vilnius, Lithuania</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+370 5 2104186</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+370 5 2784682</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Lithuania@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Lithuania.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Lithuania ELGERTA Ltd. The Elgerta Group Company</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Savanoriu av. 363, LT-49425 Kaunas, Lithuania</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+370 686 16320</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Ieva.kulesiene@elgertagroup.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.elgertagroup.com" target="_blank">http://www.elgertagroup.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Poland.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Poland Microdis Electronics Sp. z o.o</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Suchy Dwor 17 52-271 Wroclaw, Poland</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+48 71 3010400</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+48 71 3010404</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: wroclaw@microdis.net</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Poland.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Poland ul.J.Lea 114 lok.217</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>30-133 Kraków, Poland</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+48 12 6366868</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+48 12 6360085</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: krakow@microdis.net</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Poland.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Poland ul.Grochowska 278 lok.501</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>03-841 Warszawa, Poland</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+48 22 8103666</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+48 22 8103300</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: warszawa@microdis.net</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Poland.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Poland ul.A.Fredry 8</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>83-330 Żukowo, Poland</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+48 58 6450585</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+48 58 3458325</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: gdansk@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Portugal.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Portugal RC MICOELECTRÓNICA, S.A.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>C/ Castrobarto 10 28042 Madrid, Spain</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:(+351) 916 048 043</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:(+351) 220 969 011</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:(+34) 913 294 531</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: ventas@rcmicro.es</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.rcmicro.es/contactar.html" target="_blank">http://www.rcmicro.es/contactar.html</a></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="tab03" class="tab-pane fade">
                    <div class="d-flex row-cc row no-gutters">
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Romania.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Romania Microdis Electronics GmbH</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Rheinauer Strasse 1 68766 Hockenheim, Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+49 6205 28094-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+49 6205 28094-27</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Romania@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Russia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Russia Microdis Electronics GmbH</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Rheinauer Strasse 1, 68766 Hockenheim, Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+49 6205 28094-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+49 6205 28094-27</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: russia@microdis.net</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Serbia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Serbia Microdis Electronics d.o.o</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Nikodima Milasa 17/3 11120 Beograd, Serbia</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+381 11 3295003</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+381 11 3295003</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Serbia@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Serbia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Srbija Vita ELKO d.o.o</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Vojvodjanska 165, 22304 Novi Banovci, Srbija</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+381 (0)22 342 790, 341 452, 343 197, 343 198</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+381 (0)22 342 243</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: aleksandar@vitaelko.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.vitaelko.com" target="_blank">http://www.vitaelko.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Slovakia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Slovakia Microdis Electronics s.r.o.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Zvolenská cesta 14 974 05 Banská Bystrica, Slovakia</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+421 48 415 2451</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+421 48 415 2451</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Slovakia@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Slovenia.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Slovenia Microdis Electronics GmbH</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Rheinauer Strasse 1 68766 Hockenheim, Germany</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+49 6205 28094-0</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+49 6205 28094-27</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: microdis.si@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Spain.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Spain RC MICROELECTRÓNICA, S.A.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>C/ Francesc Moragas, 72, nave 3 08907 L´Hospitalet de Llobregat, Barcelona, Spain</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:(+34) 93 260 21 66</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:(+34) 93 338 36 02</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: ventas@rcmicro.es</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.rcmicro.es/contactar.html" target="_blank">http://www.rcmicro.es/contactar.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Turkey.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Turkey Microdis Components Elektronik Ltd. Şti</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Mehmet Akif Mah. Öztekin Cd. No: 10 D:2 34774 Ümraniye- Istanbul</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+90 216 3803607</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+90 216 3803608</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Turkey@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Turkey.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Turkey AssemCorp Eletronik A.S.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Serifali Mah. Miras Sok. No 2534775 Yukari Dudullu Umraniye-Istanbul Turkey</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+90 (216)939 26 00</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+90 (216)909 31 53</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Mustafa.oruc@assemcorp.com</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/UK.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>UK Gateway Electronic Components Ltd.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Innovation House, Beam Heath Way, Nantwich, Cheshire CW5 6PQ,UK</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+44 1270 615 999</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: sales@gatewaycando.com</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://www.gatewaycando.com" target="_blank">http://www.gatewaycando.com</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/Ukraine.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>Ukraine Microdis Electronics</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Lustdorfskaya doroga 5, of.100 65017 Odessa, Ukraine</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+38 048 7344 360</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+38 048 7344 360</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: Ukraine@microdis.net</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-globe-asia"></i><span><a href="http://microdis.net/contact.html" target="_blank">http://microdis.net/contact.html</a></span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/uae.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>U.A.E. Chip 1 Exchange Electronic</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>Components FZE Dubai Silicon Oasis Unit A-104-1 P.O Box 54425 Dubai, U.A.E</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+971 4 3893900</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+971 4 3893910</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: info@ae.chip-1.com</span></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                            <div class="row row-cc border-cc">
                                <div class="col-12 col-xl-4">
                                    <img src="upload/contact/USA.jpg" alt="" class="icon-flags">
                                </div>
                                <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                                    <p>USA Chip 1 Exchange USA Inc.</p>
                                </div>
                            </div>
                            <ul class="text-left">
                                <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span>30161-C Ave de las Banderas Rancho Santa Margarita, CA 92688</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel:+1 888 866 1566</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax:+1 949 589 5401</span></li>
                                <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>Email: customerservice@us.chip-1.com</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection



@section('script')
<script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="assets/js/particlesR.js"></script>
<script src="assets/js/pages.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
    
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(24.7069993, 120.9194525), // New York

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]}]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.7069993, 120.9194525),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script> -->
<script src="assets/js/front/contactUs.js"></script>
@endsection