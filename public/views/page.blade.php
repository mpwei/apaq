<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
  <!--<![endif]-->

  <head>
    @include($locateLang.'.includes.head')
    <!-- Custom css -->
    <!-- <script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "c188a967-883e-4d5e-9a5a-ff7ce32fbad0",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script> -->

<script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "10fcb13d-2025-47ef-bddf-1de65b304c9c",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>


  </head>

  <!-- body classes:  -->
  <body class="inner-page">

    @include($locateLang.'.includes.header')
    <div class="main">
      @yield('content')
    </div>

    <!-- footer start -->
    @include($locateLang.'.includes.footer')
    <!-- footer end -->
    <script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="assets/js/particlesR.js"></script>
    <script src="assets/js/pages.js"></script>
    @yield('script')

  </body>
</html>
