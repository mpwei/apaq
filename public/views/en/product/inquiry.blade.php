@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('title', '手工具,園藝,花園工具,剪刀,高枝剪|吉勝工具')
@section('keywords', '手工具,園藝,花園工具,剪刀,高枝剪,套筒,板手,棘輪板手,扭力板手')
@section('description','吉勝為手工具及園藝工具(剪刀、高枝剪..等)行銷公司，我們靈活地調整的生產程序，以滿足世界各地正在尋找市場趨勢與變化的合作夥伴之需求，每年吉勝的產品線都會推出新系列的工具產品及包裝設計，以便使客戶在市場競爭中保持成功的地位。')

@extends('template')

@section('headerid','in-header')

@section('script')
    <script src="assets/js/front/inquiry.js"></script>
    <script src="assets/js/front/shipping.js"></script>
@endsection

  @section('css')
  <link rel="stylesheet" href="assets/css/page.css">
  @endsection

<?php 
    $number = 1;
    $lang = $frontEndClass -> getlang($locateLang);
?>
@section('content')





    <main class="inquiry in-page detail-page">
        <section id="in-banner">
            <div class="container">
                <a href="./" class="pic">
                    <img src="assets/images/logo/in-logo.png" alt="Rockfirm" class="page-logo">
                </a>
            </div>
            <div class="page-title animated slideInUp has-text-centered">
                <span>Inquiry</span>
            </div>      
        </section>

        <section class="common-page product-bg">
            <div class="container">
                <div class="inquiry-list">
                    <div class="columns is-mobile list-heading has-text-centered">
                        <div class="column is-1">
                            <span>{{ $lang['car_item'] }}</span>
                        </div>
                        <div class="column is-4">
                            <span>{{ $lang['car_item_title'] }}</span>
                        </div>
                        <div class="column is-2">
                            <span>{{ $lang['car_photo'] }}</span>
                        </div>
                        <div class="column is-3">
                            <span>{{ $lang['car_quantity'] }}</span>
                        </div>
                        <div class="column is-1">
                            <span>{{ $lang['car_detail'] }}</span>
                        </div>
                        <div class="column is-1">{{ $lang['car_delete'] }}</div>
                    </div>

                    @if (!empty( Session::get('Spec')['list'] ))
                        @foreach( Session::get('Spec')['list'] as $key => $row)
                    <?php
                            $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/500?text=noPic";
                            $no = sprintf('%02d',$number++);
                    ?>
                    <div class="columns is-mobile list-item has-text-centered" id="product{{ $row['product_no'] }}">
                        <div class="column is-1">
                            <span>{{ $no }}</span>
                        </div>
                        <div class="column is-4">
                            <span>{{ $row['title'] }}</span>
                        </div>
                        <div class="column is-2">
                            <span class="pic">
                                <img src="{{ $img }}" alt="{{ $row['title'] }}">
                            </span>
                        </div>
                        <div class="column is-3">
                            <div class="field has-addons quantity-holder">
                                <div class="control">
                                    <input class="input quantity-input" type="text" placeholder="qua" listkey="{{ $key }}"  value="{{ $row['quantity'] }}">
                                </div>
                                <div class="control inc">
                                    <a class="button is-info">
                                        {{ $lang['car_update'] }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="column is-1">
                            <a href="{{ ItemMaker::url('productdetail/'.$row['title']) }}" class="inq-detail-btn"><span class="fa fa-search-plus"></span></a>
                        </div>
                        <div class="column is-1 del">
                            <span class="fa fa-trash-o remove_spec_list" type="{{ $row['product_no'] }}"></span>
                        </div>
                    </div>
                        @endforeach
                    @endif

                </div><!-- end of inquiry-list -->

                <div class="inq-contact-form">
                    <p class="page-subtitle is-uppercase has-text-centered">
                        {{ $lang['contact_info'] }}
                    </p>
                    <div class="contact-form">
                        {!! Form::open(['id'=>"active", 'name' => 'active', "url"=> ItemMaker::url('inquiry/save') ]) !!}
                            <div class="field c-name">
                                <div class="control">
                                    <input class="medium-input" type="text" placeholder="{{ $lang['contact_name'] }}" name="Contact[name]" id="name">
                                </div>
                            </div>

                            <div class="field c-phone">
                                <div class="control">
                                    <input class="medium-input" type="tel" placeholder="{{ $lang['contact_tel'] }}" name="Contact[tel]" id="tel">
                                </div>
                            </div>

                            <div class="field c-email">
                                <div class="control">
                                    <input class="" type="email" placeholder="{{ $lang['contact_mail'] }}" name="Contact[email]" id="email">
                                </div>
                            </div>

                            <div class="field c-address">
                                <div class="control">
                                    <input class="" type="text" placeholder="{{ $lang['contact_address'] }}" name="Contact[address]" id="address">
                                </div>
                            </div>

                            <div class="field c-msg">
                                <div class="control">
                                    <textarea cols="40" rows="12" placeholder="{{ $lang['contact_message'] }}" name="Contact[memo]" id="memo"></textarea>
                                </div>
                            </div>

                            <div class="field captcha">
                                <span class="control">
                                    <input class="short-input" type="text" placeholder="{{ $lang['contact_captcha'] }}" id="captcha">
                                </span>
                                <span class="changeCap" id="changeCap">{!! Captcha::img() !!}</span>
                                <span class="return-btn">
                                    <a class="re_cap"><img src="assets/images/captcha-f5.jpg" alt=""></a>
                                </span>
                            </div>

                            <div class="btn-wrap has-text-centered">
                                <a href="" class="active-btn active-btn02 has-text-centered is-uppercase" id="send-Inquiry">{{ $lang['car_send_btn'] }}</a>
                            </div>
                        {!! Form::close() !!}
                    </div><!-- end of contact-form -->
                </div>
            </div><!-- end of container-->
        </section>
        
    </main>

@stop