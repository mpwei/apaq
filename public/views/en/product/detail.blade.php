@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $Item->title." - ".$ProjectShareName)
@section('keywords', $Theme->title.','. $Category->title.','. $Item->title.','.$ProjectShareName)
@section('description',$Theme->title.','. $Category->title.','. $Item->title.','.$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection


@extends('page')
@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn02.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">Our Products</p>
          <p class="bn-description">{{$Theme->title}}</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('product')}}">Product</a></li>
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('product')}}#prod-cata{{$Theme->id}}">{{$Theme->title}}</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{$Category->title}}</li>
              </ol>
          </nav>
      </div>
  </div>
  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="submenu-wrap d-flex flex-column justify-content-center align-items-center">
  <div class="d-flex container-cc wow fadeInUp justify-content-start " data-wow-delay=".1s">
      <a href="#" class="btn btn-base d-block btn-submenu">{{$Item->title}} <span class="arrow-cc"></span></a>
  </div>
  <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
      <div class="submenu-list d-block">
        @if($Category->Item)
          @foreach($Category->Item as $item)
            <a href="{{$item->web_link}}" class="d-block">{{$item->title}}</a>
          @endforeach
        @endif
      </div>
  </div>
</div>
<div class="inner-main">
  <section class="prod-detail-sect">
      <div class="prod-detail-top d-flex justify-content-center">
          <div class="d-flex row-cc row no-gutters">
              <div class="col-12 col-md-6 py-3 py-md-5">
                  <div class="prod-detail-pic pic">
                    <?php
                      $img = (!empty($Item->image) and file_exists(public_path() . "/" . $Item->image)) ? $Item->image : "assets/images/1024.png";
                      $DAMimg = (!empty($Item->DAM_image) and file_exists(public_path() . "/" . $Item->DAM_image)) ? $Item->DAM_image : "assets/images/1024.png";
                    ?>
                    <img src="{{$img}}" alt="">
                  </div>
              </div>
              <div class="col-12 col-md-6 pb-5 py-3 py-md-5">
                  <div class="prod-detail-data">
                      <h1 class="prod-name">{{$Item->title}}</h1>
                      <p class="prod-special">{{$Item->detail_sub_title}}</p>
                      @if($Item->detail_spec)
                        <?php 
                          $spec = ($Item->detail_spec)? explode("#", $Item->detail_spec) : []; 
                        ?>
                        <ul class="data-list">
                          @foreach($spec as $row)
                            <li>{{$row}}</li>
                          @endforeach
                        </ul>
                      @endif
                      @if($Item->file)
                        <a href="{{ItemMaker::url('product/download/'.$Item->id.'/file?time='.time())}}" class="bn-link btn-cc btn-cc02 wow fadeInUp pr-3" data-wow-delay=".75s" style="visibility: visible; animation-delay: 0.75s; animation-name: fadeInUp;">
                            <span class="btn-arrow justify-content-center align-items-center d-inline-flex mr-2">
                                <i class="far fa-file-pdf"></i>
                            </span>
                            <span class="btn-word">Download</span>
                        </a>
                      @endif
                      @if($Item->packing)
                       <a href="{{ItemMaker::url('product/download/'.$Item->id.'/packing?time='.time())}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s" style="visibility: visible; animation-delay: 0.75s; animation-name: fadeInUp;">
                            <span class="btn-arrow justify-content-center align-items-center d-inline-flex mr-2">
                                <i class="far fa-file-pdf"></i>
                            </span>
                            <span class="btn-word">Taping&Packing</span>
                        </a>
                      @endif
                  </div>
              </div>
          </div>
      </div>
      <div class="prod-detail-info-wrap">
          <div class="d-flex justify-content-center">
              <div class="d-flex flex-column container-cc prod-detail-tabnav">
                  <ul class="nav nav-pills" id="prod-detail-tabs" role="tablist">
                      <li class="nav-item flex-fill">
                          <a class="nav-link active" data-toggle="pill" href="#tab01">Specification</a>
                      </li>
                      <!-- <li class="nav-item flex-fill">
                          <a class="nav-link" data-toggle="pill" href="#tab02">Dimensions and Marking</a>
                      </li> -->
                      <li class="nav-item flex-fill">
                          <a class="nav-link" data-toggle="pill" href="#tab03">Standard Ratings</a>
                      </li>
                  </ul>
                  <div class="tab-content">
                      <div id="tab01" class="tab-pane active">
                          <div class="table-responsive">
                            {!! $Item->content!!}
                          </div>
                      </div>
                      <!-- <div id="tab02" class="tab-pane fade">
                          <div class="pic border"><img src="{{$DAMimg}}" alt=""></div>
                          <div class="table-responsive my-5">
                            {!! $Item->desc!!}
                          </div>
                      </div> -->
                      <div id="tab03" class="tab-pane fade">
                        {!! $Item->desc2 !!}
                      </div>
                  </div>
              </div>
          </div>
          <div class="d-flex justify-content-center">
              <div class="container-cc text-center mt-2 mb-5">
                  <a class="btn btn-contact" href="{{ItemMaker::url('contact')}}">Contact Us</a>
                  <a class="btn btn-h-base" href="{{ItemMaker::url('product')}}">Back to List</a>
              </div>
          </div>
      </div>
  </section>
</div>

  @include($locateLang.'.includes.aside')
@stop