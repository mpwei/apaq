@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php

$uri = $_SERVER["REQUEST_URI"];
$lang = $frontEndClass->getlang($locateLang);
$product_menu = $frontEndClass->getProduct();

?>
{{-- <pre>{{print_r($Theme)}}</pre> --}}
@section('css')
    @include($locateLang.'.includes.css-in')
@endsection
@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@extends('page')
@section('content')
    <div class="in-banner" style="background-image: url(upload/banner/inner/in-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">Search：{{$key}}</p>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->

    <!-- breadcrumb end -->
    <!-- main-container start -->
    <!-- ================ -->
    <section class="main-container py-5">
        <div class="container-cc m-auto">
            <div class="isotope-container row grid-space-0">

                @if(!empty($pageData['data']))
                    @foreach($pageData['data'] as $key => $row)
                        <?php
$img = (!empty($row['image']) and file_exists(public_path() . "/" . $row['image'])) ? $row['image'] : "http://placehold.it/1024?text=noPic";
?>
                        <div class="col-sm6 col-md-4 col-lg-3 py-3 isotope-item product-{{ $row['theme_id'] }}end">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="{{ $img }}" alt="{{ $row['title'] }}">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="{{ $row['web_link'] }}">{{ $row['title'] }}</a></h3>
                                            <p class="small"></p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="{{ $row['web_link'] }}"
                                               class="btn btn-gray-transparent btn-animated btn-sm">{{$lang['detail']}}
                                                <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@stop