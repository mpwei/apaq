@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<!DOCTYPE html>
<html lang="zh-tw">



<body class="inner-page">


    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Investment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Investment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <!-- <div class="col-12 col-lg-9 investor-table-wrap"> -->
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">Stock</h2>
                            <div class="text-shadow">
                                <p>Common Share Transfer Agent and Registrar、Registration apparatus</p>
                                <ul class="pb-5 list">
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Company：</b><span>GFS CORPORATION Stock Registration Dept.</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Address：</b><span>6F., No.6, Sec. 1, Zhongxiao W. Rd., Zhongzheng Dist., Taipei City 100, Taiwan (R.O.C.)</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Website：</b><span> <a href="http://www.gfortune.com.tw" target="_blank">http://www.gfortune.com.tw</a></span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Phone：</b><span>(02)2371-1658</span></li>
                                </ul>
                                <p>Contact Us</p>
                                <ul class="pb-5 list">
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Speaker：</b><span>Mr. Lin Feng</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Acting Speaker：</b><span>Mr. Lin East River</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Tel：</b><span>037-777588</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>E-mail：</b><span>estherli@apaq.com.tw</span></li>
                                </ul>
                                <p>Stock Quotes</p>
                                <ul class="pb-5 list">
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>INPAQ traded on the GreTai Securities Market：</b><span> Electronics</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Symbol：</b><span>6449</span></li>
                                    <li class="d-flex flex-column flex-sm-row align-items-start"><b>Stock price performance：</b><span><a href="https://tw.stock.yahoo.com/q/q?s=6449" target="_blank">https://tw.stock.yahoo.com/q/q?s=6449</a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->
    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop