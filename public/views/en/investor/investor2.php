<!DOCTYPE html>
<html lang="zh-tw">

<?php include "../include/head-in.php" ?>

<body class="inner-page">
    <?php include "../include/header.php" ?>

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Investment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Investment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            <?php include "investor-aside.php" ?>
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">Financial Report</h2>
                            <div class="table-responsive">
                                <table class="table table-striped table-fixed table-content-center table-investor01">
                                    <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Q1</th>
                                            <th>Q2</th>
                                            <th>Q3</th>
                                            <th>Q4</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2018</td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2017</td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        </tr>
                                        <tr>
                                            <td>2016</td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        </tr>
                                        <tr>
                                            <td>2015</td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        </tr>
                                        <tr>
                                            <td>2014</td>
                                            <td></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "../include/aside.php" ?>
    </div>
    <!-- end of main -->

    <?php include "../include/footer.php" ?>
    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>