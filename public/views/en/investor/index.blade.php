@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass->getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')

        <!DOCTYPE html>
<html lang="zh-tw">


<body class="inner-page">


<div class="main">
    <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">Investment</p>
                <p class="bn-description"></p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Investment</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="container-cc">
                <div class="row investor">
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-1.jpg" alt="">
                                </span>
                        </p>
                        <h3>Financial Information</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Monthly-Revenue') }}">
                                    <i class="fas fa-angle-right"></i>Monthly Revenue</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Financial-Report?page=1') }}">
                                    <i class="fas fa-angle-right"></i>Financial Report</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-2.jpg" alt="">
                                </span>
                        </p>
                        <h3>Shareholders Information</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Procedure-Manual?page=1') }}">
                                    <i class="fas fa-angle-right"></i>Procedure Manual</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Notice-of-meeting?page=1') }}">
                                    <i class="fas fa-angle-right"></i>Notice of meeting</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Annual-Report?page=1') }}">
                                    <i class="fas fa-angle-right"></i>Annual Report</a>
                            </li>
                            <!-- <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>議事錄</a>
                            </li> -->
                            <li>
                                <a href="{{ ItemMaker::url('investor/Stock') }}">
                                    <i class="fas fa-angle-right"></i>Stock</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-3.jpg" alt="">
                                </span>
                        </p>
                        <h3>Stakeholder Engagement</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Stakeholder-Engagement') }}">
                                    <i class="fas fa-angle-right"></i>Stakeholder Engagement</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 offset-lg-2 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-4.jpg" alt="">
                                </span>
                        </p>
                        <h3>Corporate Governance Area</h3>
                        <ul class="investor-list">
                            @if(0)
                            <li>
                                <a href="{{ ItemMaker::url('investor/Organization-Team') }}">
                                    <i class="fas fa-angle-right"></i>Organization Structure And Operating Team</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ ItemMaker::url('investor/Board-Director') }}">
                                    <i class="fas fa-angle-right"></i>Board of Directors And Feature Committee</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Company-rule?page=1') }}">
                                    <i class="fas fa-angle-right"></i>Company rules</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Internal-audit') }}">
                                    <i class="fas fa-angle-right"></i>Internal Audit</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Main-Shareholder') }}">
                                    <i class="fas fa-angle-right"></i>Main Shareholder List</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Corporate-Governance-Implementation-Status') }}">
                                    <i class="fas fa-angle-right"></i>Corporate Governance Implementation Status</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-5.jpg" alt="">
                                </span>
                        </p>
                        <h3>Corporate Social Responsibility</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Social-Respons') }}">
                                    <i class="fas fa-angle-right"></i>Corporate Social Responsibility</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Respon-Effects') }}">
                                    <i class="fas fa-angle-right"></i>Corporate Social Responsibility Effectiveness</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Personal-Safety') }}">
                                    <i class="fas fa-angle-right"></i>Employee Benefits、Environment And Personal Safety</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of main -->


<script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="../../assets/js/particlesR.js"></script>
<script src="../../assets/js/pages.js"></script>
</body>

</html>

@include($locateLang.'.includes.aside')
@stop