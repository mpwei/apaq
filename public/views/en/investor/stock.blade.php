@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Stock - ".$ProjectShareName)
@section('keywords', "Stock ,".$ProjectShareName)
@section('description',"Stock ,".$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Intestment</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Intestment</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-3 page-aside02">
                    @include($locateLang.'.investor.aside')
                </div>
                <div class="col-12 col-lg-9 investor-table-wrap">
                    <h2 class="title-investor text-base">Stock</h2>
                    <div class="text-shadow">
                        <p>Common Share Transfer Agent and Registrar、Registration apparatus</p>
                        <ul class="pb-5">
                            <li>・Company：GFS CORPORATION Stock Registration Dept.</li>
                            <li>・Address：6F., No.6, Sec. 1, Zhongxiao W. Rd., Zhongzheng Dist., Taipei City 100, Taiwan (R.O.C.)</li>
                            <li>・Website： <a href="http://www.gfortune.com.tw"></a>http://www.gfortune.com.tw</li>
                            <li>・Phone：(02)2371-1658</li>
                        </ul>
                        <p>Contact Us</p>
                        <ul class="pb-5">
                            <li>・Speaker：Mr. Lin Feng</li>
                            <li>・Acting Speaker：Mr. Lin East River</li>
                            <li>・Tel：037-777588</li>
                            <li>・E-mail：estherli@apaq.com.tw</li>
                        </ul>
                        <p>Stock Quotes</p>
                        <ul class="pb-5">
                            <li>・INPAQ traded on the GreTai Securities Market: Electronics</li>
                            <li>・Symbol:6449</li>
                            <li>・Stock price performance<a href="https://tw.stock.yahoo.com/q/q?s=6449"></a>https://tw.stock.yahoo.com/q/q?s=6449</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop