@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
    @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Monthly Revenue - ".$ProjectShareName)
@section('keywords', "Monthly Revenue ,".$ProjectShareName)
@section('description',"Monthly Revenue ,".$ProjectShareName)

@extends('page')
@section('content')
    <div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">Investment</p>
                <p class="bn-description"></p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Investment</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="container-cc">
                <div class="row investor-detail">
                    <div class="col-12 col-lg-3 page-aside02">
                        @include($locateLang.'.investor.investor-aside')
                    </div>
                    <div class="col-12 col-lg-9 investor-table-wrap">
                        <h2 class="title-investor text-base">Monthly Revenue</h2>
                        <p class="text-right">（Unit：NT$1,000）</p>
                        @if(!isset($data))
                            @break
                        @endif
                        <ul class="nav nav-tabs style-2" role="tablist">
                            @foreach($data as $key => $value)
                                <li class="nav-item">
                                    <a class="nav-link @if($currentYear==$key) active @endif" data-toggle="tab"
                                       href="#tab_{{$key}}" role="tab">{{$key}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="col-12 tab-content">
                            @foreach($data as $key => $value)
                                <div class="col-12 tab-pane @if($currentYear==$key) active @endif form-row-seperated" id="tab_{{$key}}">
                                    {!! $value['content'] !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop