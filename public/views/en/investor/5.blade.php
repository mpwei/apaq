@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
        <!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">
    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Investment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Investment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">Stakeholder Engagement</h2>
                            <div class="text-shadow">
                                <p class="mb-5">Stakeholder Engagement:<span>建立員工、供應商、消費者或客戶等申訴之管道，供其詢問及發表意見，以瞭解利害關係人之合理期望及需求，並提供妥適回應機制，處理利害關係人所關切之議題。</span></p>
                                <p>利害關係人聯絡資訊</p>
                                <ul class="pb-5">
                                    <li>・窗口：林先生</li>
                                    <li>・連絡電話：037-777588</li>
                                    <li>・電子郵件信箱：estherli@apaq.com.tw</li>
                                    <li>本公司基於公平、誠實、守信、透明原則從事商業活動，為落實誠信經營，並積極防範不誠信行為制訂本制度，具體規範本公司人員於執行業務時應注意之事項。</li>
                                </ul>
                                <p>檢舉人應具名檢舉，並可以使用下列方式進行檢舉：</p>
                                <ul class="pb-5">
                                    <li>一、通信地址：苗栗縣竹南鎮科東三路6號4樓 管理處主管收</li>
                                    <li>二、電子郵箱：report@apaq.com.tw</li>
                                    <li>三、檢舉專線：037-777588 #15500</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->
    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop