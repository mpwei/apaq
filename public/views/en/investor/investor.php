@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass->getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')

        <!DOCTYPE html>
<html lang="zh-tw">


<body class="inner-page">


<div class="main">
    <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">Investment</p>
                <p class="bn-description"></p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Investment</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="container-cc">
                <div class="row investor">
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-1.jpg" alt="">
                                </span>
                        </p>
                        <h3>Financial Information</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="investor1.php">
                                    <i class="fas fa-angle-right"></i>Monthly Revenue</a>
                            </li>
                            <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>Financial Report</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-2.jpg" alt="">
                                </span>
                        </p>
                        <h3>Shareholders Information</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>Procedure Manual</a>
                            </li>
                            <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>Notice of meeting</a>
                            </li>
                            <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>Annual Report</a>
                            </li>
                            <!-- <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>議事錄</a>
                            </li> -->
                            <li>
                                <a href="investor8.php">
                                    <i class="fas fa-angle-right"></i>Stock</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-3.jpg" alt="">
                                </span>
                        </p>
                        <h3>Stakeholder Engagement</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="investor10.php">
                                    <i class="fas fa-angle-right"></i>Stakeholder Engagement</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 offset-lg-2 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-4.jpg" alt="">
                                </span>
                        </p>
                        <h3>Corporate Governance Area</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="investor11.php">
                                    <i class="fas fa-angle-right"></i>組織架構及經營團隊</a>
                            </li>
                            <li>
                                <a href="investor13.php">
                                    <i class="fas fa-angle-right"></i>董事會及功能委員會</a>
                            </li>
                            <li>
                                <a href="investor13.php">
                                    <i class="fas fa-angle-right"></i>Company rules</a>
                            </li>
                            <li>
                                <a href="investor13.php">
                                    <i class="fas fa-angle-right"></i>Internal Audit</a>
                            </li>
                            <li>
                                <a href="investor13.php">
                                    <i class="fas fa-angle-right"></i>主要股東名單</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-5.jpg" alt="">
                                </span>
                        </p>
                        <h3>Corporate Social Responsibility</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="http://fittech.oceanad.synology.me/zh-tw/investor/10">
                                    <i class="fas fa-angle-right"></i>企業社會責任政策</a>
                            </li>
                            <li>
                                <a href="http://fittech.oceanad.synology.me/zh-tw/investor/10">
                                    <i class="fas fa-angle-right"></i>企業社會責任實施成效</a>
                            </li>
                            <li>
                                <a href="http://fittech.oceanad.synology.me/zh-tw/investor/10">
                                    <i class="fas fa-angle-right"></i>員工福利、工作環境與人身安全保護</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of main -->


<script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="../../assets/js/particlesR.js"></script>
<script src="../../assets/js/pages.js"></script>
</body>

</html>

@include($locateLang.'.includes.aside')
@stop