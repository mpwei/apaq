<?php
$route = explode('/', $_SERVER['REQUEST_URI']);
$routeGO = $route[sizeof($route) - 1];
?>
<p class="d-block d-lg-none">
    <a class="btn btn-base btn-m-aside-trigger">Investment<i class="pl-2 fas fa-caret-down"></i>
    </a>
</p>
<ul class="investor-list aside02-list">
    <h4 class="aside-title">Financial Information</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Monthly-Revenue') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Monthly Revenue</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Financial-Report?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Financial Report</span></a>
    </li>
    <h4 class="aside-title">Shareholders Information</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Procedure-Manual?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Procedure Manual</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Notice-of-meeting?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Notice of meeting</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Annual-Report?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Annual Report</span></a>
    </li>
    <!-- <li>
        <a href="investor9.php" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>議事錄</span></a>
    </li> -->
    <li>
        <a href="{{ ItemMaker::url('investor/Stock') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Stock</span></a>
    </li>
    <h4 class="aside-title">Stakeholder Engagement</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Stakeholder-Engagement') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Stakeholder Engagement</span></a>
    </li>
    <h4 class="aside-title">Corporate Governance Area</h4>
    @if(0)
        <li>
            <a href="{{ ItemMaker::url('investor/Organization-Team') }}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Organization Structure And Operating Team</span></a>
        </li>
    @endif
    <li>
        <a href="{{ ItemMaker::url('investor/Board-Director') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Board of Directors And Feature Committee</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Company-rule?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Company rules</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Internal-audit') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Internal Audit</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Main-Shareholder') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Main Shareholder List</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Corporate-Governance-Implementation-Status') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Corporate Governance Implementation Status</span></a>
    </li>
    <h4 class="aside-title">Corporate Social Responsibility</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Social-Respons') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Corporate Social Responsibility</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Respon-Effects') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Corporate Social Responsibility Effectiveness</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Personal-Safety') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>Employee Benefits、Environment And Personal Safety</span></a>
    </li>
</ul>
