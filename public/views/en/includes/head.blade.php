    <base href="{{ItemMaker::url('/')}}">
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="author" content="KSONG">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="_token" content="{{ csrf_token() }}">
    @yield('share_meta')

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/plugins/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-free-5.3.1-web/css/all.min.css">
    <link rel="stylesheet" href="assets/plugins/animate.min.css">
    <link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    @yield('css')
    @include($locateLang.'.includes.googleanalytics')
