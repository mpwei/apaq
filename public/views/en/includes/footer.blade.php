<footer class="footer">
    <div class="footer-info d-flex justify-content-center">
        <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <div class="col-12 col-md-3">
                <a href="#" class="d-block pt-2"><img src="assets/images/logo/logo.svg" alt="鈺邦科技" class="footer-logo"></a>
            </div>
            <div class="col-12 col-md-9 row no-gutters">
                <div class="col-12 col-sm-6 footer-office">
                    <ul>
                        <li><span class="f-office-title">Headquarters(Taiwan)</span><a href="https://goo.gl/maps/CoWVp9oyoNL2" target="_blanck">4F., No.2&6, Kedong 3rd Rd., Hsinchu Science Park, Chunan Township, Miaoli County 35053, Taiwan (R.O.C.)</a></li>
                        <li><span class="f-office-title">Tel</span><a href="tel:+886-37-777588" target="_blanck">+886-37-777588</a></li>
                        <li><span class="f-office-title">Fax</span>+886-37-777566</li>
                        <!-- <li><span class="f-office-title">Recruiting</span><a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blanck"><img src="../../assets/images/logo/104logo.svg" alt=""></a></li> -->
                    </ul>
                </div>
                <div class="col-12 col-sm-6 footer-office">
                    <ul>
                        <li><span class="f-office-title">China Factory (Wuxi)</span>No. 1201 Lianfu Road, Xishan Economic Development District, Wuxi City, Jiangsu, China</li>
                        <li><span class="f-office-title">Tel</span><a href="tel:+86-510-81025298" target="_blanck">+86-510-81025298</a></li>
                        <li><span class="f-office-title">Fax</span>+86-510-81025268</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="subfooter">
        <p class="text-center mb-0 d-flex flex-sm-row flex-column justify-content-center">
            <span>Copyright ©APAQ TECHNOLOGY CO., LTD.</span>
            <span>All Rights Reserved.</span>
        </p>
    </div>
    <div class="pagetopbtn" id="PageTopBtn">
        <span class="icon-cc icon-arrow-up"></span>
        <span class="pl-3">GO TOP</span>
    </div>
</footer>

<script type="text/javascript" src="assets/plugins/jquery.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
<script src="assets/plugins/WOW-master/dist/wow.min.js"></script>
<script src="assets/plugins/TweenMax.min.js"></script>
<script type="text/javascript" src="assets/js/clicker_box.js"></script>
<script type="text/javascript" src="assets/plugins/scrollToTop.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.smooth-scroll.js"></script>
<script type="text/javascript" src="assets/js/base.js"></script>
<script>
    $(window).scroll(function(e){
                    if ($(window).scrollTop()<=0){
                        $(".main-header").removeClass("nav-scroll");
                        $(".main-header").addClass("nav-ontop");
                    }
                    else{
                        $(".main-header").removeClass("nav-ontop");
                        $(".main-header").addClass("nav-scroll");
                    }
                });
</script>
