@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
$qty = 0;
$total = 0;
?>
<header class="main-header d-flex justify-content-center">
    <div class="container-cc d-inline-flex justify-content-between">
        <div class="d-flex align-items-center">
            <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-end">
                <span class="logo-pic"><img src="assets/images/logo/logo.png" alt="APAQ 鈺邦科技"></span>
                <span class="logo-word d-flex flex-column">
                    <!-- <span>鈺邦科技股份有限公司</span> -->
                    <span>TECHNOLOGY CO., LTD.</span>
                </span>
            </a>
        </div>
        <div class="d-flex align-items-center">
            <div class="main-menu01 d-none d-lg-none d-xl-flex">
                <ul>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <span>{{$lang['product_title']}}</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <!-- 目前所在頁面，current  -->
                        <!-- <a class="current" href="../products/product-list.php">產品介紹</a> -->
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('product')}}">Conductive Polymer
                                    Capacitor</a></li>
{{--                            <li class="main__dropmenu__item"><a href="http://www.sz-gather.com/en/products.html" target="_blank">Aluminum Electrolytic Capacitor</a></li>--}}
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('application')}}">Application
                                    field</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('news')}}">{{$lang['news_title']}}</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/News')}}">News</a></li>
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/Exhibition')}}">Exhibition</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">Investor</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">Financial Information</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">Monthly
                                                Revenue</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">Financial
                                                Report</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">Shareholders Information</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">Procedure
                                                Manual</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">Notice
                                                of meeting</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">Annual
                                                Report</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Stock')}}">Stock</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <a class="dropmenu__subitem" href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">Stakeholder
                                    Engagement</a>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">Corporate Governance Area</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Board-Director')}}">Board of
                                                Directors And Feature Committee</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">Company
                                                rules</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Internal-audit')}}">Internal
                                                Audit</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">Main
                                                Shareholder List</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">Corporate
                                                Governance Implementation Status</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">Corporate Social Responsibility</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Social-Respons')}}">Corporate
                                                Social Responsibility</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">Corporate
                                                Social Responsibility Effectiveness</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">Employee
                                                Benefits、Environment And Personal Safety</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="main-h-link d-inline-flex align-items-center" id="btn-main-menu" href="#main-menu">
                <div class="collapse-button">
                    <span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </div>
                <div class="h-link-text">Menu</div>
            </div>
            <div class="main-h-link d-none d-lg-none d-xl-flex">
                <div class="main-menu02 d-flex align-items-center">
                    <div class="search-wrap">
                        <a href="#" id="btn-search-open"><span class="fas fa-search"></span></a>
                        {!! Form::open(['class'=>"search-input",'id'=>"search", 'name' => 'search', "url"=> ItemMaker::url('search') ]) !!}
                        <div class="search-form d-flex align-items-center">
                            <input type="text" class="form-control" placeholder="Search..." name="key" id="searchkey">
                            <button id="search-button" class="fas fa-search"></button>
                        </div>
                        {!! Form::close() !!}
                        <div class="search-form d-flex align-items-center">
                        </div>
                    </div>
                    <div class="lang-wrap">
                        <span class="btn-lang">
                            <span>Lang</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="lang-list">
                            <li class="active"><a href="en">Eng</a></li>
                            <li><a href="zh-tw">繁中</a></li>
                            <li><a href="zh-cn">簡中</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of container-cc -->
    <div id="main-menu" class="main-menu">
        <div id="btn-close-mainmenu">
            <div class="collapse-button">
                <div id="cross">
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="h-link-text">Close</div>
        </div>

        <div class="menu-list">
            <div class="m-search-wrap d-flex d-xl-none align-items-start">
                <input type="text" class="form-control" placeholder="Search..." id="mobilesearchkey">
                <a class="btn btn-cc btn-search mobile-search-btn"><span class="fas fa-search"></span></a>
            </div>
            <div class="m-lang-wrap lang-wrap d-flex flex-column d-xl-none">
                <span id="m-btn-lang">
                    <span>Lang</span>
                    <i class="fas fa-caret-down"></i>
                </span>
                <ul class="lang-list d-inline-flex justify-content-start">
                    <li><a href="en">Eng</a></li>
                    <li class="active"><a href="zh-tw">繁中</a></li>
                    <li><a href="zh-cn">簡中</a></li>
                </ul>
            </div>
            <ul>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">{{$lang['product_title']}}<i class=" fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('product')}}">Conductive Polymer Capacitor</a></li>
{{--                        <li class="my-1 px-2 py-2"><a href="http://www.sz-gather.com/en/products.html" target="_blank">Aluminum Electrolytic Capacitor</a></li>--}}
                        {{--                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('application')}}">Application field</a>
                </li>--}}
            </ul>
            </li>
            <li class="d-block d-xl-none">
                <a class="btn-main-submenu">{{$lang['news_title']}}<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/News')}}">News</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/Exhibition')}}">Exhibition</a></li>
                </ul>
            </li>

            <li class="d-block d-xl-none">
                <a class="btn-main-submenu">Investor<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <li class="my-1 px-2 py-2">
                        <a class="btn-main-submenu">Financial Information<i class="fas fa-caret-down pl-2"></i></a>
                        <ul class="main-submenu d-block my-2">
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">Monthly Revenue</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">Financial Report</a></li>
                        </ul>
                    </li>
                    <li class="my-1 px-2 py-2">
                        <a class="btn-main-submenu">Shareholders Information<i class="fas fa-caret-down pl-2"></i></a>
                        <ul class="main-submenu d-block my-2">
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">Monthly Revenue</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">Notice of meeting</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">Annual Report</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stock')}}">Stock</a></li>
                        </ul>
                    </li>
                    <li class="my-1 px-2 py-2">
                        <a class="btn-main-submenu">Stakeholder Engagement<i class="fas fa-caret-down pl-2"></i></a>
                        <ul class="main-submenu d-block my-2">
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">Stakeholder Engagement</a></li>
                        </ul>
                    </li>
                    <li class="my-1 px-2 py-2">
                        <a class="btn-main-submenu">Corporate Governance Area<i class="fas fa-caret-down pl-2"></i></a>
                        <ul class="main-submenu d-block my-2">
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Board-Director')}}">Board of Directors And Feature Committee</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">Company rules</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Internal-audit')}}">Internal Audit</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">Main Shareholder List</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">Corporate Governance Implementation Status</a></li>
                        </ul>
                    </li>
                    <li class="my-1 px-2 py-2">
                        <a class="btn-main-submenu">Corporate Social Responsibility<i class="fas fa-caret-down pl-2"></i></a>
                        <ul class="main-submenu d-block my-2">
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Social-Respons')}}">Corporate Social Responsibility</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">Corporate Social Responsibility Effectiveness</a></li>
                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">Employee Benefits、Environment And Personal Safety</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="btn-main-submenu">About APAQ<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    @if( $getTitle['about'])
                    @foreach($getTitle['about'] as $row )
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/'.$row['title'])}}">{{$row['title']}}</a></li>
                    @endforeach
                    @endif
                    {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about')}}">About Us</a>
            </li> --}}
            <!-- <li class="my-1 px-2 py-2"><a href="#">{{$lang['about_title']}}</a></li> -->
            {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/development-and-core')}}">Development and Core</a>
            </li> --}}
            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('certificate')}}">Certification</a></li>
            </ul>
            </li>
            <li>
                <a class="btn-main-submenu">Design Support<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/simulate')}}">Characteristic with frequency</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/life-time-1')}}">Life Time</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('search-filter')}}">Search filter</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ItemMaker::url('contact')}}">Contact Us</a>
            </li>
            @if(0)
            <li>
                <a class="btn-main-submenu">Contact<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <!--                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">{{$lang['contact_title']}}</a></li>-->
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">Contact Us</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('distributor')}}">Distributors</a></li>
                </ul>
            </li>
            @endif
            {{-- <li><a href="{{ItemMaker::url('album')}}">Album</a></li>
            <li><a href="{{ItemMaker::url('download')}}">Download</a></li> --}}
            <li><a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blank">Recruitment Information</a></li>
            <li><a href="{{ItemMaker::url('policy')}}">Privacy & Cookies Policy</a></li>
            </ul>
        </div>
    </div>

    
    <div id="main-menu-close"></div>
</header>
