@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')
          <div class="in-banner" style="background-image: url(upload/banner/inner/in-bn03.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <!-- <p class="bn-page-title">{{$lang['about_title']}}</p> -->
                    <p class="bn-page-title">{{ $thisAbout->title }}</p>
                    <p class="bn-description">APAQ TECHNOLOGY CO., LTD.</p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">{{$lang['about_bread']}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">APAQ</li>
                            <!-- <li class="breadcrumb-item active" aria-current="page">{{$lang['about_title']}}</li> -->
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            {!! $thisAbout->content !!}
            @if($thisAbout->id ==1 or $thisAbout->title == 'About Us' or $thisAbout->title == '關於我們')
            <section class="main-about-03">
                <div id="about-03" class="about-cata-wrap d-flex justify-content-center wow fadeInUp">
                    <div class="d-flex row-cc row no-gutters">
                        <div class="col-12">
                            <p class="about-title-01 about-title-03 text-center">History</p>
                            <div class="offset-md-3">
                            <ul class="timeline-cc text pt-medium">
                                @if( $history )
                                    @foreach( $history as $row )
                                    <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                        data-effect-delay="300">
                                        <p class="timeline-date"><span>{{$row->title}}</span></p>
                                        <div class="timeline-content">{!! $row->content !!}</div>
                                    </li>
                                    @endforeach
                                @endif


                                {{-- <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2006</span></p>
                                    <div class="timeline-content">
                                        <p>Industrial Technology Research Institute<br>transferred conductive polymer Technology to APAQ<br>AP-CON (winding type) developed<br>AP-CON (winding type) mass production<br>ISO 9001 certificated (UL)</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2008</span></p>
                                    <div class="timeline-content">
                                        <p>Pegatron PUreGMS approved<br>AP-CAP (chip type) developed</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2009</span></p>
                                    <div class="timeline-content">
                                        <p>QC080000 certificated(UL)<br>ASUS Best Partner<br>AP-CAP (chip type) mass production</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2012</span></p>
                                    <div class="timeline-content">
                                        <p>New WUXI Plant 1st stage mass production<br>ISO 14001 certificated(UL)</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2014</span></p>
                                    <div class="timeline-content">
                                        <p>GIGABYTE Best Partner of 2013<br>OHSAS 18001 certificated(UL)<br>New WUXI Plant 2nd stage mass production<br>Listed on Taiwan Stock Exchange (6449)
                                        </p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2015</span></p>
                                    <div class="timeline-content">
                                        <p>GIGABYTE Best Partner of 2014<br>TS 16949 certificated(UL)</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2016</span></p>
                                    <div class="timeline-content">
                                        <p>Taiwan R&D center founded.</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2017</span></p>
                                    <div class="timeline-content">
                                        <p>High voltage capacitor developed.(50v-100v)</p>
                                    </div>
                                </li>
                                <li class="d-flex align-items-start object-non-visible" data-animation-effect="fadeInUp"
                                    data-effect-delay="300">
                                    <p class="timeline-date"><span>2018</span></p>
                                    <div class="timeline-content">
                                        <p>High voltage capacitor mass production</p>
                                    </div>
                                </li> --}}
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif
        </div>

@endsection
