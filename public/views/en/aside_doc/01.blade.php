@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    
    $uri = $_SERVER["REQUEST_URI"];
    $lang = $frontEndClass -> getlang($locateLang);
    $product_menu = $frontEndClass -> getProduct();
    
?>
{{-- <pre>{{print_r($Theme)}}</pre> --}}
@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "PRODUCT LIST - ".$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn02.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Products</p>
            <p class="bn-description">PRODUCT LIST</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">PRODUCT LIST</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="detail-title wow fadeInUp" data-wow-delay=".1s">
                <h1 class="detail-title">PRODUCT LIST</h1>
            </div>
            <div class="detail-main py-5 wow fadeInUp" data-wow-delay=".2s">
                <div class="editor">
                    <!-- 編輯器內容start -->
                    <p class="img">
                        <img src="upload/product_aside/PRODUCTLIST_1.jpg" alt="">
                        <img src="upload/product_aside/PRODUCTLIST_2.jpg" alt="">
                    </p>
                    
                    <!-- 編輯器內容end -->
                </div>
            </div>
        </div>
    </div>

    @include($locateLang.'.includes.aside')
  </section>
</div>
@stop