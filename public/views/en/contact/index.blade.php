@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'Contact Us'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Contact Us</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <section class="main-contact">
        <div class="contact-cata-wrap contactus-info-list d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 mt-2 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>Taiwan headquarters (Chunan)</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/UHJaTaN1RTk" target="_blanck">4F.,
                                    No.2&6, Kedong 3rd Rd., Chunan Science Park, Miaoli County 35053, Taiwan</a></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +886-37-777588</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +886-37-777566</span></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>China Factory (Wuxi)</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/3TQHww3aaPN2" target="_blanck">No.
                                1201 Lianfu Road, Xishan Economic Development District, Wuxi City, Jiangsu, China</a>
                        </span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +86-510-81025298</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +86-510-81025268</span></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>Taipei office</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/YmwjZ9mHDN52" target="_blanck">Rm.
                                5, 6F., No.57, Ln. 69, Jingye 2nd Rd., Zhongshan Dist., Taipei City 10466, Taiwan</a>
                        </span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +886-2-77288510</span></li>
                    </ul>
                </div>
{{--                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">--}}
{{--                    <ul class="text-left">--}}
{{--                        <li>--}}
{{--                            <p><i class="fas fa-globe-asia"></i><span>Shenzhen office</p></span></li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/bzQCxFrN39N2" target="_blanck">RM--}}
{{--                                802,8F Unit 2, Bud. A, KEXING SCIENCE PARK,No.15,Keyuan Avenue, Central Zone, High-tech--}}
{{--                                Zone, Nanshan District,Shenzhen City, 518057 Guangdong Province, China</a> </span></li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Tel: +86-755-82794585</span></li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax : +86-755-82794565</span></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
                @if(0)
                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>Europe Area－European Headquarters(UK)</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/9vX9noQuTC32" target="_blanck">
                                76 Discovery Dock West 2 South Quay
                                Square London E14 9RT United Kingdom</a></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Phone：+44 (0)20 8776 3535</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>E-mail：sales@apaq.eu</span></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>French Office</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-user"></i><span>Ms Kay Chen</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>phone：+33 6688 244 90</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>E-mail：kay.chen@apaq.eu</span></li>
                    </ul>
                </div>
                @endif
                <!-- <div class="col-12 col-md-6 text-center my-2 my-md-3 my-lg-4 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li>
                            <p><i class="fas fa-globe-asia"></i><span>AssemCorp Eletronik A.S.</p></span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i><span><a href="https://goo.gl/maps/ygxcumVyqVo" target="_blanck">Serifali
                                Mah. Miras Sok. No 2534775
                                Yukari Dudullu Umraniye-Istanbul Turkey</a> </span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i><span>Phone: +90 (216)939 26 00</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i><span>Fax: +90 (216)909 31 53</span></li>
                        <li class="d-flex align-items-start"><i class="fas fa-envelope"></i><span>E-mail：Mustafa.oruc@assemcorp.com</span></li>
                    </ul>
                </div> -->
            </div>
        </div>

    </section>
    {!! Form::open(['class'=>'margin-clear','id'=>"active", 'name' => 'active', "url"=> ItemMaker::url('contact/save')
    ]) !!}
    <section>
        <div class="contact-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">
                    <p class="contact-title">If you have any requests or opinions, Please leave your message, we'll
                        contact you soon...</p>
                </div>

                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[company]" id="comapny" type="text" class="form-control" placeholder="Company*" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[department]" id="department" type="text" class="form-control" placeholder="Department*"
                            value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[name]" id="name" type="text" class="form-control" placeholder="Name*" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[country]" id="country" type="text" class="form-control" placeholder="Country*" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[phone]" id="phone" type="text" class="form-control" placeholder="Phone" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[email]" id="email" type="text" class="form-control" placeholder="Email*" value="">
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-6">
                  <div class="contact-form-gp">
                      <input name="Contact[address]" type="text" class="form-control" placeholder="Address" value="">
                  </div>
              </div>
              <div class="col-12 col-sm-12">
                  <div class="contact-form-gp contact-form-gp-cc">
                      <div class="title">Inquiry type*</div>
                      <div class="md-radio md-radio-inline">
                          <input id="category1" type="radio" name="Contact[inquiry_type]" checked="" value="1">
                          <label for="category1">General</label>
                      </div>
                      <div class="md-radio md-radio-inline">
                          <input id="category2" type="radio" name="Contact[inquiry_type]" value="2">
                          <label for="category2">Product</label>
                      </div>
                  </div>
              </div> -->
                <div class="mb-2 col-12 col-sm-6 contact_product">
                    <div class="contact-form-gp select">
                        <select class="form-control" name="Contact[category]">
                            <option>Industry Category</option>
                            @if($otherSelectOptions['category'][$locateLang])
                            @foreach($otherSelectOptions['category'][$locateLang] as $c)
                            <option value="{{$c['id']}}">{{$c['title']}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 contact_product">
                    <div class="contact-form-gp select">
                        <select class="form-control" name="Contact[inquiry_type]">
                            <option>Inquiry Item</option>
                            @if($otherSelectOptions['type'][$locateLang])
                            @foreach($otherSelectOptions['type'][$locateLang] as $c)
                            <option value="{{$c['id']}}">{{$c['title']}}</option>
                            @endforeach
                            @endif
                            <!-- <option value="10">Business Inquiry</option>
                          <option value="11">Professional Consultant For Products</option>
                          <option value="12">Comments and feedback</option>
                          {{-- <option value="13">Other</option> --}} -->
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="contact-form-gp">
                        <textarea class="form-control" rows="10" id="comment" name="Contact[message]" placeholder="Message:"></textarea>
                    </div>
                </div>
                <div class="col-12 col-sm-6 captcha">
                    <div id="changeCap" class="changeCap contact-form-gp">
                        {!! Captcha::img() !!}
                        <input type="text" placeholder="Captcha" name="captcha">
                    </div>
                </div>
                <div class="text-right col-12 col-sm-6">
                    <div class="contact-form-gp">
                        {{-- <a class="contactBtn">Send</a> --}}
                        <input type="submit" value="Send" class="btn contactBtn">
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}
</div>
<div id="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d28996.63107481984!2d120.919453!3d24.706999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6b50d1d682129a70!2z6Yi66YKm56eR5oqA!5e0!3m2!1szh-TW!2stw!4v1541640966369"
        width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

@endsection



@section('script')
<script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="assets/js/particlesR.js"></script>
<script src="assets/js/pages.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(24.7069993, 120.9194525), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.7069993, 120.9194525),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script> -->
<script src="assets/js/front/contactUs.js?12"></script>
@endsection
