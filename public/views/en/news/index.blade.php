@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/news-bn01.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">News</p>
          <p class="bn-description">News and Exhibition</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">News</li>
              </ol>
          </nav>
      </div>
  </div>
  
  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="submenu-list-wrap d-flex flex-column justify-content-center align-items-center">
  <div class="d-flex container-cc wow fadeInUp justify-content-start" data-wow-delay=".1s">
      <a href="#" class="btn btn-base d-block d-md-none btn-submenu">News Category <span class="arrow-cc"></span></a>
  </div>
  <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
      <div class="submenu-list d-block d-md-flex flex-md-fill filter-button-group">
          <ul class="nav nav-pills d-block d-md-flex" id="news-tabs-wrap" role="tablist">
              <?php $active = !$title? 'active': ''; ?>
              <li class="nav-item flex-fill">
                  <a class="nav-link {{$active}}" href="{{ItemMaker::url('news')}}">All</a>
              </li>
              @if($Category)
                @foreach($Category as $row)
                    <?php $active = $title == $row->title? 'active': ''; ?>
                    <li class="nav-item flex-fill">
                        <a class="nav-link {{$active}}" href="{{$row->web_link}}">{{$row->title}}</a>
                    </li>
                @endforeach
              @endif
          </ul>
      </div>
  </div>
</div>
<div class="inner-main">
  <div class="d-flex justify-content-center">
      <div class="container-cc">
          <div class="tab-content news-tab-content wow fadeInUp" data-wow-delay=".1s">
              <div id="tab01" class="tab-pane active">
                  <div class="row no-gutters row-cc news-list">
                      <div class="col-2 d-flex align-items-center justify-content-center news-title">No.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-title">Date</div>
                      <div class="col-6 d-flex align-items-center justify-content-center news-title">Subject</div>
                      @if( $pageData['data'] )
                        @foreach($pageData['data'] as $key=> $row)
                            <div class="col-2 d-flex align-items-center justify-content-center news-data">{{$key+1}}.</div>
                            <div class="col-4 d-flex align-items-center justify-content-center news-data">{{$row['date']}}</div>
                            <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                                <a href="{{$row['web_link']}}">{{$row['title']}}</a>
                            </div>
                        @endforeach
                      @endif

                      {{-- <div class="col-2 d-flex align-items-center justify-content-center news-data">2.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2017/03/24</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">106/03/29受邀參加元大證券舉辦之法人說明會</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">3.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2017/03/09</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">PCIM exhibition APAQ stand H-7-512 Nuremberg 16-18 May 2017</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">4.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2014/12/09</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">2014/12/09 鈺邦科技正式掛牌上市</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">5.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2014/11/20</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">2014.11.20上市前業績發表會簡報及影音檔</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">6.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2014/03/24</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">第40屆台北國際電子產業科技展（TAITRONICS）10/6-10/9</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">7.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2014/01/09</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">2014/08/01台北辦公室地址變更</a></div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">8.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2014/01/09</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">2014/5/21 榮獲ASUS績優廠商</a></div> --}}
                  </div>
                  @if($pageData['total_pages'] > 1)
                    <div class="container-cc my-5">
                      <nav aria-label="Page navigation">
                          <ul class="pagination justify-content-center">
                              <li class="page-item"><a class="page-link" href="{{$pageData['url']}}"><<</a></li>
                              @for ($i = 0; $i < $pageData['total_pages']; $i++)
                                <li class="page-item"><a class="page-link" href="{{$pageData['url'].'?page='.($i+1)}}">{{$i+1}}</a></li>
                              @endfor
                              <li class="page-item"><a class="page-link" href="{{$pageData['url'].'?page='.$pageData['total_pages']}}">>></a></li>
                          </ul>
                      </nav>
                    </div>
                  @endif
              </div>
              {{-- <div id="tab02" class="tab-pane fade">
                  <div class="row row-cc news-list">
                      <div class="col-2 d-flex align-items-center justify-content-center news-title">No.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-title">日期</div>
                      <div class="col-6 d-flex align-items-center justify-content-center news-title">主旨</div>
                      <div class="col-2 d-flex align-items-center justify-content-center news-data">1.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-data">2018/09/11</div>
                      <div class="col-6 d-flex align-items-center justify-content-start news-data news-subject">
                          <a href="news-detail.php">electronica exhibition APAQ stand B6-131 Messe Munich 13-16
                              Nov. 2018</a></div>
                  </div>
                  <div class="container-cc my-5">
                      <nav aria-label="Page navigation">
                          <ul class="pagination justify-content-center">
                              <li class="page-item"><a class="page-link" href="#">1</a></li>
                          </ul>
                      </nav>
                  </div>
              </div>
              <div id="tab03" class="tab-pane fade">
                  <div class="row row-cc news-list">
                      <div class="col-2 d-flex align-items-center justify-content-center news-title">No.</div>
                      <div class="col-4 d-flex align-items-center justify-content-center news-title">日期</div>
                      <div class="col-6 d-flex align-items-center justify-content-center news-title">主旨</div>
                  </div>
                  <div class="container-cc my-5">
                      <nav aria-label="Page navigation">
                          <ul class="pagination justify-content-center">
                              <li class="page-item"><a class="page-link" href="#">1</a></li>
                          </ul>
                      </nav>
                  </div>
              </div> --}}
          </div>
      </div>
  </div>
</div>
@include($locateLang.'.includes.aside')
@stop