@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();

?>

@section('title', 'Characteristic with frequency'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="main">
    <div class="in-banner" style="background-image: url(../../upload/banner/inner/in-bn02.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">Characteristic with frequency</p>
                <p class="bn-description">Design Support</p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Characteristic with frequency</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="row container-cc">

                <div class="col-12 col-lg-2 page-aside02">
                    <p class="d-block d-lg-none">
                        <a class="btn btn-base btn-m-aside-trigger">投資專區選單<i class="pl-2 fas fa-caret-down"></i></a>
                    </p>
                    <ul class="investor-list aside02-list">
                        <h4 class="aside-title">Design Support</h4>
                        <li class="active">
                            <a href="{{ItemMaker::url('design/simulate')}}" class="active d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Characteristic with frequency</span></a>
                        </li>
                        <li>
                            <a href="{{ItemMaker::url('design/life-time-1')}}" class="d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Life Time</span></a>
                        </li>
                    </ul>
                </div>

                <div class="col-12 col-lg-10 investor-table-wrap">
                    <h2 class="title-design text-base">Characteristic with frequency</h2>
                    <div class="cc-control text-center">
                        {!! Form::open( ["url" => ItemMaker::url('design/simulate/search'), 'method'=> 'POST', 'class' => 'form-inline contact-cata-wrap mt-5', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                        <div class="align-center d-block d-lg-inline-block align-middle cc-control-wrap mb-2">

                            <div class="d-inline-block align-middle select ml-sm-1 mb-2 mb-md-0">
                                <select name="series">
                                    <option value="">Select Series</option>
                                    @foreach($seriesDropDown as $key => $value)
                                    <option value="{{$value['series']}}" @if($value['series']==$series) selected @endif>{{$value['series']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-inline-block align-middle select ml-sm-1 mb-2 mb-md-0 ml-md-4">
                                <select name="wv">
                                    <option value="">Reta Voltage (v)</option>
                                    @foreach($wvDropDown as $key => $value)
                                    <option value="{{$value['wv']}}" @if($value['wv']==$wv) selected @endif>{{$value['wv']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-inline-block align-middle select ml-sm-1 mb-2 mb-md-0 ml-md-4">
                                <select name="cap">
                                    <option value="">Capacitance (uF)</option>
                                    @foreach($capDropDown as $key => $value)
                                    <option value="{{$value['cap']}}" @if($value['cap']==$cap) selected @endif>{{$value['cap']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="align-center d-block d-lg-inline-block align-middle cc-control-btns mb-2">
                            <input type="submit" value="Search" class="d-inline-block align-middle ml-2 ml-md-4 btn-control">
                            <a onclick="viewAll('{{ItemMaker::url('design/simulate')}}')" class="d-inline-block align-middle ml-2 ml-md-4 btn-control btn-h-control">View All</a>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <div class="prod-design-data-wrap">
                        <div class="table-responsive my-5">
                            <table width＝99.9% class="table text-center align-middle table-content-center table-investor01 table-scroller">
                                <tbody>
                                    <tr>
                                        <th>Series</th>
                                        <th>Part No.</th>
                                        <th>WV<br>(V.DC)</th>
                                        <th>Cap<br>(uF)</th>
                                        <th>Diameter<br>Ø (mm)</th>
                                        <th>Body Height<br>(mm)<br>Max</th>
                                        <th>ESR<br>(mΩmax/20%,<br>100Hz<br>to 300kHz)</th>
                                        <th>Rated Ripple<br>Current<br>(mArms/105°С<br>/100kHz)</th>
                                        <th>Rated Ripple<br>Current<br>(mArms/125°С<br>/100kHz)</th>
                                        <th>Simulation<br>Data</th>

                                    </tr>
                                    @foreach($data as $key => $value)
                                    <tr>
                                        <td>{{$value['series']}}</td>
                                        <td>{{$value['part_no']}}</td>
                                        <td>{{$value['wv']}}</td>
                                        <td>{{$value['cap']}}</td>
                                        <td>{{$value['diameter']}}</td>
                                        <td>{{$value['body_height_max']}}</td>
                                        <td>{{$value['esr']}}</td>
                                        <td>{{$value['rate_ripple_current']}}</td>
                                        <td>{{$value['rate_ripple_current2']}}</td>
                                        <td>
                                            @if(!empty($value['file']))
                                            <a href="{{ItemMaker::url('design/simulate/'.$value['id'].'?time='.time())}}" class="bn-link btn-cc btn-cc02 m-0">
                                                <span class="btn-arrow justify-content-center align-items-center d-inline-flex mr-2">
                                                    <i class="far fa-file-pdf"></i>
                                                </span>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="../../assets/js/particlesR.js"></script>
<script src="../../assets/js/pages.js"></script>
</body>

</html>

<script>
    function viewAll(url) {
        window.location = url;
    }
</script>