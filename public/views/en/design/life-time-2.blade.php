@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'Life Time'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Life Time</p>
            <p class="bn-description">Design Support</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Life Time</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <!-- 20181210 -->
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-2 page-aside02">
                    <ul class="investor-list aside02-list">
                        <h4 class="aside-title">Design Support</h4>
                        <li>
                            <a href="{{ItemMaker::url('design/simulate')}}" class="d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Characteristic with frequency</span></a>
                        </li>
                        <li class="active">
                            <a href="{{ItemMaker::url('design/life-time-1')}}" class="active d-flex align-items-start"><i class="fas fa-angle-right pr-2"></i><span>Life Time</span></a>
                        </li>
                    </ul>
                </div>

                <div class="col-12 col-lg-10 investor-table-wrap">
                    <!-- Life Time -->
                    <h2 class="title-investor text-base">Life Time</h2>

                    <!-- Life Time page-->
                    <p>Please note that the estimated life result from this calculator is for reference only and NOT a guaranteed life specification.</p>
                    <div class="table-responsive">
                        <table class="table table-bordered align-middle my-2">
                            <tbody>
                                <tr>
                                    <td>
                                        @if($type=="solid")
                                        <img src="assets/images/Conductive Polymer Aluminum Solid Capacitors.png" alt="" class="mt-0">
                                        @elseif($type=="electrolytic")
                                        <img src="assets/images/Conductive Polymer Aluminum Electrolytic Capacitors.png" alt="" class="mt-0">
                                        @else
                                        <img src="assets/images/Conductive Polymer Hybrid Aluminum Electrolytic Capacitors.png" alt="" class="mt-0">
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered align-middle mt-1">
                            <tbody>
                                <tr>
                                    <th width="20%">Series</th>
                                    <td class="form-inline contact-cata-wrap">
                                        <!-- <label class="my-1 mr-2">Series :</label> -->
                                        <div class="custom-control contact-form-gp pl-0 ml-0">
                                            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                                                @foreach($series_list as $key => $item)
                                                <option @if($key==$series)selected @endif value="{{$item['hours']}}-{{$item['temperature']}}">{{$key}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%">Specified life time</th>
                                    <td height="85px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" readonly value="{{$data['hours']}}" class="form-control" id="hours">
                                        </div>
                                        <label class="unit my-1 mr-4">Hours</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">Max operating temperature of the capacitor</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" readonly value="{{$data['temperature']}}" class="form-control" id="temperature">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>


                                <tr>
                                    <th width="20%">Ambient temperature of the capacitor</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="b" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>

                                @if($type=="solid")
                                <tr>
                                    <th width="20%">Actual ambient temperature of the capacitor</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="c" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">°C</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">Applied ripple current to capacitor</th>
                                    <td height="120px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="d" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">mArms</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="20%">Rated ripple current of capacitor<br>*Please refer to the catalog of each series about rated ripple current</th>
                                    <td height="230px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0">
                                            <input type="text" id="e" class="form-control">
                                        </div>
                                        <label class="unit my-1 mr-4">mArms</label>
                                    </td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    <div class="form-inline contact-cata-wrap mt-2 justify-content-center">

                        <div class="contact-form-gp">
                            <!-- <input type="submit" value="Calculate" class="btn contactBtn mb-0"> -->
                            <a id="btn_size" class="btn btn-h-base">Calculate</a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered align-middle">
                            <tbody>
                                <tr>
                                    <th width="20%">Estimated life time</th>
                                    <td height="85px" class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0" id="f">
                                        </div>
                                        <label class="unit my-1 mr-4">Hours</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%"></th>
                                    <td class="form-inline contact-cata-wrap">
                                        <label class="my-1 mr-2">Code :</label>
                                        <div class="custom-control contact-form-gp pl-0 ml-0 mr-0" id="g">
                                        </div>
                                        <label class="unit my-1 mr-4">Years</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="20%">Note</th>
                                    <td class="form-inline contact-cata-wrap">
                                        <label class="unit my-1 mr-4">L<sub>x</sub> : life expectance(hours) in actual use (temperature T<sub>x</sub>)</label>
                                        <label class="unit my-1 mr-4">L<sub>0</sub> : specified lifetime with the rated voltage at the upper limit of the category temperature(hour)</label>
                                        <label class="unit my-1 mr-4">T<sub>0</sub> : Maximun category temperature</label>
                                        <label class="unit my-1 mr-4">T<sub>x</sub> : Temperature of aluminum case surface in actual use</label>
                                        <label class="unit my-1 mr-4">T<sub>a</sub> : Ambient temperature in actual use</label>
                                        <label class="unit my-1 mr-4">I<sub>r</sub> : operating ripple current(actual ripple current)</label>
                                        <label class="unit my-1 mr-4">I<sub>0</sub> : rating ripple current(spec. ripple current)</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-inline contact-cata-wrap mt-2 justify-content-center">

                        <div class="contact-form-gp">
                            <!-- <input type="submit" value="Back to select the series" class="btn contactBtn mb-0"> -->
                            <a class="btn btn-h-base" href="{{ItemMaker::url('design/life-time-1')}}">Back to List</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    window.onload = function() {
        const selectElement = document.querySelector('#inlineFormCustomSelectPref');
        
        selectElement.addEventListener('change', (event) => {
            console.log();
            let result = event.target.value.split('-');
            const temperature = document.querySelector('#temperature');
            const hours = document.querySelector('#hours');
            hours.value = result[0];
            temperature.value = result[1];
        });
    <?php if($type=="solid"){ ?>
        alert("Please enter the value of Ambient temperature of the capacitor or Actual ambient temperature of the capacitor.");

        document.getElementById("b").addEventListener("keyup", (event)=>{
            if(document.getElementById("b").value!=""){
                document.getElementById("c").readOnly = true;
                document.getElementById("d").readOnly = true;
                document.getElementById("e").readOnly = true;
            }else{
                document.getElementById("c").readOnly = false;
                document.getElementById("d").readOnly = false;
                document.getElementById("e").readOnly = false;
            }
        });
        document.getElementById("c").addEventListener("keyup", (event)=>{
            if(document.getElementById("c").value!=""){
                document.getElementById("b").readOnly = true;
            }else{
                document.getElementById("b").readOnly = false;
            }
        });
    <?php } else { ?>
        alert("Please enter the value of Ambient temperature of the capacitor.");
    <?php } ?>
    document.getElementById("btn_size").addEventListener("click", (event)=>{
        <?php if($type=="solid"){ ?>
            const b = document.getElementById("b").value;
            const c = document.getElementById("c").value;
            const d = document.getElementById("d").value;
            const e = document.getElementById("e").value;
            const hours = document.getElementById("hours").value;
            const temperature = document.getElementById("temperature").value;
            if(b=="" && c==""){
                alert("Please enter the value of Ambient temperature of the capacitor or Actual ambient temperature of the capacitor.");
            }else if(b!=""){
                alert('Notice!\n(1). Please be mindful the calculated life result of the capacitor is for reference only.\n(2). Please use the capacitor with abundant life time when designing products.\nFifteen(15) years is generally considered to be the maximum for the estimated life result obtained by using this calculator. If you want to estimate life time sixteen(16) years or more, please contact your local APAQ sales office.');
                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20)/24/365 * 10) /10;
            }else if(c!="" && d!="" && e!=""){
                if(parseInt(d)>parseInt(e)){
                    alert("Notice! The value of Applied ripple current to capacitor cannot < the value of Rated ripple current of capacitor. Please enter again.");
                }else{
                    alert('Notice!\n(1). Please be mindful the calculated life result of the capacitor is for reference only.\n(2). Please use the capacitor with abundant life time when designing products.\nFifteen(15) years is generally considered to be the maximum for the estimated life result obtained by using this calculator. If you want to estimate life time sixteen(16) years or more, please contact your local APAQ sales office.');
                    let c7 = 20 * Math.pow((parseInt(d)/parseInt(e)),2);
                    document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - c7 - parseInt(c))/20));
                    document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - c7 - parseInt(c))/20)/24/365 *10) /10;
                }
            }else{
                alert("Please enter the values of Actual ambient temperature of the capacitor, Applied ripple current to capacitor and Rated ripple current of capacitor.");
            }
        <?php } else { ?>
            const b = document.getElementById("b").value;
            const hours = document.getElementById("hours").value;
            const temperature = document.getElementById("temperature").value;
            if(b==""){
                alert("Please enter the value of Ambient temperature of the capacitor.");
            }
            <?php if($type=="electrolytic"){ ?>
                alert('Notice!\n(1). Please be mindful the calculated life result of the capacitor is for reference only.\n(2). Please use the capacitor with abundant life time when designing products.\nFifteen(15) years is generally considered to be the maximum for the estimated life result obtained by using this calculator. If you want to estimate life time sixteen(16) years or more, please contact your local APAQ sales office.');
                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(10, (temperature - parseInt(b))/20)/24/365 *10) /10;
            <?php } else { ?>
                alert('Notice!\n(1). Please be mindful the calculated life result of the capacitor is for reference only.\n(2). Please use the capacitor with abundant life time when designing products.\nFifteen(15) years is generally considered to be the maximum for the estimated life result obtained by using this calculator. If you want to estimate life time sixteen(16) years or more, please contact your local APAQ sales office.');
                document.getElementById("f").innerHTML = Math.round(hours * Math.pow(2, (temperature - parseInt(b))/10));
                document.getElementById("g").innerHTML = Math.round(hours * Math.pow(2, (temperature - parseInt(b))/10)/24/365 *10) /10;
            <?php } ?>
        <?php } ?>
    });
};

</script>