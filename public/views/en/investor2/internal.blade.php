@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Internal Audit - ".$ProjectShareName)
@section('keywords', "Internal Audit ,".$ProjectShareName)
@section('description',"Internal Audit ,".$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Invesotment</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Invesotment</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-3 page-aside02">
                    @include($locateLang.'.investor.aside')
                </div>
                <div class="col-12 col-lg-9 investor-table-wrap">
                    <h2 class="title-investor text-base">Internal Audit</h2>
                    <div class="text-shadow">
                        <p>組織：</p>
                        <ul>
                            <li>本公司稽核室屬於董事會，配置稽核主管1人，專職內部稽核工作。</li>
                        </ul>
                        <p>運作：</p>
                        <ul>
                            <li>1. 每年依風險評估提出年度稽核計畫，經董事會通過後，執行稽核作業並每月出具內部稽核業務報告或追蹤報告經呈核後，呈獨立董事及監察人查閱，且列席董事會報告。</li>
                            <li>2. 每年覆核各單位之內部控制有效性自行評估表，並彙總自行評估表及稽核報告結果，以為出具內部控制聲明書之依據。</li>
                            <li>3. 提供各功能部門管理與流程諮詢性建議，逐步提昇稽核作業價值並改善營運績效品質。</li>
                        </ul>
                        <p>獨立董事、監察人與內部稽核主管及會計師溝通政策：</p>
                        <ul>
                            <li>1.
                                獨立董事與會計師至少每年一次定期會議，會計師就本公司財務狀況、海內外子公司財務及整體運作情形及內控查核情形向獨立董事報告，並針對有無重大調整分錄或法令修訂有無影響帳列情形充分溝通；若遇重大異常事項時得隨時召集會議。</li>
                            <li>2. 內部稽核主管與獨立董事至少每季一次定期會議，就本公司內部稽核執行狀況及內控運作情形提出報告；若遇重大異常事項時得隨時召集會議。</li>
                        </ul>
                        <p>獨立董事、監察人與內部稽核主管及會計師溝通政策：</p>
                        <div class="pb-5 table-cc">
                            <table class="table table-striped table-fixed table-content-center table-investor01">
                                <thead>
                                    <tr>
                                        <th>日期</th>
                                        <th>溝通對象</th>
                                        <th>溝通重點</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>2017/03/15</td>
                                        <td>會計師</td>
                                        <td class="text-left font-cc">・會計師就105年財務情形進行說明，並針對部分會計原則適用問題進行討論。<br>・會計師針對與會人員所提問題進行討論及溝通</td>
                                    </tr>
                                    <tr>
                                        <td>2017/03/15</td>
                                        <td>內部稽核主管</td>
                                        <td class="text-left font-cc">・105年第四季稽核業務執行報告。</td>
                                    </tr>
                                    <tr>
                                        <td>2017/05/03</td>
                                        <td>內部稽核主管</td>
                                        <td class="text-left font-cc">・106年第一季稽核業務執行報告。</td>
                                    </tr>
                                    <tr>
                                        <td>2017/08/07</td>
                                        <td>內部稽核主管</td>
                                        <td class="text-left font-cc">・106年第二季稽核業務執行報告。</td>
                                    </tr>
                                    <tr>
                                        <td>2017/11/02</td>
                                        <td>內部稽核主管</td>
                                        <td class="text-left font-cc">・106年第三季稽核業務執行報告。</td>
                                    </tr>
                                    <tr>
                                        <td>2018/02/27</td>
                                        <td>會計師</td>
                                        <td class="text-left font-cc">・會計師就106年財務情形進行說明，並針對部分會計原則適用問題進行討論。<br>・會計師針對與會人員所提問題進行討論及溝通。
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2018/02/27</td>
                                        <td>內部稽核主管</td>
                                        <td class="text-left font-cc">・106年第四季稽核業務執行報告。</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop