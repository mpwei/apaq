@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Organizational Structure And Team - ".$ProjectShareName)
@section('keywords', "Organizational Structure And Team ,".$ProjectShareName)
@section('description',"Organizational Structure And Team ,".$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Investment</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Investment</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-3 page-aside02">
                    @include($locateLang.'.investor.aside')
                </div>
                <div class="col-12 col-lg-9 investor-table-wrap">
                    <h2 class="title-investor text-base">Organizational Structure And Team</h2>
                    <img src="upload/invest/invest.jpg" alt="">
                    <div class="row">
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：董事長</li>
                                <li>・姓名：鄭敦仁</li>
                                <li>・經歷：成功大學材料博士</li>
                                <li class="text-indent">工業技術研究院材料所正研究員</li>
                                <li class="text-indent">乾坤科技研發資深經理</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：執行長</li>
                                <li>・姓名：林清封</li>
                                <li>・經歷：成功大學材料博士</li>
                                <li class="text-indent">美國愛荷華州立大學化工博士</li>
                                <li class="text-indent">永剛科技(股)公司執行副總經理</li>
                                <li class="text-indent">立敦科技(股)公司執行副總經理</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：總經理</li>
                                <li>・姓名：林溪東</li>
                                <li>・經歷：中正理工學院機械碩士</li>
                                <li class="text-indent">中山科學研究院計畫品質策進會副主委</li>
                                <li class="text-indent">佳邦科技(股)公司品保處處長</li>
                                <li class="text-indent">禾邦電子(中國)有限公司總經理</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：業務處副總經理</li>
                                <li>・姓名：林瀚淵</li>
                                <li>・經歷：成功大學化學工程所碩士</li>
                                <li class="text-indent">工業技術研究院IEK產業分析師</li>
                                <li class="text-indent">工業技術研究院生醫所副研究員</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：海外業務處副總經理兼品保中心副總經理</li>
                                <li>・姓名：Liong Chee Keong</li>
                                <li>・經歷：Certificate in Management</li>
                                <li class="text-indent">中山科學研究院計畫品質策進會副主委</li>
                                <li class="text-indent">(University Of Malaya)</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：行政處副總經理</li>
                                <li>・姓名：鄭富仁</li>
                                <li>・經歷：中原大學企業管理系</li>
                                <li class="text-indent">鈺邦科技(股)公司總經理特助</li>
                                <li class="text-indent">佳鼎電子(股)公司奈米事業部生產處長</li>
                                <li class="text-indent">永剛科技(股)有限公司總經理特助</li>
                                <li class="text-indent">溫州泰慶皮革有限公司副廠長</li>
                                <li class="text-indent">上海威泰製革廠副總經理兼廠長</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：行銷部協理</li>
                                <li>・姓名：劉士山</li>
                                <li>・經歷：成功大學電機研究所碩士</li>
                                <li class="text-indent">工業技術研究院材料化工研究所研究員</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：研發處處長</li>
                                <li>・姓名：陳明宗</li>
                                <li>・經歷：清華大學材料所碩士</li>
                                <li class="text-indent">聯華電子(股)公司高級工程師</li>
                                <li class="text-indent">工業技術研究院材料化工研究所副研究員</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：新產品處處長</li>
                                <li>・姓名：錢明谷</li>
                                <li>・經歷：清華大學材料科學工程學系學士、碩士</li>
                                <li class="text-indent">工業技術研究院材料化工研究所副研究員</li>
                                <li class="text-indent">統達能源(股)公司總經理室經理</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：財務部經理</li>
                                <li>・姓名：李佩玲</li>
                                <li>・經歷：文化大學會計系</li>
                                <li class="text-indent">安侯建業聯合會計師事務所審計部副理</li>
                                <li class="text-indent">禾邦電子(蘇州)有限公司管理處副理</li>
                                <li class="text-indent">佳邦科技(股)公司會計經理</li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 text-shadow text-shadow-cc">
                            <ul>
                                <li>・職稱：新產品處處長</li>
                                <li>・姓名：郭少隴</li>
                                <li>・經歷：逢甲大學會計系</li>
                                <li class="text-indent">新加坡優普集團優普電子(股)公司資深經理</li>
                                <li class="text-indent">鈺邦電子(無錫)有限公司財務部經理</li>
                                <li class="text-indent">鈺邦科技(股)公司總經理室專案經理</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop