@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Monthly Revenue - ".$ProjectShareName)
@section('keywords', "Monthly Revenue ,".$ProjectShareName)
@section('description',"Monthly Revenue ,".$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Investment</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Investment</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-3 page-aside02">
                    @include($locateLang.'.investor.aside')
                </div>
                <div class="col-12 col-lg-9 investor-table-wrap">
                    <h2 class="title-investor text-base">Monthly Revenue</h2>
                    <p class="text-right">（Unit：NT$1,000）</p>
                    <ul class="nav nav-tabs style-2" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#h2tab1" role="tab">2018</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#h2tab2" role="tab">2017</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#h2tab3" role="tab">2016</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#h2tab4" role="tab">2015</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#h2tab4" role="tab">2014</a></li>
                    </ul>
                    <div class="table-responsive">
                        <table class="table table-striped table-fixed table-content-center table-investor01">
                        <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Monthly turnover</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>232,209</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>149,505</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>230,233</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>214,364</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>200,379</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>202,495</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>174,536</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>1,403,721</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop