    <p class="d-block d-lg-none">
        <a class="btn btn-base btn-m-aside-trigger" >Investment<i class="pl-2 fas fa-caret-down"></i>
        </a>
    </p>
    <ul class="investor-list aside02-list">
        <h4 class="aside-title">Financial Information</h4>
        <li class="active">
            <a href="{{ItemMaker::url('investor/monthly-revenue')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Monthly Revenue</span></a>
        </li>
        <li>
            <a href="{{ItemMaker::url('investor/financial-report')}}" class="active d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Financial Report</span></a>
        </li>
        <h4 class="aside-title">Shareholders Information</h4>
        <li>
            <a href="{{ItemMaker::url('investor/stock')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Procedure Manual</span></a>
        </li>
        {{-- <li>
            <a href="investor9.php" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Notice of meeting</span></a>
        </li> --}}
        {{-- <li>
            <a href="investor9.php" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Annual Report</span></a>
        </li> --}}
        <!-- <li>
            <a href="investor9.php" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>議事錄</span></a>
        </li> -->
        {{-- <li>
            <a href="investor9.php" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Stock</span></a>
        </li> --}}
        {{-- <h4 class="aside-title">Stakeholder Engagement</h4>
        <li>
            <a href="{{ItemMaker::url('investor/stakeholder-engagement')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Stakeholder Engagement</span></a>
        </li> --}}
        <h4 class="aside-title">Corporate Governance Area</h4>
        <li>
            <a href="{{ItemMaker::url('investor/organizational-structure-and-team')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Organizational Structure And Team</span></a>
        </li>
        {{-- <li>
            <a href="#" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>董事會及功能委員會</span></a>
        </li>
        <li>
            <a href="#" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Company rules</span></a>
        </li> --}}
        <li>
            <a href="{{ItemMaker::url('investor/internal-audit')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Internal Audit</span></a>
        </li>
        <li>
            <a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>Corporate Governance Implementation Status</span></a>
        </li>
        {{-- <li>
            <a href="#" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>主要股東名單</span></a>
        </li> --}}
        <h4 class="aside-title">Corporate Social Responsibility</h4>
        <li>
            <a href="{{ItemMaker::url('investor/organizational-structure-and-team')}}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>企業社會責任政策</span></a>
        </li>
        {{-- <li>
            <a href="#" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>企業社會責任政策成效</span></a>
        </li>
        <li>
            <a href="#" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>員工福利、工作環境與人身安全保護</span></a>
        </li> --}}
    </ul>
