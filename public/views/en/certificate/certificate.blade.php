@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
    @include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')
    <div class="in-banner" style="background-image: url(upload/banner/inner/in-bn03.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <!-- <p class="bn-page-title">{{$lang['about_title']}}</p> -->
                <p class="bn-page-title">Certification</p>
                <p class="bn-description">APAQ TECHNOLOGY CO., LTD.</p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ItemMaker::url('about')}}}">{{$lang['about_bread']}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Certification</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="about-bg02">
            <div class="about-cata-wrap">
                <div class="d-block d-sm-flex justify-content-center ceri-wrap01">
                    <div class="row-cc row no-gutters">
                        @foreach($data as  $key => $value)
                            <div class="col-12 col-sm-4 ceri-item px-4 mb-5 mb-sm-1 wow fadeInUp">
                                <p class="img p-1">
                                    <img alt="" src="{{$value['image']}}"/>
                                </p>

                                <p class="about-title-01 py-3">{{$value['title']}}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
