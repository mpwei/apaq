@extends('backend.template')

@section('title',"最新消息修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			廣告管理 <small>Banner Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/廣告管理/首頁廣告')}}">廣告管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					廣告修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">



					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 廣告管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/廣告管理/首頁廣告') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Banner[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}


													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'Banner[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}


													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'Banner[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

                                                    {{--ItemMaker::select([
                                                        'labelText' => '廣告區塊',
                                                        'inputName' => 'Banner[block]',
                                                        'options' => $Block,
                                                        'helpText' =>'列表圖片區塊所要顯示的內容',
                                                        'value' => ( !empty($data['block']) )? $data['block'] : ''
                                                    ])--}}

													{{ItemMaker::textInput([
														'labelText' => '標題',
														'inputName' => 'Banner[title]',
                                                        'helpText' =>'輸入「,」前台可以將標題換行',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}


													{{ItemMaker::textArea([
														'labelText' => '內容',
														'inputName' => 'Banner[content]',
                                                        'helpText' =>'輸入「,」前台可以將標題換行',
														'value' => ( !empty($data['content']) )? $data['content'] : ''
													])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '連結',
                                                        'inputName' => 'Banner[link]',
                                                        'helpText' =>'',
                                                        'value' => ( !empty($data['link']) )? $data['link'] : ''
                                                    ])}}

                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '連結另開',
                                                        'inputName' => 'Banner[is_href]',
                                                        'helpText' =>'',
                                                        'value' => ( !empty($data['is_href']) )? $data['is_href'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
														'labelText' => '圖片',
														'inputName' => 'Banner[image]',
														'helpText' =>'第一區塊圖片建議大小為 ：寬最大1920px，高不限 ',
														'value' => ( !empty($data['image']) )? $data['image'] : ''
													])}}


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>
<script src="vendor/main/news.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

        var news_check = new News();
	</script>
@stop
