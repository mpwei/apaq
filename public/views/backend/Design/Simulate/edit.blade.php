@extends('backend.template')

@section('title',"模擬資料修改 "."-".$ProjectName)

@section('css')
    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css"/>
@stop
@section('content')

    <!--fancybox for File Manager-->
    <a href="" id="toFileManager"></a>

    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        模擬資料管理
        <small>Dividend Information Manager</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">模擬資料管理</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                模擬資料修改
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->


    <div class="row">
        <div class="col-md-12">


        <!--<form action="{{ ItemMaker::url('backen-admin/模擬資料/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
            {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

            <div class="portlet light portlet-fit portlet-datatable">
                <div class="portlet-title portlet-title2">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 項目管理
										</span>
                    </div>
                    <div class="actions btn-set">
                        <a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix ) }}" type="button" name="back"
                           class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button"
                           data-container="body" data-placement="top" data-original-title="返回">
                            <i class="fa fa-mail-reply"></i> 返回
                        </a>

                        <button type="submit" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button"
                                data-container="body" data-placement="top" data-original-title="儲存">
                            <i class="fa fa-check"></i> 儲存
                        </button>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs nav_news">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"> 模擬資料 </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                <div class="form-body form_news_pad">
                                    {{ItemMaker::idInput([
                                        'inputName' => 'Simulate[id]',
                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                    ])}}

                                    {{ItemMaker::radio_btn([
										'labelText' => '是否顯示',
										'inputName' => 'Simulate[is_visible]',
										'helpText' =>'',
										'options' => $StatusOption,
										'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
									])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Rank',
                                        'inputName' => 'Simulate[rank]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                    ])}}

                                    {{ItemMaker::textInput([
                                       'labelText' => 'Series',
                                       'inputName' => 'Simulate[series]',
                                       'helpText' =>'',
                                       'value' => ( !empty($data['series']) )? $data['series'] : ''
                                   ])}}

                                    {{ItemMaker::textInput([
                                       'labelText' => 'Part No.',
                                       'inputName' => 'Simulate[part_no]',
                                       'helpText' =>'',
                                       'value' => ( !empty($data['part_no']) )? $data['part_no'] : ''
                                   ])}}

                                    {{ItemMaker::textInput([
                                       'labelText' => 'WV(V.DC)',
                                       'inputName' => 'Simulate[wv]',
                                       'helpText' =>'',
                                       'value' => ( !empty($data['wv']) )? $data['wv'] : ''
                                   ])}}

                                    {{ItemMaker::textInput([
                                       'labelText' => 'Cap(uF)',
                                       'inputName' => 'Simulate[cap]',
                                       'helpText' =>'',
                                       'value' => ( !empty($data['cap']) )? $data['cap'] : ''
                                   ])}}

                                    {{ItemMaker::textInput([
                                       'labelText' => 'DiameterØ(mm)',
                                       'inputName' => 'Simulate[diameter]',
                                       'helpText' =>'',
                                       'value' => ( !empty($data['diameter']) )? $data['diameter'] : ''
                                   ])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Body Height (mm) Max',
                                        'inputName' => 'Simulate[body_height_max]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['body_height_max']) )? $data['body_height_max'] : ''
                                    ])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'ESR (mΩmax/20%, 100kHz to 300kHz)',
                                        'inputName' => 'Simulate[esr]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['esr']) )? $data['esr'] : ''
                                    ])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Rated Ripple Current (mArms/ 105°С/ 100kHz)',
                                        'inputName' => 'Simulate[rate_ripple_current]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['rate_ripple_current']) )? $data['rate_ripple_current'] : ''
                                    ])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Rated Ripple Current (mArms/ 125°С/ 100kHz)',
                                        'inputName' => 'Simulate[rate_ripple_current2]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['rate_ripple_current2']) )? $data['rate_ripple_current2'] : ''
                                    ])}}

                                    {{ItemMaker::filePicker([
                                        'labelText' => 'Simulation Date',
                                        'inputName' => 'Simulate[file]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['file']) )? $data['file'] : ''
                                    ])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
            <!--</form>
             END FORM-->
            {!! Form::close() !!}
        </div>
    </div>

@stop

@section("script")

    <script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


    <script src="vendor/main/colorpick.js" type="text/javascript"></script>
    <script src="vendor/main/datatable.js" type="text/javascript"></script>
    <script src="vendor/main/rank.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
            toastrAlert('success', '{{$Message}}');
            @endif
        });

    </script>
@stop
