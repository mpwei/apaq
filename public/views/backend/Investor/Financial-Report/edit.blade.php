@extends('backend.template')

@section('title',"Financial-Report修改 "."-".$ProjectName)

@section('css')
    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css"/>
@stop
@section('content')

    <!--fancybox for File Manager-->
    <a href="" id="toFileManager"></a>

    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Financial-Report管理
        <small>Dividend Information Manager</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">Financial-Report管理</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                Financial-Report修改
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->


    <div class="row">
        <div class="col-md-12">


        <!--<form action="{{ ItemMaker::url('backen-admin/Financial-Report/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
            {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

            <div class="portlet light portlet-fit portlet-datatable">
                <div class="portlet-title portlet-title2">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 項目管理
										</span>
                    </div>
                    <div class="actions btn-set">
                        <a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix ) }}" type="button" name="back"
                           class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button"
                           data-container="body" data-placement="top" data-original-title="返回">
                            <i class="fa fa-mail-reply"></i> 返回
                        </a>

                        <button type="submit" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button"
                                data-container="body" data-placement="top" data-original-title="儲存">
                            <i class="fa fa-check"></i> 儲存
                        </button>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs nav_news">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"> Financial-Report </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                <div class="form-body form_news_pad">
                                    {{ItemMaker::idInput([
                                        'inputName' => 'FinancialInfo[id]',
                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                    ])}}

                                    {{ItemMaker::idInput([
                                        'inputName' => 'FinancialInfo[category]',
                                        'value' => 'Financial-Information'
                                    ])}}

                                    {{ItemMaker::idInput([
                                        'inputName' => 'FinancialInfo[sub_category]',
                                        'value' => 'Financial-Report'
                                    ])}}

                                    {{ItemMaker::radio_btn([
										'labelText' => '是否顯示',
										'inputName' => 'FinancialInfo[is_visible]',
										'helpText' =>'',
										'options' => $StatusOption,
										'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
									])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Rank',
                                        'inputName' => 'FinancialInfo[rank]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                    ])}}

                                    {{ItemMaker::textInput([
                                        'labelText' => 'Year',
                                        'inputName' => 'FinancialInfo[year]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['year']) )? $data['year'] : ''
                                    ])}}

                                    {{ItemMaker::filePicker([
                                        'labelText' => 'Q1',
                                        'inputName' => 'FinancialInfo[q1]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['q1']) )? $data['q1'] : ''
                                    ])}}

                                    {{ItemMaker::filePicker([
                                        'labelText' => 'Q2',
                                        'inputName' => 'FinancialInfo[q2]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['q2']) )? $data['q2'] : ''
                                    ])}}

                                    {{ItemMaker::filePicker([
                                        'labelText' => 'Q3',
                                        'inputName' => 'FinancialInfo[q3]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['q3']) )? $data['q3'] : ''
                                    ])}}

                                    {{ItemMaker::filePicker([
                                        'labelText' => 'Q4',
                                        'inputName' => 'FinancialInfo[q4]',
                                        'helpText' =>'',
                                        'value' => ( !empty($data['q4']) )? $data['q4'] : ''
                                    ])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
            <!--</form>
             END FORM-->
            {!! Form::close() !!}
        </div>
    </div>

@stop

@section("script")

    <script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


    <script src="vendor/main/colorpick.js" type="text/javascript"></script>
    <script src="vendor/main/datatable.js" type="text/javascript"></script>
    <script src="vendor/main/rank.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
            toastrAlert('success', '{{$Message}}');
            @endif
        });

    </script>
@stop
