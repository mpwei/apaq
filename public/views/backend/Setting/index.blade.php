@extends('backend.template')

@section('title',"網站基本設定修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			網站基本設定管理 <small>News Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/網站基本設定')}}">網站基本設定管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					網站基本設定修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">


					<!--<form action="{{ ItemMaker::url('backen-admin/網站基本設定/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ItemMaker::url('backen-admin/網站基本設定/update'), 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											網站基本設定管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/網站基本設定/') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">


                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '運費設定',
                                                        'inputName' => 'Setting[shipping_cost]',
                                                        'helpText' =>'假設空白就是不收運費',
                                                        'value' => ( !empty($data['shipping_cost']) )? $data['shipping_cost'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '滿額免運費設定',
                                                        'inputName' => 'Setting[shipping_cost_free]',
                                                        'helpText' =>'假設空白就是依照運費',
                                                        'value' => ( !empty($data['shipping_cost_free']) )? $data['shipping_cost_free'] : ''
                                                    ])--}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '地址',
                                                        'inputName' => 'Setting[address]',
                                                        //'helpText' =>'用來設定聯絡我們「Area of Intrest」的下拉內容，請用「,」隔開',
                                                        'value' => ( !empty($data['address']) )? $data['address'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '電話',
                                                        'inputName' => 'Setting[tel]',
                                                        //'helpText' =>'用來設定聯絡我們「Area of Intrest」的下拉內容，請用「,」隔開',
                                                        'value' => ( !empty($data['tel']) )? $data['tel'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '主要聯絡人信箱',
                                                        'inputName' => 'Setting[email]',
                                                        //'helpText' =>'用來設定聯絡我們「Area of Intrest」的下拉內容，請用「,」隔開',
                                                        'value' => ( !empty($data['email']) )? $data['email'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '訂單通知信',
                                                        'inputName' => 'Mail[mails]',
                                                        'helpText' =>'用來設定訂單通知信，請用「,」隔開每個信箱,',
                                                        'value' => ( !empty($mails['mails']) )? $mails['mails'] : ''
                                                    ])}}


                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
