@extends('backend.template') @section('title',"OverView "."-".$ProjectName) @section('body_class' , 'page-sidebar-closed')
@section('css')
<link href="vendor/backend/scripts/fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="vendor/backend/scripts/plugins.css" rel="stylesheet" type="text/css" />

<link href="vendor/backend/assets/pages/css/about.min.css" rel="stylesheet" type="text/css" /> @endsection @section('content') {{--
<!-- BEGIN CONTENT HEADER -->
<div class="row margin-bottom-40 about-header about-header2 ">
    <div class="col-md-12">
        <h1>About Us</h1>
        <h2>Life is either a great adventure or nothing</h2>
        <a class="btn btn-danger" href="{{ ItemMaker::url('/') }}" target="_blank">GO Home</a>
    </div>
</div>
--}}
<!-- END CONTENT HEADER -->
<!-- BEGIN CARDS -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN CARDS -->
        @foreach( $MenuList as $key => $value )
            <?php
                if (is_array($value['link'])) {
                    $link = reset($value["link"]);
                } else {
                    $link = $value["link"];
                }
            ?>
            <a href="{{ ItemMaker::url($link) }}" target="_top">
                <div class="col-lg-3 col-md-6">
                    <div class="portlet light">
                        <div class="card-icon">
                            <?php print($value['icon']);?>
                        </div>
                        <div class="card-title">
                            <span> {{ $key }} </span>
                        </div>
                        <div class="card-desc">
                            <span>
                                <?php print($value['slogan'])?>
                            </span>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
        {{-- <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/首頁管理" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-home icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 首頁管理 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    首頁管理 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/關於我們" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-wrench icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 關於我們 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    關於我們簡述 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/作品範例" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-basket icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 產品 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    產品簡述 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/部落格" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-paper-plane icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 最新消息 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    最新消息 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/聯絡我們" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-envelope-open icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 聯絡我們 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    聯絡我們簡述 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="https://www.oceanad.com.tw/zh-tw/backen-admin/帳號管理" target="_top">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="portlet light ">
                    <div class="row">
                        <div class="card-icon col-xs-5 col-sm-12">
                            <i class="icon-users icons font-green-haze theme-font"></i>
                        </div>
                        <div class="col-xs-7 col-sm-12">
                            <div class="card-title">
                                <span> 帳號管理 </span>
                            </div>
                            <div class="card-desc">
                                <span>
                                    帳號管理簡述 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a> --}}

    </div>
</div>
<!-- END PAGE CONTENT-->

@endsection @section('script')
<script src="vendor/backend/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="vendor/backend/plugins/jquery.uniform.min.js" type="text/javascript"></script>
<script src="vendor/backend/plugins/jquery.cookie.min.js" type="text/javascript"></script>

@endsection