<!-- BEGIN HEADER INNER -->
<div class="page-header-inner ">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
        <a href="{{ ItemMaker::url('backen-admin')}}">
            <img src="vendor/backend/assets/layouts/layout2/img/logo-default.png" alt="logo" class="logo-default" /> </a>
        <div class="menu-toggler sidebar-toggler">
            <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
        </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN PAGE ACTIONS -->
    <!-- DOC: Remove "hide" class to enable the page header actions -->


    <div class="page-actions page-actions_us">

        <div class="user_title">
            <!-- <span class="user_id"> {{ $ProjectName }}</span> -->
            <span class="fantsy_id"> 後端管理系統 </span>
        </div>
    </div>


    <!-- END PAGE ACTIONS -->
    <!-- BEGIN PAGE TOP -->
    <div class="page-top">
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN INBOX DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-language" id="languages-select">

                    <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="selected">
                            <img alt="" src="vendor/backend/assets/global/img/flags/tw.png">
                            <span class="langname"> Tw </span>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item">
                            <a href="/zh-tw/backen-admin" class="@if($locateLang == 'zh-tw') current @endif" data-toggle="" data-hover="" data-close-others="true">
                                <img alt="" src="vendor/backend/assets/global/img/flags/tw.png">
                                <span class="langname"> Tw </span>
                            </a>
                        </li>
                        <li class="dropdown-item">
                            <a href="/zh-cn/backen-admin" class="@if($locateLang == 'zh-cn') current @endif" data-toggle="" data-hover="" data-close-others="true">
                                <img alt="" src="vendor/backend/assets/global/img/flags/tw.png">
                                <span class="langname"> Cn </span>
                            </a>
                        </li>
                        <li class="dropdown-item">
                            <a href="/en/backen-admin" class="@if($locateLang == 'en') current @endif" data-toggle="" data-hover="" data-close-others="true">
                                <img alt="" src="vendor/backend/assets/global/img/flags/us.png">
                                <span class="langname"> En </span>
                            </a>
                        </li>
                    </ul>

                </li>
                {{--
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar2">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span class="badge badge-default"> {{ count($Contact_count) }} </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="external">
                        <h3>您有
                            <span class="bold">{{ count($Contact_count) }} 新</span> 訊息
                        </h3>
                        <a href="{{ItemMaker::url('backen-admin/與我們聯絡/')}}">顯示全部</a>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">

                            @if( !empty($Contact_count) ) @foreach($Contact_count as $row)
                            <li>
                                <a href="{{ ItemMaker::url('backen-admin/與我們聯絡/edit/'.$row['id'] ) }}">
                                    <span class="subject">
                                        <span class="from">{{ $row['name'] }} {{ $row['first_name'] }}</span>
                                        <span class="time">{{ $row['created_at'] }} </span>
                                    </span>
                                </a>
                            </li>
                            @endforeach @endif
                        </ul>
                    </li>
                </ul>
                </li>
                --}}
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username">
                            <i class="icon-user"></i>{{Auth::user()->name}} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        {{--
                        <li>
                            <a href="page_user_profile_1.html">
                                <i class="icon-user"></i> My Profile </a>
                        </li> --}}

                        <li>
                            <a href=" {{ ItemMaker::url('auth/logout') }} ">
                                <i class="icon-key"></i> 登出 </a>
                        </li>
                    </ul>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
</div>
<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->