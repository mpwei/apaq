@extends('backend.template')

@section('title',"最新消息修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			最新消息管理 <small>News Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/最新消息/訊息')}}">最新消息管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					最新消息修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
 {{--                    <div class="note note-danger">
                        <p> 注意事項:  </p>
                        <li>內頁圖片是顯示在內頁的第一個區塊。</li>
                        <li>影片代碼目前支援youtube 和 vimeo</li>      
                        <li>youtube 影片代碼在 https://www.youtube.com/watch?v=「 -VMFdpdDYYA 」, exp : youtube,-VMFdpdDYYA</li>   
                        <li>vimeo 影片代碼在 https://vimeo.com/ 「 99125284 」, exp : vimeo,99125284</li>  
                    </div>--}} 
                    <!--注意事項END-->

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => 'hidFrame' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 最新消息管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/最新消息/訊息') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>
                                        <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a>

                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
{{--                                             <li >
                                                <a href="#photo" data-toggle="tab"> 內頁圖片 </a>
                                            </li> --}}

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'News[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'News[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}

                                                    {{--ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示於首頁',
                                                        'inputName' => 'News[is_show_home]',
                                                        'helpText' =>'是否顯示於首頁輪播中',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_show_home']) )? $data['is_show_home'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::select([
                                                        'labelText' => '所屬分類',
                                                        'inputName' => 'News[category_id]',
                                                        'options' => $category,
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])--}}

													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'News[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

													{{ItemMaker::textInput([
														'labelText' => '標題',
														'inputName' => 'News[title]',
                                                        'helpText' =>'標題為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}

													{{ItemMaker::textArea([
														'labelText' => '列表簡述',
														'inputName' => 'News[list_slogan]',
														'helpText' =>'列表簡述，限定200個字',
														'value' => ( !empty($data['list_slogan']) )? $data['list_slogan'] : ''
													])}}


                                                    {{--ItemMaker::editor([
                                                        'labelText' => '內容',
                                                        'inputName' => 'News[content]',
                                                        'helpText' =>'最新消息內容',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::select([
                                                        'labelText' => '列表圖片顯示類型',
                                                        'inputName' => 'News[show_type]',
                                                        'options' => $Show_type,
                                                        'helpText' =>'列表圖片區塊所要顯示的內容',
                                                        'value' => ( !empty($data['show_type']) )? $data['show_type'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::idInput([
                                                        'inputName' => 'show_type',
                                                        'value' => ( !empty($data['show_type']) )? $data['show_type'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::photo([
														'labelText' => '圖片',
														'inputName' => 'News[image]',
														'helpText' =>'列表所顯示的圖片',
														'value' => ( !empty($data['image']) )? $data['image'] : ''
													])--}}

                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '影片連結',
                                                        'inputName' => 'News[image]',
                                                        'helpText' =>'用來存放youtube 和 vimeo的影片連結，直接填入影片代碼 exp: -VMFdpdDYYA 或 99125284',
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::datePicker([
                                                        'labelText' => '時間標',
                                                        'inputName' => 'News[date]',
                                                        'helpText' => '前台顯示會依照此欄位做排序，由新到舊',
                                                        'value' => ( !empty($data['date']) )? $data['date'] : ''
                                                    ])--}}
                                                </div>
                                            </div>

                                            <div class="tab-pane form-row-seperated" id="photo">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => '最新消息/訊息-Photo',
                                                            'datas' => ( !empty($data['photo']) )? $data['photo'] : [],
                                                            'table_set' =>  $bulidTable,
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>
<script src="vendor/main/news.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

        var news_check = new News();
	</script>
@stop
