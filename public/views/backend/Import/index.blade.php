@extends('backend.template')

@section('css')

<link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />

@stop

@section('content')

<!-- 內容最上方大標題 -->
<h3 class="page-title"> 資料匯入管理
    <small>Store List</small>
</h3>
<!-- 內容最上方大標題END -->
<!--網站導覽-->
<div class="page-bar">
<ul class="page-breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="{{ ItemMaker::url("backen-admin/") }}">Home</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <span>資料匯入</span>
    </li>
</ul>

</div>
<!--網站導覽END-->
<!-- END PAGE HEADER-->
<div class="row">
<!--內容區開始-->
<div class="col-md-12">
    <!--注意事項-->
    {{-- <div class="note note-danger">
    </div> --}}
    <!--注意事項END-->
    <!--內容區白色區塊-->
    <div class="portlet light portlet-fit portlet-datatable ">
        <!--內容白色區塊標題-->
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">資料匯入</span>
            </div>
            <!--actions-->
            <div class="actions">

                {{-- <div class="btn-group btn-group-devided" >
						<a href="{{ItemMaker::url("backen-admin/".$routePreFix."/create")}}" class="btn green-sharp btn-circle btn-sm">
							<i class="fa fa-plus"></i> 新增
						</a>
                </div> --}}

                {{-- @include('Backend.include.actionMenu') --}}

            </div>
            <!--actions END-->
        </div>
        <!--內容白色區塊標題END-->

        <!--內容白色區塊表格-->
        <div class="portlet-body">
            <div class="table-container">
                {!! Form::open([ "files"=>true, "url" => ItemMaker::url( "Backend/資料匯入/save" )]) !!}
                    <div>
                        {{ItemMaker::select([
                            'labelText' => '欲匯入的項目',
                            'inputName' => 'type',
                            'options' => [
                                [
                                    "id"=> "Member",
                                    "title" => "會員匯入"
                                ],
                                [
                                    "id"=> "ProductSpecSize",
                                    "title" => "庫存修正"
                                ],
                                [
                                    "id"=> "MemberCard",
                                    "title" => "會員優惠匯入"
                                ]
                            ],
                            "value" => ''
                        ])}}
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">檔案</label>
                        <div class="col-md-10">
                            <input
                            	type="file"
                            	class="form-control"
                            	name="data"
                            	value=""
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            <input type="submit"  value="送出">
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
            <!--內容白色區塊表格END-->
        </div>
        <!-- 內容白色區塊END-->
    </div>
    <!--內容區END-->
</div>

@stop
@section('script')

	<script src="vendor/Backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>

	<script src="vendor/Backend/plugins/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
	<script type="text/javascript" src="vendor/Backend/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="vendor/main/datatable.js"></script>
	<script>

		//資料刪除
		activeDataTable();
		dataDelete();
        changeStatic_ajax();
        changeStatic();

        @if( !empty( $Message ) )
            toastrAlert('info', '{{$Message}}');
        @endif
	</script>
@stop
