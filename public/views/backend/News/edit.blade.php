@extends('backend.template')

@section('title',"最新消息修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			最新消息管理 <small>News Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/最新消息/消息列表')}}">最新消息管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					最新消息修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">


					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 最新消息管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/最新消息/消息列表') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button><!--
                                        <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a>

                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div>
                                         <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
{{--                                             <li >
                                                <a href="#photo" data-toggle="tab"> 內頁圖片 </a>
                                            </li> --}}

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'News[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'News[is_visible]',
                                                        'helpText' =>'是否顯示於前台的「列表」中',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                    {{--ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示首頁',
                                                        'inputName' => 'News[is_home]',
                                                        'helpText' =>'是否顯示於前台的「首頁」中',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_home']) )? $data['is_home'] : ''
                                                    ])--}}


                                                    {{--ItemMaker::radio_btn([
                                                        'labelText' => '是否置頂',
                                                        'inputName' => 'News[is_top]',
                                                        'helpText' =>'是否置頂',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_top']) )? $data['is_top'] : ''
                                                    ])--}}

													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'News[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬分類',
                                                        'inputName' => 'News[category_id]',
                                                        'options' => $parent['belong']['NewsCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}

													{{ItemMaker::textInput([
														'labelText' => '標題',
														'inputName' => 'News[title]',
                                                        'helpText' =>'標題為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}

                                                    {{ItemMaker::datePicker([
                                                        'labelText' => '日期',
                                                        'inputName' => 'News[date]',
                                                        'helpText' =>'',
                                                        'value' => ( !empty($data['date']) && $data['date'] != '0000-00-00')? $data['date'] : date('Y-m-d')
                                                    ])}}
                                                    
													{{--ItemMaker::textArea([
                                                        'labelText' => '列表簡述',
                                                        'inputName' => 'News[list_slogan]',
                                                        'helpText' =>'列表簡述，限定200個字',
                                                        'value' => ( !empty($data['list_slogan']) )? $data['list_slogan'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '詳細頁日期',
                                                        'inputName' => 'News[date_show]',
                                                        'helpText' =>'',
                                                        'value' => ( !empty($data['date_show']) )? $data['date_show'] : ''
                                                    ])--}}

                                                    {{ItemMaker::editor([
                                                        'labelText' => '文章內容',
                                                        'inputName' => 'News[content]',
                                                        'helpText' =>'文章內容',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    {{--ItemMaker::datePicker([
                                                        'labelText' => '上架日期',
                                                        'inputName' => 'News[start_date]',
                                                        'helpText' =>'',
                                                        'value' => ( !empty($data['start_date']) && $data['start_date'] != '0000-00-00')? $data['start_date'] : date('Y-m-d')
                                                    ])--}}
                                                    
                                                    
                                                    {{--ItemMaker::photo([
                                                        'labelText' => '列表圖片',
                                                        'inputName' => 'News[image]',
                                                        'inputDelName' => 'News[del_photo]',
                                                        'helpText' =>'顯示於列表圖片，建議大小為：600px X 600px',
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])--}}
 
                                                    {{--ItemMaker::photo([
                                                        'labelText' => '詳細頁圖片',
                                                        'inputName' => 'News[image_detail]',
                                                        'inputDelName' => 'News[del_photo]',
                                                        //'helpText' =>'顯示於詳細頁圖片，建議大小為：1920px X 1280px',
                                                        'value' => ( !empty($data['image_detail']) )? $data['image_detail'] : ''
                                                    ])--}}

                                                </div>
                                            </div>

{{--                                             <div class="tab-pane form-row-seperated" id="photo">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => '最新消息/訊息-Photo',
                                                            'datas' => ( !empty($data['photo']) )? $data['photo'] : [],
                                                            'table_set' =>  $bulidTable,
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div> --}}

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>
<script src="vendor/main/news.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

        var news_check = new News();
	</script>
@stop
