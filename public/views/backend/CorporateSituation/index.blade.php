@extends('backend.template')

@section('css')
<link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
<style>
    div[v-cloak] {
        display: none
    }
</style>
@stop

@section('content')

<!-- 內容最上方大標題 -->
<h3 class="page-title"> {{ $data->title }} </h3>
<!-- 內容最上方大標題END -->
<!--網站導覽-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{ ItemMaker::url('backen-admin/') }}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>

        <li>
            <span>{{ $data->title }}</span>
        </li>
    </ul>

</div>
<!--網站導覽END-->
<!-- END PAGE HEADER-->
<div class="row" id="app" v-cloak>
    <!--內容區開始-->
    <div class="col-md-12">
        <!--注意事項--
    <div class="note note-danger">
    </div>
    <!--注意事項END-->
        <!--內容區白色區塊-->
        <div class="portlet light portlet-fit portlet-datatable ">
            <!--內容白色區塊表格-->
            <div class="portlet-body">
                <div class="table-container">
                    <form id="form" class="form-horizontal" enctype="multipart/form-data" action="{{ route('corporate-situation.store', ['locale?' => $data->language ]) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-md-2 text-right">
                                    <label for="title">Title<span red>*</span></label>
                                </div>
                                <div class="col-md-10">
                                    <input id="title" name="title" type="text" class="form-control" value="{{ $data->title }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2 text-right">
                                    <label for="pdf_title">Content status<span red>*</span></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="checkbox" name="status" value=1 value="{{ $data->status }}" @if($data->status) checked @endif>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Content
                                </label>
                                <div class="col-md-10">
                                    <textarea name="content" data-provide="markdown" rows="10" class="ckeditor tooltips form-control" data-container="body" data-placement="top">{{ $data->content }}</textarea>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Events
                                </label>
                                <div class="col-md-10">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer">
                                        <thead>
                                            <tr role="row" class="heading" id="FormTags">
                                                <th>Date*</th>
                                                <th>Topic*</th>
                                                <th>Result*</th>
                                                <th>Order <a class="btn ml-3" @click="addEvent"><i class="fa fa-plus"></i></a>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="FormLine">
                                            <tr v-for="(event, index) in events">
                                                <td><input type="hidden" :name="'events[' + index + '][corporate_situation_id]'" value="{{ $data->id }}" /><input type="hidden" :name="'events[' + index + '][id]'" :value="event.id" /><input type="date" :name="'events[' + index + '][date]'" v-model="event.date" required /></td>
                            <td><input type="text" :name="'events[' + index + '][topic]'" v-model="event.topic" required /></td>
                            <td><input type="text" :name="'events[' + index + '][result]'" v-model="event.result" required /></td>
                            <td><input type="text" :name="'events[' + index + '][order]'" v-model="event.order" /><a class="btn ml-3" @click="removeEvent(index)"><i class="fa fa-trash"></i></td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                </div> --}}
                <div class="form-group">
                    <label class="col-md-2 control-label">
                        Pdfs
                    </label>
                    <div class="col-md-10">
                        <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer">
                            <thead>
                                <tr role="row" class="heading" id="FormTags">
                                    <th><input id="pdf_title" name="pdf_title" type="text" class="form-control" value="{{ $data->pdf_title }}" required></th>
                                    <th><input id="pdf_file" name="pdf_file" type="text" class="form-control" value="{{ $data->pdf_file }}" required></th>
                                    <th>Status</th>
                                    <th>Order <a class="btn ml-3" @click="addPdf"><i class="fa fa-plus"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="FormLine">
                                <tr v-for="(pdf, index) in pdfs" :key="pdf.id">
                                    <td>
                                        <input type="hidden" :name="'pdfs[' + index + '][corporate_situation_id]'" value="{{ $data->id }}" />
                                        <input type="hidden" :name="'pdfs[' + index + '][id]'" :value="pdf.id" />
                                        <input type="hidden" :name="'pdfs[' + index + '][path]'" :value="pdf.path" />
                                        <input type="text" :name="'pdfs[' + index + '][title]'" v-model="pdf.title" required />
                                    </td>
                                    <td>
                                        <input type="file" :name="'pdfs[' + index + '][file]'" v-model="pdf.file" v-if="pdf.id" />
                                        <input type="file" :name="'pdfs[' + index + '][file]'" v-model="pdf.file" v-if="!pdf.id" required />
                                        <a :href="pdf.path">@{{ pdf.path }}</a>
                                    </td>
                                    <td>
                                        <input type="checkbox" :name="'pdfs[' + index + '][status]'" value=1 v-model="pdf.status">
                                    </td>
                                    <td><input type="text" :name="'pdfs[' + index + '][order]'" v-model="pdf.order" /><a class="btn ml-3" @click="removePdf(index)"><i class="fa fa-trash"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a @click="save" class="btn btn-primary pull-right">Submit</a>
            </div>
            </form>
        </div>
    </div>
    <!--內容白色區塊表格END-->
</div>
<!-- 內容白色區塊END-->
</div>
<!--內容區END-->
</div>

@stop
@section('script')

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>

<script src="vendor/backend/plugins/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript" src="vendor/backend/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="vendor/main/datatable.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

<script>
    new Vue({
    el: '#app',
    data() {
        return {
            events: {!! $data->events !!},
            pdfs: {!! $data->pdfs !!}
        }
    },
    methods: {
        save: function()
        {
            var confirm_result = confirm("是否確認送出這筆資料?");
            if(confirm_result){
                var form = document.getElementById('form');
                for(var i=0; i < form.elements.length; i++){
                    if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
                        this.message = 'There are some required fields!';
                        alert(this.message);
                        return false;
                    }
                }
                form.submit();
            }
        },
        addEvent: function()
        {
            this.events.push({
                'date':'',
                'topic':'',
                'result':'',
                'order':'',
            })
        },
        addPdf: function()
        {
            this.pdfs.push({
                'title':'',
                'file': '',
                'order':'',
                'status': true,
                'id': 'new'+ this.pdfs.length
            })
        },
        removePdf: function(index)
        {
            this.pdfs.splice(index, 1);
        },
        removeEvent: function(index)
        {
            this.events.splice(index, 1);
        }
    }
 });
    
</script>
<style>
    .el-switch__core {
        -webkit-border-radius: 10px !important;
        border-radius: 10px !important;
    }
</style>
@stop