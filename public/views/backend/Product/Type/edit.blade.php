@extends('backend.template')

@section('title',"類型修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類型管理 <small>Type Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
                <li>
                    產品
                    <i class="fa fa-angle-right"></i>
                </li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">類型管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					類型修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    <div class="note note-danger">
                        <p> 注意事項: 大圖的排序都需要優先於小圖。<br>
                        ex. 選擇一組四張，排序將會是 大, 小, 小, 小<br>
                            選擇一組三張，排序也是 大, 小, 小</p>
                    </div>
                    <!--注意事項END-->

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => 'hidFrame' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類型管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>
                                        <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a>

                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
<!--                                             <li>
                                                <a href="#time" data-toggle="tab"> 分店照片 </a>
                                            </li> -->

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductType[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'ProductType[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬主題',
                                                        'inputName' => 'ProductType[theme_id]',
                                                        'options' => $parent['belong']['ProductTheme'],
                                                        'value' => ( !empty($data['theme_id']) )? $data['theme_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬類別',
                                                        'inputName' => 'ProductType[category_id]',
                                                        'options' => $parent['belong']['ProductCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}



                                                    {{ItemMaker::select([
                                                        'labelText' => '排版方式',
                                                        'inputName' => 'ProductType[composing]',
                                                        'options' => $otherSelectOptions,
                                                        'value' => ( !empty($data['composing']) )? $data['composing'] : ''
                                                    ])}}


													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'ProductType[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

													{{ItemMaker::textInput([
														'labelText' => '類型名稱',
														'inputName' => 'ProductType[title]',
                                                        'helpText' =>'類型名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定100字',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '副標題',
                                                        'inputName' => 'ProductType[sub_title]',
                                                        'helpText' =>'類型名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定80字',
                                                        'value' => ( !empty($data['sub_title']) )? $data['sub_title'] : ''
                                                    ])}}

                                                    {{--ItemMaker::photo([
                                                        'labelText' => '門市照片-小圖',
                                                        'inputName' => 'ProductType[sm_image]',
                                                        "helpText" => "建議大小：360px X 360px",
                                                        'value' => ( !empty($data['sm_image']) )? $data['sm_image'] : ''
                                                    ])--}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '列表圖',
                                                        'inputName' => 'ProductType[image]',
                                                        "helpText" => "建議大小：大(400 x 980)px 或 小(400 x 240)px",
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}

                                                    {{ItemMaker::selectMulti([
                                                        'labelText' => '所擁有的尺寸屬性',
                                                        'inputName' => 'ProductType[size_role][]',
                                                        'options' => $parent['belong']['ProductSize'],
                                                        'value' => ( !empty($data['size_role']) )? json_decode($data['size_role'], true) : ''
                                                    ])}}

                                                </div>
                                            </div>

                                            <div class="tab-pane form-row-seperated" id="time">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-OverSeaPhoto',
                                                            'datas' => ( !empty($parent['has']['OverSeaPhoto']) )? $parent['has']['OverSeaPhoto'] : [],
                                                            'table_set' =>  $bulidTable,
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div> 

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>

		$(document).ready(function() 
        {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
