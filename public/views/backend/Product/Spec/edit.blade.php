@extends('backend.template')

@section('title',"規格修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			規格管理 <small>Spec Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
                <li>
                    產品
                    <i class="fa fa-angle-right"></i>
                </li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">規格管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					規格修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => 'hidFrame' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 規格管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>
                                        <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a>

                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                            <li>
                                                <a href="#format" data-toggle="tab"> Product Detail </a>
                                            </li>
                                            <li>
                                                <a href="#photo" data-toggle="tab"> 照片 </a>
                                            </li>

                                            <li>
                                                <a href="#size" data-toggle="tab"> 庫存與尺碼 </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductSpec[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'ProductSpec[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬項目',
                                                        'inputName' => 'ProductSpec[item_id]',
                                                        'options' => $parent['belong']['ProductItem'],
                                                        'value' => ( !empty($data['item_id']) )? $data['item_id'] : ''
                                                    ])}}




                                                    {{--ItemMaker::select([
                                                        'labelText' => '所屬城市',
                                                        'inputName' => 'ProductSpec[city_id]',
                                                        'options' => $parent['belong']['OverSeaCity'],
                                                        'value' => ( !empty($data['city_id']) )? $data['city_id'] : ''
                                                    ])--}}


													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'ProductSpec[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

													{{ItemMaker::textInput([
														'labelText' => 'Style Name',
														'inputName' => 'ProductSpec[title]',
                                                        //'helpText' =>'Style Name為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Style Name - 英文版',
                                                        'inputName' => 'ProductSpec[en_title]',
                                                        //'helpText' =>'Style Name為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '產品編號',
                                                        'inputName' => 'ProductSpec[product_no]',
                                                        'value' => ( !empty($data['product_no']) )? $data['product_no'] : ''
                                                    ])}}

                                                    {{ItemMaker::numberInput([
                                                        'labelText' => '價格',
                                                        'inputName' => 'ProductSpec[price]',
                                                        'value' => ( !empty($data['price']) )? $data['price'] : ''
                                                    ])}}

                                                    {{ItemMaker::numberInput([
                                                        'labelText' => '優惠價格',
                                                        'inputName' => 'ProductSpec[dis_price]',
                                                        "helpText" => '如果有輸入此價格，將會不能計入',
                                                        'value' => ( !empty($data['dis_price']) )? $data['dis_price'] : ''
                                                    ])}}

                                                    {{ItemMaker::colorPicker([
                                                        'labelText' => '顏色',
                                                        'inputName' => 'ProductSpec[show_color]',
                                                        'value' => ( !empty($data['show_color']) )? $data['show_color'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '圖片(顯示在購物清單)',
                                                        'inputName' => 'ProductSpec[image]',
                                                        "helpText" => "建議大小：160px X 110px",
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '材質照片',
                                                        'inputName' => 'ProductSpec[skin_image]',
                                                        "helpText" => "建議大小：50px X 50px",
                                                        'value' => ( !empty($data['skin_image']) )? $data['skin_image'] : ''
                                                    ])}}


                                                    {{--ItemMaker::selectMulti([
                                                        'labelText' => '規格相關描述',
                                                        'inputName' => 'ProductSpec[format_id][]',
                                                        //"helpText" => "限定300字",
                                                        "options" => $parent['belong']['ProductFormat'],
                                                        'value' => ( !empty($data['format_id']) )? json_decode($data['format_id'],true) : ''
                                                    ])--}}

                                                    {{ItemMaker::selectMulti([
                                                        'labelText' => '相關產品',
                                                        'inputName' => 'ProductSpec[related_product][]',
                                                        'options' => $parent['belong']['ProductItem'],
                                                        'value' => ( !empty($data['related_product']) )? json_decode($data['related_product'], true) : []
                                                    ])}}

                                                </div>
                                            </div>
                                            <div class="tab-pane form-row-seperated" id="format">
                                                <div class="form-body form_news_pad">
                                                
                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品描述',
                                                        'inputName' => 'ProductSpec[content]',
                                                        "helpText" => "限定300字",
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品描述-英文版',
                                                        'inputName' => 'ProductSpec[en_content]',
                                                        "helpText" => "限定300字",
                                                        'value' => ( !empty($data['en_content']) )? $data['en_content'] : ''
                                                    ])}}

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-ProductFormat',
                                                            'datas' => ( !empty($parent['has']['ProductFormat']) )? $parent['has']['ProductFormat'] : [],
                                                            'table_set' =>  $bulidTable['format'],
                                                            'pic' => false
                                                        ]
                                                     ) }}
                                                </div>
                                            </div> 

                                            <div class="tab-pane form-row-seperated" id="photo">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-ProductSpecPhoto',
                                                            'datas' => ( !empty($parent['has']['ProductSpecPhoto']) )? $parent['has']['ProductSpecPhoto'] : [],
                                                            'table_set' =>  $bulidTable['photo'],
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div>


                                            <div class="tab-pane form-row-seperated" id="size">
                                                <div class="form-body form_news_pad">

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-ProductSpecSize',
                                                            'datas' => ( !empty($parent['has']['ProductSpecSize']) )? $parent['has']['ProductSpecSize'] : [],
                                                            'table_set' =>  $bulidTable['size'],
                                                            'pic' => false
                                                        ]
                                                     ) }}
                                                </div>
                                            </div> 

 

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>


		$(document).ready(function() 
        {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
