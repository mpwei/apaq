@extends('backend.template')

@section('title',"主類別修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			主類別管理 <small>Theme Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">主類別管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					主類別修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    <!--注意事項END-->

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 主類別管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>

                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
<!--                                             <li>
                                                <a href="#time" data-toggle="tab"> 分店照片 </a>
                                            </li> -->

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductTheme[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'ProductTheme[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}

                                                    {{--ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示於首頁',
                                                        'inputName' => 'ProductTheme[is_show_home]',
                                                        'helpText' =>'是否顯示於首頁',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_show_home']) )? $data['is_show_home'] : ''
                                                    ])--}}

													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'ProductTheme[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

													{{ItemMaker::textInput([
														'labelText' => '主類別名稱',
														'inputName' => 'ProductTheme[title]',
                                                        //'helpText' =>'主題名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
														'value' => ( !empty($data['title']) )? $data['title'] : ''
													])}}

                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '副標題',
                                                        'inputName' => 'ProductTheme[sub_title]',
                                                        //'helpText' =>'主題名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號',
                                                        'value' => ( !empty($data['sub_title']) )? $data['sub_title'] : ''
                                                    ])--}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '分類列表圖',
                                                        'inputName' => 'ProductTheme[image]',
                                                        "helpText" => "",
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}

                                                    {{--ItemMaker::photo([
                                                        'labelText' => '主題圖片-手機版',
                                                        'inputName' => 'ProductTheme[sm_image]',
                                                        "helpText" => "建議大小：360px X 360px",
                                                        'value' => ( !empty($data['sm_image']) )? $data['sm_image'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::photo([
                                                        'labelText' => '分類列表圖',
                                                        'inputName' => 'ProductTheme[icon]',
                                                        "helpText" => "建議大小：500 x 500",
                                                        'value' => ( !empty($data['icon']) )? $data['icon'] : ''
                                                    ])--}}



                                                </div>
                                            </div>

{{--                                             <div class="tab-pane form-row-seperated" id="time">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-OverSeaPhoto',
                                                            'datas' => ( !empty($parent['has']['OverSeaPhoto']) )? $parent['has']['OverSeaPhoto'] : [],
                                                            'table_set' =>  $bulidTable,
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div>  --}}

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>

		$(document).ready(function() 
        {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
