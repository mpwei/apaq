@extends('backend.template')

@section('title',"項目修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品項目修改 <small>Item Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
                <li>
                    產品
                    <i class="fa fa-angle-right"></i>
                </li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/產品/Items')}}">產品項目</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品項目修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    <!--注意事項END-->

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品項目管理
										</span>
                                    </div>
                                    <div class="actions btn-set">

 										<a href=" {{ ItemMaker::url('backen-admin/產品/Items') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button><!--
                                        <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a>

                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                            {{-- <li>
                                                <a href="#meta" data-toggle="tab"> 縮圖 </a>
                                            </li> --}}

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductItem[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示',
                                                        'inputName' => 'ProductItem[is_visible]',
                                                        'helpText' =>'是否顯示於前台的「列表」中',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}
                                                    
                                                    {{--ItemMaker::radio_btn([
                                                        'labelText' => '是否為最新產品',
                                                        'inputName' => 'ProductItem[is_new]',
                                                        'helpText' =>'是否為最新產品',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_new']) )? $data['is_new'] : ''
                                                    ])--}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬主類別',
                                                        'inputName' => 'ProductItem[theme_id]',
                                                        'options' => $parent['belong']['ProductTheme'],
                                                        'value' => ( !empty($data['theme_id']) )? $data['theme_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬次類別',
                                                        'inputName' => 'ProductItem[category_id]',
                                                        'options' => $Tree,
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}


													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'ProductItem[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '產品名稱',
                                                        'inputName' => 'ProductItem[title]',
                                                        'helpText' =>'項目名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定100字',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '內頁產品特色',
                                                        'inputName' => 'ProductItem[detail_sub_title]',
                                                        'helpText' =>'項目名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定100字',
                                                        'value' => ( !empty($data['detail_sub_title']) )? $data['detail_sub_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::editor([
                                                        'labelText' => 'Specification',
                                                        'inputName' => 'ProductItem[content]',
                                                        'helpText' =>'',
                                                        //'disabled' => 'disabled',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => 'Dimensions and Marking 圖片',
                                                        'inputName' => 'ProductItem[DAM_image]',
                                                        'inputDelName' => 'ProductItem[del_dam_photo]',
                                                        "helpText" => "",
                                                        'value' => ( !empty($data['DAM_image']) )? $data['DAM_image'] : ''
                                                    ])}}

                                                    {{ItemMaker::editor([
                                                        'labelText' => 'Dimensions and Marking',
                                                        'inputName' => 'ProductItem[desc]',
                                                        'helpText' =>'',
                                                        //'disabled' => 'disabled',
                                                        'value' => ( !empty($data['desc']) )? $data['desc'] : ''
                                                    ])}}

                                                    {{ItemMaker::editor([
                                                        'labelText' => 'Standard Ratings',
                                                        'inputName' => 'ProductItem[desc2]',
                                                        'helpText' =>'',
                                                        //'disabled' => 'disabled',
                                                        'value' => ( !empty($data['desc2']) )? $data['desc2'] : ''
                                                    ])}}

                                                    {{ItemMaker::tags([
                                                        'labelText' => '列表產品特色',
                                                        'inputName' => 'ProductItem[list_spec]',
                                                        "helpText" => "",
                                                        'value' => ( !empty($data['list_spec']) )? $data['list_spec'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '內頁產品圖',
                                                        'inputName' => 'ProductItem[image]',
                                                        'inputDelName' => 'ProductItem[del_photo]',
                                                        "helpText" => "建議大小：500 x 500",
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}

                                                    {{ItemMaker::tags([
                                                        'labelText' => '內頁產品簡述',
                                                        'inputName' => 'ProductItem[detail_spec]',
                                                        "helpText" => "建議大小：500 x 500",
                                                        'value' => ( !empty($data['detail_spec']) )? $data['detail_spec'] : ''
                                                    ])}}

                                                    {{ItemMaker::filePicker([
                                                        'labelText' => '檔案下載',
                                                        'inputName' => 'ProductItem[file]',
                                                        'inputDelName' => 'ProductItem[del_file]',
                                                        "helpText" => "",
                                                        'value' => ( !empty($data['file']) )? $data['file'] : ''
                                                    ])}}

                                                    {{ItemMaker::filePicker([
                                                        'labelText' => 'Packing下載',
                                                        'inputName' => 'ProductItem[packing]',
                                                        'inputDelName' => 'ProductItem[packing]',
                                                        "helpText" => "",
                                                        'value' => ( !empty($data['packing']) )? $data['packing'] : ''
                                                    ])}}
                                                </div>
                                            </div>


                                            <div class="tab-pane form-row-seperated" id="meta">
                                                <div class="form-body form_news_pad">


                                                    {{--FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-ProductPhoto',
                                                            'datas' => ( !empty($parent['has']['ProductPhoto']) )? $parent['has']['ProductPhoto'] : [],
                                                            'table_set' =>  $bulidTable['photo'],
                                                            'pic' => true
                                                        ]
                                                     ) --}}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>

		$(document).ready(function() 
        {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
