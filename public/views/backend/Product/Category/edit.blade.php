@extends('backend.template')

@section('title',"類別修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/backend/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			次類別管理 <small>Category Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('backen-admin/')}}">首頁</a>
					<i class="fa fa-angle-right"></i>
				</li>
                <li>
                    產品
                    <i class="fa fa-angle-right"></i>
                </li>
				<li>
					<a href="{{ ItemMaker::url('backen-admin/'.$routePreFix)}}">次類別管理</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					次類別修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    <!--注意事項END-->

					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 次類別管理
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('backen-admin/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductCategory[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

													{{ItemMaker::radio_btn([
														'labelText' => '是否顯示',
														'inputName' => 'ProductCategory[is_visible]',
														'helpText' =>'是否顯示於前台的「列表」中',
														'options' => $StatusOption,
														'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
													])}}


													{{ItemMaker::textInput([
														'labelText' => '排序',
														'inputName' => 'ProductCategory[rank]',
														'helpText' =>'前台列表顯示之排序',
														'value' => ( !empty($data['rank']) )? $data['rank'] : ''
													])}}


                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬主類別',
                                                        'inputName' => 'ProductCategory[theme_id]',
                                                        'options' => $parent['belong']['ProductTheme'],
                                                        'value' => ( !empty($data['theme_id']) )? $data['theme_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '次分類名稱',
                                                        'inputName' => 'ProductCategory[title]',
                                                        'helpText' =>'分類名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定 100字',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{--ItemMaker::textInput([
                                                        'labelText' => '次分類副標題',
                                                        'inputName' => 'ProductCategory[sub_title]',
                                                       // 'helpText' =>'分類名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定 100字',
                                                        'value' => ( !empty($data['sub_title']) )? $data['sub_title'] : ''
                                                    ])--}}

                                                    {{--ItemMaker::textarea([
                                                        'labelText' => '簡述',
                                                        'inputName' => 'ProductCategory[slogan]',
                                                        //'helpText' =>'分類名稱為必填且標題會轉變為連結，所以標題中不能有「-」「/」符號,限定 100字',
                                                        'value' => ( !empty($data['slogan']) )? $data['slogan'] : ''
                                                    ])--}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '圖片',
                                                        'inputName' => 'ProductCategory[image]',
                                                        "helpText" => "建議大小：500px X 500px",
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}


                                                </div>
                                            </div>

{{--                                             <div class="tab-pane form-row-seperated" id="time">
                                                <div class="form-body form_news_pad">
                                                    

                                                    {{ FormMaker::photosTable(
                                                        [
                                                            'nameGroup' => $routePreFix.'-OverSeaPhoto',
                                                            'datas' => ( !empty($parent['has']['OverSeaPhoto']) )? $parent['has']['OverSeaPhoto'] : [],
                                                            'table_set' =>  $bulidTable,
                                                            'pic' => true
                                                        ]
                                                     ) }}
                                                </div>
                                            </div>  --}}

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/backend/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>

		$(document).ready(function() 
        {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
