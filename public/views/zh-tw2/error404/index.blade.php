@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

  
@extends('page')

@section('content')


      <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title']}}</a></li>
              <li class="breadcrumb-item active">404</li>
            </ol>
          </div>
        </div>
      <!-- breadcrumb end -->

      <!-- main-container start -->     
      <section class="main-container light-gray-bg text-center margin-clear">

        <div class="container">
          <div class="row">

            <!-- main start -->           
            <div class="main col-lg-6 offset-lg-3 pv-40">

              <!-- page-title start -->             
              <h1 class="page-title extra-large"><span class="text-default">404</span></h1>
              <h2 class="mt-4">Ooops! Page Not Found</h2>
              <div class="separator"></div>
              <!-- page-title end -->
             
               <p class="lead">The requested URL was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly.</p>
              {!! Form::open(['id'=>"form3",'class'=>"search-box margin-clear", 'method'=>"get", 'name' => 'form', "url"=> ItemMaker::url('search') ]) !!}
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" placeholder="{{ $lang['search']}}" id="key" name="key">
                  <i class="icon-search form-control-feedback" onclick="document.forms.form3.submit()"></i>
                </div>
              {!! Form::close()  !!}
              <a href="{{ ItemMaker::url('./') }}" class="btn btn-default btn-animated btn-lg">Return Home <i class="fa fa-home"></i></a>

            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->

@endsection
