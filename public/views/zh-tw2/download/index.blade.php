@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@section('bodyclass','gradient-background-header transparent-header front-page')

@extends('page')

@section('content')

  
    <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'assets/images/inner-slider.png' }}'); background-position: 50% 27%;">
      <!-- breadcrumb start -->
      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{$lang['home_title']}}</a></li>
            <li class="breadcrumb-item active">{{$lang['download_bread']}}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center offset-md-2 pv-20">
            <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{$lang['download_bread']}}</h2> 
          </div>
        </div>
      </div>

    </div>
    <!-- banner end -->

    <!-- main-container start -->   
    <section class="main-container">
      <div class="container">
        <div class="row">
          <div class="page-title-wrap mb-20">
            <h1 class="page-title">{{$lang['download_bread']}}</h1>
            <div class="separator-2"></div>
            <!-- page-title end -->
          </div>  

          <!-- main start -->         
          <div class="main col-md-12">
                  @if(!empty($pageData['data']))
                      @foreach ($pageData['data'] as $row)
                       <?php
                          $date = str_replace("-",".",$row->date);
                          $img = ( !empty($row->image) AND file_exists(public_path()."/". $row->image))?$row->image : "http://placehold.it/1024?text=noPic";
                       ?>
            <div class="image-box style-3-b">
              <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                  <div class="overlay-container">
                    <img src="{{ $img }}" alt="">
                    
                  </div>
                </div>
                <div class="col-sm-6 col-md-8 col-lg-9">
                  <div class="body">
                    <h3 class="title"><a href="{{ ItemMaker::url('download/'.$row->id) }}">{{$row->title}}</a></h3>
                    <div class="separator-2"></div>
                    <p class="mb-10">{{$row->content}}</p>
                    <a href="{{ ItemMaker::url('download/'.$row->id) }}" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Download<i class="fa fa-arrow-down pl-10"></i></a>
                  </div>
                </div>
              </div>
            </div>
                      @endforeach
                    @endif
                  <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                      {!! $frontEndClass->getPageHtml($pageData) !!}
                    </ul>
                  </nav>
          </div>
          <!-- main end -->
           
        </div>
      </div>
    </section>
@stop