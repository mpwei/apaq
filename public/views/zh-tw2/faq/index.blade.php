@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection


@extends('page')

@section('content')



      <!-- banner start -->
      <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'assets/images/inner-slider.png' }}'); background-position: 50% 27%;">
        <!-- breadcrumb start -->       
     
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./')}}">{{$lang['home_bread']}}</a></li>
              <li class="breadcrumb-item active">{{ $lang['faq_title'] }}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end -->
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{ $lang['faq_title'] }}</h2>
              <div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
            </div>
          </div>
        </div>
      </div>
      <!-- breadcrumb end -->

      <!-- main-container start -->     
      <section class="main-container">

        <div class="container">
          <div class="row">

            <!-- main start --> 
            <div class="main col-12"> 

              <div class="page-title-wrap mb-20">
                <h1 class="page-title">{{ $lang['faq_title'] }}</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
               </div> 
             <div id="accordion-faq" class="collapse-style-2" role="tablist" aria-multiselectable="true">
                  @if(!empty($faq))
                   @foreach($faq as $row)
                   <div class="card">
                     <div class="card-header" role="tab" id="heading{{ $row -> id }}">
                       <h4 class="mb-0">
                         <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse{{ $row -> id }}" class="collapsed" aria-expanded="true" aria-controls="collapse{{ $row -> id }}">
                           <i class="fa fa-question-circle pr-10"></i>{{ $row -> title }}
                         </a>
                       </h4>
                     </div>
                     <div id="collapse{{ $row -> id }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $row -> id }}">
                       <div class="card-block">
                         {!! $row -> content !!}
                       </div>
                     </div>
                   </div>
                   @endforeach
                  @endif
             </div>
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->




@stop