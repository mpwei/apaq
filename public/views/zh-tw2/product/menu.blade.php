@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')
<?php
    $uri = $_SERVER["REQUEST_URI"];
    $product_menu = $frontEndClass -> getProduct();
?>

            <aside class="col-lg-4 col-xl-3 hidden-md-down">
            @if(!empty($product_menu))
              <div class="sidebar">
                <div class="block clearfix">
                  <h3 class="title">{{$lang['productcategory_bread']}}</h3>
                  <div class="separator-2"></div>
                  <nav class="main-navigation  animated">
                    <ul class="nav  flex-column">
                    @foreach( $product_menu as $row)
                      <li class="nav-item">
                        <a class="{{ (strpos(urldecode($uri),$row['title'])) ? 'active' : '' }}" href="{{ ItemMaker::url('product/'.$row['title']) }}" >{{ $row['title'] }}</a>   
                        @if(!empty($row['category']))  
                        <a class="aside-dropdown" data-toggle="collapse" data-parent="#Accordion" href="#Accordion{{ $row['id'] }}" aria-expanded="true" aria-controls="Accordion{{ $row['id'] }}"><i class="fa fa-angle-double-right"></i></a> 
                          <ul class="flex-column collapse" id="Accordion{{ $row['id'] }}"   role="tabpanel">
                                @foreach( $row['category'] as $subrow)
                            <li class="nav-item"><a class="{{ (strpos(urldecode($uri),$subrow['title'])) ? 'active' : '' }}" href="{{ ItemMaker::url('product/'.$row['title'] . '/' . $subrow['title']) }}">{{ $subrow['title'] }}</a></li>
                                @endforeach
                          </ul>
                        @endif
                        </li>
                      @endforeach
                    </ul> 
                  </nav>
                </div>
            
              </div>
            @endif
            </aside>