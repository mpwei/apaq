@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $uri = $_SERVER["REQUEST_URI"];
    $lang = $frontEndClass -> getlang($locateLang);
    $product_menu = $frontEndClass -> getProduct();
?>

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection
@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('bodyclass','gradient-background-header transparent-header front-page')
@extends('page')
@section('content')


      <!-- banner start -->
      <div class="banner slideshow d-flex align-items-center" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'upload/index-slide/slide-1.jpg' }}'); background-position: 50% 27%;     min-height: 350px;">
         
        <div class="container  ">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20 ">
              <h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">  <strong>{{ $lang['product_title']}} </strong></h2>
               
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->
      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{$lang['home_bread']}}</a></li>
            <li class="breadcrumb-item"><i class="fa fa-product pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('product') }}">{{ $lang['product_bread']}}</a></li>
            <li class="breadcrumb-item active">{{ $lang['product_search_bread'] . ' ' . $key }} </li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->
      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

        <div class="container">
          <div class="row">

            <div class="main col-12">

              <div class="page-title-wrap mb-20">
                                <h1 class="page-title">{{$lang['product_bread']}}</h1>
                                <div class="separator-2"></div>
                                <!-- page-title end -->
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ut quisquam ab harum hic enim quibusdam aut quasi recusandae temporibus quo voluptatibus, dolorem consectetur ipsam facere ipsa. Commodi sunt, inventore!</p>
              </div>   

            </div>

          </div>
        </div>

        <div class="container-fluid">
          <div class="isotope-container row grid-space-0">
            
            @if(!empty($pageData['data']))
              @foreach($pageData['data'] as $key => $row)
               <?php
                  $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/1024?text=noPic";
               ?>
            <div class="col-md-6 col-lg-4 isotope-item product-{{ $row['theme_id'] }}end">
              <div class="image-box text-center">
                <div class="overlay-container">
                  <img src="{{ $img }}" alt="{{ $row['title'] }}">
                  <div class="overlay-top">
                    <div class="text">
                      <h3><a href="{{ $row['web_link'] }}">{{ $row['title'] }}</a></h3>
                      <p class="small"></p>
                    </div>
                  </div>
                  <div class="overlay-bottom">
                    <div class="links">
                      <a href="{{ $row['web_link'] }}" class="btn btn-gray-transparent btn-animated btn-sm">{{$lang['detail']}} <i class="pl-10 fa fa-arrow-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              @endforeach
            @endif
          </div>
        </div>

      </section>
      <!-- main-container end -->
 






@stop