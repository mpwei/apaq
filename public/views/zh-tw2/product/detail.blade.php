@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
    $relate_no = 1;
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection


@extends('page')
  @section('script')

  <script src="assets/js/front/front.js"></script>
  @endsection
@section('content')

        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
                  <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{$lang['home_bread']}}</a></li>
                  <li class="breadcrumb-item"><i class="fa fa-product pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('product/') }}">{{ $lang['product_bread'] }}</a></li>
                  <li class="breadcrumb-item"><i class="fa fa-product pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('product/'.$theme) }}">{{ $theme }}</a></li>
                  <li class="breadcrumb-item"><i class="fa fa-product pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('product/'.$theme.'/'.$category) }}">{{ $category }}</a></li>
                  <li class="breadcrumb-item active">{{$item}}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end --> 
           <!-- banner start -->           
           <div class="pv-40 banner light-gray-bg">
             <div class="container clearfix">
          <h1 class="title">{{$item}}</h1>
                   <div class="separator-2"></div>
               <!-- slideshow start -->              
               <div class="slideshow row">
            <div class=" col-xl-8 ">
                   <div class="owl-carousel content-slider-with-controls">
                          @if(!empty($Photo))
                                @foreach($Photo as $key => $row)
                                 <?php
                                    $img = ( !empty($row -> image) AND file_exists(public_path()."/". $row -> image))?$row -> image : "http://placehold.it/1024?text=noPic";
                                 ?>
                              <div class="overlay-container overlay-visible">
                                <img src="{{ $img }}" alt="">
                              </div>
                                @endforeach
                          @endif
                       </div>
                  </div> 
                        <!-- sidebar start -->                        
                        <div class=" col-xl-4 ">
                          <div class="sidebar">
                            <div class="block clearfix">
                              <h3 class="title">{{ $lang['product_desc'] }}</h3> 
                              {!! $data -> content !!}
                    
                          <input type="hidden" id="item_no" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" id="item_no" name="item_no" value="{{ $data -> id }}" class="margin-clear btn btn-default addcart-alert">
                              <a class="btn btn-animated btn-default btn-lg addcarbtn">{{ $lang['in_inquirycar'] }} <i class="fa fa-external-link"></i></a>
                              <h5 class="mt-4">Share This</h5>
                              <div class="separator-2"></div>
                              <ul class="social-links circle small">
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                <li class="xing"><a target="_blank" href="http://www.xing.com"><i class="fa fa-xing"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <!-- sidebar end --> 
               </div>
               <!-- slideshow end --> 
             </div>
           </div>
           <!-- banner end --> 
           <!-- main-container start -->           
           <section class="main-container padding-ver-clear">
             <div class="container pv-40">
               <div class="row"> 
                 <!-- main start -->                 
                 <div class="main col-lg-12">
                   
                   <div class="editor-wrap">
                     {!! $data -> desc !!}
                   </div>
                   <div class="separator-2"></div>
                    <a class="back_btn btn btn-animated btn-default-transparent btn-md" href=" javascript:history.back();"><i class="icon-back"></i>{{ $lang['back'] }}</a>
                 </div>
                 <!-- main end -->  
               </div>
             </div>
           </section>
           <!-- main-container end -->
@stop