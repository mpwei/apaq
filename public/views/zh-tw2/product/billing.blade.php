@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
    $total_quantity = 0;
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@section('script')
    <script src="assets/js/front/inquiry.js"></script>
@endsection

@section('content')


      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
            <li class="breadcrumb-item active">{{ $lang['productinquiry_title'] }}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->

      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

      <div class="container">
        <div class="row">

              <!-- main start -->
              <!-- ================ -->
              <div class="main col-12 cart-checkout">

          <!-- page-title start -->
          <!-- ================ -->
          <h1 class="page-title">{{ $lang['productinquiry_title']}}</h1>
          <div class="separator-2"></div>
          <!-- page-title end -->

          <div class="table-responsive">
            <table class="table cart">
              <thead>
                <tr>
                  <th>{{ $lang['car_item_title'] }} </th>
                  <th>{{ $lang['car_quantity'] }}</th>
                </tr>
              </thead>
              <tbody>
              @if (!empty( Session::get('Spec')['list'] ))
                @foreach( Session::get('Spec')['list'] as $row )
                <?php 
                  $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/500?text=noPic";
                ?>
                <tr>
                  <td class="product"><a href="shop-product.html">{{ $row['title'] }}</a> </td>
                  <td class="quantity">
                    <div class="form-group">
                      <input type="text" class="form-control" value="{{ $row['quantity'] }}" disabled="">
                    </div>                      
                  </td>
                </tr>
                <?php
                  $total_quantity += $row['quantity'];
                ?>
                  @endforeach
                @endif
                <!-- <tr>                    
                  <td class="total-quantity" colspan="2">Discount Coupon</td>
                  <td class="price">TheProject25672</td>
                  <td class="amount">-20%</td>
                </tr> -->
                <tr>
                  <td class="total-quantity" colspan="3">共 {{ $total_quantity}} 件</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="space-bottom"></div>
          <fieldset>
            <legend>{{ $lang['contact_info'] }}</legend>
            {!! Form::open(['class'=>'form-horizontal','url'=>'zh-tw/member/shipping','id'=>'billing-information']) !!}
             

              <div class="row">
                <div class="col-xl-3">
                  <h3 class="title">聯絡人資料</h3>
                </div>
                <div class="col-xl-8 offset-xl-1">
                  <div class="form-group row">
                    <label for="first_name" class="col-lg-2 control-label text-lg-right col-form-label">姓名<small class="text-default">*</small></label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="form-name" name="first_name" value="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="phone" class="col-lg-2 control-label text-lg-right col-form-label">聯絡電話<small class="text-default">*</small></label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="form-phone" value="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="email" class="col-lg-2 control-label text-lg-right col-form-label">Email<small class="text-default">*</small></label>
                    <div class="col-lg-10">
                      <input type="email" class="form-control" id="form-email" name="email" value="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="space"></div>
              <div class="row">
                <div class="col-xl-3">
                  <h3 class="title mt-5 mt-lg-0">備註</h3>
                </div>
                <div class="col-xl-8 offset-xl-1">
                  <div class="form-group row">
                    <div class="col-12">
                      <textarea class="form-control" rows="4" name="form-memo" id="form-memo"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </fieldset>

          <div class="space-bottom"></div>

          <div class="text-right"> 
            <a href="{{ ItemMaker::url('shipping') }}" class="btn btn-group btn-default"><i class="icon-left-open-big"></i> 回詢問車清單</a>
            <input class="btn btn-group btn-default" type="submit" id="send-Inquiry" value="{{ $lang['car_pay_finished'] }}">
          </div>
              </div>
              <!-- main end -->
        </div>
      </div>
      </section>
      <!-- main-container end -->

      <div id="loading-item">
        <div class="loading-label-wrap">
          <p class="text-center"><span class="loader-c loadercc-circles"> </span><span>請稍後…</span></p>
        </div>
      </div> 


		@endsection


