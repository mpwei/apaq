@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection


@section('bodyclass','gradient-background-header transparent-header front-page')
  
@extends('page')

@section('content')

      <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : './../assets/images/bg/background-img-2.png' }}'); background-position: 50% 27%;">
        <!-- breadcrumb start -->       
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./')}}">{{ $lang['home_title'] }}</a></li>
              <li class="breadcrumb-item active">{{ $lang['contact_title'] }}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end -->
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{ $lang['contact_title'] }}</h2>
             
               
            </div>
          </div>
        </div>

      </div>
      <!-- banner end -->

      <!-- main-container start -->     
      <section class="main-container">

          <div class="container">
            <div class="row">

              <!-- main start -->
              <!-- ================ -->
              <div class="main col-lg-8 space-bottom">
                <p class="lead">{{ $lang['contact_subtitle']}}</p>
                <div class="alert alert-success hidden-xs-up" id="MessageSent">
                  {{ $lang['contact_messagesend']}}
                </div>
                <div class="alert alert-danger hidden-xs-up" id="MessageNotSent">
                  {{ $lang['contact_messagenosend']}}
                </div>
                <div class="contact-form">
                    {!! Form::open(['class'=>'margin-clear','id'=>"active", 'name' => 'active', "url"=> ItemMaker::url('contact/save') ]) !!}
                    <div class="form-group has-feedback">
                      <label for="name">{{ $lang['contact_name'] }}</label>
                      <input type="text" class="form-control" id="name" name="Contact[name]" placeholder="">
                      <i class="fa fa-user form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                      <label for="email">{{ $lang['contact_mail'] }}*</label>
                      <input type="email" class="form-control" id="email" name="Contact[email]" placeholder="">
                      <i class="fa fa-envelope form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                      <label for="phone">{{ $lang['contact_tel'] }}*</label>
                      <input type="text" class="form-control" id="phone" name="Contact[phone]" placeholder="">
                      <i class="fa fa-phone form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                      <label for="message">{{ $lang['contact_message'] }}*</label>
                      <textarea class="form-control" rows="6" id="message" name="Contact[message]" placeholder=""></textarea>
                      <i class="fa fa-pencil form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback captcha">
                      <label for="captcha">{{ $lang['contact_captcha'] }}*</label>
                        <div id="changeCap" class="changeCap">
                          {!! Captcha::img() !!}
                        </div>
                        <input class="form-control" type="text" placeholder="驗證碼" id="captcha" name="captcha">
                    </div>
                    <input type="submit" value="確認送出" class="submit-button btn btn-default">
                    {!! Form::close() !!}
                </div>
              </div>
              <!-- main end -->

              <!-- sidebar start -->
              <!-- ================ -->
              <aside class="col-lg-3 offset-xl-1">
                <div class="sidebar">
                  <div class="side vertical-divider-left">
                    <h3 class="title"><img class="logo" src="../../assets/images/logo/logo-2.svg" alt="Ocean"></h3>
                    <div class="separator-2 mt-20"></div>
                    <ul class="list">
                        <li><i class="fa fa-home pr-10"></i>{{ $Setting['address'] }}</li>
                        <li><i class="fa fa-phone pr-10"></i><abbr title="Phone">P:</abbr> {{ $Setting['tel'] }}</li> 
                        <li><i class="fa fa-envelope pr-10"></i><a href="mailto:{{ $Setting['email'] }}">{{ $Setting['email'] }}</a></li>
                    </ul>
                    <ul class="social-links circle small margin-clear clearfix animated-effect-1">
                      <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                      <li class="skype"><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                      <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                      <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                      <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
                      <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
                      <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                    <div class="separator-2 mt-20 "></div>
                    <a class="btn btn-gray collapsed map-show btn-animated" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap">{{ $lang['contact_showmap']}} <i class="fa fa-plus"></i></a>

                  </div>
                </div>
              </aside>
              <!-- sidebar end -->
            </div>
          </div>
        </section>
      <!-- main-container end -->

      <!-- section start -->      
      <section id="collapseMap">
        <div id="map-canvas"></div>
      </section>
      <!-- section end -->


@endsection



@section('script')
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyBPu_inBd6fiRxQvxptHdK6W6uxYA6B4N4"></script>
      <script type="text/javascript" src="assets/js/google.map.config.js"></script>
    <script src="assets/js/front/contactUs.js"></script>
@endsection
