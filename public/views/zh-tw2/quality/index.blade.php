@section('title', '手工具,園藝,花園工具,剪刀,高枝剪|吉勝工具')
@section('keywords', '手工具,園藝,花園工具,剪刀,高枝剪,套筒,板手,棘輪板手,扭力板手')
@section('description','吉勝為手工具及園藝工具(剪刀、高枝剪..等)行銷公司，我們靈活地調整的生產程序，以滿足世界各地正在尋找市場趨勢與變化的合作夥伴之需求，每年吉勝的產品線都會推出新系列的工具產品及包裝設計，以便使客戶在市場競爭中保持成功的地位。')
@section('headerid','in-header')


  @section('css')
  <link rel="stylesheet" href="assets/css/page.css">
  @endsection
  
@extends('template')

@section('content')

  <main class="in-page">
    <section id="in-banner">
      <div class="container">
        <a href="./" class="pic">
          <img src="assets/images/logo/in-logo.png" alt="Rockfirm" class="page-logo">
        </a>
      </div>
      <div class="page-title animated slideInUp page-title-about has-text-centered">
        <span>QUALITY MANAGEMENT</span>
      </div>    
    </section>

    <section class="quality">
      <div class="quality-bg animated fadeIn">
        <div class="page-content container clearfix">
          <div class="quality-content-wrap">
            <div class="quality-block columns is-mobile">
              <div class="column context">
                <p>All of our products are subjected to strict quality control systems to guarantee their precision and reliability. The product has to be inspected by all testing processes including size, hardness, torque values and lifetime cycle. </p>
              </div>
              <div class="column is-4-tablet is-6-mobile is-quality-icon align-right-icon clearfix">
                <div class="pic">
                  <img src="assets/images/quality/icon-qua-01.png" alt="" class="icon-quality is-pulled-right">
                  <svg class="hollow_circle01">
                    <circle cx="110" cy="110" r="104" stroke="white" stroke-width="2" fill="none" />
                  </svg>
                </div>
              </div>
            </div>

            <div class="quality-block columns is-mobile">
              <div class="column is-4-tablet is-6-mobile is-quality-icon clearfix">
                <div class="pic">
                  <img src="assets/images/quality/icon-qua-02.png" alt="" class="icon-quality is-pulled-left">
                  <svg class="hollow_circle01">
                    <circle cx="110" cy="110" r="104" stroke="white" stroke-width="2" fill="none" />
                  </svg>
                </div>
              </div>
              <div class="column context">
                <p>To ensure the accuracy of inspection and testing provides each product manufacturing related quality conforming to DIN/ISO/GS standard accurately.</p>
              </div>
            </div>

            <div class="quality-block">
              <div class="context">
                <p>Through in-depth understanding of manufacturing, product management and automated production facilities. RCCK-FIRM has the most advanced production technology and state-of the-art expertise to ensure that we can develop the highest quality of our product. </p>
              </div>
            </div>

            <div class="quality-block columns is-mobile">
              <div class="column context">
                <p>We simply claim that we can be as good as you want it to be. This message is simple: worldwide customers are looking for today is a manufacturing partner role play toys that understands market trends and changes. This may affect them to strengthen their competition in market. Therefore, we are flexible to adjust our production process to meet your need. We roll out serial new tools and package designs from our product lines every year to keep our clients' success in this competitive market, because we believe the only way to make successful for ourselves is make all client's success.</p>
              </div>
              <div class="column is-4-tablet is-6-mobile is-quality-icon align-right-icon clearfix">
                <div class="pic">
                  <img src="assets/images/quality/icon-qua-03.png" alt="" class="icon-quality is-pulled-right">
                  <svg class="hollow_circle01">
                    <circle cx="110" cy="110" r="104" stroke="white" stroke-width="2" fill="none" />
                  </svg>
                </div>
              </div>
            </div>

            <div class="quality-block">
              <div class="context">
                <p>In order to provide the full service of Hand Tools and Garden Tools development, ROCK-FIRM fully understand the development, from initial concepts' research to mass production. We also have exported for all our retro toys products worldwide. This one-stop-shop service not only save our clients' time, but also let us understand the need from our clients. Therefore, we can provide the best service for both cost and quality.</p>
              </div>
            </div>
          </div><!-- end of quality-content-wrap -->
        </div><!-- end of page-content -->
      </div>
    </section>
    
  </main>

@endsection




  @section('script')

  <script>
    var tmax;
    var delay = .15
    var progress = 0;
    jQuery(".collapse-button").click(function() {
      if(!jQuery('#main-nav').hasClass('opened')) {
        resumeTmax();
      } else {
        clearTmax();
      }

    });

    function resumeTmax() {
      tmax = new TimelineMax();
      tmax.progress(progress)
      jQuery("#main-nav li a").each(function(idx, ele){
        tmax.fromTo(ele, .5, {
          ease:Linear.easeNone,
          opacity: 0,
          top: 30
        }, {
          ease:Linear.easeNone,
          opacity: 1,
          top: 0
        }, .2 + idx * delay)
      });
    }

    function clearTmax() {
      progress = tmax.progress();
      tmax.clear();
      tmax.kill();
    }
  </script>
  <script>
    // init controller
    var ScrollMagic_controller = new ScrollMagic.Controller();
    jQuery('.quality-block').each(function(idx, elem){
      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        offset: -50
      })
      .setClassToggle(this, "active")
      .reverse(false)
      .addTo(ScrollMagic_controller);

      var scene02 = new  ScrollMagic.Scene({
        triggerElement: this,
        duration: 100,
        offset: -100
      })
      .setTween(this, 0.2, {opacity: 1})
        .addTo(ScrollMagic_controller);

      var scene03 = new  ScrollMagic.Scene({
        triggerElement: this,
        duration: 200,
        offset: -80
      })
      .setTween(jQuery(elem).children('.context'), 0.5, {opacity: 1})
        .addTo(ScrollMagic_controller);
    });
  </script>
  @endsection