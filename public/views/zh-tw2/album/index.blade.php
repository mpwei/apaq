@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)
@section('bodyclass','gradient-background-header transparent-header front-page')

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@extends('page')

@section('content')

      <!-- banner start -->
      <div class="banner slideshow d-flex align-items-center" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'upload/index-slide/slide-1.jpg' }}'); background-position: 50% 27%;     min-height: 350px;">
         
        <div class="container  ">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20 ">
              <h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">  <strong>{{ $lang['album_title'] }}</strong></h2>
               
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->
      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
            <li class="breadcrumb-item active">{{ $lang['album_title'] }}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->


      <!-- main-container start -->   
      <section class="main-container">

        <div class="container">
          <div class="row">
            <div class="page-title-wrap mb-20">
              <h1 class="page-title">{{ $lang['album_title'] }}</h1>
              <div class="separator-2"></div>
              <!-- page-title end -->
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ut quisquam ab harum hic enim quibusdam aut quasi recusandae temporibus quo voluptatibus, dolorem consectetur ipsam facere ipsa. Commodi sunt, inventore!</p>
            </div>  

            <!-- main start -->         
            <div class="main col-md-12">
               <div class="isotope-container row grid-space-10">
                        
                        @if(!empty($pageData['data']))
                            @foreach ($pageData['data'] as $row)
                             <?php
                                $date = str_replace("-",".",$row->date);
                                $img = ( !empty($row->image) AND file_exists(public_path()."/". $row->image))?$row->image : "http://placehold.it/1024?text=noPic";
                             ?>
                        <div class="col-lg-4 col-md-6 isotope-item ">
                          <div class="image-box shadow-2 text-center mb-20">
                            <div class="overlay-container">
                              <img src="{{ $img }}" alt="{{ $row -> title }}">
                              <div class="overlay-top">
                                <div class="text">
                                  <h3><a href="{{ ItemMaker::url('album_detail/'.$row->id) }}">{{ $row -> title }}</a></h3>
                                  <p class="small"></p>
                                </div>
                              </div>
                              <div class="overlay-bottom">
                                <div class="links">
                                  <a href="{{ ItemMaker::url('album_detail/'.$row->id) }}" class="btn btn-gray-transparent btn-animated btn-sm">{{ $lang['detail']}} <i class="pl-10 fa fa-arrow-right"></i></a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                            @endforeach
                          @endif
                      </div>
                        
                          <!-- pagination start -->
                    <nav aria-label="Page navigation">
                      <ul class="pagination justify-content-center text-center-xs">
                        {!! $frontEndClass->getPageHtml($pageData) !!}
                      </ul>
                    </nav>
                <!-- pagination end -->         
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->


@stop