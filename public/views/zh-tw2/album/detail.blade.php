@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@extends('page')
@section('script')
      <script type="text/javascript" src="assets/js/subnav.js"></script>
@endsection
<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@section('content')

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

      <!-- banner start -->
      <div class="banner slideshow d-flex align-items-center" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'upload/index-slide/slide-1.jpg' }}'); background-position: 50% 27%;     min-height: 350px;">
         
        <div class="container  ">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20 ">
              <h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">  <strong>{{ $title }}</strong></h2>
               
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->
      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
            <li class="breadcrumb-item"><i class="fa pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('album') }}">{{$lang['album_bread']}}</a></li>
            <li class="breadcrumb-item active">{{ $title }}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->

      

      <section class="main-container">

        <div class="container">
          <div class="row">
            <div class="page-title-wrap mb-20">
              <h1 class="page-title">{{$lang['album_bread']}}</h1>
              <div class="separator-2">{{ $title }}</div>
              <!-- page-title end -->
              <p>{{ $content }}</p>
            </div>  

            <!-- main start -->         
            <div class="main col-md-12">
               <div class="isotope-container row grid-space-10">
                        
                  @if(!empty($pageData['data']))
                      @foreach ($pageData['data'] as $row)
                       <?php
                          $date = str_replace("-",".",$row->date);
                          $img = ( !empty($row->image) AND file_exists(public_path()."/". $row->image))?$row->image : "http://placehold.it/1024?text=noPic";
                       ?>

                        <div class="col-lg-4 col-md-6 isotope-item ">
                          <div class="image-box  text-center mb-20">
                            <div class="overlay-container">
                              <img src="{{ $img }}" alt="">
                              <a href="{{ $img }}" class="overlay-link small popup-img" title="{{ $row -> title }}">
                                                <i class="fa fa-plus"></i>
                                              </a>
                            </div>
                          </div>
                        </div>

                      @endforeach
                    @endif
                      </div>  
                      <div class="separator-2"></div>
                       
                        <a class="back_btn btn btn-animated btn-default-transparent btn-md" href=" javascript:history.back();"><i class="icon-back"></i>Back</a>            
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>

@stop