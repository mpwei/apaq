<div class="light-gray-bg footer-top border-clear">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="call-to-action text-center">
                <div class="row">
                  <div class="col-sm-12">
                   <h2 class="title ">Find Us</h2>
                   <div class="separator"></div>
                  </div>
                  
                  <div class="col-sm-4">
                    <i class="fa fa-map-marker pr-10 text-default"></i>{{ $Setting['address'] }}
                  </div>
                  <div class="col-sm-4">
                    <i class="fa fa-phone pr-10 text-default"></i><a a href="tel:{{ $Setting['tel'] }}">{{ $Setting['tel'] }}</a>
                  </div>
                  <div class="col-sm-4">
                    <a href="mailto:{{ $Setting['email'] }}"><i class="fa fa-envelope-o pr-10"></i>{{ $Setting['email'] }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer id="footer" class="clearfix "> 
        <!-- .footer start -->
        <!-- ================ -->
        <div class="footer">
          <div class=" container container-fluid">
            <div class="footer-inner">
              <div class="row"> 
                <div class="col-sm-12 text-center">
                  <div class="footer-content">
                    <h2 class="title">Follow Us</h2>
                    <div class="separator"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <ul class="social-links circle animated-effect-1">
                      <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                      <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                      <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                      <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li> 
                    </ul>
                  </div>
                </div>  
                <!-- section start -->
                <!-- ================ -->
                <section id="collapseMap">
                  <div id="map-canvas"></div>
                </section>
                <!-- section end -->
              </div>
            </div>
          </div>
        </div> 
        <!-- .footer end -->

        <!-- .subfooter start -->
        <!-- ================ -->
        <div class="subfooter default-bg">
          <div class="container">
            <div class="subfooter-inner">
              <div class="row">
                <div class="col-md-12">
                  <p class="text-center">Copyright © 2018 ocean by <a target="_blank" href="http://www.oceanad.com.tw">oceanad</a>. All Rights Reserved</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- .subfooter end --> 
 </footer>