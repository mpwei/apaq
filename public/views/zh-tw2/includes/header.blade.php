
@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
    $qty  = 0;
    $total  = 0;
?> 
<!-- header-container end -->
  <div class="header-container">
        <!-- header-top start -->
        <!-- classes:  -->
        <!-- "dark": dark version of header top e.g. class="header-top dark" -->
        <!-- "colored": colored version of header top e.g. class="header-top colored" -->
        <!-- ================ -->
       <!-- ================ -->
        <div class="header-top colored">
          <div class="container">
            <div class="row">
              <div class="col-3 col-sm-6 col-lg-9">
                <!-- header-top-first start -->
                <!-- ================ -->
                <div class="header-top-first clearfix">
                 
                  <ul class="list-inline hidden-md-down"> 
                    <li class="list-inline-item"><i class="fa fa-phone pr-1 pl-2"></i>{{ $Setting['tel'] }}</li>
                    <li class="list-inline-item"><i class="fa fa-envelope-o pr-1 pl-2"></i>{{ $Setting['email'] }}</li>
                  </ul>
                </div>
                <!-- header-top-first end -->
              </div>
              <div class="col-9 col-sm-6 col-lg-3"> 
                <!-- header-top-second start -->
                <!-- ================ -->
                <div id="header-top-second"  class="clearfix  text-right">
                    <ul class="social-links lang-links  circle small clearfix hidden-sm-down">
                      <li > <a href="page-signup.html" >EN</a></li>
                      <li ><a href="page-signup.html"  >繁</a></li> 
                    </ul>
                    <div class="social-links lang-links  hidden-md-up circle small">
                      <div class="btn-group dropdown">
                        <button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe"></i></button>
                        <ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
                           <li ><a href="page-signup.html" >EN</a></li>
                           <li ><a href="page-signup.html" >繁</a></li> 
                        </ul>
                      </div>
                    </div>
                </div>
                <!-- header-top-second end -->
              </div>
            </div>
          </div>
        </div>
        <!-- header-top end -->

        <!-- header start -->
        <!-- classes:  -->
        <!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
        <!-- "fixed-desktop": enables fixed navigation only for desktop devices e.g. class="header fixed fixed-desktop clearfix" -->
        <!-- "fixed-all": enables fixed navigation only for all devices desktop and mobile e.g. class="header fixed fixed-desktop clearfix" -->
        <!-- "dark": dark version of header e.g. class="header dark clearfix" -->
        <!-- "centered": mandatory class for the centered logo layout -->
        <!-- ================ -->
       <header class="header fixed fixed-desktop clearfix">
          <div class="container">
            <div class="row">
              <div class="col-md-auto hidden-md-down pl-3">
                <!-- header-first start -->
                <!-- ================ -->
                <div class="header-first clearfix">

                  <!-- logo -->
                  <div id="logo" class="logo">
                    <a href="{{ ItemMaker::url('./')}}"><img id="logo_img" src="../../assets/images/logo/logo-2.svg" alt="The Ocean"></a>
                  </div>

                  
                </div>
                <!-- header-first end -->

              </div>
              <div class="col-lg-8 col-xl-9">

                <!-- header-second start -->
                <!-- ================ -->
                <div class="header-second clearfix">
                  
                <!-- main-navigation start -->
                <!-- classes: -->
                <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                <!-- ================ -->
                <div class="main-navigation  animated">
                  <nav class="navbar navbar-toggleable-md navbar-light p-0">
                    
                    
                    
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-brand clearfix hidden-lg-up">
                
                      <!-- logo -->
                      <div id="logo-mobile" class="logo">
                        <a href="{{ ItemMaker::url('./')}}"><img id="logo-img-mobile" src="../../assets/images/logo/logo-2.svg" alt="The Ocean"></a>
                      </div> 
                    </div>

                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                      <!-- header dropdown buttons -->
                    <div class="header-dropdown-buttons hidden-lg-up mb-2 pb-">
                      <div class="btn-group">
                        <button type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" id="header-drop-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-search"></i></button>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-animation" aria-labelledby="header-drop-3">
                          <li>
                            {!! Form::open(['id'=>"form",'class'=>"search-box margin-clear", 'method'=>"get", 'name' => 'form', "url"=> ItemMaker::url('search') ]) !!}
                              <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="{{ $lang['search']}}" id="key" name="key">
                                <i class="icon-search form-control-feedback" onclick="document.forms.form.submit()"></i>
                              </div>
                            {!! Form::close()  !!}
                          </li>
                        </ul>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" id="header-drop-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-basket-1"></i><span class="cart-count default-bg lcart">{{ count(Session::get('Spec')['list']) }}</span></button>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
                          <li>
                            <div class="cms-table-wrap">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th class="quantity">{{ $lang['car_quantity'] }}</th>
                                    <th class="product">{{ $lang['car_item_title'] }}</th>
                                   
                                  </tr>
                                </thead>
                                <tbody class="cart-area">
                                    @if(!empty(Session::get('Spec')['list']))
                                    @foreach(Session::get('Spec')['list'] as $row)
                                     <?php
                                        $qty += $row['quantity'];
                                        $total = $row['quantity'] * $row['price'];
                                        $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/1024?text=noPic";
                                     ?>
                                      <tr>
                                        <td class="quantity">{{ $row['quantity'] }} x</td>
                                        <td class="product"><a href="{{ ItemMaker::url('productdetail/'.$row['title']) }}">{{ $row['title'] }}</a></td>
                                      </tr>
                                        @endforeach
                                    @endif
                                  <tr>
                                    <td class="total-quantity" colspan="2">Total {{ $qty }} 件</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <div class="panel-body text-right">
                              <a href="{{ ItemMaker::url('shipping') }}" class="btn btn-group btn-gray btn-sm">{{ $lang['go_shopping_car'] }}</a>
                              <a href="{{ ItemMaker::url('billing') }}" class="btn btn-group btn-dark btn-sm">{{ $lang['go_billing'] }}</a>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <!-- header dropdown buttons end-->
                      <!-- main-menu -->
                      <ul class="navbar-nav ml-xl-auto"> 
                        <!-- mega-menu start -->
                        <li class="nav-item dropdown {!! $frontEndClass -> getnowpage('about') !!} ">
                          <a href="{{ ItemMaker::url('about') }}" class="nav-link dropdown-toggle" id="first-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{{ $lang['about_title']}}</a>
                          <ul class="dropdown-menu" aria-labelledby="first-dropdown">   
                              @if(!empty($getTitle['about']))
                                @foreach($getTitle['about'] as $row)
                              <li class="{!! $frontEndClass -> getnowpage($row['title']) !!}"><a href="{{ ItemMaker::url('about/'.$row['title']) }}">{{ $row['title'] }}</a></li>
                                @endforeach
                              @endif 
                          </ul> 
                        </li>
                        <!-- mega-menu end -->
                        <!-- mega-menu start -->
                        <li class="nav-item dropdown {!! $frontEndClass -> getnowpage('product') !!}">
                          <a href="{{ ItemMaker::url('product/') }}" class="nav-link dropdown-toggle" id="second-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{{ $lang['product_title']}}</a>
                          <ul class="dropdown-menu" aria-labelledby="second-dropdown"> 
                            @if(!empty($getTitle['product']))
                              @foreach($getTitle['product'] as $row)
                            <li @if(!empty($row['category'])) class="dropdown " @endif>
                              <a  href="{{ ItemMaker::url('product/'.$row['title'] ) }}"  @if(!empty($row['category'])) class="dropdown-toggle {!! $frontEndClass -> getnowpage($row['title']) !!}" data-toggle="dropdown" @endif >{{ $row['title'] }}</a>
                               @if(!empty($row['category'])) 
                              <ul class="dropdown-menu">
                                @foreach($row['category'] as $category_row)
                                <li><a class="{!! $frontEndClass -> getnowpage($category_row['title']) !!}" href="{{ ItemMaker::url('product/'.$row['title'] . '/' .$category_row['title'] ) }}">{{ $category_row['title'] }}</a></li>
                                @endforeach
                              </ul>
                              @endif
                            </li>
                              @endforeach
                            @endif
                          </ul> 
                        </li> 
                        <li class="nav-item dropdown {!! $frontEndClass -> getnowpage('news') !!}"> <a href="{{ ItemMaker::url('news') }}" class="nav-link  dropdown-toggle" id="third-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $lang['news_title']}}</a>
                          <ul class="dropdown-menu" aria-labelledby="third-dropdown">  
                              @if(!empty($getTitle['news']))
                                @foreach($getTitle['news'] as $row)
                            <li><a class="{!! $frontEndClass -> getnowpage($row['title']) !!}"  href="{{ ItemMaker::url('news/'.$row['title']) }}">{{ $row['title'] }}</a></li> 
                                @endforeach
                              @endif
                          </ul> 
                        </li>
                        <li class="nav-item {!! $frontEndClass -> getnowpage('download') !!}"><a  href="{{ ItemMaker::url('download') }}" class="nav-link" aria-haspopup="true" aria-expanded="false">{{ $lang['download_title']}}</a></li>
                        <li class="nav-item {!! $frontEndClass -> getnowpage('album') !!}"><a  href="{{ ItemMaker::url('album') }}" class="nav-link" aria-haspopup="true" aria-expanded="false">{{ $lang['album_title']}}</a></li>
                        <li class="nav-item {!! $frontEndClass -> getnowpage('faq') !!}"><a  href="{{ ItemMaker::url('faq') }}" class="nav-link" aria-haspopup="true" aria-expanded="false">{{ $lang['faq_title']}}</a></li>
                        <li class="nav-item {!! $frontEndClass -> getnowpage('contact') !!}"> <a href="{{ ItemMaker::url('contact') }}" class="nav-link"  aria-haspopup="true" aria-expanded="false">{{ $lang['contact_title']}}</a></li>
                      </ul>
                      <!-- main-menu end -->
                    </div>
                  </nav>
                </div>
                <!-- main-navigation end -->
                </div>
                <!-- header-second end -->

              </div>
              <div class="col-auto ml-auto pr-3 hidden-md-down">
                <!-- header dropdown buttons -->
                <div class="header-dropdown-buttons">
                  <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" id="header-drop-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-search"></i></button>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-animation" aria-labelledby="header-drop-1">
                      <li>
                        {!! Form::open(['id'=>"form2",'class'=>"search-box margin-clear", 'method'=>"get", 'name' => 'form', "url"=> ItemMaker::url('search') ]) !!}
                          <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="{{ $lang['search']}}" id="key" name="key">
                            <i class="icon-search form-control-feedback" onclick="document.forms.form2.submit()"></i>
                          </div>
                        {!! Form::close()  !!}
                      </li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" id="header-drop-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-basket-1"></i><span class="cart-count default-bg lcart">{{ count(Session::get('Spec')['list']) }}</span></button>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
                      <li>
                        <div class="cms-table-wrap">
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <th class="quantity">{{ $lang['car_quantity'] }}</th>
                                <th class="product">{{ $lang['car_item_title'] }}</th>
                               
                              </tr>
                            </thead>
                            <tbody class="cart-area">
                                @if(!empty(Session::get('Spec')['list']))
                                @foreach(Session::get('Spec')['list'] as $row)
                                 <?php
                                    $qty += $row['quantity'];
                                    $total = $row['quantity'] * $row['price'];
                                    $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/1024?text=noPic";
                                 ?>
                                  <tr>
                                    <td class="quantity">{{ $row['quantity'] }} x</td>
                                    <td class="product"><a href="{{ ItemMaker::url('productdetail/'.$row['title']) }}">{{ $row['title'] }}</a></td>
                                  </tr>
                                    @endforeach
                                @endif
                              <tr>
                                <td class="total-quantity" colspan="2">Total {{ $qty }} 件</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="panel-body text-right">
                          <a href="{{ ItemMaker::url('shipping') }}" class="btn btn-group btn-gray btn-sm">{{ $lang['go_shopping_car'] }}</a>
                          <a href="{{ ItemMaker::url('billing') }}" class="btn btn-group btn-dark btn-sm">{{ $lang['go_billing'] }}</a>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- header dropdown buttons end-->
              </div>
            </div>
          </div>
        </header>
        <!-- header end -->
      </div>