@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

<?php 
    $homebanner = $frontEndClass -> getBanner();
?> @section('title', $ProjectShareName) @section('keywords', $ProjectShareName) @section('description',$ProjectShareName)
@section('css')
<link href="assets/css/custom/custom-home.css" rel="stylesheet"> @endsection

@extends('template') @section('content')

<!-- header-container end -->
<!-- banner start -->
<!-- ================ -->
      <!-- banner start -->      
      <div class="banner clearfix"> 

        <!-- slideshow start -->        
        <div class="slideshow"> 
          <!-- slider revolution start -->           
         <div class="slider-revolution-5-container">
            <div id="slider-banner-fullwidth-big-height" class="slider-banner-fullwidth-big-height dotted rev_slider" data-version="5.0">
              <ul class="slides">
          @if(!empty($homebanner)) @foreach($homebanner as $row)
          <!-- slide 1 start -->
          <!-- ================ -->
          <li class="text-center" data-transition="slidingoverlayleft" data-slotamount="default" data-masterspeed="default" data-title="">

            <!-- main image -->
            @if($row -> link)
            <a href="{{ $row -> link }}" target="{{ $row -> is_href ? '_target' : '' }}">
              @endif
              <img src="{{ !empty($row -> image) ? $row -> image : 'upload/index-slide/slide-1.jpg' }}" alt="slidebg1" data-bgposition="center center"
                data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg"> @if($row -> content)
              <!-- Transparent Background -->
              <div class="tp-caption dark-translucent-bg" data-x="center" data-y="center" data-start="0" data-transform_idle="o:1;" data-transform_in="o:0;s:600;e:Power2.easeInOut;"
                data-transform_out="o:0;s:600;" data-width="5000" data-height="5000">
              </div>

              <!-- LAYER NR. 1 -->
              <div class="tp-caption xlarge_white" data-x="center" data-y="110" data-start="1000" data-end="2500" data-splitin="chars"
                data-splitout="chars" data-elementdelay="0.1" data-endelementdelay="0.1" data-transform_idle="o:1;" data-transform_in="x:[50%];o:0;s:600;e:Power4.easeInOut;"
                data-transform_out="x:[-50%];o:0;s:200;e:Power2.easeInOut;" data-mask_in="x:0;y:0;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">{!! nl2br($row->content, false) !!}
              </div>
              <!-- LAYER NR. 1 -->

              @endif @if($row -> link)
            </a>
            @endif
          </li>
          <!-- slide 1 end -->
          @endforeach @endif
              </ul>
               <div class="tp-bannertimer"></div>
            </div>
          </div>
          <!-- slider revolution end --> 
        </div>
        <!-- slideshow end --> 
      </div>
      <!-- banner end -->

      <div id="page-start"></div>

      <!-- section start -->
     
      <section class="light-gray-bg pv-30 clearfix ">
        <div class="container">
          <div class="row justify-content-md-center">
            <div class="col-md-10  ">
              <h1 class="text-center large">Welcome to The <span class="text-default">Ocean</span></h1>
              <div class="separator"></div>
              <p class="large text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam voluptas facere vero ex tempora saepe perspiciatis ducimus sequi animi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe eum incidunt odit aut voluptatum, delectus quod. Tenetur, <span class="text-default">voluptatem esse, quisquam</span> error debitis delectus laboriosam ab voluptate rerum unde necessitatibus est!</p>
            </div>
          </div>
        </div>
      </section>
      <!-- section end -->

     

      <!-- section -->
     
      <section class="pv-40 dark-translucent-bg" style="background: url(../../assets/images/bg/background-img-01.jpg) 50% 70% no-repeat;">
        <div class="container">
          <h1 class="large text-center text-default">our <span class="text-default">SERVICE</span></h1>
          <div class="separator"></div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="feature-box-2 object-non-visible right" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon white-bg small circle text-default"><i class="fa fa-check"></i></span>
                <div class="body">
                  <h3 class="title">SOCIAL EVENT PLANNER</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  
                  
                  <div class="separator-3"></div>
                </div>
              </div>
              <br>
              <div class="feature-box-2 object-non-visible right" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon default-bg small circle"><i class="icon-check"></i></span>
                <div class="body">
                  <h3 class="title">PACKAGING</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <div class="separator-3"></div>
                </div>
              </div>
              <div class="feature-box-2 object-non-visible right" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon white-bg small circle text-default"><i class="fa fa-check"></i></span>
                <div class="body">
                  <h3 class="title">PHOTOS</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <div class="separator-3"></div>
                </div>
              </div>
               
              <br>
            </div>
            <div class="col-md-6">
              <div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon default-bg small circle"><i class="fa fa-check"></i></span>
                <div class="body">
                  <h3 class="title">BRANDING</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <div class="separator-2"></div>
                   
                </div>
              </div>
              <br>
              <div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon white-bg small circle text-default"><i class="fa fa-check"></i></span>
                <div class="body">
                  <h3 class="title">CORPORATE IDENTITY SYSTEM</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <div class="separator-2"></div>
                </div>
              </div>
              <div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <span class="icon default-bg small circle"><i class="fa fa-check"></i></span>
                <div class="body">
                  <h3 class="title">WEB DESIGN</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  <div class="separator-2"></div>
                </div>
              </div>
              
              <br>
            </div>
          </div>
        </div>
      </section>
      <!-- section end -->

      <!-- section start -->      
      <section class="light-gray-bg pv-40">
        <div class="container">
          <h1 class="large text-center"><span class="text-default">work</span></h1>
          <div class="separator"></div>
          
        </div>
        <div class="container-fluid">
          <div class="row grid-space-0">
            <div class="col-sm-3">
              <a href="../work/detail.php" class="overlay-link"></a>
              <div class="team-member image-box style-2 text-center">
                <div class="overlay-container overlay-visible">
                  <img src="../../upload/index-product/work-01.png" alt="">
                  <div class="overlay-bottom">
                    <span class=" text-center">work01</span>
                  </div>
                </div>
                
                 
              </div>
            </div>
            <div class="col-sm-3">
              <a href="../work/detail.php" class="overlay-link"></a>
              <div class="team-member image-box style-2 text-center">
                <div class="overlay-container overlay-visible">
                  <img src="../../upload/index-product/work-02.png" alt="">
                  <div class="overlay-bottom">
                    <span class=" text-center">work01</span>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="col-sm-3">
              <a href="../work/detail.php" class="overlay-link"></a>
              <div class="team-member image-box style-2 text-center">
                <div class="overlay-container overlay-visible">
                  <img src="../../upload/index-product/work-03.png" alt="">
                  <div class="overlay-bottom">
                    <span class=" text-center">work01</span>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="col-sm-3">
              <a href="../work/detail.php" class="overlay-link"></a>
              <div class="team-member image-box style-2 text-center">
                <div class="overlay-container overlay-visible">
                  <img src="../../upload/index-product/work-04.png" alt="">
                  <div class="overlay-bottom">
                    <span class=" text-center">work01</span>
                  </div>
                </div>
               
              </div>
            </div>
          </div>
        </div>
        
         <div class="text-center mt-20" ><a href="#" class="btn btn-default btn-md margin-clear radius-50 smooth-scroll">View More</a>
                </div>
      </section>
      <!-- section end -->

      <!-- section start -->      
      <section class="default-translucent-bg parallax-2 pv-40" style="background: url(../../assets/images/bg/background-img-2.png) 50% 50% no-repeat;">
        <br>
        <br>
        <br>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="call-to-action text-center">
                <div class="row justify-content-md-center">
                  <div class="col-sm-8  ">
                    <h2 class="title">Contact us</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus error pariatur deserunt laudantium nam, mollitia quas nihil inventore, quibusdam?</p>
                    <div class="separator"></div>
                    <button type="submit" class="btn btn-lg btn-gray-transparent radius-50   margin-clear">Contact us<!-- <i class="fa fa-send"></i> --></button>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <br>
        <br>
      </section>
      <!-- section end -->


@endsection