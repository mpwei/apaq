<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from pexetothemes.com/demos/story_demos/restaurant/news/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Nov 2016 14:11:08 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>SHYANG-SOON Industry.</title>
    <!-- Mobile Devices Viewport Resset-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- <meta name="viewport" content="initial-scale=1.0, user-scalable=1" /> -->
    <!--[if lt IE 9]>
<script src="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/themes/thestory/js/html5shiv.js"></script>
<![endif]-->
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/72x72\/",
            "ext": ".png",
            "source": {
                "concatemoji": "http:\/\/pexetothemes.com\/demos\/story_demos\/restaurant\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.5"
            }
        };
        ! function (a, b, c) {
            function d(a) {
                var c, d, e, f = b.createElement("canvas"),
                    g = f.getContext && f.getContext("2d"),
                    h = String.fromCharCode;
                return g && g.fillText ? (g.textBaseline = "top", g.font = "600 32px Arial", "flag" === a ? (g.fillText(h(55356, 56806, 55356, 56826), 0, 0), f.toDataURL().length > 3e3) : "diversity" === a ? (g.fillText(h(55356, 57221), 0, 0), c = g.getImageData(16, 16, 1, 1).data, g.fillText(h(55356, 57221, 55356, 57343), 0, 0), c = g.getImageData(16, 16, 1, 1).data, e = c[0] + "," + c[1] + "," + c[2] + "," + c[3], d !== e) : ("simple" === a ? g.fillText(h(55357, 56835), 0, 0) : g.fillText(h(55356, 57135), 0, 0), 0 !== g.getImageData(16, 16, 1, 1).data[0])) : !1
            }

            function e(a) {
                var c = b.createElement("script");
                c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }
            var f, g;
            c.supports = {
                simple: d("simple"),
                flag: d("flag"),
                unicode8: d("unicode8"),
                diversity: d("diversity")
            }, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.simple && c.supports.flag && c.supports.unicode8 && c.supports.diversity || (g = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='woocommerce-layout-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-layout18f6.css?ver=2.4.12' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen18f6.css?ver=2.4.12' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='wp-pagenavi-css' href='wp-content/plugins/wp-pagenavi/pagenavi-css44fd.css?ver=2.70' type='text/css' media='all' />
    <link rel='stylesheet' id='pexeto-font-0-css' href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C400italic%2C700&amp;ver=4.4.5' type='text/css' media='all' />
    <link rel='stylesheet' id='pexeto-font-1-css' href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&amp;ver=4.4.5' type='text/css' media='all' />
    <link rel='stylesheet' id='pexeto-pretty-photo-css' href='wp-content/themes/thestory/css/prettyPhoto4735.css?ver=1.7.1' type='text/css' media='all' />
    <link rel='stylesheet' id='pexeto-woocommerce-css' href='wp-content/themes/thestory/css/woocommerce4735.css?ver=1.7.1' type='text/css' media='all' />
    <link rel='stylesheet' id='pexeto-stylesheet-css' href='wp-content/themes/thestory/style4735.css?ver=1.7.1' type='text/css' media='all' />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0/css/font-awesome.css" rel="stylesheet">
    <style id='pexeto-stylesheet-inline-css' type='text/css'>
        body,
        .page-wrapper,
        #sidebar input[type="text"],
        #sidebar input[type="password"],
        #sidebar textarea,
        .comment-respond input[type="text"],
        .comment-respond textarea {
            background-color: #f5f4f0;
        }
        
        .header-wrapper,
        .pg-navigation,
        .mobile.page-template-template-fullscreen-slider-php #header,
        .mobile.page-template-template-fullscreen-slider-php .header-wrapper {
            background-color: #212738;
        }
        
        .dark-header #header {
            background-color: rgba(33, 39, 56, 0.7);
        }
        
        .fixed-header-scroll #header {
            background-color: rgba(33, 39, 56, 0.95);
        }
        
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        .pt-price {
            font-family: Georgia, serif;
        }
        
        #menu ul li a {
            font-family: Georgia, serif;
            font-size: 15px;
        }
        
        .page-title h1 {
            font-family: Georgia, serif;
            font-size: 58px;
        }
        
        body>iframe {
            height: 0;
        }
    </style>
    <!--[if lte IE 8]>
<link rel='stylesheet' id='pexeto-ie8-css'  href='http://pexetothemes.com/demos/story_demos/restaurant/wp-content/themes/thestory/css/style_ie8.css?ver=1.7.1' type='text/css' media='all' />
<![endif]-->
    <script type='text/javascript' src='wp-includes/js/jquery/jqueryc1d8.js?ver=1.11.3'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min1576.js?ver=1.2.1'></script>
    <!-- styles for Simple Google Map -->
    <style type='text/css'>

    </style>
    <!-- end styles for Simple Google Map -->
    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>

    <style>
        /* FAQ COLLAPSE/EXPAND STYLES */
        
        .faqanswer {
            display: none;
            background: #e5e5e5;
            padding: 12px 20px 0 30px;
        }
        
        .faqanswer p {
            font-size: 13px;
            line-height: 17px;
        }
        
        a.active {
            font-weight: bold;
        }
        
        .togglefaq {
            text-decoration: none;
            color: #333;
            font-size: 13px;
            padding: 10px 30px;
            line-height: 20px;
            display: block;
            border: 1px solid #d0d0d0;
            margin-bottom: -1px;
        }
        
        .icon-plus {
            color: #5ec4cd;
            margin-right: 20px;
            font-size: 20px;
            float: left;
        }
        
        .icon-minus {
            color: #5ec4cd;
            margin-right: 20px;
            font-size: 20px;
            float: left;
        }
        
        p {
            margin: 0;
            padding-bottom: 20px;
        }
    </style>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-10643569-3', 'pexetothemes.com');
        ga('send', 'pageview');
    </script>
</head>
<body class="page page-id-76 page-template-default woocommerce-cart woocommerce-page fixed-header no-slider icons-style-light">
<div id="main-container" >
	<div  class="page-wrapper" >
		<!--HEADER -->
				<div class="header-wrapper" >
				<? require_once("includes/header.php") ?>

	<div class="page-title-wrapper"><div class="page-title">
		<div class="content-boxed">
			<h1>INQUIRY</h1>
					</div>
	</div>
</div>

</div>
<div id="content-container" class="content-boxed layout-right">

		<div class="content-box">
		<div class="woocommerce">
	<div class="woocommerce-message">Products List</div>

<form action="http://pexetothemes.com/demos/story_demos/restaurant/cart/" method="post">


<table class="shop_table cart" cellspacing="0">
	<thead>
		<tr>
			<th class="product-remove">&nbsp;</th>
			<th class="product-thumbnail">&nbsp;</th>
			<th class="product-name">PRODUCT</th>
			<th class="product-quantity">QUANTITY</th>
		</tr>
	</thead>
	<tbody>
		
						<tr class="cart_item">

					<td class="product-remove">
						<a href="index9eda.html?remove_item=812b4ba287f5ee0bc9d43bbf5bbe87fb&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="95" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/ravioli-dinner/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-205242-ravioli-dinner-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Ravioli dinner" srcset="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-205242-ravioli-dinner-s-300x200.jpg 300w, http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-205242-ravioli-dinner-s.jpg 948w" sizes="(max-width: 90px) 100vw, 90px" /></a>					</td>

					<td class="product-name">
						<a href="product/ravioli-dinner/index.html">Ravioli dinner </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[812b4ba287f5ee0bc9d43bbf5bbe87fb][qty]" value="1" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="indexb7b1.html?remove_item=f4b9ec30ad9f68f89b29639786cb62ef&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="94" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/apple-pie/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-1042349-apple-pie-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Apple Pie" srcset="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-1042349-apple-pie-s-300x200.jpg 300w, http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-1042349-apple-pie-s.jpg 948w" sizes="(max-width: 90px) 100vw, 90px" /></a>					</td>

					<td class="product-name">
						<a href="product/apple-pie/index.html">Apple Pie </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[f4b9ec30ad9f68f89b29639786cb62ef][qty]" value="2" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="indexa6fd.html?remove_item=98dce83da57b0395e163467c9dae521b&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="93" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/hummus-with-pita-bread/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-201248-hummus-with-pita-bread-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Hummus with pita bread" srcset="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-201248-hummus-with-pita-bread-s-300x200.jpg 300w, http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/photodune-201248-hummus-with-pita-bread-s.jpg 948w" sizes="(max-width: 90px) 100vw, 90px" /></a>					</td>

					<td class="product-name">
						<a href="product/hummus-with-pita-bread/index.html">Hummus with pita bread </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[98dce83da57b0395e163467c9dae521b][qty]" value="1" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="indexa7bd.html?remove_item=92cc227532d17e56e07902b254dfad10&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="92" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/chicken-skewers/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-196751-chicken-skewers-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Chicken skewers" /></a>					</td>

					<td class="product-name">
						<a href="product/chicken-skewers/index.html">Chicken skewers </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[92cc227532d17e56e07902b254dfad10][qty]" value="1" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="indexb92e.html?remove_item=8613985ec49eb8f757ae6439e879bb2a&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="90" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/tomato-salad/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-193774-tomato-salad-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Tomato salad" /></a>					</td>

					<td class="product-name">
						<a href="product/tomato-salad/index.html">Tomato salad  </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[8613985ec49eb8f757ae6439e879bb2a][qty]" value="1" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="index00c3.html?remove_item=7647966b7343c29048673252e490f736&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="89" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/pasta-and-tomato-sauce/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/photodune-199621-pasta-and-tomato-sauce-s.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Pasta and tomato sauce" /></a>					</td>

					<td class="product-name">
						<a href="product/pasta-and-tomato-sauce/index.html">Pasta and tomato sauce </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[7647966b7343c29048673252e490f736][qty]" value="1" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
								<tr class="cart_item">

					<td class="product-remove">
						<a href="indexe437.html?remove_item=c45147dee729311ef5b5c3003946c48f&amp;_wpnonce=d74d14575a" class="remove" title="Remove this item" data-product_id="116" data-product_sku="">&times;</a>					</td>

					<td class="product-thumbnail">
						<a href="product/classic-bluberry-pie/index.html"><img width="90" height="60" src="wp-content/uploads/sites/3/2014/06/blueberry.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="blueberry" srcset="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/blueberry-300x200.jpg 300w, http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/blueberry-1024x682.jpg 1024w, http://pexetothemes.com/demos/story_demos/restaurant/wp-content/uploads/sites/3/2014/06/blueberry.jpg 1600w" sizes="(max-width: 90px) 100vw, 90px" /></a>					</td>

					<td class="product-name">
						<a href="product/classic-bluberry-pie/index.html">Classic Bluberry Pie </a>					</td>


					<td class="product-quantity">
						<div class="quantity"><input type="number" step="1" min="0"  name="cart[c45147dee729311ef5b5c3003946c48f][qty]" value="2" title="Qty" class="input-text qty text" size="4" /></div>
					</td>

				</tr>
						<tr>
			<td colspan="6" class="actions">
				
				<input type="submit" class="button" name="update_cart" value="UPDATE CART" />

				
				<input type="hidden" id="_wpnonce" name="_wpnonce" value="d74d14575a" /><input type="hidden" name="_wp_http_referer" value="/demos/story_demos/restaurant/cart/" />			</td>
		</tr>

			</tbody>
</table>


</form>
<div class="cart-collaterals">

	<div class="cart_totals ">

	
	<h2>CONTACT</h2>

	<div class="widget-contact-form">
			<form action="http://pexetothemes.com/demos/story_demos/restaurant/wp-content/themes/thestory/includes/send-email.php" method="post" 
			id="submit-form" class="pexeto-contact-form">
			<div class="error-box error-message"></div>
			<div class="info-box sent-message"></div>
			<input type="text" name="name" class="required placeholder" id="name_text_box" 
			placeholder="Name" />
			<input type="text" name="email" class="required placeholder email" 
			id="email_text_box" placeholder="Your e-mail" />
			<textarea name="question" class="required"
			id="question_text_area"></textarea>
			<input type="hidden" name="widget" value="true" />

			<a class="button send-button"><span>Send</span></a>
			<div class="contact-loader"></div><div class="check"></div>

			</form><div class="clear"></div></div>
	
</div>

</div>

</div>
		<div class="clear"></div>
		</div>
		 <!-- end main content holder (#content/#full-width) -->
<div class="clear"></div>
</div> <!-- end #content-container -->
</div>
<? require_once("includes/footer.php") ?>
</div> <!-- end #main-container -->


<!-- FOOTER ENDS -->

<script type='text/javascript' src='wp-content/plugins/anti-spam/js/anti-spam-4.1.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/demos\/story_demos\/restaurant\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/demos\/story_demos\/restaurant\/product\/apple-pie\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"http:\/\/pexetothemes.com\/demos\/story_demos\/restaurant\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min18f6.js?ver=2.4.12'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/single-product.min18f6.js?ver=2.4.12'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/demos\/story_demos\/restaurant\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/demos\/story_demos\/restaurant\/product\/apple-pie\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min18f6.js?ver=2.4.12'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/demos\/story_demos\/restaurant\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/demos\/story_demos\/restaurant\/product\/apple-pie\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min18f6.js?ver=2.4.12'></script>
<script type='text/javascript' src='wp-includes/js/underscore.minaff7.js?ver=1.6.0'></script>
<script type='text/javascript' src='wp-content/themes/thestory/js/main4735.js?ver=1.7.1'></script>
<script type='text/javascript' src='wp-includes/js/comment-reply.min4235.js?ver=4.4.5'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min4235.js?ver=4.4.5'></script>
<script type="text/javascript">var PEXETO = PEXETO || {};PEXETO.ajaxurl="wp-admin/admin-ajax.html";PEXETO.lightboxOptions = {"theme":"pp_default","animation_speed":"normal","overlay_gallery":false,"allow_resize":true};PEXETO.disableRightClick=false;PEXETO.stickyHeader=true;jQuery(document).ready(function($){
					PEXETO.init.initSite();PEXETO.woocommerce.init(true);$(".pexeto-contact-form").each(function(){
			$(this).pexetoContactForm({"wrongCaptchaText":"The text you have entered did not match the text on the image. Please try again.","failText":"An error occurred. Message not sent.","validationErrorText":"Please complete all the fields correctly","messageSentText":"Message sent"});
		});});</script>
		</body></html>