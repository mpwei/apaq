@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('script')
<script src="assets/js/front/member.js"></script>
@endsection

@section('content')

          <!-- banner start -->   
          <div class="banner dark-translucent-bg" style="background-image:url('assets/images/inner-slider.png'); background-position: 50% 27%;">
            <!-- breadcrumb start -->     
            <div class="breadcrumb-container">
              <div class="container">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['member_title'] }}</a></li>
                  <li class="breadcrumb-item active">{{ $lang['member_data_title'] }}</li>
                </ol>
              </div>
            </div>
            <!-- breadcrumb end -->
            <div class="container">
              <div class="row">
                <div class="col-md-8 text-center offset-md-2 pv-20">
                  <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{ $lang['member_data_title'] }}</h2>
          
                </div>
              </div>
            </div>

          </div>


            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container with-sidebar">

                <div class="container">
                      <div class="row">
                        <!-- sidebar start --> 
                        
                        <!-- sidebar end -->
                        <div class="col-lg-12">
              <div class="alert alert-success" id="MessageSent" style="display:none;">
                會員資料已更新
              </div>
              <div class="alert alert-danger" id="MessageNotSent" style="display:none;">
                糟了! 有東西出錯
              </div>
              <fieldset>
                <legend>個人資料</legend>
                {!! Form::open(['class'=>'well form-horizontal','url'=>'zh-tw/member/update','id'=>'memberForm']) !!}
                <input type="hidden" name="Member_front" value="1">
                  <div class="row">
                    <div class="col-12 ">
                      <div class="form-group row">
                        <label for="username" class="col-md-3 control-label">{{ $lang['member_name'] }}<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" id="form-name" name="first_name"  value="{{ $data->first_name }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="email" class="col-md-3 control-label">Email<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="email" class="form-control" value="{{ $data->email }}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="form-password" class="col-md-3 control-label">{{ $lang['password'] }}<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="password" class="form-control" id="form-password" name="password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="form-check_password" class="col-md-3 control-label">{{ $lang['check_password'] }}<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="password" class="form-control" id="form-check_password" name="check_password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="form-phone" class="col-md-3 control-label">{{ $lang['member_tel'] }}<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="text" id="form-phone" name="phone" class="form-control" value="{{ $data->phone }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="form-address" class="col-md-3 control-label">{{ $lang['member_address'] }}<small class="text-default">*</small></label>
                        <div class="col-md-9">
                          <input type="text" id="form-address" name="address" class="form-control" value="{{ $data->address }}">
                        </div>
                      </div>
                    </div>
                  </div>
                {!! Form::close() !!}
              </fieldset> 
              <div class="text-right">  
                  <a class="btn btn-group btn-outline-secondary" id="member_cancel"><i class="icon-cancel"></i>{{ $lang['member_cancel'] }}</a>
                  <a class="btn btn-group btn-default" id="member_btn" href=""><i class="icon-ok">{{ $lang['member_save'] }}</i></a>
              </div>
            </div>

            </div><!-- end of row -->
                        
        </div>

        </section>
        <!-- main-container end -->
            
            
@endsection