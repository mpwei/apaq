@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 

    $lang = $frontEndClass -> getlang($locateLang);

?>

@extends('page')



@section('title', $ProjectShareName)

@section('keywords', $ProjectShareName)

@section('description',$ProjectShareName)

@section('content')
            <div class="breadcrumb-container">
              <div class="container">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./')}}">{{ $lang['home_title'] }}</a></li>
                  <li class="breadcrumb-item active">THANK YOU</li>
                </ol>
              </div>
            </div>
            <!-- breadcrumb end -->

            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container">

            <div class="container">
              <div class="row">

                <!-- main start -->
                <!-- ================ -->
                <div class="main col-12">

                    <!-- page-title start -->
                    <!-- ================ -->
                    <h1 class="page-title text-center">Thank You <i class="fa fa-smile-o pl-10"></i></h1>
                    <div class="separator"></div>
                    <!-- page-title end -->

                    <p class="lead text-center">購物完成，感謝您的支持！</p>
                    <p class="text-center">
                        <a href="{{ ItemMaker::url('member/order')}}" class="btn btn-default btn-lg">{{ $lang['member_orderdetail'] }}</a> 
                    </p>


                </div>
                <!-- main end -->

              </div>
            </div>


            </section>
@endsection

                        