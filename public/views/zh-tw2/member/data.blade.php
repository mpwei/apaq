@inject('homeClass', 'App\Http\Controllers\MemberController')

@extends('template')
@section('script_select2')
	<!-- Select2 Js -->
        <script src="assets/js/select2.min.js" type="text/javascript"></script>
@endsection
@section('content')
            
<div class="wrapper-area" style="padding-top: 0px;">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
	        <!-- Header Area Start Here -->
	        
	        <!-- Header Area End Here -->
			<!-- Inner Page Banner Area Start Here -->
			<div class="inner-page-banner-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="pagination-area">
								<h2>My Account</h2>
								<ul>
									<li><a href="#">Home</a> /</li>
									<li>Account</li>
								</ul>
							</div>
						</div>
					</div>
				</div>  
			</div> 
			<!-- Inner Page Banner Area End Here -->	
			<!-- Login Registration Page Area Start Here -->
			<div class="login-registration-page-area">
				<div class="container"><div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="billing-details-area">
								<h2 class="cart-area-title">Member Data</h2>
							    <form id="checkout-form">
							    	<div class="row">
							    		<!-- First Name -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="first-name">First Name *</label>
												<input type="text" id="first-name" class="form-control">
											</div>
										</div>
										<!-- last Name -->
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="last-name">Last Name *</label>
												<input type="text" id="last-name" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
							    		<!-- Company Name -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="company-name">Company Name</label>
												<input type="text" id="company-name" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
							    		<!-- Email -->
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="email">E-mail Address *</label>
												<input type="text" id="email" class="form-control">
											</div>
										</div>
										<!-- Phone -->
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="phone">Phone *</label>
												<input type="text" id="phone" class="form-control">
											</div>
										</div>
									</div>									
									<div class="row">
							    		<!-- Country -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="country">Country</label>
												<div class="custom-select">
													<select id="country" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
														<option value="0">Select your country</option>
														<option value="1">Bangladesh</option>
														<option value="2">Spain</option>
														<option value="3">Belgium</option>
														<option value="3">Ecuador</option>
														<option value="3">Ghana</option>
														<option value="3">South Africa</option>
														<option value="3">India</option>
														<option value="3">Pakistan</option>
														<option value="3">Thailand</option>
														<option value="3">Malaysia</option>
														<option value="3">Italy</option>
														<option value="3">Japan</option>
														<option value="4">Germany</option>
														<option value="5">USA</option>
													</select><span class="select2 select2-container select2-container--classic select2-container--below" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-country-container"><span class="select2-selection__rendered" id="select2-country-container" title="Bangladesh">Bangladesh</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
							    		<!-- Address -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<label class="control-label">Address</label>
												<input type="text" id="address-line1" class="form-control">
												<input type="text" id="address-line2" class="form-control">
											</div>
										</div>										
									</div>		
									<div class="row">
							    		<!-- Town / City -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="town-city">Town / City</label>
												<input type="text" id="town-city" class="form-control">
												
											</div>
										</div>										
									</div>	
									<div class="row">
							    		<!-- District -->
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="district">District *</label>
												<div class="custom-select">
													<select id="district" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
														<option value="0">Select Your District</option>
														<option value="1">Dhaka</option>
														<option value="2">Rajshahi</option>
														<option value="4">Chittagong</option>
														<option value="5">GAZIPUR</option>
														<option value="6">GOPALGANJ</option>
														<option value="7">JAMALPUR</option>
														<option value="8">KISHOREGONJ</option>
														<option value="9">MADARIPUR</option>
														<option value="10">MANIKGANJ</option>
														<option value="11">MUNSHIGANJ</option>
														<option value="12">MYMENSINGH</option>
														<option value="13">NARAYANGANJ</option>
														<option value="14">NARSINGDI</option>
														<option value="15">NETRAKONA</option>
													</select><span class="select2 select2-container select2-container--classic select2-container--above" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-district-container"><span class="select2-selection__rendered" id="select2-district-container" title="Select Your District">Select Your District</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
											</div>
										</div>
										<!-- Postcode / ZIP -->
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">											
											<div class="form-group">
												<label class="control-label" for="postcode">Postcode / ZIP</label>
												<input type="text" id="postcode" class="form-control">
											</div>
										</div>
									</div>	
									<div class="row">
							    		<!-- CREAT AN ACCOUNT? -->
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
											<div class="form-group">
												<span><input type="checkbox" name="remember">CREAT AN ACCOUNT?</span>	
											</div>
										</div>										
									</div>
								</form>
							</div>
						</div>
						
					</div>
	        <!-- Login Registration Page Area End Here -->
			<!-- Footer Area Start Here -->
	        
	        <!-- Footer Area End Here -->
		</div>

@endsection