@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')
<?php
    $uri = $_SERVER["REQUEST_URI"];
    $lang = $frontEndClass -> getlang($locateLang);
?>
<nav class="sidenav sidenav02">
  <div class="sidenav-btn">{{ $lang['member_title'] }}<span class="fa fa-chevron-down"></span></div>
  <ul class="flex-column">
    <li class="sidenav-layer01 {!! $frontEndClass -> getnowpage('order') == ' active' ? '' : 'current' !!}">
      <a href="{{ ItemMaker::url('member')}}">帳戶資訊</a>
    </li>
    <li class="sidenav-layer01 {!! $frontEndClass -> getnowpage('order') == ' active' ? 'current' : '' !!}">
      <a href="{{ ItemMaker::url('member/order')}}">訂單</a>
    </li>
  </ul>
</nav>