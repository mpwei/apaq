@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('script')
<script src="assets/js/front/signin.js"></script>
@endsection

@section('content')


      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
            <li class="breadcrumb-item active">{{ $lang['member_login'] }}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->

      <!-- main-container start -->
      <!-- ================ -->
      <div class="main-container dark-translucent-bg member-page" style="background-image:url('assets/images/background-img-6.jpg');">
        <div class="container">
          <div class="row justify-content-center px-1">
            <div class="col-auto maxw-95">
              <!-- main start -->
              <!-- ================ -->
              <div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                <div class="form-block p-30 light-gray-bg border-clear">
                  <h2 class="title text-center">{{ $lang['login'] }}</h2>
                  <hr>
                  {!! Form::open(['class'=>'form-horizontal','url'=>'zh-tw/sign-in','id'=>'FormSign']) !!}
                    <div class="form-group has-feedback row">
                      <label for="inputEmail" class="col-md-3 control-label text-md-right col-form-label">{{ $lang['member_account'] }} <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'E-mail']) !!}
                        <i class="fa fa-envelope form-control-feedback pr-4"></i>
                      </div>
                    </div>
                    <div class="form-group has-feedback row">
                      <label for="inputPassword" class="col-md-3 control-label text-md-right col-form-label">{{ $lang['member_password'] }} <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        <i class="fa fa-lock form-control-feedback pr-4"></i>
                      </div>
                    </div>
                   
                    <div class="form-group row">
                      <div class="offset-md-3 col-md-8">
                        <input class="btn btn-group btn-default btn-cta text-center text-white" type="submit" value="登 入" id="signBtn"/>
                        <!-- <button type="submit" class="btn btn-group btn-default btn-cta text-center">註冊</button> -->
                        <p><a href="{{ ItemMaker::url('password-forget')}}" class="forget-link">{{ $lang['member_forgetpassword'] }}</a></p>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <p class="text-center">
                    <span>{{ $lang['member_member_yet'] }} </span>
                    <span class="pl-10 btn-group"><span><a href="{{ ItemMaker::url('register') }}" class="btn btn-default text-white"> {{ $lang['member_regist'] }}</a> </span></span>
                  </p>
                </div>
              </div>
              <!-- main end -->
            </div>
          </div>
        </div>
      </div>
@endsection