@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('script')
    <script src="assets/js/front/billing.js"></script>
@endsection

<?php
  $totalSum = 0;
  $total_quantity = 0;
?>
<?php $Payment = array("貨到付款","貨到付款","貨到付款","線上刷卡","貨到付款","銀行轉帳") ?>

@section('content')


            <!-- banner start -->   
            <div class="banner dark-translucent-bg" style="background-image:url('assets/images/inner-slider.png'); background-position: 50% 27%;">
              <!-- breadcrumb start -->     
              <div class="breadcrumb-container">
                <div class="container">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
                    <li class="breadcrumb-item"><a class="link-dark" href="{{ ItemMaker::url('member') }}">{{ $lang['member_title'] }}</a></li>
                    <li class="breadcrumb-item active">{{ $lang['member_orderdetail'] }}</li>
                  </ol>
                </div>
              </div>
              <!-- breadcrumb end -->
              <div class="container">
                <div class="row">
                  <div class="col-md-8 text-center offset-md-2 pv-20">
                    <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{ $lang['member_orderdetail'] }}</h2>
            
                  </div>
                </div>
              </div>
            </div>
            <!-- banner end -->


            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container with-sidebar">

                <div class="container">
                      <div class="row">
                        <!-- sidebar start --> 
                        
                        <!-- sidebar end -->
                        <div class="col-lg-12">
                          <div class="cms-table-wrap">  
                           <table class="table cart">
                              <thead>
                                <tr>
                                  <th>{{ $lang['car_item_title'] }} </th>
                                  <th class="hidden-xs-down">{{ $lang['car_price'] }} </th>
                                  <th class="hidden-xs-down">{{ $lang['car_quantity'] }}</th>
                                  <th class="amount hidden-xs-down">{{ $lang['car_amount'] }} </th>
                                </tr>
                              </thead>
                              <tbody>
                                @if (!empty( $data->recordlist )) <?php $no=1; ?>
                                    @foreach( $data->recordlist as $row )

                                <tr>
                                  <td class="product"><a href="products/detail.php">{{ $row['title'] }}</a></td>
                                  <td class="price">${{ $row['single_price'] }} </td>
                                  <td class="quantity">
                                    <div class="form-group">
                                      <input type="text" class="form-control" value="{{ $row['qty'] }}" disabled>
                                    </div>                      
                                  </td>
                                  <td class="amount">${{ $row['single_price'] * $row['qty'] }} </td>
                                </tr>
                                <?php
                                    $total_quantity += $row['qty'];
                                ?>
                                    @endforeach
                                @endif
                                
                                <tfoot>
                                    
                                    <tr>
                                      <td class="total-quantity" colspan="3">共 {{ $total_quantity}} 件</td>
                                      <td class="total-amount">運費 {{ $data->ship_cost > 0 ? '$'.$data->ship_cost : '免運費' }}</td>
                                    </tr>
                                  <tr>
                                    <td class="total-quantity" colspan="3">總計</td>
                                    <td class="amount">${{ $data->pay_price }}</td>
                                  </tr>
                                 
                                </tfoot>
                              </tbody>
                           </table>
                          </div>
                          <div class="space-bottom"></div>
                          <div class="cms-table-wrap">  
                               <table class="table">
                                <thead>
                                  <tr>
                                    <th colspan="2">訂購人資料 </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>姓名</td>
                                    <td class="information">{{ $data->contact_name }} </td>
                                  </tr>
                                  <tr>
                                    <td>Email</td>
                                    <td class="information">{{ $data->email }} </td>
                                  </tr>
                                  <tr>
                                    <td>聯絡電話</td>
                                    <td class="information">{{ $data->contact_tel }}</td>
                                  </tr>
                                  <tr>
                                    <td>地址</td>
                                    <td class="information">{{ $data->address }}</td>
                                  </tr>
                                </tbody>
                               </table>
                          </div>
              
                        </div>

                    </div><!-- end of row -->
                    
                </div>

            </section>
            <!-- main-container end -->
@endsection

                        