@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('script')
<script src="assets/js/front/signin.js"></script>
@endsection

@section('content')


      <div class="breadcrumb-container">
        <div class="container">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
            <li class="breadcrumb-item active">{{ $lang['member_login'] }}</li>
          </ol>
        </div>
      </div>
      <!-- breadcrumb end -->

      <!-- main-container start -->
      <!-- ================ -->
      
      <div class="main-container dark-translucent-bg member-page" style="background-image:url('assets/images/background-img-6.jpg');">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-auto maxw-95">
              <!-- main start -->
              <!-- ================ -->
              <div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                <div class="form-block p-30 light-gray-bg border-clear">
                  <h2 class="title text-center">{{ $lang['join_member'] }}</h2>
                  <hr>
                  <div class="alert alert-success hidden-xs-up" id="MessageSent">
                    {{ $lang['join_members'] }}
                  </div>
                  <div class="alert alert-danger hidden-xs-up" id="MessageNotSent">
                    糟了! 有東西出錯
                  </div>
          {!! Form::open(['url'=>'zh-tw/register','id'=>'FormRegist','class'=>'form-horizontal']) !!}
                    <div class="form-group has-feedback row">
                      <label for="inputUserName" class="col-md-3 control-label text-md-right col-form-label">{{ $lang['member_name'] }} <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
            {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>$lang['member_name']]) !!}
                        <i class="fa fa-user form-control-feedback pr-4"></i>
                      </div>
                    </div>
                    <div class="form-group has-feedback row">
                      <label for="inputEmail" class="col-md-3 control-label text-md-right col-form-label">Email <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'E-mail']) !!}
                        <i class="fa fa-envelope form-control-feedback pr-4"></i>
                      </div>
                    </div>
                    <div class="form-group has-feedback row">
                      <label for="password" class="col-md-3 control-label text-md-right col-form-label">{{ $lang['password'] }} <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        <i class="fa fa-lock form-control-feedback pr-4"></i>
                      </div>
                    </div>
                    <div class="form-group has-feedback row">
                      <label for="password_again" class="col-md-3 control-label text-md-right col-form-label">{{ $lang['check_password'] }} <span class="text-danger small">*</span></label>
                      <div class="col-md-8">
                        <input type="password" class="form-control" id="check_password" name="check_password" placeholder="Password" required>
                        <i class="fa fa-lock form-control-feedback pr-4"></i>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-md-3 col-md-8">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" required id="reg_chk" value="1"> 同意我們的 <a href="{{ ItemMaker::url('privacy') }}" target="_blank">隱私權政策</a> 和 <a href="{{ ItemMaker::url('user-terms') }}" target="_blank">使用條款</a>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-md-3 col-md-8">
                        <a href="#" id="registrBtn" class="btn btn-group btn-default btn-cta text-center text-white" onclick="return false;">{{ $lang['member_register'] }}</a>
                        <!-- <button type="submit" class="btn btn-group btn-default btn-cta text-center">註冊</button> -->
                      </div>
                    </div>
                  </form>
                  <hr>
                  <p class="text-center">
                    <span>{{ $lang['member_ismember'] }} </span>
                    <span class="pl-10 btn-group"><span><a href="{{ ItemMaker::url('sign-in') }}" class="btn btn-default text-white"> {{ $lang['login'] }}</a> </span></span>
                  </p>
                </div>
              </div>
              <!-- main end -->
            </div>
          </div>
        </div>
      </div>
@endsection