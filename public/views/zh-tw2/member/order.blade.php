@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>
@extends('page')

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('content')

            <!-- banner start -->   
            <div class="banner dark-translucent-bg" style="background-image:url('assets/images/inner-slider.png'); background-position: 50% 27%;">
              <!-- breadcrumb start -->     
              <div class="breadcrumb-container">
                <div class="container">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
                    <li class="breadcrumb-item"><a class="link-dark" href="{{ ItemMaker::url('member') }}">{{ $lang['member_title'] }}</a></li>
                    <li class="breadcrumb-item active">{{ $lang['member_order_title'] }}</li>
                  </ol>
                </div>
              </div>
              <!-- breadcrumb end -->
              <div class="container">
                <div class="row">
                  <div class="col-md-8 text-center offset-md-2 pv-20">
                    <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{ $lang['member_order_title'] }}</h2>
            
                  </div>
                </div>
              </div>
            </div>
            <!-- banner end -->


            <!-- main-container start -->
            <!-- ================ -->
            <section class="main-container with-sidebar">

                <div class="container">
                      <div class="row">
                        <!-- sidebar start --> 
                        
                        <!-- sidebar end -->
                        <div class="col-lg-12">
			              <div id="accordion-faq" class="collapse-style-1 collapse-style-4" role="tablist" aria-multiselectable="true">

							@if (!empty( $data ))
								 @foreach( $data as $key => $row)
									<?php 
										$this_qty = 0 ;
										foreach($row -> RecordList as $list){
											$this_qty += $list -> qty;
										}
									        $date = explode(" ",$row->created_at);
									        $date = explode("-", $date[0] );
									        $mon = ['','Jane ','February ',"March ","April ","May ","June ","July ","August ","September ","October ","November ","December "];
									?>
			                <div class="card">
			                <div class="card-header" role="tab" id="headingOne">
			                <h4 class="mb-0">
			                 <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse{{ $row -> id }}" class="collapsed" aria-expanded="true" aria-controls="collapseOne">
			                   <span ><i class="fa fa-list-ul pr-3"></i>#{{ $row->order_no }}</span>
			                 </a>
			                </h4>
			                </div>
			                <div id="collapse{{ $row -> id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne">
			                <div class="card-block">
			                  <p class="post-date py-2 m-0"> <i class="fa fa-calendar"></i> <span class="day">{{ $date[2] }}</span> <span class="month">{{ $mon[(int)$date[1]] . ' ' . $date[0] }}</span> </p>  
			                  <p class="py-2 m-0" > <i class=" fa fa-shopping-basket"></i> <span >共 {{ $this_qty }} 件</span> </p> 
			                  <p class="py-2 m-0" > <i class=" fa fa-dollar"></i> <span >{{ $row->pay_price }}</span> </p> 
			                  <div class="separator-2"></div>
			                  <p class="py-2 m-0 text-right" > <i class=" fa fa-external-link"></i> <span ><a href="{{ ItemMaker::url('member/order/'.$row->id) }}">{{ $lang['member_orderdetail'] }}</a></span> </p>  
			                </div>
			                </div>
			                </div>
								@endforeach
							@endif
			                </div>

			              </div>
						</div>

            </div><!-- end of row -->
                        
        </div>

        </section>
        <!-- main-container end -->
            
            
@endsection
