@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

  
@extends('page')

@section('content')

      <!-- banner start -->
      <div class="banner dark-translucent-bg" style="background-image:url('assets/images/inner-slider.png'); background-position: 50% 27%;">
        <!-- breadcrumb start -->
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
              <li class="breadcrumb-item active">{{ $title }}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end -->
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">  <strong>{{ $title }}</strong></h2>
               
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->

      <!-- breadcrumb end -->

      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

      <div class="container">
        <div class="row">

          <!-- main start -->
          <!-- ================ -->
          <div class="main col-12">
            <h3 class="title">{{ $title }}</h3>
            <div class="separator-2"></div>

          <div class="edit-wrap pv-20">
            <p>{!! $content !!}</p>
          </div><!-- end of edit-wrap -->
          <div class="w-100 pv-20 text-center">
          <a href="javascript:history.back();" class="btn btn-gray-transparent"><i class="fa fa-chevron-left pr-10"></i> {{ $lang['back'] }}</a>
        　</div>
          </div>
          <!-- main end -->

        </div>
      </div>


      </section>

@endsection
