@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
    $mon = ['','January ','February ',"March ","April ","May ","June ","July ","August ","September ","October ","November ","December "];
    $date = explode("-",$News->date);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@extends('page')

@section('content')
          <div class="breadcrumb-container">
            <div class="container">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
                <li class="breadcrumb-item"><a class="link-dark" href="{{ ItemMaker::url('news') }}">{{ $lang['news_title'] }}</a></li>
                <li class="breadcrumb-item active">{{ $News -> title }}</li>
              </ol>
            </div>
          </div>
          <!-- breadcrumb end --> 
            <section class="main-container"> 
              <div class="container">
                 
                  <div class="main">
                  <h1 class="title">{{ $News -> title}}</h1>
                  <div class="separator-2"></div>
                  <div class="editor-wrap">
                    {!! $News -> content !!}
                  </div>
                  <div class="separator-2"></div>
                   
                    <a class="back_btn btn btn-animated btn-default-transparent btn-md" href=" javascript:history.back();"><i class="icon-back"></i>{{ $lang['back'] }}</a> 
                  </div>
                 
               
              </div>
            </section>
            <!-- main-container end -->

@stop