@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@extends('page')
@section('content')
      <!-- banner start -->   
      <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'assets/images/inner-slider.png' }}'); background-position: 50% 27%;">
        <!-- breadcrumb start -->
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./')}}">{{$lang['home_bread']}}</a></li>
              <li class="breadcrumb-item active">{{$lang['news_bread']}}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end --> 
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{$lang['news_bread']}}</h2>
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->
      <section class="main-container">
              <div class="container">
                <div class="row">
                  <!-- main start -->
                  <!-- ================ -->
                  <div class="main col-12"> 
                   <div class=" row grid-space-10">
                        @if(!empty($pageData['data']))
                            @foreach ($pageData['data'] as $row)
                             <?php
                                $date = explode("-",$row->date);
                                $img = ( !empty($row->image) AND file_exists(public_path()."/". $row->image))?$row->image : "http://placehold.it/1024?text=noPic";
                                $mon = ['','January ','February ',"March ","April ","May ","June ","July ","August ","September ","October ","November ","December "];
                             ?>
                      <div class="col-md-6 col-lg-4  ">
                        <div class="image-box style-2 mb-20 shadow-2 bordered light-gray-bg text-center">
                          <div class="overlay-container">
                            <img src="{{ $img }}" alt="{{ $row -> title }}">
                            
                          </div>
                          <div class="body">
                            <div class="date">{{ $date['0'] .'.'.$date['1'].'.'.$date['2'] }}</div>
                            <h3>{{ $row -> title }}</h3>
                            <div class="separator"></div>
                            <a href="{{ ItemMaker::url('news_detail/'.$row->title) }}" class="btn btn-default btn-hvr hvr-shutter-out-horizontal margin-clear">{{ $lang['readmore']}}<i class="fa fa-arrow-right pl-10"></i></a>
                          </div>
                        </div>
                      </div>
                            @endforeach
                          @endif
                    </div>
                        <nav aria-label="Page navigation">
                              <ul class="pagination justify-content-center">
                                {!! $frontEndClass->getPageHtml($pageData) !!}
                              </ul>
                        </nav>
                  </div>
                  <!-- main end -->
                </div>
              </div>
            </section>

@stop