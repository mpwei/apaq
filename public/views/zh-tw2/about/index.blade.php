@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@extends('page')

@section('content')

      <!-- banner start -->
      <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'upload/page-slide/inner-slider.png' }}'); background-position: 50% 27%;">
        <!-- breadcrumb start -->
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
              <li class="breadcrumb-item active">{{ $title }}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end -->
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">  <strong>{{ $lang['about_title'] }}</strong></h2>
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->

      <!-- main-container start -->
      <section class="main-container">
          <div class="container">
            <div class="row">
              <div class="page-title-wrap col-12">
                <h1 class="page-title">{{ $title }}</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                
              </div>  
            </div>
          </div>

          <!-- main start -->
          <div class="main editor-wrap">
                {!! $content !!}
          </div>
          <!-- main end --> 
        
      </section>
      <!-- main-container end -->
       
@endsection
