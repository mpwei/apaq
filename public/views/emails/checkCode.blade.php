
<section class="contact">
	<div>親愛的 {{ $data->first_name }}, {{ $data->last_name }} 貴賓您好：</div>
	<div>
		感謝您撥空註冊為 購物站會員，請麻煩將「驗證碼」回填到網站上面做最後確認。
		感謝您的配合！
		驗證碼為 ：「 <span style="color: red;">{{ $data->check_code }}</span> 」
	</div>

</section>
