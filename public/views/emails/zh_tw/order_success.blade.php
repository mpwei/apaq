<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title> Shop K - 訂單完成 </title>
</head>

<body>

                <table width="100%" border="0" style=" border-collapse: collapse; border-spacing: 0; ">
                       
                    <tr>
                    	<td colspan="2" style="text-align: left; padding: 5px 0;">
                        <p style="font-family: 'TrajanProRegular', Baskerville, Palatino, 'Times New Roman', 'PingFang TC', '微軟正黑體', sans-serif;">
                        	<span style="font-size:24px;">＊此郵件是系統自動發送，請勿直接回覆此郵件！ </span><br>
                        </p>
                        <p>
                        您好，恭喜您！ <br>
                        我們已經收到您的訂購資訊，為了保護您的個人資料安全，本通知信將不顯示訂單明細。<br>
                        請至【會員中心-訂單記錄】查詢。 <br><br><br>
                        -----------------------------------------------------------------------------<br><br>
                        <a style="text-decoration:none;" href="http://shop-k.demo-tw.com/">Shop K</a><br></p>
                        </td>
                    </tr> 
                </table>
            </td>
            <td width="8%">&nbsp;</td>
          </tr>   
        </tbody>
     </table>
</body>
</html>
