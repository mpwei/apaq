<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title> - 密碼重置</title>   <!-- PASSWORD RESET -->
</head>
<body>
    <table width="100%" border="0" align="center" style="text-align: center; border: solid 1px #000; background: #fff; max-width: 580px; margin: 20px auto; line-height: 150%; font-family: 'Myriad Pro', 'PingFang TC', '微軟正黑體', Helvetica, Arial, sans-serif; font-weight: 300; border-collapse: collapse; "   >
        <tbody>
          <tr>
            <th scope="col">&nbsp;</th>
            <th scope="col" style="border-bottom:solid 1px #000; padding: 45px 0 35px 0;">
            	<img src="{{ $web }}assets/img/assets/img/logo-center.png" alt="彌斯拉"/>
            </th>
            <th scope="col">&nbsp;</th>
          </tr>
          <tr class="content">
            <td width="8%">&nbsp;</td>
            <td width="84%" style="padding: 50px 0;">
                <div style="padding: 20px 0; font-family: 'TrajanProRegular', Baskerville, Palatino, 'Times New Roman', 'PingFang TC', '微軟正黑體', sans-serif; text-transform: uppercase;">
            		<h1 style="font-size: 24px; font-weight: 300;">密碼重置</h1>
                	<span style="font-size: 12px; ">您好 {{ $first_name }} ,</span> 
             	</div>
                <p style="font-size:14px;"> 
                    如有任何其他問題，請聯繫我們的客服：<br>
                    <a style="color: inherit;" href="mailto:{{ $webmail }}" target="_blank" >{{ $webmail }}</a>
				        </p>
                <br><br>
                <div style="background: #e6e6e6; padding: 30px 0; margin: 10px 0; font-size: 36px; font-weight: 300; letter-spacing: 3px; ">新密碼 {{ $password }}</div>
            </td>
            <td width="8%">&nbsp;</td>
          </tr>   
          <tr>
            <td>&nbsp;</td>
            <td  style="border-top:solid 1px #000; padding: 28px 0;">
            	<span  style="font-size: 12px; font-family: 'TrajanProRegular', Baskerville, Palatino, 'Times New Roman', 'PingFang TC', '微軟正黑體', sans-serif; text-transform: uppercase;">© Oceanad. all rights reserved</span>
            </td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
     </table>
     
         

</body>
</html>
