
<section class="contact">
	@if($data->contact_type == 'new')
	<?php
		$message = [
			"zh-tw"=>[
				"mail_title" => '聯絡我們 : '.$data->name.' (先生/小姐)  ',
				"mail" => "E-mail",
				"content" => "Message",
				"time" => "留言時間",
			],
			"zh-cn"=>[
				"mail_title" => '聯絡我們 : '.$data->name.' (先生/小姐) ',
				"title" => "稱謂",
				"name" => "名",
				"first_name" => "姓",
				"phone" => "电话",
				"country" => "國家名稱",
				"subject" => "询问事项",
				"content" => "询问内容",
				"time" => "留言时间",
				
			],
            "en"=>[
                "mail_title" => 'Contact us : '.$data->name.' (Mr./Ms.)  ',
                "mail" => "E-mail",
                "content" => "Message",
                "time" => "Created at",
            ],
		];

		$text = $message[ $locateLang ];
	?>
	<h2> {{ $text['mail_title'] }}</h2>
	<p>{{ $text['mail'] }} ： {{$data->email}}</p>
	<p>{{ $text['content'] }} ： {{$data->message}}</p>
	<p>{{ $text['time'] }} ： {{$data->created_at}}</p>
	@endif

	@if($data->contact_type == 'reply')
		<h2> {{$data->name}} (先生 / 小姐),您好</h2>
		<p>先前您所詢問的問題 ： </p>
		<p style="margin-left:15px;" >{{$data->message}} </p>

		<p>我們的答覆是 ： </p>
		<p style="margin-left:15px;" >{{$data->answer_content}} </p>
	@endif

</section>
