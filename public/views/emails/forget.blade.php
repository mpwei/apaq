
<section class="contact">
	<div>親愛的 {{ $data->first_name }}, {{ $data->last_name }} 貴賓您好：</div>
	<div>
		您剛剛在購物站申請忘記密碼，以下為我們新產生給您的密碼，請用這組密碼登入並立即更改密碼。
		感謝您。
		新密碼：「 <span style="color: red;">{{ $new_password }}</span> 」
	</div>

</section>
