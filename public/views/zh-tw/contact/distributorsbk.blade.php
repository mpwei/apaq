@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', 'Contact Us'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">Distributors</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Distributors</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="contact-cata-menu-wrap prod-cata-menu-wrap d-flex flex-column justify-content-center align-items-center">
    <div class="d-flex container-cc wow fadeInUp justify-content-start" data-wow-delay=".1s">
        <a href="#" class="btn btn-base d-block d-md-none btn-submenu">類別 <span class="arrow-cc"></span></a>
    </div>
    <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
        <div class="contact-cata-menu-list prod-cata-menu-list submenu-list d-block d-md-flex flex-md-fill">
            <a href="{{ItemMaker::url('contact/distributors')}}#prod-cata01" class="d-block d-md-flex flex-fill">A~H</a>
            <a href="{{ItemMaker::url('contact/distributors')}}#prod-cata02" class="d-block d-md-flex flex-fill">I~Q</a>
            <a href="{{ItemMaker::url('contact/distributors')}}#prod-cata03" class="d-block d-md-flex flex-fill">R~Z</a>
        </div>
    </div>
</div>
<div class="inner-main">
    <section class="main-contact main-contact-cc">
        <div class="contact-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Bulgaria.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Bulgaria-Microdis Electronics EOOD</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Profesor Georgi Zlatarski street 13 1700
                            Sofia, Bulgaria</li>
                        <li><i class="fas fa-phone"></i>Tel:+359 2 4211124</li>
                        <li><i class="fas fa-fax"></i>Fax:+359 2 4211123</li>
                        <li><i class="fas fa-envelope"></i>Email: Bulgaria@microdis.net</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://microdis.net/contact.html">http://microdis.net/contact.html</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Croatia.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Croatia-Microdis Electronics GmbH</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Rheinauer Strasse 1 68766 Hockenheim, Germany</li>
                        <li><i class="fas fa-phone"></i>Tel:+49 6205 28094-0</li>
                        <li><i class="fas fa-fax"></i>Fax:+49 6205 28094-27</li>
                        <li><i class="fas fa-envelope"></i>Email: microdis.hr@microdis.net</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://microdis.net/contact.html">http://microdis.net/contact.html</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Czech-Republic.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Czech Republic-Microdis Electronics s.r.o</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>K Nemocnici 103 251 62 Tehovec, Czech Rep.</li>
                        <li><i class="fas fa-phone"></i>Tel:+420 323 661780</li>
                        <li><i class="fas fa-fax"></i>Fax:+420 323 661838</li>
                        <li><i class="fas fa-envelope"></i>Email: Czech@microdis.net</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Czech-Republic.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Czech Republic-Microdis Electronics s.r.o</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Čapkova 22 678 01 Blansko, Czech Rep</li>
                        <li><i class="fas fa-phone"></i>Tel: +420 516 414561</li>
                        <li><i class="fas fa-fax"></i>Fax:+420 516 411138</li>
                        <li><i class="fas fa-envelope"></i>Email: microdis.hr@microdis.net</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://microdis.net/contact.html">http://microdis.net/contact.html</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Egypt.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Egypt-EGYelectronics</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>58, Section Six-Zahaa Maadi-App.33 P.OBox:
                            114321, Cairo, Egypt</li>
                        <li><i class="fas fa-phone"></i>Tel: +2(02) 29705574,+2(012) 11142487</li>
                        <li><i class="fas fa-fax"></i>Fax:+2(02) 27515108</li>
                        <li><i class="fas fa-phone"></i>Mobile:+2 (010) 68828820</li>
                        <li><i class="fas fa-envelope"></i>Email: ramy.raafat@egyelectronics.com</li>
                        <li><i class="fas fa-globe-asia"></i><a href="www.egyelectronics.com">www.egyelectronics.com</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Egypt.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Estonia-SIA "Microdis Electronics" Latvia</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Maskavas iela 322b 1063 Riga, Latvia</li>
                        <li><i class="fas fa-phone"></i>Tel: +3716 7187170</li>
                        <li><i class="fas fa-fax"></i>Fax:+3716 7187171</li>
                        <li><i class="fas fa-envelope"></i>Email: Latvia@microdis.net</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://microdis.net/contact.html">http://microdis.net/contact.html</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Germany.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Germany-Chip 1 Exchange GmbH & Co. KG</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Martin-Behaim-Straße 10-21 63263 Neu-Isenburg
                            Germany</li>
                        <li><i class="fas fa-phone"></i>Tel: +49-(0) 6102-8169-0</li>
                        <li><i class="fas fa-fax"></i>Fax: +49-(0) 6102-8169-105</li>
                        <li><i class="fas fa-envelope"></i>Email: info@de.chip-1.com</li>
                        <li><i class="fas fa-globe-asia"></i><a href="www.chip-1.com">www.chip-1.com</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Germany.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Germany-WDI AG</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Industriestrasse 25a Industriezentrum 22880
                            Wedel (Holstein) Germany</li>
                        <li><i class="fas fa-phone"></i>Tel: +49 4103 1800-0</li>
                        <li><i class="fas fa-fax"></i>Fax: +49 4103 1800-200</li>
                        <li><i class="fas fa-envelope"></i>Email: fladiges@wdi.ag</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://www.wdi.ag">http://www.wdi.ag</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center pl-3 pr-3 mb-5 wow fadeInUp">
                    <div class="row row-cc border-cc">
                        <div class="col-12 col-xl-4">
                            <img src="upload/contact/Hungary.jpg" alt="" class="icon-flags">
                        </div>
                        <div class="col-12 col-xl-8 d-flex align-items-xl-end">
                            <p>Hungary-Microdis Electronics Kft</p>
                        </div>
                    </div>
                    <ul class="text-left">
                        <li><i class="fas fa-map-marker-alt"></i>Bécsi u. 100. 1. Emelet 1034 Budapest, Hungary</li>
                        <li><i class="fas fa-phone"></i>Tel: +36 1 236 0353</li>
                        <li><i class="fas fa-fax"></i>Fax: +36 1 236 0355</li>
                        <li><i class="fas fa-envelope"></i>Email: Hungary@microdis.net</li>
                        <li><i class="fas fa-globe-asia"></i><a href="http://microdis.net/contact.html">http://microdis.net/contact.html</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </section>
</div>

@endsection



@section('script')
<script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="assets/js/particlesR.js"></script>
<script src="assets/js/pages.js"></script>
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
    
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(24.7069993, 120.9194525), // New York

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]}]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.7069993, 120.9194525),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script> -->
    <script src="assets/js/front/contactUs.js"></script>
@endsection
