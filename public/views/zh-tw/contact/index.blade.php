@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
?>

@section('title', 'Contact Us'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')

<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn04.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">聯絡我們</p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">聯絡我們</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <section class="main-contact">
        <div class="contact-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 col-md-6 text-center mb-5 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li class="d-flex align-items-start">
                            <p><i class="fas fa-globe-asia"></i>台灣總部</p>
                        </li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>地址: 竹南科學園區苗栗縣竹南鎮科東三路2、6號4樓</li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>電話: +886-37-777588</li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i>傳真: +886-37-777566</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center mb-5 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li class="d-flex align-items-start">
                            <p><i class="fas fa-globe-asia"></i>無錫工廠</p>
                        </li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>地址: 214100無錫市錫山經濟開發區聯福路1201<br>（聯福路與安泰一路交叉口）</li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>電話: +86-510-81025298</li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i>傳真: +86-510-81025268</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li class="d-flex align-items-start">
                            <p><i class="fas fa-globe-asia"></i>中華區－台北辦公室</p>
                        </li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>地址: 104台北市中山區敬業二路69巷57號6樓之5</li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>電話: +886-2-77288510</li>
                    </ul>
                </div>
{{--                <div class="col-12 col-md-6 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">--}}
{{--                    <ul class="text-left">--}}
{{--                        <li class="d-flex align-items-start">--}}
{{--                            <p><i class="fas fa-globe-asia"></i>深圳辦公室</p>--}}
{{--                        </li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>地址: 518057 廣東省深圳市南山區科技園中區科苑路15號<br>（科興科學園A棟2單元8樓802室）</li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>電話: +86-510-81025298</li>--}}
{{--                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i>傳真: +86-510-81025268</li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
                @if(0)
                <div class="col-12 col-md-6 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li class="d-flex align-items-start">
                            <p><i class="fas fa-globe-asia"></i>歐洲區－歐洲總部（英國）</p>
                        </li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>Address：76 Discovery Dock West 2 South Quay
                            Square London E14 9RT United Kingdom</li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>Phone：+44 (0)20 8776 3535</li>
                        <li class="d-flex align-items-start"><i class="fas fa-envelope"></i>E-mail：sales@apaq.eu</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">
                    <ul class="text-left">
                        <li class="d-flex align-items-start">
                            <p><i class="fas fa-globe-asia"></i>法國辦公室</p>
                        </li>
                        <li class="d-flex align-items-start"><i class="fas fa-map-marker-alt"></i>Liaison：Ms Kay Chen</li>
                        <li class="d-flex align-items-start"><i class="fas fa-phone"></i>Liaison：Ms Kay Chen8</li>
                        <li class="d-flex align-items-start"><i class="fas fa-fax"></i>E-mail：kay.chen@apaq.eu</li>
                    </ul>
                </div>
                @endif
            </div>
        </div>

    </section>
    {!! Form::open(['class'=>'margin-clear','id'=>"active", 'name' => 'active', "url"=> ItemMaker::url('contact/save')
    ]) !!}
    <section>
        <div class="contact-cata-wrap d-flex justify-content-center wow fadeInUp">
            <div class="d-flex row-cc row no-gutters">
                <div class="col-12 text-center mt-5 mb-5 pl-3 pr-3 wow fadeInUp">
                    <p class="contact-title">如有任何問題，歡迎填寫以下聯絡表單</p>
                </div>

                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[company]" id="comapny" type="text" class="form-control" placeholder="公司*" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[department]" id="department" type="text" class="form-control" placeholder="部門*"
                            value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[name]" id="name" type="text" class="form-control" placeholder="聯絡人姓名*" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[country]" id="country" type="text" class="form-control" placeholder="國家" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[phone]" id="phone" type="text" class="form-control" placeholder="電話" value="">
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 col-md-4">
                    <div class="contact-form-gp">
                        <input name="Contact[email]" id="email" type="text" class="form-control" placeholder="Email*" value="">
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-6">
                  <div class="contact-form-gp">
                      <input name="Contact[address]" type="text" class="form-control" placeholder="Address" value="">
                  </div>
              </div>
              <div class="col-12 col-sm-12">
                  <div class="contact-form-gp contact-form-gp-cc">
                      <div class="title">Inquiry type*</div>
                      <div class="md-radio md-radio-inline">
                          <input id="category1" type="radio" name="Contact[inquiry_type]" checked="" value="1">
                          <label for="category1">General</label>
                      </div>
                      <div class="md-radio md-radio-inline">
                          <input id="category2" type="radio" name="Contact[inquiry_type]" value="2">
                          <label for="category2">Product</label>
                      </div>
                  </div>
              </div> -->
                <div class="mb-2 col-12 col-sm-6 contact_product">
                    <div class="contact-form-gp select">
                        <select class="form-control" name="Contact[category]">
                            <option>產業類別</option>
                            @if($otherSelectOptions['category'][$locateLang])
                            @foreach($otherSelectOptions['category'][$locateLang] as $c)
                            <option value="{{$c['id']}}">{{$c['title']}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="mb-2 col-12 col-sm-6 contact_product">
                    <div class="contact-form-gp select">
                        <select class="form-control" name="Contact[inquiry_type]">
                            <option>洽詢項目</option>
                            @if($otherSelectOptions['type'][$locateLang])
                            @foreach($otherSelectOptions['type'][$locateLang] as $c)
                            <option value="{{$c['id']}}">{{$c['title']}}</option>
                            @endforeach
                            @endif
                            <!-- <option value="10">Business Inquiry</option>
                          <option value="11">Professional Consultant For Products</option>
                          <option value="12">Comments and feedback</option>
                          {{-- <option value="13">Other</option> --}} -->
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="contact-form-gp">
                        <textarea class="form-control" rows="10" id="comment" name="Contact[message]" placeholder="訊息:"></textarea>
                    </div>
                </div>
                <div class="col-12 col-sm-6 captcha">
                    <div id="changeCap" class="changeCap contact-form-gp">
                        {!! Captcha::img() !!}
                        <input type="text" placeholder="Captcha" name="captcha">
                    </div>
                </div>
                <div class="text-right col-12 col-sm-6">
                    <div class="contact-form-gp">
                        {{-- <a class="contactBtn">送出</a> --}}
                        <input type="submit" value="送出" class="btn contactBtn">
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}
</div>
<div id="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d28996.63107481984!2d120.919453!3d24.706999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6b50d1d682129a70!2z6Yi66YKm56eR5oqA!5e0!3m2!1szh-TW!2stw!4v1541640966369"
        width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

@endsection



@section('script')
<script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="assets/js/particlesR.js"></script>
<script src="assets/js/pages.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(24.7069993, 120.9194525), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.7069993, 120.9194525),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script> -->
<script src="assets/js/front/contactUs.js?12"></script>
@endsection
