@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 

    $uri = $_SERVER["REQUEST_URI"];
    $lang = $frontEndClass -> getlang($locateLang);
    $product_menu = $frontEndClass -> getProduct();
    
?>
{{-- <pre>{{print_r($Theme)}}</pre> --}}
@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/in-bn01.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">產品介紹</p>
          <p class="bn-description">主要生產超小型、低阻抗、耐高溫、長壽命導電性高分子<br>捲繞型固態電容, 晶片型電容</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首頁</a></li>
                  <li class="breadcrumb-item active" aria-current="page">產品介紹</li>
              </ol>
          </nav>
      </div>
  </div>
  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="prod-cata-menu-wrap d-flex flex-column justify-content-center align-items-center">
  <div class="d-flex container-cc wow fadeInUp justify-content-start" data-wow-delay=".1s">
      <a href="#" class="btn btn-base d-block d-md-none btn-submenu">Industry Category<span class="arrow-cc"></span></a>
  </div>
  <div class="d-flex row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
      <div class="prod-cata-menu-list submenu-list d-block d-md-flex flex-md-fill">
          @if($Theme)
            @foreach($Theme as $row)
              <a href="{{ItemMaker::url('product')}}#prod-cata0{{$row['id']}}" class="d-block d-md-flex flex-fill">{{$row['title']}}</a>
            @endforeach
          @endif
      </div>
  </div>
</div>
<div class="inner-main">
  <section class="prod-cata-sect">
      @if($Theme)
        @foreach($Theme as $key=>$row)
          <?php
            $picPos = ['left-pic','right-pic'];
            $blcPos = ['justify-content-end','justify-content-start'];
            $count = 0;
            $img  = (!empty($row['image']) and file_exists(public_path() . "/" . $row['image'])) ? $row['image'] : "assets/images/1024.png";
          ?>
          <div id="prod-cata0{{$row['id']}}" class="prod-cata-wrap d-flex justify-content-center wow fadeInUp">
              <div class="d-flex row-cc {{$blcPos[$key%2]}} row no-gutters">
                  <div class="col-12 col-md-8 col-lg-6">
                      <p class="prod-title">{{$row['title']}}</p>
                      @if($row['category'])
                        @foreach($row['category'] as $category)
                          <div class="prod-type">{{$category['title']}}</div>
                          <div class="prod-list">
                              <div class="row">
                                @if($category['item'])
                                  @foreach($category['item'] as $item)
                                    <div class="col-6 col-sm-4 prod-item">
                                        <div class="title">
                                            <a href="{{$item['web_link']}}">{{$item['title']}}</a>
                                        </div>
                                        <div class="prod-item-info">
                                          <?php 
                                            $spec = ($item['list_spec'])? explode("#", $item['list_spec']) : [];  
                                          ?>
                                          @if($spec)
                                            <ul>
                                              @foreach($spec as $a)
                                                <li>{{$a}}</li>
                                              @endforeach
                                            </ul>
                                          @endif
                                        </div>
                                        <a href="{{$item['web_link']}}" class="btn-detail">詳細內容 <span class="fas fa-caret-down"></span></a>
                                    </div>
                                  @endforeach
                                @endif
                              </div>
                          </div>
                        @endforeach
                      @endif
                  </div>
              </div>
              <div class="prod-cata-pic {{$picPos[$key%2]}}">
                  <div class="prod-cata-pic-bg">
                      <img src="{{$img}}" alt="">
                  </div>
              </div>
          </div>
        @endforeach
      @endif

      @include($locateLang.'.includes.aside')
      {{-- <div id="prod-cata02" class="prod-cata-wrap d-flex justify-content-center wow fadeInUp">
          <div class="d-flex row-cc justify-content-start row no-gutters">
              <div class="col-12 col-md-8 col-lg-6">
                  <p class="prod-title">Conductive Polymer Solid Electrolytic Chip Capacitors</p>
                  <div class="prod-type">Cap</div>
                  <div class="prod-list">
                      <div class="row">
                          <div class="col-6 col-sm-4 prod-item">
                              <div class="title">
                                  <a href="product-detail.php"><b>AREA</b> series</a>
                              </div>
                              <div class="prod-item-info">
                                  <ul>
                                      <li>105°C 2000H</li>
                                      <li>2.5~25V</li>
                                      <li>High Reliability</li>
                                  </ul>
                              </div>
                              <a href="product-detail.php" class="btn-detail">Details<span class="fas fa-caret-down"></span></a>
                          </div>
                          <div class="col-6 col-sm-4 prod-item">
                              <div class="title">
                                  <a href="product-detail.php"><b>AREC</b> series</a>
                              </div>
                              <div class="prod-item-info">
                                  <ul>
                                      <li>105°C 2000H</li>
                                      <li>2.5~16V</li>
                                      <li>High Capacitance</li>
                                  </ul>
                              </div>
                              <a href="product-detail.php" class="btn-detail">Details<span class="fas fa-caret-down"></span></a>
                          </div>
                          <div class="col-6 col-sm-4 prod-item">
                              <div class="title">
                                  <a href="product-detail.php"><b>AR5K</b> series</a>
                              </div>
                              <div class="prod-item-info">
                                  <ul>
                                      <li>105°C 5000H</li>
                                      <li>2.5~25V</li>
                                      <li>Long Life</li>
                                  </ul>
                              </div>
                              <a href="product-detail.php" class="btn-detail">Details<span class="fas fa-caret-down"></span></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="prod-cata-pic right-pic">
              <div class="prod-cata-pic-bg">
                  <img src="upload/product/prod-cate02.png" alt="">
              </div>
          </div>
      </div>

      <div id="prod-cata03" class="prod-cata-wrap d-flex justify-content-center wow fadeInUp">
          <div class="d-flex row-cc justify-content-end row no-gutters">
              <div class="col-12 col-md-8 col-lg-6">
                  <p class="prod-title">Conductive Polymer Hybrid Aluminum Electrolytic Capacitors</p>

                  <div class="prod-type">Radial Type</div>
                  <div class="prod-list">
                      <div class="row">
                          <div class="col-6 col-sm-4 prod-item">
                              <div class="title">
                                  <a href="product-detail.php"><b>AREA</b> series</a>
                              </div>
                              <div class="prod-item-info">
                                  <ul>
                                      <li>105°C 2000H</li>
                                      <li>2.5~25V</li>
                                      <li>High Reliability</li>
                                  </ul>
                              </div>
                              <a href="product-detail.php" class="btn-detail">Details<span class="fas fa-caret-down"></span></a>
                          </div>
                      </div>
                  </div>
                  <div class="prod-type">Radial Type</div>
                  <div class="prod-list">
                      <div class="row">
                          <div class="col-6 col-sm-4 prod-item">
                              <div class="title">
                                  <a href="product-detail.php"><b>AREA</b> series</a>
                              </div>
                              <div class="prod-item-info">
                                  <ul>
                                      <li>105°C 2000H</li>
                                      <li>2.5~25V</li>
                                      <li>High Reliability</li>
                                  </ul>
                              </div>
                              <a href="product-detail.php" class="btn-detail">Details<span class="fas fa-caret-down"></span></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="prod-cata-pic left-pic">
              <div class="prod-cata-pic-bg">
                  <img src="upload/product/prod-cate03.png" alt="">
              </div>
          </div>
      </div> --}}
  </section>
</div>
@stop