@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $uri = $_SERVER["REQUEST_URI"];
    $lang = $frontEndClass -> getlang($locateLang);
    $product_menu = $frontEndClass -> getProduct();
    
?>

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection
@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@extends('page')
@section('content')


      <!-- banner start -->   
      <div class="banner dark-translucent-bg" style="background-image:url('{{ !empty($pagebanner -> image) ? $pagebanner -> image : 'assets/images/inner-slider.png' }}'); background-position: 50% 27%;">
        <!-- breadcrumb start -->  
        <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{$lang['home_bread']}}</a></li>
              <li class="breadcrumb-item active">{{$lang['product_bread']}}</li>
            </ol>
          </div>
        </div>
        <!-- breadcrumb end -->
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center offset-md-2 pv-20">
              <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">{{$lang['product_bread']}}</h2>
 
            </div>
          </div>
        </div>

      </div>
      <!-- banner end -->

      <!-- main-container start -->   
      <section class="main-container">

        <div class="container">
          <div class="row">
            <!-- sidebar start -->
            @include($locateLang.'.product.menu')
            <!-- sidebar end -->
            <div class="col-lg-8  offset-xl-1">
                <div class="page-title-wrap mb-20">
                  <h1 class="page-title">{{$lang['product_bread']}}</h1>
                  <div class="separator-2"></div>
                  <!-- page-title end -->
                </div>   
                <!-- main start -->         
                <div class="main">
                   <div class=" row grid-space-10">
                           
                              @if(!empty($pageData['data']))
                                @foreach($pageData['data'] as $key => $row)
                                 <?php
                                    $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/1024?text=noPic";
                                 ?>
                            <div class=" col-md-4  ">
                              <div class="image-box shadow-2 text-center mb-20">
                                <div class="overlay-container">
                                  <img src="{{ $img }}" alt="">
                                  <div class="overlay-top">
                                    <div class="text">
                                      <h3><a href="{{ $row['web_link'] }}">{{ $row['title'] }}</a></h3>
                                    </div>
                                  </div>
                                  <div class="overlay-bottom">
                                    <div class="links">
                                      <a href="{{ $row['web_link'] }}" class="btn btn-gray-transparent btn-animated btn-sm">{{$lang['detail']}}<i class="pl-10 fa fa-arrow-right"></i></a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                                @endforeach
                              @endif
                          </div>              
                </div>
                <!-- main end -->
            </div>

          </div>
        </div>
      </section>
      <!-- main-container end -->



@stop