@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $lang = $frontEndClass -> getlang($locateLang);
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
<link href="assets/css/custom/custom-in.css" rel="stylesheet">
@endsection

@section('script')
    <script src="assets/js/front/shipping.js"></script>
@endsection


@extends('page')
@section('content')


      <!-- breadcrumb start -->     
     
      <div class="breadcrumb-container">
          <div class="container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{ ItemMaker::url('./') }}">{{ $lang['home_title'] }}</a></li>
              <li class="breadcrumb-item active">{{ $lang['productinquiry_title']}}</li>
            </ol>
          </div>
        </div>
      <!-- breadcrumb end -->

      <section class="main-container"> 
            <div class="container">
              <div class="row">

                <!-- main start -->               
                <div class="main col-12">

                  <!-- page-title start -->                 
                  <h1 class="page-title">{{ $lang['productinquiry_title']}}</h1>
                  <div class="separator-2"></div>
                  <!-- page-title end -->
                    
                   {!! Form::open(['class'=>'','id'=>"shipping", 'name' => 'shipping', "url"=> ItemMaker::url('updFromShoppingBag') ]) !!}
                   <input type="hidden" value="1" name="key">
                  <div class="table-responsive">
                    <table class="table cart table-hover table-colored">
                      <thead>
                        <tr>
                          <th>{{ $lang['car_item_title']}} </th>
                          <th>{{ $lang['car_quantity']}}</th>
                          <th>{{ $lang['car_delete']}} </th>
                        </tr>
                      </thead>
                      <tbody>
                        @if (!empty( Session::get('Spec')['list'] ))
                            @foreach( Session::get('Spec')['list'] as $key => $row)
                        <?php
                                $img = ( !empty($row['image']) AND file_exists(public_path()."/". $row['image']))?$row['image'] : "http://placehold.it/500?text=noPic";
                            ?>
                        <tr class="remove-data">
                          <td class="product"><a href="{{ ItemMaker::url('productdetail/'.str_replace('/','-',$row['title'])) }}">{{ $row['title'] }}</a></td>
                          <td class="quantity">
                            <div class="form-group">
                              <input type="text" class="form-control" value="{{ $row['quantity'] }}" name="{{ $key }}" class="quantity-input">
                            </div>
                          </td>
                          <td class="remove"><a class="btn btn-remove btn-sm btn-gray remove_spec_list" type="{{ $row['product_no'] }}">{{ $lang['car_delete']}}</a></td>
                        </tr>
                            @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                  <div class="text-right">
                    <a href="" type="submit" class="btn btn-group btn-default">{{ $lang['car_update_car']}}</a>
                    <a href="{{ ItemMaker::url('billing') }}" class="btn btn-group btn-default">{{ $lang['go_billing'] }}</a>
                  </div>
                  </form>
                  <div class="w-100 pv-20 text-center">
                    <a href="javascript:history.back();" class="btn btn-gray-transparent"><i class="fa fa-chevron-left pr-10"></i> {{ $lang['back'] }}</a>
                  </div>

                </div>
                <!-- main end -->

              </div>
            </div>


            </section>

@endsection
