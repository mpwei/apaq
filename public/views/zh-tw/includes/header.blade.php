@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();
$qty = 0;
$total = 0;
?>
<header class="main-header d-flex justify-content-center">
    <div class="container-cc d-inline-flex justify-content-between">
        <div class="d-flex align-items-center">
            <!-- <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"> -->
            <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="logo-pic"><img src="assets/images/logo/logo.png" alt="APAQ 鈺邦科技"></span></a>
            <span class="logo-word d-flex flex-column">
                <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="company_name">鈺邦科技股份有限公司</span></a>
                <a href="{{ItemMaker::url('/')}}" class="header-logo d-flex align-items-center"><span class="company_name_en">TECHNOLOGY CO., LTD.</span></a>
                <a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blank"><span>
                        <p class="job104">104徵才資訊</p>
                    </span></a>
            </span>

        </div>
        <div class="d-flex align-items-center">
            <div class="main-menu01 d-none d-lg-none d-xl-flex">
                <ul>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <span>{{$lang['product_title']}}</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <!-- 目前所在頁面，current  -->
                        <!-- <a class="current" href="../products/product-list.php">產品介紹</a> -->
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('product')}}">固態產品</a></li>
{{--                            <li class="main__dropmenu__item"><a href="http://www.sz-gather.com/products.html" target="_blank">液態產品</a></li>--}}
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('application')}}">應用領域</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('news')}}">{{$lang['news_title']}}</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/最新消息')}}">最新消息</a></li>
                            <li class="main__dropmenu__item"><a href="{{ItemMaker::url('news/展覽資訊')}}">展覽資訊</a></li>
                        </ul>
                    </li>
                    <li class="main__dropmenu__wrap">
                        <span class="btn__main__dropmenu">
                            <a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">投資人專區</a>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="main__dropmenu">
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">財務資訊</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">每月營收</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">合併財務報表</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">股東會資訊</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">議事手冊</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">開會通知</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">年報</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Stock')}}">股務資訊</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <a class="dropmenu__subitem" href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">利害關係人</a>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">公司治理</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Organization-Team')}}">組織架構及經營團隊</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Board-Director')}}">董事會及功能委員會</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">公司規章</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Internal-audit')}}">內部稽核</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">主要股東名單</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">公司治理運作情形</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="main__dropmenu__item">
                                <div class="wrap__dropmenu__subitem">
                                    <a class="dropmenu__subitem">企業社會責任</a>
                                    <i class="fas fa-chevron-right"></i>
                                    <ul class="sub__dropmenu">
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Social-Respons')}}">企業社會責任政策</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">企業社會責任實施成效</a></li>
                                        <li class="sub__dropmenu__item"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">員工福利、工作環境與人身安全保護</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="main-h-link d-inline-flex align-items-center" id="btn-main-menu" href="#main-menu">
                <div class="collapse-button">
                    <span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </div>
                <div class="h-link-text">Menu</div>
            </div>
            <div class="main-h-link d-none d-lg-none d-xl-flex">
                <div class="main-menu02 d-flex align-items-center">
                    <div class="search-wrap">
                        <a href="#" id="btn-search-open"><span class="fas fa-search"></span></a>
                        {!! Form::open(['class'=>"search-input",'id'=>"search", 'name' => 'search', "url"=> ItemMaker::url('search') ]) !!}
                        <div class="search-form d-flex align-items-center">
                            <input type="text" class="form-control" placeholder="產品關鍵字..." name="key">
                            <button id="search-button" class="fas fa-search"></button>
                        </div>
                        {!! Form::close() !!}
                        <div class="search-form d-flex align-items-center">
                        </div>
                    </div>
                    <div class="lang-wrap">
                        <span class="btn-lang">
                            <span>語系</span>
                            <i class="fas fa-caret-down"></i>
                        </span>
                        <ul class="lang-list">
                            <li><a href="en">Eng</a></li>
                            <li class="active"><a href="zh-tw">繁中</a></li>
                            <li><a href="zh-cn">簡中</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of container-cc -->
    <div id="main-menu" class="main-menu">
        <div id="btn-close-mainmenu">
            <div class="collapse-button">
                <div id="cross">
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="h-link-text">Close</div>
        </div>

        <div class="menu-list">
            <div class="m-search-wrap d-flex d-xl-none align-items-start">
                <input type="text" class="form-control" placeholder="產品關鍵字..." id="mobilesearchkey">
                <a class="btn btn-cc btn-search mobile-search-btn"><span class="fas fa-search"></span></a>
            </div>
            <div class="m-lang-wrap lang-wrap d-flex flex-column d-xl-none">
                <span id="m-btn-lang">
                    <span>語系</span>
                    <i class="fas fa-caret-down"></i>
                </span>
                <ul class="lang-list d-inline-flex justify-content-start">
                    <li><a href="en">Eng</a></li>
                    <li class="active"><a href="zh-tw">繁中</a></li>
                    <li><a href="zh-cn">簡中</a></li>
                </ul>
            </div>
            <ul>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">{{$lang['product_title']}}<i class=" fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('product')}}">固態產品</a></li>
{{--                        <li class="my-1 px-2 py-2"><a href="http://www.sz-gather.com/products.html" target="_blank">液態產品</a></li>--}}
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('application')}}">應用領域</a></li>
                    </ul>
                </li>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">{{$lang['news_title']}}<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/最新消息')}}">最新消息</a></li>
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('news/展覽資訊')}}">展覽資訊</a></li>
                    </ul>
                </li>
                <li class="d-block d-xl-none">
                    <a class="btn-main-submenu">投資人專區<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">財務資訊<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Monthly-Revenue')}}">每月營收</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Financial-Report?page=1')}}">合併財務報表</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">股東會資訊<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Procedure-Manual?page=1')}}">議事手冊</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Notice-of-meeting?page=1')}}">開會通知</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Annual-Report?page=1')}}">年報</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stock')}}">股務資訊</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">利害關係人<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Stakeholder-Engagement')}}">利害關係人</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">公司治理<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Organization-Team')}}">組織架構及經營團隊</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Board-Director')}}">董事會及功能委員會</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Company-rule?page=1')}}">公司規章</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Internal-audit')}}">內部稽核</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Main-Shareholder')}}">主要股東名單</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Corporate-Governance-Implementation-Status')}}">公司治理運作情形</a></li>
                            </ul>
                        </li>
                        <li class="my-1 px-2 py-2">
                            <a class="btn-main-submenu">企業社會責任<i class="fas fa-caret-down pl-2"></i></a>
                            <ul class="main-submenu d-block my-2">
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Social-Respons')}}">企業社會責任政策</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Respon-Effects')}}">企業社會責任實施成效</a></li>
                                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('investor/Personal-Safety')}}">員工福利、工作環境與人身安全保護</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="btn-main-submenu">關於我們<i class="fas fa-caret-down pl-2"></i></a>
                    <ul class="main-submenu d-block my-2">
                        @if( $getTitle['about'])
                        @foreach($getTitle['about'] as $row )
                        <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/'.$row['title'])}}">{{$row['title']}}</a></li>
                        @endforeach
                        @endif
                        {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about')}}">About Us</a>
                </li> --}}
                <!-- <li class="my-1 px-2 py-2"><a href="#">{{$lang['about_title']}}</a></li> -->
                {{-- <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('about/development-and-core')}}">Development and Core</a>
                </li> --}}
                <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('certificate')}}">證書</a></li>
            </ul>
            </li>
            <li>
                <a class="btn-main-submenu">設計工具<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/simulate')}}">頻譜特性</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('design/life-time-1')}}">Life Time</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('search-filter')}}">篩選搜尋</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ItemMaker::url('contact')}}">聯絡我們</a>
            </li>
            @if(0)
            <li>
                <a class="btn-main-submenu">聯絡我們<i class="fas fa-caret-down pl-2"></i></a>
                <ul class="main-submenu d-block my-2">
                    <!--                            <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">{{$lang['contact_title']}}</a></li>-->
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('contact')}}">聯絡我們</a></li>
                    <li class="my-1 px-2 py-2"><a href="{{ItemMaker::url('distributor')}}">經銷商</a></li>
                </ul>
            </li>
            @endif
            {{-- <li><a href="{{ItemMaker::url('album')}}">相簿</a></li>
            <li><a href="{{ItemMaker::url('download')}}">檔案下載</a></li> --}}
            <li><a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=483a4326445c3f6756583a1d1d1d1d5f2443a363189j56&jobsource=joblist_a_relevance" target="_blank">徵才資訊</a></li>
            <li><a href="{{ItemMaker::url('policy')}}">隱私權政策</a></li>
            </ul>
        </div>
    </div>

    
    <div id="main-menu-close"></div>
</header>
