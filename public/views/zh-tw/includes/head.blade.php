    <base href="{{ItemMaker::url('/')}}">
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="author" content="APAQ">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="鈺邦科技股份有限公司主要生產超小型、低阻抗、耐高溫、長壽命導電性高分子捲繞型固態電容、晶片型電容,APAQ 國際化專業
固態電容製造公司">
    <meta name="keywords" content="鈺邦, 鈺邦科技股份有限公司, APAQ, 固態電容製造公司">
    <meta name="_token" content="{{ csrf_token() }}">
    @yield('share_meta')
 
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/plugins/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-free-5.3.1-web/css/all.min.css">
    <link rel="stylesheet" href="assets/plugins/animate.min.css">
    <link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    @yield('css')
    @include($locateLang.'.includes.googleanalytics')