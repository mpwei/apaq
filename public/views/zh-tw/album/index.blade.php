@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$product_title_check = 1;
$lang = $frontEndClass->getlang($locateLang);
$getTitle = $frontEndClass->getSubtitle();

?>

@section('title', 'album'.'-'.$ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
    @include($locateLang.'.includes.css-in')
@endsection

@extends('page')

@section('content')


    <div class="in-banner" style="background-image: url(upload/banner/inner/in-bn03.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">相簿管理</p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">{{ $lang['home_title'] }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ItemMaker::url('album')}}">{{ $lang['album_title'] }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>

    <section class="main-container">

        <div class="container">
            <div class="m-auto">

                <div class="page-title-wrap mb-20">
                    <h1 class="page-title"></h1>
                    <div class="separator-2"></div>

                </div>

                <section id="product" class="light-gray-bg padding-bottom-clear">

                    <div class="space-bottom">
                        <div class="row m-auto">
                            @foreach($category as $item)
                                <div id="set_width" class="image-box col-12 col-md-6 col-lg-4 p-0 shadow text-center">
                                    <div class="overlay-container col-12">
                                        <div class="img-wrap">
                                            <img src="{{!empty($item->image) ? $item->image : '../upload/invest/invest-2.jpg'}}" alt="">
                                        </div>
                                        <div class="overlay-top">

                                        </div>
                                        <div class="overlay-bottom">
                                            <div class="text">
                                                <h3 class="mb-3"><a href="{{ItemMaker::url('album/detail/'.$item->id)}}">{{$item->title}}</a></h3>
                                            </div>
                                            <div class="links">
                                                <a href="{{ItemMaker::url('album/detail/'.$item->id)}}" class="btn btn-gray-transparent btn-animated">查看更多<i class="pl-2 fa fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

@endsection




@section('script')
    <script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
    <script src="assets/js/particlesR.js"></script>
    <script src="assets/js/pages.js"></script>
    <script src="assets/js/front/contactUs.js?12"></script>
@endsection