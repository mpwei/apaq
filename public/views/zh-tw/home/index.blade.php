@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass->getlang($locateLang);
$homebanner = $frontEndClass->getBanner();
$getTitle = $frontEndClass->getSubtitle();
$historyListHtml = '';
?>

<!DOCTYPE html>
<html lang="en">

<base href="{{ItemMaker::url('/')}}">
<meta charset="utf-8">
<title>{{$ProjectShareName}}</title>
<meta name="author" content="KSONG">
<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{$ProjectShareName}}">
<meta name="keywords" content="{{$ProjectShareName}}">
<meta name="_token" content="{{ csrf_token() }}">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.ico">

<link rel="shortcut icon" href="assets/images/favicon.ico">
<link rel="stylesheet" href="assets/plugins/bootstrap-4.1.3-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/fontawesome-free-5.3.1-web/css/all.min.css">
<link rel="stylesheet" href="assets/plugins/animate.min.css">
<link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
@include($locateLang.'.includes.css')
@include($locateLang.'.includes.googleanalytics')
@include($locateLang.'.includes.consult-plugin')

<body class="front-index unprepared">
    @include($locateLang.'.includes.header')

    <div class="main">
        <div class="idx-banner-wrap d-flex justify-content-end">
            <div id="idx-banner-slider" class="owl-carousel owl-carousel1">
                <div class="idx-bn-pic"><img src="upload/banner/home/bn01.jpg" alt="APAQ TECHNOLOGY CO., LTD."></div>
                <div class="idx-bn-pic"><img src="upload/banner/home/bn01.png" alt="APAQ TECHNOLOGY CO., LTD."></div>
                <div class="idx-bn-pic"><img src="upload/banner/home/DIP-1.jpg" alt="APAQ TECHNOLOGY CO., LTD."></div>
                <div class="idx-bn-pic"><img src="upload/banner/home/DIP-2.jpg" alt="APAQ TECHNOLOGY CO., LTD."></div>
            </div>
            <div class="container-cc idx-bn-content">
                <div class="idx-bn-context">
                    <h1 class="main-slogan wow fadeInUp" data-wow-delay=".2s">
                        <p><b>晶片型</b>電容</p>
                        <p><b>導電性高分子</b>固態電容</p>
                    </h1>
                    <div class="sub-slogan wow fadeInUp" data-wow-delay=".55s">
                        <p>APAQ 國際化專業</p>
                        <p>固態電容製造公司</p>
                    </div>
                    <a href="{{ItemMaker::url('product')}}" class="bn-link btn-cc btn-cc01 wow fadeInUp" data-wow-delay=".75s">
                        <span class="btn-arrow">
                            <span class="arrow-cc-down"></span>
                        </span>
                        <span class="btn-word">產品介紹</span>
                    </a>
                </div>
            </div>
            <div id="idxbnNav" class="owl-nav"></div>
        </div>
        <!-- end of banner -->
        <section class="idx-sect">
            <div class="idx-news-wrap d-flex justify-content-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9">
                        <p class="main-title wow fadeInUp" data-wow-delay=".22s">最新消息</p>
                        <div class="news-list wow fadeInUp" data-wow-delay=".35s">
                            @if($news)
                            @foreach($news as $row)
                            <?php
$cateogry = ($row->Category) ? $row->Category->title : '';

?>
                            <div class="news-item row no-gutters">
                                <a href="{{$row->web_link}}" class="idx-news-link"></a>
                                <div class="col-4">
                                    <p>{{$cateogry}}</p>
                                </div>
                                <div class="col-8 col-sm-6">
                                    <a href="{{$row->web_link}}">{{$row->title}}</a>
                                </div>
                                <div class="col-12 col-sm-2 text-left text-sm-right">
                                    <p class="date">{{$row->date}}</p>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            {{-- <div class="news-item row no-gutters">
                            <a href="#" class="idx-news-link"></a>
                            <div class="col-4">
                                <p>Exhibition</p>
                            </div>
                            <div class="col-8 col-sm-6">
                                <a href="#">electronica 2018</a>
                            </div>
                            <div class="col-12 col-sm-2 text-left text-sm-right">
                                <p class="date">2018.08.23</p>
                            </div>
                        </div>
                        <div class="news-item row no-gutters">
                            <a href="#" class="idx-news-link"></a>
                            <div class="col-4">
                                <p>Exhibition</p>
                            </div>
                            <div class="col-8 col-sm-6">
                                <a href="#">PCIM</a>
                            </div>
                            <div class="col-12 col-sm-2 text-left text-sm-right">
                                <p class="date">2018.08.23</p>
                            </div>
                        </div>
                        <div class="news-item row no-gutters">
                            <a href="#" class="idx-news-link"></a>
                            <div class="col-4">
                                <p>News</p>
                            </div>
                            <div class="col-8 col-sm-6">
                                <a href="#">106/03/29受邀參加元大證券舉辦之法人說明會</a>
                            </div>
                            <div class="col-12 col-sm-2 text-left text-sm-right">
                                <p class="date">2018.08.23</p>
                            </div>
                        </div> --}}
                        </div>
                        <!-- end of news-list -->
                        <a href="{{ItemMaker::url('news')}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">詳細資訊</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect idx-prod-sect wow fadeInUp" data-wow-delay=".05s">
            <div class="idx-prod-wrap d-flex justify-content-center">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9 wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                        <p class="main-title">產品介紹</p>
                        <div class="idx-description">
                            <p>主要生產超小型、低阻抗、耐高溫、長壽命導電性高分子</p>
                            <p>捲繞型固態電容, 晶片型電容</p>
                        </div>
                        <div class="idx-prod-list wow fadeInUp" data-wow-delay=".75s" data-wow-duration="1.5s">
                            <div id="customNav" class="owl-nav"></div>
                            <div class="owl-carousel owl-carousel2">

                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata01')}}"><img src="/upload/product/prod-cate01.png" alt="捲繞式(引線型)"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata01')}}">捲繞式(引線型)</a>
                                </div>
                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata02')}}"><img src="/upload/product/prod-cate02.png" alt="捲繞式(貼片型)"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata02')}}">捲繞式(貼片型)</a>
                                </div>
                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata03')}}"><img src="/upload/product/prod-cate03.png" alt="晶片型"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata03')}}">晶片型</a>
                                </div>
                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata04')}}"><img src="/upload/product/Hybrid cate01.png" alt="Hybrid"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata04')}}">混合型電容</a>
                                </div>
                            </div>
                        </div>
                        <!-- end of idx-prod-list -->
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect">
            <div class="idx-application-wrap d-flex flex-column align-items-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9">
                        <p class="main-title wow fadeInUp" data-wow-delay=".25s">產品應用</p>
                        <div class="idx-description wow fadeInUp" data-wow-delay=".55s">
                            <p>鈺邦產品應用範圍極廣，</p>
                            <p>主要應用於 Automotive 汽車, Computing 計算, Industry 工業, Power Supply 電源, Smart Device 智能設備等</p>
                        </div>
                        <a href="{{ItemMaker::url('application')}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">詳細資訊</span>
                        </a>
                    </div>
                </div>
                <div class="idx-app-list-wrap">
                    <div class="idx-app-list d-block wow fadeInUp" data-wow-delay="1.15s">
                        <div>
                            <a href="{{ItemMaker::url('application')}}" class="idx-app-link"></a>
                            <img src="upload/home/application/app01.png" alt="Automotive 汽車">
                            <p>Automotive 汽車</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('application')}}" class="idx-app-link"></a>
                            <img src="upload/home/application/app02.png" alt="Computing 計算">
                            <p>Computing 計算</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('application')}}" class="idx-app-link"></a>
                            <img src="upload/home/application/app03.png" alt="Industry 工業">
                            <p>Industrial 工業</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('application')}}" class="idx-app-link"></a>
                            <img src="upload/home/application/app04.png" alt="Power Supply 電源">
                            <p>Power Supply 電源</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('application')}}" class="idx-app-link"></a>
                            <img src="upload/home/application/app05.png" alt="Smart Device 智能設備">
                            <p>Smart Device 智能設備</p>
                        </div>
                    </div>
                </div>
                <!-- end of app-list -->
                <div class="global-animate">
                    <div class="globe js-globe">
                        <img src="assets/images/home/pic-global-outer.png" alt="">
                        <!-- <ul class="globe-list js-list"></ul> -->
                        <canvas class="globe-canvas js-canvas">
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect">
            <div class="idx-about-wrap d-flex flex-column flex-lg-row justify-content-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-9 col-lg-6">
                        <p class="main-title wow fadeInUp" data-wow-delay=".35s">關於鈺邦</p>
                        <div class="idx-about-context idx-description wow fadeInUp" data-wow-delay=".55s">
                            <h3>鈺邦至 2005 年成立以來, 著重於研發技術</h3>
                            <p>網羅工研院導電性高分子固態電容設計開發的優秀技術研發人才,
                                <br>致力於成為國際化專業性的固態電容製造公司。</p>
                            <p><br>目前主要生產超小型、低阻抗、耐高溫、長壽命導電性高分子捲繞型固態電容,
                                晶片型電容。在強化技術核心的條件下同時全面引進無鉛材料，從生產製程到出貨包裝，嚴格執行綠色生產政策，與客戶一同成為地球環境保 護的尖兵，共同保護美麗的生態環境而努力!</p>
                        </div>
                        <!-- end of news-list -->
                        @if(!empty($about))
                        <a href="{{ItemMaker::url('about/'.$about)}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">詳細資訊</span>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="idx-core-wrap">
                    <img src="assets/images/home/idx-core-01.png" alt="" class="idx-core-bg wow fadeIn" data-wow-offset="30" data-wow-delay="1.5s" data-wow-duration="1.5s">
                    <img src="assets/images/home/idx-core-logo.png" alt="" class="idx-core-logo">
                    <img src="assets/images/home/idx-core-02.png" alt="" class="idx-core-bg wow flipInY" data-wow-offset="100" data-wow-delay="2s" data-wow-duration="2s">
                    <img src="assets/images/home/idx-core-03.png" alt="品質政策, 社會責任政策與承諾, 環境政策" class="idx-core-bg wow fadeIn" data-wow-offset="30" data-wow-delay="2s">
                </div>
            </div>
        </section>

        <section class="idx-sect idx-history-sect">
            <div class="idx-history-wrap d-flex justify-content-center wow fadeInUp">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-8 col-lg-6">
                        <p class="main-title">歷史沿革</p>
                        <section class="owl-horizontal-timeline">
                            <div class="timeline">
                                <div class="owl-nav" id="owl-timeline-nav"></div>
                                <div class="cc-timeline-wrap">

                                    <div class="owl-carousel owl-thumbs d-flex align-items-center" data-slider-id="1">
                                        @foreach($history as $item)
                                        <div class='owl-thumb-item'>{{$item->title}}</div>
                                        @endforeach
                                    </div>

                                    <div class="owl-carousel owl-timeline" data-slider-id="1">
                                        @foreach($history as $item)
                                        <div class="text-center" data-time="2005">
                                            <em>{{$item->title}}</em>
                                            {!! $item->content !!}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="idx-history-pic-wrap">
                <img src="assets/images/home/pic-idx-history01.png" alt="" class=" wow bounceInLeft" data-wow-offset="10" data-wow-delay=".75s" data-wow-duration="1.25s">
                <img src="assets/images/home/pic-idx-history02.png" alt="" class=" wow bounceInLeft" data-wow-offset="10" data-wow-delay="1s" data-wow-duration="2.25s">
            </div>
        </section>

        <section class="idx-sect idx-achievement-sect">
            <div class="idx-achievement-wrap d-flex justify-content-center wow fadeInUp" data-wow-delay=".05s">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-8 col-lg-6">
                        <p class="main-title">成就</p>
                        <div class="idx-achievement-list">
                            <div class="idx-achieve-item idx-achieve-item1 d-flex align-items-center wow fadeInUp" data-wow-delay=".1s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve01-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve01.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">銷售數量</p>
                                    <p class="mb-0 stat-number"><span class="stat-count timer" data-from="1737254000" data-to="2112138061" data-speed="8000" data-refresh-interval="100"></span> PCS PRODUCT SOLD
                                    </p>
                                </div>
                            </div>
                            <div class="idx-achieve-item idx-achieve-item2 d-flex align-items-center wow fadeInUp" data-wow-delay=".2s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve02-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve02.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">年營業額</p>
                                    <p class="mb-0 stat-number"><span class="stat-count" data-from="2029713000" data-to="2382434602" data-speed="8000" data-refresh-interval="100"></span> NTD OF ANNUAL
                                        TURNOVER</p>
                                </div>
                            </div>
                            <div class="idx-achieve-item d-flex align-items-center wow fadeInUp" data-wow-delay=".3s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve03-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve03.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">市佔率</p>
                                    <p class="mb-0 stat-number"><span class="stat-count2" data-from="1" data-to="20" data-speed="500" data-refresh-interval="1"></span> %
                                    </p>
                                </div>
                            </div>
                            <div class="idx-achieve-item d-flex align-items-center wow fadeInUp" data-wow-delay=".35s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve04-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve04.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">專利</p>
                                    <p class="mb-0 stat-number"><span class="stat-count2" data-from="1" data-to="268" data-speed="500" data-refresh-interval="1"></span>
                                        ITEM OF PATENT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="idx-global-map">
                <div class="map-container__map"><img src="assets/images/map.png" class="map">
                    <div class="map-dot map-dot--1 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".25s">
                    </div>
                    <div class="map-dot map-dot--2 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.5s" data-wow-delay=".6s">
                    </div>
                    <div class="map-dot map-dot--3 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.1s" data-wow-delay=".15s">
                    </div>
                    <div class="map-dot map-dot--4 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".25s" data-wow-delay=".25s">
                    </div>
                    <div class="map-dot map-dot--5 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".15s">
                    </div>
                    <div class="map-dot map-dot--6 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.1s" data-wow-delay=".2s">
                    </div>
                    <div class="map-dot map-dot--7 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".35s">
                    </div>
                </div>
            </section>

            <!-- <div class="idx-global-map">
            <img src="assets/images/map.png" alt="map">
        </div> -->
        </section>
    </div>
    <!-- end of main -->

    <div class="page-loader d-flex align-items-center justify-content-center">
        <span class="loader-logo animated fadeInUp"><img src="assets/images/logo/logo.svg" alt="" style="width: 140px"></span>
        <span class="loader-word has-text-centered pl-3 animated fadeInDown">鈺邦科技</span>
    </div>

    @include($locateLang.'.includes.footer')

    <script type="text/javascript" src="assets/plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
    <!-- <script type="text/javascript" src="assets/plugins/OwlCarousel2-Thumbs-master/dist/owl.carousel2.thumbs.min.js"></script> -->
    <script src="assets/plugins/jquery.countTo.js"></script>
    <script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
    <script src="assets/plugins/three.js"></script>
    <script src="assets/plugins/ThreeOrbitControls.js"></script>
    <script src="assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <!-- start home js -->
    <script type="text/javascript" src="assets/js/globe.js"></script>
    <script type="text/javascript" src="assets/js/home.js"></script>

</body>

</html>
