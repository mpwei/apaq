@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
    $lang = $frontEndClass -> getlang($locateLang);
    $homebanner = $frontEndClass -> getBanner();
    $getTitle = $frontEndClass -> getSubtitle();
    $historyListHtml = '';
?>

<!DOCTYPE html>
<html lang="en">

    <base href="{{ItemMaker::url('/')}}">
    <meta charset="utf-8">
    <title>{{$ProjectShareName}}</title>
    <meta name="author" content="KSONG">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{$ProjectShareName}}">
    <meta name="keywords" content="{{$ProjectShareName}}">
    <meta name="_token" content="{{ csrf_token() }}">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/plugins/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-free-5.3.1-web/css/all.min.css">
    <link rel="stylesheet" href="assets/plugins/animate.min.css">
    <link rel="stylesheet" href="assets/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    @include($locateLang.'.includes.css')
    @include($locateLang.'.includes.googleanalytics')

<body class="front-index unprepared">
    @include($locateLang.'.includes.header')

    <div class="main">
        <div class="idx-banner-wrap d-flex justify-content-end">
            <div id="idx-banner-slider" class="owl-carousel owl-carousel1">
                <div class="idx-bn-pic"><img src="upload/banner/home/idx-bn01.jpg" alt="APAQ TECHNOLOGY CO., LTD."></div>
                <div class="idx-bn-pic"><img src="upload/banner/home/idx-bn02.jpg" alt="APAQ TECHNOLOGY CO., LTD."></div>
            </div>
            <div class="container-cc idx-bn-content">
                <div class="idx-bn-context">
                    <h1 class="main-slogan wow fadeInUp" data-wow-delay=".2s">
                        <p>Global and Professional</p>
                        <p>Polymer Capacitor Manufacturer</p>
                    </h1>
                    <div class="sub-slogan wow fadeInUp" data-wow-delay=".55s">
                        <p>APAQ TECHNOLOGY CO., LTD.</p>
                    </div>
                    <a href="{{ItemMaker::url('product')}}" class="bn-link btn-cc btn-cc01 wow fadeInUp" data-wow-delay=".75s">
                        <span class="btn-arrow">
                            <span class="arrow-cc-down"></span>
                        </span>
                        <span class="btn-word">our products</span>
                    </a>
                </div>
            </div>
            <div id="idxbnNav" class="owl-nav"></div>
        </div>
        <!-- end of banner -->
        <section class="idx-sect">
            <div class="idx-news-wrap d-flex justify-content-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9">
                        <p class="main-title wow fadeInUp" data-wow-delay=".22s">News</p>
                        <div class="news-list wow fadeInUp" data-wow-delay=".35s">
                            @if($news)
                                @foreach($news as $row)
                                    <?php
                                        $cateogry = ($row->Category)? $row->Category->title: '';
                                    ?>
                                    <div class="news-item row no-gutters">
                                        <a href="{{$row->web_link}}" class="idx-news-link"></a>
                                        <div class="col-4">
                                            <p>{{$cateogry}}</p>
                                        </div>
                                        <div class="col-8 col-sm-6">
                                            <a href="{{$row->web_link}}">{{$row->title}}</a>
                                        </div>
                                        <div class="col-12 col-sm-2 text-left text-sm-right">
                                            <p class="date">{{$row->date}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            {{-- <div class="news-item row no-gutters">
                                <a href="#" class="idx-news-link"></a>
                                <div class="col-4">
                                    <p>Exhibition</p>
                                </div>
                                <div class="col-8 col-sm-6">
                                    <a href="#">electronica 2018</a>
                                </div>
                                <div class="col-12 col-sm-2 text-left text-sm-right">
                                    <p class="date">2018.08.23</p>
                                </div>
                            </div>
                            <div class="news-item row no-gutters">
                                <a href="#" class="idx-news-link"></a>
                                <div class="col-4">
                                    <p>Exhibition</p>
                                </div>
                                <div class="col-8 col-sm-6">
                                    <a href="#">PCIM</a>
                                </div>
                                <div class="col-12 col-sm-2 text-left text-sm-right">
                                    <p class="date">2018.08.23</p>
                                </div>
                            </div>
                            <div class="news-item row no-gutters">
                                <a href="#" class="idx-news-link"></a>
                                <div class="col-4">
                                    <p>News</p>
                                </div>
                                <div class="col-8 col-sm-6">
                                    <a href="#">106/03/29受邀參加元大證券舉辦之法人說明會</a>
                                </div>
                                <div class="col-12 col-sm-2 text-left text-sm-right">
                                    <p class="date">2018.08.23</p>
                                </div>
                            </div> --}}
                        </div>
                        <!-- end of news-list -->
                        <a href="{{ItemMaker::url('news')}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">Read more</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect idx-prod-sect wow fadeInUp" data-wow-delay=".05s">
            <div class="idx-prod-wrap d-flex justify-content-center">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9 wow fadeInUp" data-wow-delay=".3s" data-wow-duration="1s">
                        <p class="main-title">Our Product</p>
                        <div class="idx-description">
                            <p>Our main product is conductive polymer including solid capacitor / chip capacitor with low profile ultra low ESR, high temperature resistant and long endurance.</p>
                        </div>
                        <div class="idx-prod-list wow fadeInUp" data-wow-delay=".75s" data-wow-duration="1.5s">
                            <div id="customNav" class="owl-nav"></div>
                            <div class="owl-carousel owl-carousel2">

                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata01')}}"><img src="/upload/product/prod-cate01.png" alt="捲繞型"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata01')}}">AP-CON(Radial Lead Type)</a>
                                </div>
                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata02')}}"><img src="/upload/product/prod-cate02.png" alt="晶片型"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata02')}}">AP-CON(Surface Mount Type)</a>
                                </div>
                                <div class="prod-item">
                                    <div class="prod-pic">
                                        <a href="{{ItemMaker::url('product#prod-cata03')}}"><img src="/upload/product/prod-cate03.png" alt="混合型"></a>
                                    </div>
                                    <a href="{{ItemMaker::url('product#prod-cata03')}}">AP-CAP(Stacking Type)</a>
                                </div>
                            </div>
                        </div>
                        <!-- end of idx-prod-list -->
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect">
            <div class="idx-application-wrap d-flex flex-column align-items-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-10 col-lg-9">
                        <p class="main-title wow fadeInUp" data-wow-delay=".25s">Application</p>
                        <div class="idx-description wow fadeInUp" data-wow-delay=".55s">
                            <p>APAQ’s product application range is very wide, the main apply on Automotive, Computing, Industrial, Power Supply, Smart Device etc.</p>
                        </div>
                        <a href="#" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">Read more</span>
                        </a>
                    </div>
                </div>
                <div class="idx-app-list-wrap">
                    <div class="idx-app-list d-block wow fadeInUp" data-wow-delay="1.15s">
                        <div>
                            <a href="{{ItemMaker::url('product')}}" class="idx-app-link" ></a>
                            <img src="upload/home/application/app01.png" alt="Automotive 汽車">
                            <p>Automotive</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('product')}}" class="idx-app-link" ></a>
                            <img src="upload/home/application/app02.png" alt="Computing 計算">
                            <p>Computing</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('product')}}" class="idx-app-link" ></a>
                            <img src="upload/home/application/app03.png" alt="Industry 工業">
                            <p>Industrial</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('product')}}" class="idx-app-link" ></a>
                            <img src="upload/home/application/app04.png" alt="Power Supply 電源">
                            <p>Power Supply</p>
                        </div>
                        <div>
                            <a href="{{ItemMaker::url('product')}}" class="idx-app-link" ></a>
                            <img src="upload/home/application/app05.png" alt="Smart Device 智能設備">
                            <p>Smart Device</p>
                        </div>
                    </div>
                </div>
                <!-- end of app-list -->
                <div class="global-animate">
                    <div class="globe js-globe">
                        <img src="assets/images/home/pic-global-outer.png" alt="">
                        <!-- <ul class="globe-list js-list"></ul> -->
                        <canvas class="globe-canvas js-canvas">
                    </div>
                </div>
            </div>
        </section>

        <section class="idx-sect">
            <div class="idx-about-wrap d-flex flex-column flex-lg-row justify-content-center wow fadeInUp" data-wow-delay=".1s">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-9 col-lg-6">
                        <p class="main-title wow fadeInUp" data-wow-delay=".35s">About APAQ</p>
                        <div class="idx-about-context idx-description wow fadeInUp" data-wow-delay=".55s">
                            <p class="text-justify"><span class="text-base">Since APAQ founding in 2005, attach important to research and development.</span>　We gathered the outstanding research and development members of Conductive Polymer Aluminum Solid Capacitors from Industrial Technology Research Institute (ITRI).The target of APAQ is to become a global and professional polymer capacitor manufacturer.</p>
                            <p class="text-justify"><br> Our main product is conductive polymer including solid capacitor / chip capacitor with low profile, ultra low ESR, high temperature resistant and long endurance. To protect our environments, we executed non-lead production in raw material and production procedure aggressively. To create win-win situation, we form strategic alliance with our clients and join resources together.</p>
                        </div>
                        <!-- end of news-list -->
                        <?php
                            $aboutLink = ( $getTitle['about'] )?$getTitle['about'][0]['web_link']: 'javascript:;';
                        ?>
                        <a href="{{ $aboutLink}}" class="bn-link btn-cc btn-cc02 wow fadeInUp" data-wow-delay=".75s">
                            <span class="btn-arrow">
                                <span class="arrow-cc-right"></span>
                            </span>
                            <span class="btn-word">Read more</span>
                        </a>
                    </div>
                </div>
                <div class="idx-core-wrap">
                    <img src="assets/images/home/idx-core-01-en.png" alt="" class="idx-core-bg wow fadeIn"
                        data-wow-offset="30" data-wow-delay="1.5s" data-wow-duration="1.5s">
                    <img src="assets/images/home/idx-core-logo.png" alt="" class="idx-core-logo">
                    <img src="assets/images/home/idx-core-02-en.png" alt="" class="idx-core-bg wow flipInY"
                        data-wow-offset="100" data-wow-delay="2s" data-wow-duration="2s">
                    <img src="assets/images/home/idx-core-03-en.png" alt="品質政策, 社會責任政策與承諾, 環境政策" class="idx-core-bg wow fadeIn"
                        data-wow-offset="30" data-wow-delay="2s">
                </div>
            </div>
        </section>

        <section class="idx-sect idx-history-sect">
            <div class="idx-history-wrap d-flex justify-content-center wow fadeInUp">
                <div class="d-flex row-cc justify-content-end row no-gutters">
                    <div class="col-12 col-md-8 col-lg-6">
                        <p class="main-title">History</p>
                        <section class="cd-horizontal-timeline">
                            <div class="timeline">
                                <ul class="cd-timeline-navigation d-flex">
                                    <li><a class="btn-nav btn-nav-prev prev" href="#0"></a></li>
                                    <li><a class="btn-nav btn-nav-next next inactive" href="#0"></a></li>
                                </ul>
                                <div class="events-wrapper">
                                    <div class="events">
                                        <ol>
                                            @if( $history AND 0 )
                                                <?php
                                                    $count = 1;
                                                    $c = count($history);

                                                ?>
                                                @foreach( $history as $h )
                                                    <?php
                                                        $select = ($count == $c)? 'class="selected"' :'';
                                                        $date = date("m/d/Y", strtotime($h->date));
                                                        $historyListHtml .= '<li '.$select.' data-date="'.$date.'">
                                                                        <em>'.$h->title.'</em>
                                                                        <p>'.$h->content.'</p>
                                                                    </li>';
                                                    ?>
                                                    <li><a {{$select}} data-date="{{$date}}" href="#0">{{$h->title}}</a></li>
                                                    <?php $count++;?>
                                                @endforeach
                                            @endif
                                            <li><a data-date="01/01/2005" href="#0">2005</a></li>
                                            <li><a data-date="01/01/2006" href="#0">2006</a></li>
                                            <li><a data-date="01/01/2008" href="#0">2008</a></li>
                                            <li><a data-date="01/01/2009" href="#0">2009</a></li>
                                            <li><a data-date="01/01/2012" href="#0">2012</a></li>
                                            <li><a data-date="01/01/2014" href="#0">2014</a></li>
                                            <li><a data-date="01/01/2015" href="#0">2015</a></li>
                                            <li><a data-date="01/01/2016" href="#0">2016</a></li>
                                            <li><a data-date="01/01/2017" href="#0">2017</a></li>
                                            <li><a class="selected" data-date="01/01/2018" href="#0">2018</a></li>
                                        </ol>
                                        <span class="filling-line" aria-hidden="true"></span>
                                    </div>
                                    <!-- .events -->
                                </div>
                                <!-- .events-wrapper -->

                            </div>
                            <!-- .timeline -->

                            <div class="events-content">
                                <ol>
                                    {!! $historyListHtml !!}
                                    <li data-date="01/01/2005">
                                        <em>2005</em>
                                        <p>APAQ established</p>
                                    </li>
                                    <li data-date="01/01/2006">
                                        <em>2006</em>
                                        <p>ITRI (Industrial Technology Research Institute) transferred conductive polymer Technology to APAQ<br>
                                        AP-CON (winding type) developed<br>
                                        AP-CON (winding type) mass production<br>
                                        ISO 9001 certificated (UL)</p>
                                    </li>
                                    <li data-date="01/01/2008">
                                        <em>2008</em>
                                        <p>Pegatron PUreGMS approved<br>AP-CAP (chip type) developed</p>
                                    </li>
                                    <li data-date="01/01/2009">
                                        <em>2009</em>
                                        <p>QC080000 certificated(UL)<br>
                                        ASUS Best Partner<br>
                                        AP-CAP (chip type) mass production</p>
                                    </li>
                                    <li data-date="01/01/2012">
                                        <em>2012</em>
                                        <p>New WUXI Plant 1st stage mass production<br>ISO 14001 certificated(UL)</p>
                                    </li>
                                    <li data-date="01/01/2014">
                                        <em>2014</em>
                                        <p>GIGABYTE Best Partner of 2013<br>
                                        OHSAS 18001 certificated(UL)<br>
                                        New WUXI Plant 2nd stage mass production<br>
                                        Listed on Taiwan Stock Exchange (6449)</p>
                                    </li>
                                    <li data-date="01/01/2015">
                                        <em>2015</em>
                                        <p>GIGABYTE Best Partner of 2014<br>TS 16949 certificated(UL)</p>
                                    </li>
                                    <li data-date="01/01/2016">
                                        <em>2016</em>
                                        <p>Taiwan R&D center founded.</p>
                                    </li>
                                    <li data-date="01/01/2017">
                                        <em>2017</em>
                                        <p>High voltage capacitor developed.(50v-100v)</p>
                                    </li>
                                    <li class="selected" data-date="01/01/2018">
                                        <em>2018</em>
                                        <p>High voltage capacitor mass production</p>
                                    </li>
                                </ol>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
            <div class="idx-history-pic-wrap">
                <img src="assets/images/home/pic-idx-history01.png" alt="" class=" wow bounceInLeft"
                    data-wow-offset="10" data-wow-delay=".75s" data-wow-duration="1.25s">
                <img src="assets/images/home/pic-idx-history02.png" alt="" class=" wow bounceInLeft"
                    data-wow-offset="10" data-wow-delay="1s" data-wow-duration="2.25s">
            </div>
        </section>

        <section class="idx-sect idx-achievement-sect">
            <div class="idx-achievement-wrap d-flex justify-content-center wow fadeInUp" data-wow-delay=".05s">
                <div class="d-flex row-cc justify-content-start row no-gutters">
                    <div class="col-12 col-md-8 col-lg-6">
                        <p class="main-title">Achievement</p>
                        <div class="idx-achievement-list">
                            <div class="idx-achieve-item idx-achieve-item1 d-flex align-items-center wow fadeInUp" data-wow-delay=".1s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve01-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve01.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">PRODUCT SOLD</p>
                                    <p class="mb-0 stat-number"><span class="stat-count timer" data-from="1631303200" data-to="1631304000" data-speed="8000" data-refresh-interval="100"></span> PCS PRODUCT SOLD</p>
                                </div>
                            </div>
                            <div class="idx-achieve-item idx-achieve-item2 d-flex align-items-center wow fadeInUp" data-wow-delay=".2s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve02-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve02.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">ANNUAL TURNOVER</p>
                                    <p class="mb-0 stat-number"><span class="stat-count" data-from="1941715000" data-to="1941720000" data-speed="8000" data-refresh-interval="100"></span> NTD OF ANNUAL TURNOVER</p>
                                </div>
                            </div>
                            <div class="idx-achieve-item d-flex align-items-center wow fadeInUp" data-wow-delay=".3s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve03-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve03.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">MARKET SHARE</p>
                                    <p class="mb-0 stat-number"><span class="stat-count2" data-from="1" data-to="23" data-speed="500" data-refresh-interval="1"></span> %</p>
                                </div>
                            </div>
                            <div class="idx-achieve-item d-flex align-items-center wow fadeInUp" data-wow-delay=".35s">
                                <div class="icon-achieve mr-4">
                                    <img src="assets/images/icons/icon-achieve04-bg.png" alt="achieve">
                                    <img src="assets/images/icons/icon-achieve04.png" alt="achieve">
                                </div>
                                <div>
                                    <p class="mb-0">PATENT</p>
                                    <p class="mb-0 stat-number"><span class="stat-count2" data-from="1" data-to="253" data-speed="500" data-refresh-interval="1"></span> ITEM OF PATENT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="idx-global-map">
                <div class="map-container__map"><img src="assets/images/map.png" class="map">
                    <div class="map-dot map-dot--1 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".25s">
                    </div>
                    <div class="map-dot map-dot--2 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.5s" data-wow-delay=".6s">
                    </div>
                    <div class="map-dot map-dot--3 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.1s" data-wow-delay=".15s">
                    </div>
                    <div class="map-dot map-dot--4 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".25s" data-wow-delay=".25s">
                    </div>
                    <div class="map-dot map-dot--5 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".15s">
                    </div>
                    <div class="map-dot map-dot--6 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration="1.1s" data-wow-delay=".2s">
                    </div>
                    <div class="map-dot map-dot--7 js-map-dot wow zoomIn" data-wow-offset="-30" data-wow-duration=".5s" data-wow-delay=".35s">
                    </div>
                </div>
            </section>

            <!-- <div class="idx-global-map">
                <img src="assets/images/map.png" alt="map">
            </div> -->
        </section>
    </div>
    <!-- end of main -->

    <div class="page-loader d-flex align-items-center justify-content-center">
        <span class="loader-logo animated fadeInUp"><img src="assets/images/logo/logo.svg" alt="" style="width: 140px"></span>
        <span class="loader-word has-text-centered pl-3 animated fadeInDown">TECHNOLOGY CO., LTD.</span>
    </div>

    @include($locateLang.'.includes.footer')

    <script type="text/javascript" src="assets/plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
    <script src="assets/plugins/jquery.countTo.js"></script>
    <script src="assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
    <script src="assets/plugins/three.js"></script>
    <script src="assets/plugins/ThreeOrbitControls.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/87/three.js"></script> -->
    <!-- <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/617753/ThreeOrbitControls.js"></script> -->
    <!-- start home js -->
    <script type="text/javascript" src="assets/js/globe.js"></script>
    <script type="text/javascript" src="assets/js/timeline.js"></script>
    <script type="text/javascript" src="assets/js/home.js"></script>

</body>

</html>
