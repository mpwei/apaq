@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
    $lang = $frontEndClass -> getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/news-bn01.jpg)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
          <p class="bn-page-title">隱私權政策</p>
          <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">首頁</a></li>
                  <li class="breadcrumb-item active" aria-current="page">隱私權政策</li>
              </ol>
          </nav>
      </div>
  </div>

  <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
  <div class="d-flex justify-content-center">
      <div class="container-cc">
          <div class="tab-content news-tab-content wow fadeInUp" data-wow-delay=".1s">
              <div id="tab01" class="tab-pane active">
                <section style="background-image: url(upload/about/about-01.png);background-repeat: no-repeat; background-position: 50% 50%">
                  <div class="about-cata-wrap d-flex justify-content-center wow fadeInUp" id="about-01">
                  <div class="d-flex row-cc row no-gutters">
                  <div class="col-12">
                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 22.5px; line-height: normal; font-family: Arial; color: rgb(38, 38, 38);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;"></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">非常歡迎您光臨鈺邦科技股份有限公司</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;"> (</span><span class="s1" style="font-kerning: none;">以下簡稱本網站</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">)</span><span class="s1" style="font-kerning: none;">，為了讓您能夠安心的使用本網站的各項服務與資訊，特此向您說明本網站的隱私權保護政策，以保障您的權益，請您詳閱下列內容：</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">一、隱私權保護政策的適用範圍</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">隱私權保護政策內容，包括本網站如何處理在您使用網站服務時收集到的個人識別資料。隱私權保護政策不適用於本網站以外的相關連結網站，也不適用於非本網站所委託或參與管理的人員。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">二、個人資料的蒐集、處理及利用方式</span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">當您造訪本網站或使用本網站所提供之功能服務時，我們將視該服務功能性質，請您提供必要的個人資料，並在該特定目的範圍內處理及利用您的個人資料；非經您書面同意，本網站不會將個人資料用於其他用途。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本網站在您使用服務信箱、問卷調查等互動性功能時，會保留您所提供的姓名、電子郵件地址、聯絡方式及使用時間等。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">於一般瀏覽時，伺服器會自行記錄相關行徑，包括您使用連線設備的</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">IP</span><span class="s1" style="font-kerning: none;">位址、使用時間、使用的瀏覽器、瀏覽及點選資料記錄等，做為我們增進網站服務的參考依據，此記錄為內部應用，決不對外公佈。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">為提供精確的服務，我們會將收集的問卷調查內容進行統計與分析，分析結果之統計數據或說明文字呈現，除供內部研究外，我們會視需要公佈統計數據及說明文字，但不涉及特定個人之資料。</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">三、資料之保護</span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本網站主機均設有防火牆、防毒系統等相關的各項資訊安全設備及必要的安全防護措施，加以保護網站及您的個人資料採用嚴格的保護措施，只由經過授權的人員才能接觸您的個人資料，相關處理人員皆簽有保密合約，如有違反保密義務者，將會受到相關的法律處分。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">如因業務需要有必要委託其他單位提供服務時，本網站亦會嚴格要求其遵守保密義務，並且採取必要檢查程序以確定其將確實遵守。</span></li>
                    <li class="li3" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s4" style="font-kerning: none;">本網站將利用您所提供的資料來答覆您的問題、提供您所索取本網站產品資訊及服務。您自願提供的個人資訊，表明您已同意本網站可授權第三方來收集、追蹤和處理這些資料。</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p4" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s1" style="font-kerning: none;">四、兒童權利</span></p>

                  <p class="p3" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s4" style="font-kerning: none;">未經家長或監護人許可，本網站將不會收集</span><span class="s5" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">13</span><span class="s4" style="font-kerning: none;">歲以下兒童的個人身分資料。</span><span class="s5" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">13</span><span class="s4" style="font-kerning: none;">歲以下的兒童只有在家長或監護人同意之下才可向本網站提供個人身分資料。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">四、網站對外的相關連結</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本網站的網頁提供其他網站的網路連結，您也可經由本網站所提供的連結，點選進入其他網站。但該連結網站不適用本網站的隱私權保護政策，您必須參考該連結網站中的隱私權保護政策。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">五、與第三人共用個人資料之政策</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本網站絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">前項但書之情形包括不限於：</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s6" style="text-decoration-line: underline; font-kerning: none;"></span></p>

                  <ul class="ul1" style="color: rgb(0, 0, 0); font-size: medium;">
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">經由您書面同意。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">法律明文規定。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">為免除您生命、身體、自由或財產上之危險。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">與公務機關或學術研究機構合作，基於公共利益為統計或學術研究而有必要，且資料經過提供者處理或蒐集者依其揭露方式無從識別特定之當事人。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">當您在網站的行為，違反服務條款或可能損害或妨礙網站與其他使用者權益或導致任何人遭受損害時，經網站管理單位研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">有利於您的權益。</span></li>
                    <li class="li1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;;"><span class="s3" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Wingdings;"></span><span class="s1" style="font-kerning: none;">本網站委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。</span></li>
                  </ul>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p5" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0);"><span class="s7" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">六、</span><span class="s1" style="font-kerning: none;">Cookie</span><span class="s7" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">之使用</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">為了提供您最佳的服務，本網站會在您的電腦中放置並取用我們的</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">，若您不願接受</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">的寫入，您可在您使用的瀏覽器功能項中設定隱私權等級為高，即可拒絕</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;">Cookie</span><span class="s1" style="font-kerning: none;">的寫入，但可能會導致網站某些功能無法正常執行</span><span class="s2" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica; font-kerning: none;"> </span><span class="s1" style="font-kerning: none;">。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">七、隱私權保護政策之修正</span></p>

                  <p class="p1" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; color: rgb(0, 0, 0);"><span class="s1" style="font-kerning: none;">本網站隱私權保護政策將因應需求隨時進行修正，修正後的條款將刊登於網站上。</span></p>

                  <p class="p2" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>

                  <p class="p4" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s1" style="font-kerning: none;">九、關於隱私權處理或我們的隱私權政策聲明</span></p>

                  <p class="p4" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s1" style="font-kerning: none;">如果您對於我們的隱私權保護政策、網路隱私權處理聲明或您的個人信息處理有任何疑問，請聯繫我們：</span></p>

                  <p class="p4" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: &quot;PingFang TC&quot;; "><span class="s1" style="font-kerning: none;">鈺邦科技股份有限公司</span></p>

                  <p class="p6" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; "><span class="s1" style="font-kerning: none;">Email:sales_capacitors@apaq.com.tw</span></p>

                  <p class="p7" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; "><span class="s8" style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;PingFang TC&quot;; font-kerning: none;">電話：</span><span class="s4" style="font-kerning: none;">+886-37-777588</span></p>

                  <p class="p6" style="margin: 0px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: rgb(0, 0, 0); min-height: 14px;"><span class="s1" style="font-kerning: none;"></span></p>
                  </div>
                  </div>
                  </div>
                  </section>


              </div>
          </div>
      </div>
  </div>
</div>
@include($locateLang.'.includes.aside')
@stop
