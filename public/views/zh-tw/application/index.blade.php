@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php 
    $product_title_check = 1;
    $lang = $frontEndClass -> getlang($locateLang);
    $getTitle = $frontEndClass -> getSubtitle();
?>

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)


@section('css')
@include($locateLang.'.includes.css-in')
<link rel="stylesheet" href="assets/css/application.css">
@endsection

@extends('page')

@section('content')
<div class="in-banner" style="background-image: url(/upload/banner/inner/in-bn-application.png)">
  <div class="bn-content d-flex flex-column justify-content-center align-items-center">
    <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
      <p class="bn-page-title">Application</p>
      <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Application</li>
        </ol>
      </nav>
    </div>
  </div>
  <canvas id="inbn_particlesR"></canvas>
</div>
<div class="inner-main">
  <div class="wrap-filter">
    <!-- filter start-->
    <div class="container-cc container-filter">
      @if(count($applications)>3)<div class="prev-button" style="display: none;"></div>@endif
      <div class="row-item">
        @foreach($applications as $key => $application)
        <div class="item item-computing @if($key==0)item-active @endif" style="background-image: url({{ $application->background_image }});@if($key>=3)display:none;@endif" data-type="{{ str_replace(" ", "_", $application->title) }}">
          <div class="item-title">{{ $application->title }}</div>
        </div>
        @endforeach
      </div>
      @if(count($applications)>3)<div class="next-button"></div>@endif
    </div>
    <!-- content start-->
    <div class="container-cc">
      @foreach($applications as $key => $application)
      <div class="container-item {{ str_replace(" ", "_", $application->title) }} @if($key==0) default @endif">
        <div class="row-series">
          @foreach($application->sub_applications as $sub_applications_key => $sub_application)
          <div class="select @if($sub_applications_key==0)select-active @endif" data-type="{{ $sub_application->id }}">
            <p>{{ $sub_application->title }}</p>
          </div>
          @endforeach
        </div>
        @foreach($application->sub_applications as $sub_applications_key => $sub_application)
        <div class="row-series-content {{ $sub_application->id }} @if($sub_applications_key==0) default @endif">
          @foreach($sub_application->products as $item_key => $item)
          <div class="col-12 col-md-6 col-sm-12 d-flex">
            <div class="product-pic"><img src="{{ $item->image }}" alt=""></div>
            @if($item->list_spec)
            <?php 
              $spec = ($item->list_spec)? explode("#", $item->list_spec) : []; 
            ?>
            <ul>
              <li class="product-title"><a href="{{ $item->web_link }}">{{ $item->title }}</a></li>
              @foreach($spec as $row)
              <li>{{$row}}</li>
              @endforeach
            </ul>
            @endif
          </div>
          @endforeach
        </div>
        @endforeach
      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  let applications_length = {{ count($applications) }};
let start = 0;
$('.container-item:not(.default)').hide();
$('.item').on('click',function(){
	var type = $(this).data('type')
    $('.container-item.'+type).show().siblings('.container-item').hide();
    $('.item').removeClass('item-active')
    $(this).addClass('item-active')
})

$('.row-series-content:not(.default)').hide();
$('.select').on('click',function(){
	var type = $(this).data('type')
    $('.row-series-content.'+type).show().siblings('.row-series-content').hide();
    $(this).addClass('select-active').siblings('.select').removeClass('select-active')
})
$('.prev-button').on('click',function(){
  if(start>0){
    start-=1;
  }
  $('.item').hide();
  let items = document.getElementsByClassName('item');
  for(let index = start; index<applications_length && index<start+3; index++){
    items[index].style['display'] = 'block';
  }
  if(start+2>=applications_length-1){
    $('.next-button').hide();
  }else{
    $('.next-button').show();
  }
  if(start==0){
    $('.prev-button').hide();
  }else{
    $('.prev-button').show();
  }
})

$('.next-button').on('click',function(){
  if(start<applications_length-3){
    start+=1;
  }
  $('.item').hide();
  let items = document.getElementsByClassName('item');
  for(let index = start; index<applications_length && index<start+3; index++){
    items[index].style['display'] = 'block';
  }
  if(start+2>=applications_length-1){
    $('.next-button').hide();
  }else{
    $('.next-button').show();
  }
  if(start==0){
    $('.prev-button').hide();
  }else{
    $('.prev-button').show();
  }
})
</script>
@endsection