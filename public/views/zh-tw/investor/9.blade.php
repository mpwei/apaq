@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{--
<pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Invesotment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Invesotment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">企業社會責任政策</h2>
                            <div class="text-shadow">
                                <p>政策：</p>
                                <ul>
                                    <li>以人權為藥物，以人性為基石。</li>
                                    <li>員工安全健康，保護環境生態。</li>
                                    <li>尊湊法令法規，關懷社會大眾。</li>
                                </ul>
                                <p>承諾：</p>
                                <ul>
                                    <li>遵守法令：嚴格遵守相關國際公約、國家法令法規、公司與顧客有關準則規定，努力符合顧客、員工和社會期許。</li>
                                    <li>預防為先：貫徹預防為先，落實執行的觀念致力於健全社會責任機制保障員工權益。</li>
                                    <li>持續改善：持續改善社會責任管理的績效；改善員工健康安全符合及超越勞動、環安衛相關法規以及社會責任標準，建立健康安全的工作環境與勞動人權。</li>
                                    <li>員工參與：落實政策宣導與訓練，鼓勵員工參與各項活動增進彼此瞭解已達和諧。</li>
                                    <li>廉潔經營：嚴格遵守企業道德規範，誠信為先廉潔務實經營、嚴守法紀，穩健踏實、永續經營。</li>
                                </ul>
                                <p>企業社會責任：</p>
                                <div class="pb-5 table-cc">
                                    <table class="table table-striped table-fixed table-content-center table-investor01">
                                        <thead>
                                            <tr>
                                                <th>企業社會責任</th>
                                                <th>下載</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>誠信經營作業程序及行為指南</td>
                                                <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            </tr>
                                            <tr>
                                                <td>企業社會責任政策</td>
                                                <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->

    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop