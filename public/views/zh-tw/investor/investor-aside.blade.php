<?php
$route = explode('/', $_SERVER['REQUEST_URI']);
$routeGO = $route[sizeof($route) - 1];
?>
<p class="d-block d-lg-none">
    <a class="btn btn-base btn-m-aside-trigger">投資專區選單<i class="pl-2 fas fa-caret-down"></i>
    </a>
</p>
<ul class="investor-list aside02-list">
    <h4 class="aside-title">財務資訊</h4>
    <li @if($routeGO == 'Monthly-Revenue') class="active" @endif>
        <a href="{{ ItemMaker::url('investor/Monthly-Revenue') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>每月營收</span></a>
    </li>
    <li @if($routeGO == 'Financial-Report') class="active" @endif>
        <a href="{{ ItemMaker::url('investor/Financial-Report?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>合併財務報表</span></a>
    </li>
    <h4 class="aside-title">股東會資訊</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Procedure-Manual?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>議事手冊</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Notice-of-meeting?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>開會通知</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Annual-Report?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>年報</span></a>
    </li>
    <!-- <li>
        <a href="investor9.php" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>議事錄</span></a>
    </li> -->
    <li>
        <a href="{{ ItemMaker::url('investor/Stock') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>股務資訊</span></a>
    </li>
    <h4 class="aside-title">利害關係人</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Stakeholder-Engagement') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>利害關係人</span></a>
    </li>
    <h4 class="aside-title">公司治理</h4>
    @if(1)
        <li>
            <a href="{{ ItemMaker::url('investor/Organization-Team') }}" class="d-flex align-items-start">
                <i class="fas fa-angle-right pr-2"></i><span>組織架構及經營團隊</span></a>
        </li>
    @endif
    <li>
        <a href="{{ ItemMaker::url('investor/Board-Director') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>董事會及功能委員會</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Company-rule?page=1') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>公司規章</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Internal-audit') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>內部稽核</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Main-Shareholder') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>主要股東名單</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Corporate-Governance-Implementation-Status') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>公司治理運作情形</span></a>
    </li>
    <h4 class="aside-title">企業社會責任</h4>
    <li>
        <a href="{{ ItemMaker::url('investor/Social-Respons') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>企業社會責任政策</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Respon-Effects') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>企業社會責任實施成效</span></a>
    </li>
    <li>
        <a href="{{ ItemMaker::url('investor/Personal-Safety') }}" class="d-flex align-items-start">
            <i class="fas fa-angle-right pr-2"></i><span>員工福利、工作環境與人身安全保護</span></a>
    </li>
</ul>
