@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass -> getlang($locateLang);
?>
{{--
<pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')
<!DOCTYPE html>
<html lang="zh-tw">

<body class="inner-page">

    <div class="main">
        <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
            <div class="bn-content d-flex flex-column justify-content-center align-items-center">
                <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                    <p class="bn-page-title">Invesotment</p>
                    <p class="bn-description"></p>
                    <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Invesotment</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <canvas id="inbn_particlesR"></canvas>
        </div>
        <!-- end of in-banner -->
        <div class="inner-main">
            <div class="d-flex justify-content-center detail-wrap">
                <div class="container-cc">
                    <div class="row investor-detail">
                        <div class="col-12 col-lg-3 page-aside02">
                            @include($locateLang.'.investor.investor-aside')
                        </div>
                        <div class="col-12 col-lg-9 investor-table-wrap">
                            <h2 class="title-investor text-base">主要股東名單</h2>
                            <div class="text-shadow">
                                <p class="text-center">股權比例占前十名之股東名稱、持股股數及比例</p>
                                <p class="text-right pr-4"><span>107年04月28日；單位：股</span></p>
                                <div class="pb-5 table-cc">
                                    <table class="table table-striped table-fixed table-content-center table-investor01">
                                        <thead>
                                            <tr>
                                                <th>主要股東名稱</th>
                                                <th>持股股數</th>
                                                <th>持股比例(%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>佳邦科技股份有限公司</td>
                                                <td>9,380,886</td>
                                                <td>11.64</td>
                                            </tr>
                                            <tr>
                                                <td>華誠創業投資股份有限公司</td>
                                                <td>5,568,012</td>
                                                <td>6.91</td>
                                            </tr>
                                            <tr>
                                                <td>華敏投資股份有限公司</td>
                                                <td>3,210,015</td>
                                                <td>3.98</td>
                                            </tr>
                                            <tr>
                                                <td>台灣人壽保險股份有限公司</td>
                                                <td>2,000,000</td>
                                                <td>2.48</td>
                                            </tr>
                                            <tr>
                                                <td>鄭敦仁</td>
                                                <td>1,938,954</td>
                                                <td>2.41</td>
                                            </tr>
                                            <tr>
                                                <td>花旗託管野村國際股份有限公司投資專戶</td>
                                                <td>1,713,000</td>
                                                <td>2.13</td>
                                            </tr>
                                            <tr>
                                                <td>統一大滿貫基金專戶</td>
                                                <td>1,513,000</td>
                                                <td>1.88</td>
                                            </tr>
                                            <tr>
                                                <td>中國信託商業銀行股份有限公司</td>
                                                <td>889,000</td>
                                                <td>1.10</td>
                                            </tr>
                                            <tr>
                                                <td>元大高科技基金專戶</td>
                                                <td>885,000</td>
                                                <td>1.10</td>
                                            </tr>
                                            <tr>
                                                <td>元大新主流基金專戶</td>
                                                <td>599,000</td>
                                                <td>0.74</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->

    <script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>

    <script src="../../assets/js/particlesR.js"></script>
    <script src="../../assets/js/pages.js"></script>
</body>

</html>
@include($locateLang.'.includes.aside')
@stop