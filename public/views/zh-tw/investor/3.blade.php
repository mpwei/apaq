@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

@section('css')
  @include($locateLang.'.includes.css-in')
@endsection
@section('title', "Financial Report - ".$ProjectShareName)
@section('keywords', "Financial Report ,".$ProjectShareName)
@section('description',"Financial Report ,".$ProjectShareName)


@extends('page')
@section('content')
<div class="in-banner" style="background-image: url(upload/banner/inner/invest-bn01.jpg)">
    <div class="bn-content d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
            <p class="bn-page-title">投資人專區</p>
            <p class="bn-description"></p>
            <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ItemMaker::url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">投資人專區</li>
                </ol>
            </nav>
        </div>
    </div>
    <canvas id="inbn_particlesR"></canvas>
</div>
<!-- end of in-banner -->
<div class="inner-main">
    <div class="d-flex justify-content-center detail-wrap">
        <div class="container-cc">
            <div class="row investor-detail">
                <div class="col-12 col-lg-3 page-aside02">
                    @include($locateLang.'.investor.investor-aside')
                </div>
                <div class="col-12 col-lg-9 investor-table-wrap">
                    <h2 class="title-investor text-base">議事手冊</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-fixed table-content-center table-investor01">
                            <thead>
                                <tr>
                                    <th>年度</th>
                                    <th>報表</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2018</td>
                                    <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                </tr>
                                <tr>
                                    <td>2017</td>
                                    <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                </tr>
                                <tr>
                                    <td>2016</td>
                                    <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                </tr>
                                <tr>
                                    <td>2015</td>
                                    <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                </tr>
                                <tr>
                                    <td>2014</td>
                                    <td><a href="#"><img src="../../assets/images/icons/icon-pdf.png" alt="" class="icon-pdf"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include($locateLang.'.includes.aside')

@stop