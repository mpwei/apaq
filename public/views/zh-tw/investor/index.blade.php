@inject('frontEndClass', 'App\Http\Controllers\FrontEndController')

<?php
$lang = $frontEndClass->getlang($locateLang);
?>
{{-- <pre>{{print_r($pageData)}}</pre> --}}

@section('title', $ProjectShareName)
@section('keywords', $ProjectShareName)
@section('description',$ProjectShareName)

@section('css')
@include($locateLang.'.includes.css-in')
@endsection

@extends('page')
@section('content')

        <!DOCTYPE html>
<html lang="zh-tw">


<body class="inner-page">


<div class="main">
    <div class="in-banner" style="background-image: url(../../upload/banner/inner/invest-bn01.jpg)">
        <div class="bn-content d-flex flex-column justify-content-center align-items-center">
            <div class="d-flex flex-column row-cc row no-gutters wow fadeInUp" data-wow-delay=".1s">
                <p class="bn-page-title">投資人專區</p>
                <p class="bn-description"></p>
                <nav aria-label="breadcrumb" class="breadcrumb-wrap container-cc">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">首頁</a></li>
                        <li class="breadcrumb-item active" aria-current="page">投資人專區</li>
                    </ol>
                </nav>
            </div>
        </div>
        <canvas id="inbn_particlesR"></canvas>
    </div>
    <!-- end of in-banner -->
    <div class="inner-main">
        <div class="d-flex justify-content-center detail-wrap">
            <div class="container-cc">
                <div class="row investor">
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-1.jpg" alt="">
                                </span>
                        </p>
                        <h3>財務資訊</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Monthly-Revenue') }}">
                                    <i class="fas fa-angle-right"></i>每月營收</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Financial-Report?page=1') }}">
                                    <i class="fas fa-angle-right"></i>合併財務報表</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-2.jpg" alt="">
                                </span>
                        </p>
                        <h3>股東會資訊</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Procedure-Manual?page=1') }}">
                                    <i class="fas fa-angle-right"></i>議事手冊</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Notice-of-meeting?page=1') }}">
                                    <i class="fas fa-angle-right"></i>開會通知</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Annual-Report?page=1') }}">
                                    <i class="fas fa-angle-right"></i>年報</a>
                            </li>
                            <!-- <li>
                                <a href="investor2.php">
                                    <i class="fas fa-angle-right"></i>議事錄</a>
                            </li> -->
                            <li>
                                <a href="{{ ItemMaker::url('investor/Stock') }}">
                                    <i class="fas fa-angle-right"></i>股務資訊</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-3.jpg" alt="">
                                </span>
                        </p>
                        <h3>利害關係人</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Stakeholder-Engagement') }}">
                                    <i class="fas fa-angle-right"></i>利害關係人</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 offset-lg-2 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-4.jpg" alt="">
                                </span>
                        </p>
                        <h3>公司治理</h3>
                        <ul class="investor-list">
                            @if(1)
                            <li>
                                <a href="{{ ItemMaker::url('investor/Organization-Team') }}">
                                    <i class="fas fa-angle-right"></i>組織架構及經營團隊</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ ItemMaker::url('investor/Board-Director') }}">
                                    <i class="fas fa-angle-right"></i>董事會及功能委員會</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Company-rule?page=1') }}">
                                    <i class="fas fa-angle-right"></i>公司規章</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Internal-audit') }}">
                                    <i class="fas fa-angle-right"></i>內部稽核</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Main-Shareholder') }}">
                                    <i class="fas fa-angle-right"></i>主要股東名單</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Corporate-Governance-Implementation-Status') }}">
                                    <i class="fas fa-angle-right"></i>公司治理運作情形</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 investor-list">
                        <p class="investor-img">
                                <span>
                                    <img src="../../../upload/invest/invest-5.jpg" alt="">
                                </span>
                        </p>
                        <h3>企業社會責任</h3>
                        <ul class="investor-list">
                            <li>
                                <a href="{{ ItemMaker::url('investor/Social-Respons') }}">
                                    <i class="fas fa-angle-right"></i>企業社會責任政策</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Respon-Effects') }}">
                                    <i class="fas fa-angle-right"></i>企業社會責任實施成效</a>
                            </li>
                            <li>
                                <a href="{{ ItemMaker::url('investor/Personal-Safety') }}">
                                    <i class="fas fa-angle-right"></i>員工福利、工作環境與人身安全保護</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of main -->


<script src="../../assets/plugins/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="../../assets/plugins/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<script src="../../assets/js/particlesR.js"></script>
<script src="../../assets/js/pages.js"></script>
</body>

</html>

@include($locateLang.'.includes.aside')
@stop