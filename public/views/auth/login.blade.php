<!DOCTYPE html>
<html lang="zh-tw">
  
<!-- Mirrored from wbpreview.com/previews/WB0F56883/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Nov 2016 14:31:13 GMT -->
<head>
<base href="{{ItemMaker::url('/')}}">
    <meta charset="utf-8">
    <title>Manager</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="backend/css/bootstrap.min.css" rel="stylesheet">
    <link href="backend/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link rel="stylesheet" href="backend/css/typica-login.css">

    <link rel="shortcut icon" href="backend/favicon.html">

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand"><img src="backend/logo.svg" alt="Ocean Design" width="214px" style="height:auto;"></a>
        </div>
      </div>
    </div>

    <div class="container">

        <div id="login-wraper">
            <form class="login-form" autocomplete="off"  role="form" method="POST" action="{{ ItemMaker::url('auth/login') }}">
            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <legend>登入 <span class="blue">後台管理</span></legend>
            
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button class="close" data-close="alert"></button>
							<ul>
								@foreach ($errors->all() as $error)
									<span>{{ $error }} </span>
								@endforeach
							</ul>
						</div>
					@endif
                <div class="body">
                    <input type="text" autocomplete="off" placeholder="帳號"  name="email" value="{{ old('email') }}"/ style="margin-bottom: 45px;">
                    <input type="password" autocomplete="off" placeholder="密碼" name="password"/>
                </div>
            
                <div class="footer">
                    <input type="text" name="captcha" data-type="required"  placeholder="驗證碼" value="" style="width: 135px;">
                    <a href="javascript:;" id="captcha">
                        {!! Captcha::img() !!}
                    </a>
                </div>
                <div class="footer">
                    <div>      
                        <button type="submit" class="btn btn-success">登入</button>
                    </div>
                </div>
            
            </form>
        </div>

    </div>

    <script src="backend/js/jquery.js"></script>
    <script src="backend/js/bootstrap.js"></script>
    <script src="backend/js/backstretch.min.js"></script>
    <script src="backend/js/typica-login.js"></script>

	<script>
	    document.querySelector('a[id="captcha"]').addEventListener("click", function () {
	        var d = new Date();
	        var img = this.childNodes[1];
	        img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();
	    });

	</script>
  </body>

</html>
