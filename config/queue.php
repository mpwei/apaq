<?php
// config/queue.php
return [
    'default' => 'database',
    'connections' => [
        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'expire' => 60,
            'connection_name'=>'',
        ],
    ],
];
