<?php

use App\Http\Models\CorporateSituation;
use Illuminate\Database\Seeder;

class CorporateSituationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CorporateSituation::create([
            'language' => 'en',
            'title' => 'Corporate Governance Implementation Status',
            'content' => 'en for test',
        ]);
        CorporateSituation::create([
            'language' => 'zh-tw',
            'title' => '公司治理運作情形',
            'content' => 'zh-tw for test',
        ]);
        CorporateSituation::create([
            'language' => 'zh-cn',
            'title' => '公司治理运作情形',
            'content' => 'zh-cn for test',
        ]);
    }
}
