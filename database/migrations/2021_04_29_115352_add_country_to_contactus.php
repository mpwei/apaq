<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryToContactus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cn_contactus', function (Blueprint $table) {
            $table->string('country');
        });
        Schema::table('en_contactus', function (Blueprint $table) {
            $table->string('country');
        });
        Schema::table('tw_contactus', function (Blueprint $table) {
            $table->string('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cn_contactus', function (Blueprint $table) {
            $table->dropColumn('country');
        });
        Schema::table('en_contactus', function (Blueprint $table) {
            $table->dropColumn('country');
        });
        Schema::table('tw_contactus', function (Blueprint $table) {
            $table->dropColumn('country');
        });
    }
}
