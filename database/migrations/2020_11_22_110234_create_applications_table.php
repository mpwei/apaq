<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tw_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('background_image');
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('cn_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('background_image');
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('en_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('background_image');
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tw_applications');
        Schema::dropIfExists('cn_applications');
        Schema::dropIfExists('en_applications');
    }
}
