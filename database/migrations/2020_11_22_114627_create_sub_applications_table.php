<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tw_sub_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('application_id');
            $table->string('title');
            $table->string('products')->nullable();
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('cn_sub_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('application_id');
            $table->string('title');
            $table->string('products')->nullable();
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('en_sub_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('application_id');
            $table->string('title');
            $table->string('products')->nullable();
            $table->integer('rank');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tw_sub_applications');
        Schema::drop('cn_sub_applications');
        Schema::drop('en_sub_applications');
    }
}
