<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCorporateSituationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corporate_situations', function (Blueprint $table) {
            $table->string('pdf_title');
            $table->string('pdf_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corporate_situations', function (Blueprint $table) {
            $table->dropColumn('pdf_title');
            $table->dropColumn('pdf_file');
        });
    }
}
